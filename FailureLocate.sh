# echo "Deploy Started..."

#kill the geth process 
pkill geth

sleep 1

#Run the Chain: first argument($1) -> chain name
geth  --datadir ./Ethereum/$1 --rpc -rpcaddr "0.0.0.0"  --rpcport "8545" --rpccorsdomain "*" --port "30303" --nodiscover  --rpcapi "db,eth,net,web3,miner,net,personal,net,txpool,admin"  --networkid 666  --targetgaslimit "9000000000000" --allow-insecure-unlock --verbosity 1 & 

echo "Your Chain: ##$1## is started."

sleep 2

geth --jspath "./Ethereum/Utils/" --exec "loadScript('gethMining.js')" attach ipc:./Ethereum/$1/geth.ipc & 

echo "Mining is started."
sleep 2

# node ./ExecutionWatch/EW_FailureLocating.js "ABCToken"
# Run the Process
count=0
for entry in "./ExecutionWatch/tr_contracts"/*.sol
do
	filename="$(echo "$entry" | cut -d"/" -f 4)"
	name="$(echo "$filename" | cut -d"." -f 1)"

	#Counting the smart contract 
	count=`expr $count + 1`

	echo $count $name >> ./FailureLocatingResult.txt

	node ./ExecutionWatch/EW_FailureLocating.js "$name"

done



sleep 2

pkill geth

echo "Exit Geth..."

