echo "Deploy Started..."

pkill geth

sleep 1

geth  --datadir ./Ethereum/$1 --rpc -rpcaddr "0.0.0.0"  --rpcport "8545" --rpccorsdomain "*" --port "30303" --nodiscover  --rpcapi "db,eth,net,web3,miner,net,personal,net,txpool,admin"  --networkid 666  --targetgaslimit "9000000000000" --allow-insecure-unlock --verbosity 1 & 

echo "Run Geth..."

sleep 2

geth --jspath "./Ethereum/Utils/" --exec "loadScript('gethMining.js')" attach ipc:./Ethereum/$1/geth.ipc & 

sleep 2

# echo "ContractName: BatchInitializer" 
# echo "+++++++StaticAnlysis++++++++" 
# #"BatchInitializer"
# python ./ExecutionWatch/DiscoverConstraints.py "Affiliate"
# python ./ExecutionWatch/ExecutionWatch.py "Affiliate"
# python ./ExecutionWatch/AllocationFuncInject.py "Affiliate"


# echo "++++++++FailureDetect+++++++" 
# node ./ExecutionWatch/EW_FailureDetection_envSet.js "BatchInitializer" 
# # node ./ExecutionWatch/EW_FailureDetection_both.js "$name" >> EW_FailureDetection.txt
# node ./ExecutionWatch/EW_FailureDetection_both.js "BaseToken"
# node ./ExecutionWatch/EW_FailureDetection_envSet.js "Affiliate"

# Deploye contract
count=0
for entry in "./ExecutionWatch/org_contracts"/*.sol
do
	filename="$(echo "$entry" | cut -d"/" -f 4)"
	name="$(echo "$filename" | cut -d"." -f 1)"
	echo $name

	# echo "ContractName: $name" >> EW_envSet_FD.txt
	# echo "+++++++StaticAnlysis++++++++"  >> EW_envSet_FD.txt
	# python ./ExecutionWatch/DiscoverConstraints.py "$name" >> EW_envSet_FD.txt
	# python ./ExecutionWatch/ExecutionWatch.py "$name" 
	# python ./ExecutionWatch/AllocationFuncInject.py "$name" >> EW_envSet_FD.txt

	count=`expr $count + 1`
	
	echo $count $name

	sleep 1

	# node ./ExecutionWatch/EW_FailureDetection_envSet.js "$name"	>> EW_envSet_FD.txt
	# node ./ExecutionWatch/EW_FailureLocating.js  "$name"	>> EW_envSet_FL_AFF.txt
	# echo "+++++++++Deployment+++++++++" >> EW_FailureDetection.txt
	# node ./ExecutionWatch/EW_Deploy.js "$name"	>> EW_FailureDetection.txt
	# echo "++++++++Pre-Allocate++++++++" >> EW_FailureDetection.txt
	# node ./ExecutionWatch/EW_preAllocate.js "$name" >> EW_FailureDetection.txt
	# echo "++++++++FailureDetect+++++++" >> EW_FailureDetection.txt
	# node ./ExecutionWatch/EW_FailureDetection_single.js "$name" >> EW_FailureDetection.txt
	node ./ExecutionWatch/EW_FailureDetection_both.js "$name" >> EW_TimeOverHead_2.txt


done

sleep 2

pkill geth

echo "Exit Geth..."