# PrivateChain

## To Run Exited Private Chain
If you just want to run the existed chain in `Ethereum`, you need to follow the steps presented below.

Step1:

To run the existed chain, you simply execute the shell with existed ``chainName`` (i.e. newChain)
```
./run_geth.sh newChain
```
Step2(Optional):

If you want to play with the geth console, you jest need to simpley run the shell script ``run_console.sh`` with corresponding ``chainName``
```
./run_console.sh newChain
```

## To Publish a New Chain
On the other hand, if you want to have your own chain, you can follow steps below to publish a new chain.

Step 1

Initialize a new chain with a chain name(i.e. chainName = myNewChain).
```
./init_geth.sh myNewChain
```
Step 2.1 

To Start the new Chain and Present the console

```
./run_geth.sh myNewChain
./run_console.sh myNewChain
```

Step 2.2 

Run the following command line in the geth console to create new accounts
```
>loadScript("gethSettingUp.js")
```
In addtion, the first sleeping block can by mined by running:
```
>miner.start()	# To start mining
>miner.stop()		# To stop mining
```

Step 2.3

I also write a ``.js`` file to present the current information of the chain by simpley running

```
>loadScript("ShowInfo.js")
```

## To terminate geth
For most entry shell scripts, we end up running ``pkill`` to kill the geth. However, if any uncertain circumstances occur to terminate the shell script, you can execute ``killGeth.sh`` in ``../Ethereum`` folder to terminate geth.
```
./killGeth.sh
```
