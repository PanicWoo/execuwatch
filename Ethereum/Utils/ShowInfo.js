// var nodeInformation = admin.nodeInfo
// console.log(nodeInformation)
var coinbaseAcc = eth.coinbase;
var allAccounts = eth.accounts;
var count = 0;
var gasPrice = eth.gasPrice;
var blockNum = eth.blockNumber;
var gasLimit = eth.getBlock("latest").gasLimit;

console.log("		Balance: " + eth.getBalance(eth.coinbase));
for(a in allAccounts){
	if (allAccounts === coinbaseAcc){
		console.log("account" +  count + "CoinBase): " + allAccounts[a]);
	}else{
		console.log("account" +  count + ": " + allAccounts[a]);
	}
	count++;
}
console.log("Gas Price: " + gasPrice);
console.log("Gas Limit: " + gasLimit);
console.log("Total Block Number: " + blockNum);