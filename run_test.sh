
pkill geth

sleep 1

geth  --datadir ./Ethereum/$1 --rpc -rpcaddr "0.0.0.0"  --rpcport "8545" --rpccorsdomain "*" --port "30303" --nodiscover  --rpcapi "db,eth,net,web3,miner,net,personal,net,txpool,admin"  --networkid 666  --targetgaslimit "9000000000000" --allow-insecure-unlock  --verbosity 1 & 

echo "Run Geth..."

sleep 2

geth --jspath "./Ethereum/Utils/" --exec "loadScript('gethMining.js')" attach ipc:./Ethereum/$1/geth.ipc & 

sleep 2



while IFS= read -r theVersion
do
	count=0
	echo "+++++++++++++++++++++++++++++" >> LFC.txt
  echo "+++++++++++$theVersion+++++++++++" >> LFC.txt
	echo "+++++++++++++++++++++++++++++" >> LFC.txt
	# update the version of the solc based on the solidity version

	python ./Utils/G_solcVersionControl.py $theVersion

	# Deploye contract
	for entry in "./ContractTest$theVersion"/*.sol
	do
		filename="$(echo "$entry" | cut -d"/" -f 3)"
		name="$(echo "$filename" | cut -d"." -f 1)"

		python ./Utils/Sta_ReConstructor_LineMap.py "ContractTest$theVersion" "$name"
		# python Debbuger_testFunc.py "ContractTest$theVersion" "$name"

		sleep 1

		count=`expr $count + 1`
		
		echo $count $name
    echo "Contract Name: $name" >> LFC.txt
    echo "+++++++++++Deploy++++++++++++" >> LFC.txt
		node ./Utils/DeployContract.js  "$name" >> LFC.txt

		# rm "$entry"

    if [ -f "./DC_src/$name.json" ];then
      echo "Ready to execute"
      echo "+++++++++EnvSetting++++++++++" >> LFC.txt
    	node ./FailureDetection/PreAllocate.js "$name" >> ./LFC.txt
      echo "++++++++FailureDetect++++++++" >> LFC.txt
    	node ./FailureDetection/FailureDetecting.js "$name" >> ./LFC.txt
      echo "++++++++FailureLocate++++++++" >> LFC.txt
    	node ./FailureDetection/FailureLocating_lineMap.js "$name" >> ./LFC.txt 
    fi

	done

done < "./SolcVersion.txt"



# sleep 2
# count=0

# for entry in "./DC_src"/*.json
# do
# 	filename="$(echo "$entry" | cut -d"/" -f 3)"
# 	name="$(echo "$filename" | cut -d"." -f 1)"

# 	node ./FailureDetection/PreAllocate.js "$name" >> ./LFC.txt
# 	node ./FailureDetection/FailureDetecting.js "$name" >> ./LFC.txt
# 	node ./FailureDetection/FailureLocating_lineMap.js "$name" >> ./LFC.txt

# 	count=`expr $count + 1`

# 	# echo "$count : $name"
# 	# mv "./DC_src/$name.json" "./DC_src_processed"
# 	# sleep 5
# 	# rm "$entry"

# done


sleep 2

pkill geth

echo "Exit Geth..."