# Aim: Collect the 3 classes constraints


import re 
import sys
import json

##################################
# ANALYSE/EXTRACT VARIABLES
##################################
def AnalyseVariables(varContent):
	Vars = []
	isStruct = False
	StructCount = -1
	for line in varContent:
		#Check the content of the "struct"
		if isStruct:
			if "{" in line:
				if StructCount == -1:
					StructCount += 2
				else:
					StructCount += 1
			elif "}" in line:
				StructCount -= 1
			if StructCount == 0:
				isStruct = False
				StructCount = -1
			else:
				continue
		if line.strip().lower().startswith("struct "):
			isStruct = True
			if "{" in line:
				StructCount += 2
		# Analyse the line whetherh contains some new-defined global variables
		# eles = re.findall(r"^(string|uint|int|address|bytes|byte|bool)(\d+|)(\[]|)(.+?);",line.strip())
		eles = re.findall(r"^(string|uint|int|address|bool)(\d+|)(\[]|)\s(.+?);",line.strip())
		if len(eles) == 0:
			continue
		elif "private" in eles[0][-1]:
			continue
		if eles[0][2].startswith("["):
			ArrayType = eles[0][2]
			isArray = True
		else:
			ArrayType = None
			isArray = False
		typeName = eles[0][0]
		# (&@^*&^#>?@>:>@"!")!
		if eles[0][-1].split("=")[0].strip() == "":
			continue
		varName = eles[0][-1].split("=")[0].strip().split()[-1]
		Vars.append((typeName,varName,isArray,ArrayType))
	return Vars


##################################
# FIND CONTRACT RANGES
##################################
def findContractRanges(content,mainContractName):
	lineNo = 0
	contractRanges = []
	contractStartPoint = None
	findContract = False
	currContractName = None

	# we should find all contract class and analysis all
	for line in content:
		#inject the console contract
		if line.startswith("pragma solidity"):
			with open(home + "/consoleContract_LineMap.txt","r") as f:
				consoleContract = f.read()
				content[lineNo] += consoleContract +  "\n"
		# find MAIN CONTRACT
		if line.startswith("contract ") and (not findContract):
			contractStartPoint = lineNo
			#get the contract name 
			currContractName = line.split()[1]
			if currContractName.endswith("{"):
				currContractName = currContractName[:-1]

			findContract = True
		# record CONTRACTS
		elif findContract and ( line.strip().startswith("function ") or line.strip().startswith("modifier ") or line.strip().startswith("constructor ")): # too limited 
			contractRanges.append( (currContractName, (contractStartPoint,lineNo) ) )
			findContract = False
			# BREAK if the main contract has been found 
			if currContractName == mainContractName:
				break
		elif findContract and ( line.strip().startswith("contract ") or (lineNo == len(content)) ):
			# to handle the contract without the function to terminate 
			contractRanges.append( (currContractName , (contractStartPoint,lineNo)) )
			contractStartPoint = lineNo
			currContractName = line.split()[1]
		lineNo += 1

	return contractRanges

##################################
# FIND FUNCTION RANGES
##################################
def findFunctionRanges(content,contractStartLineNo):
	# 'isFunc' is responsible to the chekc whther the found head line belongs to a function or not
	def isFunc(content,startPoint):
		while True:
			funcLine = content[startPoint].split("//")[0].strip()
			if funcLine.endswith("{"):# or funcLine.endswith("}") :
				return True
			elif funcLine.endswith(";"):
				return False
			startPoint += 1
		
	functionRanges = {} # name: [ (startLineNo,finishLineNo, funcNo), (.. , .. , ..), .. ] 
	lineNo = contractStartLineNo
	isFunction = False
	functionRangeCount = -1
	contractRangeCount = -1
	scanningStart = False
	for line in content[contractStartLineNo:]:
		#skip the comment line
		if line.strip().startswith("//") or line.strip().startswith("/*") or line.strip().startswith("*") or line.strip().startswith("*/"):
			lineNo += 1
			continue
		
		# working on current function
		if isFunction:
			if functionRangeCount == -1 and (len(re.findall(r"{",line)) > 0):
					functionRangeCount += 1
			functionRangeCount += len(re.findall(r"{",line))
			functionRangeCount -= len(re.findall(r"}",line)) 
			# check whether we should terminate
			if functionRangeCount == 0:
				isFunction = False
				functionRangeCount = -1
				functionFinishPointer = lineNo
				#insert the function range to the "functionRange"
				if functionName in functionRanges:
					funcNo = len(functionRanges[functionName]) + 1
					functionRanges[functionName].append( (functionStartPoint,functionFinishPointer,funcNo) )
				else:
					functionRanges[functionName] = [(functionStartPoint,functionFinishPointer,1)]
				lineNo += 1
				continue
			else:
				lineNo += 1
				continue
			
		#identify the function start point
		if line.strip().startswith("function"):
			if isFunc(content,lineNo):
				isFunction = True
			else:
				lineNo += 1
				continue
			
			functionStartPoint = lineNo
			# (&@^*&^#>?@>:>@"!")!
			functionName = line.strip().split("function")[-1].split()[0]
			if functionName != "()":
				functionName = functionName.split("(")[0]
			if len(re.findall(r"{",line)) > 0:
				functionRangeCount += len(re.findall(r"{",line)) + 1
				functionRangeCount -= len(re.findall(r"}",line)) 
		else:
			# working on only main contract range
			if "{" in line and (not scanningStart):
				contractRangeCount += 1
				scanningStart = True
			contractRangeCount += len(re.findall(r"{",line))
			contractRangeCount -= len(re.findall(r"}",line))

		if scanningStart and (contractRangeCount == 0) :
			return functionRanges
		
		lineNo += 1

	# worse case, if the range can not be recongnize
	return functionRanges


##################################
# FIND MODIFIERS 
##################################	
def findModifier(GVcontent):
	modifiers = []
	return modifiers

##################################
# FIND FUNCTION INPUTS DETAILS
##################################
def findFuncInputDetails(abi):
	funcInputDetails = {}
	if len(abi) > 0:
		for item in abi:
			if item["type"] == "function" :
				funcInputDetails[str(item["name"])] = []
				if len(item["inputs"]) > 0:
					for theInput in item["inputs"]:
						funcInputDetails[str(item["name"])].append([str(theInput["name"]),str(theInput["type"])])
	
	return funcInputDetails

##################################
# FIND ALLOCATION for VARIABLE
##################################
def findVarValue(content,VarName):
	for line in content:
		if line.strip().startswith("//") or line.strip().startswith("/*"):
			continue
		#Whether the variable has been allocated with any value
		if VarName in line and "=" in line:
			value = re.findall(r'%s[\s]*=[\s]*(\d+)[\s]*;',line.strip())
			if len(value) > 0:
				return value[0],line
	return None

##################################
# FIND .transfer(
##################################
def findSpecificConstraint(content):
	totalSpec = 0
	for line in content:
		if line.strip().startswith("//") or line.strip().startswith("/*"):
			continue

		if ".transfer(" in line:
			transferContent = re.findall(r'.transfer\((.+)\)',line)
			if len(transferContent) > 0:
				transferContent = transferContent[0]
				if "," not in transferContent:
					totalSpec += 1
		elif ".send(" in line:
			sendContent = re.findall(r'.send\((.+)\)',line)[0]
			if "," not in sendContent:
				totalSpec += 1
		elif ".call(" in line and ".value(" in line:
			totalSpec += 1
	
	return totalSpec

##################################
# FIND CONSTRAINTS of FUNCTIONS
##################################
def findConstraints(content,funcStartPoint,funcEndPoint,funcInputDetail,statistic):
	funcContent = content[funcStartPoint+ 1:funcEndPoint]
	if len(funcInputDetail) == 0:
		return {}
	
	constraints = {}
	for eachInput in funcInputDetail:
		inputName = eachInput[0]
		inputType = eachInput[1]
		constraints[inputType] = "noConstraints"


	existMapping = False
	for line in funcContent:
		# 1. Extract the require&assert constraint content
		constraintsContent = ""
		if line.strip().startswith("require"):
			requirementContent = re.findall(r'require[\s]*\((.+?)(,|)\);',line)
			if len(requirementContent) > 0:
				constraintsContent = requirementContent[0][0]
		elif line.strip().startswith("assert"):
			assertionContent = re.findall(r'assert[\s]*\((.+?)\);',line)
			if len(assertionContent) > 0:
				constraintsContent = assertionContent[0][0]
		
		# print(constraintsContent)


		if constraintsContent == "": #Nothing is found
			continue
		else:
			statistic[1] += 1
		

		# Extract the Constraints based on inputs
		for eachInput in funcInputDetail:
			inputName = eachInput[0]
			inputType = eachInput[1]
			
			# Input CASE1: address (NOT array)
			if inputType == "address":
				if constraintsContent != "":
					option1 = re.findall(r'%s[\s]*(!=|==)[\s]*(.+)'%inputName,constraintsContent.strip())
					option2 = re.findall(r'(.+)[\s]*(!=|==)[\s]*%s'%inputName,constraintsContent.strip())
					if len(option1) > 0:
						constraints[inputType] = option1[0][0] + option1[0][1]
						statistic[0] += 1
					elif len(option2) > 0:
						constraints[inputType]= option2[0][0] + option2[0][1]
						statistic[0]  += 1

			
			# Input CASE2: uint (NOT array)
			elif inputType.startswith("uint") and (not (inputType.endswith("]"))) :
				if constraintsContent != "":
					# compare with digits
					option1 = re.findall(r'%s[\s]*(!=|==|>=|<=|>|<)[\s]*(\d+)'%inputName,constraintsContent.strip())
					option2 = re.findall(r'(\d+)[\s]*(!=|==|>=|<=|>|<)[\s]*%s'%inputName,constraintsContent.strip())
					# compare with variables 
					option3 = re.findall(r'%s[\s]*(!=|==|>=|<=|>|<)[\s]*(.+)'%inputName,constraintsContent.strip())
					option4 = re.findall(r'(.+)[\s]*(!=|==|>=|<=|>|<)[\s]*%s'%inputName,constraintsContent.strip())
					if len(option1) > 0:
						if option1[0][0] == "<=":
							constraints[inputType] = [0,int(option1[0][1])]
							statistic[0]  += 1
						elif option1[0][0] == ">=":
							constraints[inputType] = [int(option1[0][1]),"infi"]
							statistic[0]  += 1
						elif option1[0][0] == ">":
							constraints[inputType] = [int(option1[0][1]) + 1,"infi"]
							statistic[0]  += 1
						elif option1[0][0] == "<":
							constraints[inputType] = [0,int(option1[0][1]) - 1]
							statistic[0]  += 1
						elif option1[0][0] == "==":
							constraints[inputType] = [int(option1[0][1]),int(option1[0][1])]
							statistic[0]  += 1
						else:
							constraints[inputType] = "noConstraints"
					elif len(option2) > 0:
						constraints[inputType]= option2[0][0] + option2[0][1]
					elif len(option3) > 0:
						if findVarValue(content,option3[0][1]) != None:
							if option3[0][0] == "<=":
								constraints[inputType] = [0,int(option3[0][1])]
								statistic[0]  += 1
							elif option3[0][0] == ">=":
								constraints[inputType] = [int(option3[0][1]),"infi"]
								statistic[0]  += 1
							elif option3[0][0] == ">":
								constraints[inputType] = [int(option3[0][1]) + 1,"infi"]
								statistic[0]  += 1
							elif option3[0][0] == "<":
								constraints[inputType] = [0,int(option3[0][1]) - 1]
								statistic[0]  += 1
							elif option3[0][0] == "==":
								constraints[inputType] = [int(option3[0][1]),int(option3[0][1])]
								statistic[0]  += 1
							else:
								constraints[inputType] = "noConstraints"
						else:
							constraints[inputType] = "noConstraints"
					elif len(option4) > 0:
						constraints[inputType] = [option4[0][0] + option4[0][1]+ "==>" + str(findVarValue(content,option4[0][0]))] 
			
			# Input CASE3: int (NOT array)
			elif inputType.startswith("int") and (not (inputType.endswith("]"))):
				if constraintsContent != "":
					# compare with digits
					option1 = re.findall(r'%s[\s]*(!=|==|>=|<=|>|<)[\s]*[-]*[\s]*(\d+)'%inputName,constraintsContent.strip())
					option2 = re.findall(r'[-]*[\s]*(\d+)[\s]*(!=|==|>=|<=|>|<)[\s]*%s'%inputName,constraintsContent.strip())
					# compare with variables 
					option3 = re.findall(r'%s[\s]*(!=|==|>=|<=|>|<)[\s]*(.+)'%inputName,constraintsContent.strip())
					option4 = re.findall(r'(.+)[\s]*(!=|==|>=|<=|>|<)[\s]*%s'%inputName,constraintsContent.strip())
					if len(option1) > 0:
						if option1[0][0] == "<=":
							constraints[inputType] = ["infi",int(option1[0][1])]
							statistic[0]  += 1
						elif option1[0][0] == ">=":
							constraints[inputType] = [int(option1[0][1]),"infi"]
							statistic[0]  += 1
						elif option1[0][0] == ">":
							constraints[inputType] = [int(option1[0][1]) + 1,"infi"]
							statistic[0]  += 1
						elif option1[0][0] == "<":
							constraints[inputType] = ["infi",int(option1[0][1]) - 1]
							statistic[0]  += 1
						elif option1[0][0] == "==":
							constraints[inputType] = [int(option1[0][1]),int(option1[0][1])]
							statistic[0]  += 1
						else:
							constraints[inputType] = "noConstraints"
					elif len(option2) > 0:
						constraints[inputType] = option2[0][1] + option2[0][0]
					elif len(option3) > 0:
						if findVarValue(content,option3[0][1]) != None:
							if option3[0][0] == "<=":
								constraints[inputType] = ["infi",int(option3[0][1])]
								statistic[0]  += 1
							elif option3[0][0] == ">=":
								constraints[inputType] = [int(option3[0][1]),"infi"]
								statistic[0]  += 1
							elif option3[0][0] == ">":
								constraints[inputType] = [int(option3[0][1]) + 1,"infi"]
								statistic[0]  += 1
							elif option3[0][0] == "<":
								constraints[inputType] = ["infi",int(option3[0][1]) - 1]
								statistic[0]  += 1
							elif option3[0][0] == "==":
								constraints[inputType] = [int(option3[0][1]),int(option3[0][1])]
								statistic[0]  += 1
							else:
								constraints[inputType] = "noConstraints"
						else:
							constraints[inputType] = "noConstraints"
					elif len(option4) > 0:
						constraints[inputType] = option4[0][1] + option4[0][0]
			

		# 2. mapping
		if "allowance" in constraintsContent or "allowed" in constraintsContent or "balances" in constraintsContent or "balanceOf" in constraintsContent:
			existMapping = True
	
	constraintsContent = []
	for eachInput in funcInputDetail:
		inputType = eachInput[1]
		if inputType.startswith("uint"):
			if existMapping:
				constraintsContent.append([1,500])
				statistic[0]  += 1
			else:
				if isinstance(constraints[inputType],list):
					constraintsContent.append(constraints[inputType])
				else:
					constraintsContent.append("noConstraints")
		else:
			constraintsContent.append(constraints[inputType])
	return constraintsContent



#The home address
home = "/Users/BAOandPAN/Desktop/HonoursProject"

if __name__ == "__main__":
	statistic = [0,0,0]
	contractName = sys.argv[1]
	sourceFolder = home + "/ExecutionWatch/org_contracts"
	with open(sourceFolder + "/"+contractName + ".sol","r") as f:
		content = f.readlines()

	#Get the contract ranges 
	# e.g. contractRange: (contractClassName, (startPoint,endPoint))
	contractRanges = findContractRanges(content,contractName)

	#Get the ABI from the compilation result
	with open(sourceFolder + "/" + contractName + ".json","r") as json_f:
		info = json.load(json_f)
		current_ABI = info["ABI"]
		current_ABI = json.loads(current_ABI)
	

	#Get the Function Inputs Details
	curr_funcInputDetails = findFuncInputDetails(current_ABI)
	# print(curr_funcInputDetails)

	#Go through each contract class
	if len(contractRanges) > 0:
		constraints = {}
		for eachConRange in contractRanges:
			#find the function ranges in corresponding functions
			funcRanges = findFunctionRanges(content,eachConRange[1][0])

			#Go through each function
			for funcName in funcRanges:
				if funcName not in curr_funcInputDetails:
					continue
				funcStartPoint = funcRanges[funcName][0][0]
				funcEndPoint = funcRanges[funcName][0][1] + 1
				funcInputDetail = curr_funcInputDetails[funcName]
				constraints[funcName] = findConstraints(content,funcStartPoint,funcEndPoint,funcInputDetail,statistic)
	
	# print(constraints)
	# print("\n")
	totalSpec = findSpecificConstraint(content)
	statistic[2] = totalSpec
	with open(home + "/ExecutionWatch/Constraints/" + contractName + ".json", "w") as write_file:
		json.dump(constraints, write_file)
	# print(statistic)
	
	