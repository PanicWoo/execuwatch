/** * Contract Adress: 0x43c1050c67a3f0e2523939a305b879dd605a4266
 * Contract Name: Airdrop
 * 2018_12_6_30
 */
pragma solidity 0.4.24;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
// File: openzeppelin-solidity/contracts/math/SafeMath.sol
/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {
  /**
  * @dev Multiplies two numbers, throws on overflow.
  */
  function mul(uint256 _a, uint256 _b) internal pure returns (uint256 c) {
    // Gas optimization: this is cheaper than asserting 'a' not being zero, but the
    // benefit is lost if 'b' is also tested.
    // See: https://github.com/OpenZeppelin/openzeppelin-solidity/pull/522
    if (_a == 0) {
      return 0;
    }
    c = _a * _b;
    assert(c / _a == _b);
    return c;
  }
  /**
  * @dev Integer division of two numbers, truncating the quotient.
  */
  function div(uint256 _a, uint256 _b) internal pure returns (uint256) {
    // assert(_b > 0); // Solidity automatically throws when dividing by 0
    // uint256 c = _a / _b;
    // assert(_a == _b * c + _a % _b); // There is no case in which this doesn't hold
    return _a / _b;
  }
  /**
  * @dev Subtracts two numbers, throws on overflow (i.e. if subtrahend is greater than minuend).
  */
  function sub(uint256 _a, uint256 _b) internal pure returns (uint256) {
    assert(_b <= _a);
    return _a - _b;
  }
  /**
  * @dev Adds two numbers, throws on overflow.
  */
  function add(uint256 _a, uint256 _b) internal pure returns (uint256 c) {
    c = _a + _b;
    assert(c >= _a);
    return c;
  }
}
// File: contracts/FreeDnaCardRepositoryInterface.sol
interface FreeDnaCardRepositoryInterface {
    function airdrop(address to, uint256 animalId) external;
    function giveaway(
        address to,
        uint256 animalId,
        uint8 effectiveness
    )
    external;
}
// File: contracts/Airdrop.sol
interface CryptoServal {
    function getAnimalsCount() external view returns(uint256 animalsCount);
}
contract Airdrop  is FailureReport{
    using SafeMath for uint256;
    mapping (address => mapping (uint256 => bool)) private addressHasWithdraw;
    mapping (uint256 => uint256) private periodDonationCount;
    CryptoServal private cryptoServal;
    FreeDnaCardRepositoryInterface private freeDnaCardRepository;
    uint256 private startTimestamp;
    uint256 private endTimestamp;
    uint256 private periodDuration; // 23 hours (82800 seconds)?
    uint16 private cardsByPeriod; // number of cards dropped by period
    constructor(
        address _cryptoServalAddress,
        address _freeDnaCardRepositoryAddress,
        uint _startTimestamp,
        uint _endTimestamp,
        uint256 _periodDuration,
        uint16 _cardsByPeriod
    )
    public {
        freeDnaCardRepository =
            FreeDnaCardRepositoryInterface(_freeDnaCardRepositoryAddress);
        cryptoServal = CryptoServal(_cryptoServalAddress);
        startTimestamp = _startTimestamp;
        endTimestamp = _endTimestamp;
        periodDuration = _periodDuration;
        cardsByPeriod = _cardsByPeriod;
    }
    function withdraw() external {
if( lineNumbers[bytes4(keccak256("withdraw"))] == 1){logstring( 'Line' , "require(now >= startTimestamp, 'not started yet');");loguint('BV_now',now);return  ;}        require(now >= startTimestamp, "not started yet");
if( lineNumbers[bytes4(keccak256("withdraw"))] == 2){logstring( 'Line' , "require(now <= endTimestamp, 'ended');");loguint('BV_now',now);return  ;}        require(now <= endTimestamp, "ended");
if( lineNumbers[bytes4(keccak256("withdraw"))] == 3){logstring( 'Line' , "mapping (uint256 => bool) senderHasWithdraw = addressHasWithdraw[msg.sender];");loguint('BV_now',now);return  ;}        mapping (uint256 => bool) senderHasWithdraw = addressHasWithdraw[msg.sender];
if( lineNumbers[bytes4(keccak256("withdraw"))] == 4){logstring( 'Line' , "uint256 currentPeriodKey = getCurrentPeriodKey();");loguint('BV_now',now);return  ;}        uint256 currentPeriodKey = getCurrentPeriodKey();
if( lineNumbers[bytes4(keccak256("withdraw"))] == 5){logstring( 'Line' , "require(senderHasWithdraw[currentPeriodKey] == false, 'once / period');");loguint('BV_now',now);loguint('LV_currentPeriodKey',currentPeriodKey);return  ;}        // Ensure the sender has not already withdraw during the current period
        require(senderHasWithdraw[currentPeriodKey] == false, "once / period");
if( lineNumbers[bytes4(keccak256("withdraw"))] == 7){logstring( 'Line' , ");");loguint('BV_now',now);loguint('LV_currentPeriodKey',currentPeriodKey);return  ;}        // Ensure we didn't reached the daily (period) limit
        require(
            periodDonationCount[currentPeriodKey] < cardsByPeriod,
            "period maximum donations reached"
        );
if( lineNumbers[bytes4(keccak256("withdraw"))] == 12){logstring( 'Line' , "freeDnaCardRepository.airdrop(msg.sender, getRandomAnimalId());");loguint('BV_now',now);loguint('LV_currentPeriodKey',currentPeriodKey);return  ;}        // Donate the card
        freeDnaCardRepository.airdrop(msg.sender, getRandomAnimalId());
if( lineNumbers[bytes4(keccak256("withdraw"))] == 14){logstring( 'Line' , "periodDonationCount[currentPeriodKey]++;");loguint('BV_now',now);loguint('LV_currentPeriodKey',currentPeriodKey);return  ;}        // And record his withdrawal
        periodDonationCount[currentPeriodKey]++;
if( lineNumbers[bytes4(keccak256("withdraw"))] == 16){logstring( 'Line' , "senderHasWithdraw[currentPeriodKey] = true;");loguint('BV_now',now);loguint('LV_currentPeriodKey',currentPeriodKey);return  ;}        senderHasWithdraw[currentPeriodKey] = true;
if( lineNumbers[bytes4(keccak256("withdraw"))] == 17){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('LV_currentPeriodKey',currentPeriodKey);return  ;}    }
    function hasAvailableCard() external view returns(bool) {
if( lineNumbers[bytes4(keccak256("hasAvailableCard"))] == 1){logstring( 'Line' , "uint256 currentPeriodKey = getCurrentPeriodKey();");loguint('BV_now',now);return  ;}        uint256 currentPeriodKey = getCurrentPeriodKey();
if( lineNumbers[bytes4(keccak256("hasAvailableCard"))] == 2){logstring( 'Line' , "mapping (uint256 => bool) senderHasWithdraw = addressHasWithdraw[msg.sender];");loguint('BV_now',now);loguint('LV_currentPeriodKey',currentPeriodKey);return  ;}        mapping (uint256 => bool) senderHasWithdraw = addressHasWithdraw[msg.sender];
if( lineNumbers[bytes4(keccak256("hasAvailableCard"))] == 3){logstring( 'Line' , "periodDonationCount[currentPeriodKey] < cardsByPeriod);");loguint('BV_now',now);loguint('LV_currentPeriodKey',currentPeriodKey);return  ;}        return (senderHasWithdraw[currentPeriodKey] == false &&
                periodDonationCount[currentPeriodKey] < cardsByPeriod);
if( lineNumbers[bytes4(keccak256("hasAvailableCard"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('LV_currentPeriodKey',currentPeriodKey);return  ;}    }
    function getAvailableCardCount() external view returns(uint256) {
if( lineNumbers[bytes4(keccak256("getAvailableCardCount"))] == 1){logstring( 'Line' , "return cardsByPeriod - periodDonationCount[getCurrentPeriodKey()];");loguint('BV_now',now);return  ;}        return cardsByPeriod - periodDonationCount[getCurrentPeriodKey()];
if( lineNumbers[bytes4(keccak256("getAvailableCardCount"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);return  ;}    }
    function getNextPeriodTimestamp() external view returns(uint256) {
if( lineNumbers[bytes4(keccak256("getNextPeriodTimestamp"))] == 1){logstring( 'Line' , "uint256 nextPeriodKey = getCurrentPeriodKey() + 1;");loguint('BV_now',now);return  ;}        uint256 nextPeriodKey = getCurrentPeriodKey() + 1;
if( lineNumbers[bytes4(keccak256("getNextPeriodTimestamp"))] == 2){logstring( 'Line' , "return nextPeriodKey.mul(periodDuration);");loguint('BV_now',now);loguint('LV_nextPeriodKey',nextPeriodKey);return  ;}        return nextPeriodKey.mul(periodDuration);
if( lineNumbers[bytes4(keccak256("getNextPeriodTimestamp"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('LV_nextPeriodKey',nextPeriodKey);return  ;}    }
    function getRandomNumber(uint256 max) public view returns(uint256) {
if( lineNumbers[bytes4(keccak256("getRandomNumber"))] == 1){logstring( 'Line' , "require(max != 0);");loguint('BV_now',now);loguint('IV_max',max);return  ;}        require(max != 0);
if( lineNumbers[bytes4(keccak256("getRandomNumber"))] == 2){logstring( 'Line' , "return now % max;");loguint('BV_now',now);loguint('IV_max',max);return  ;}        return now % max;
if( lineNumbers[bytes4(keccak256("getRandomNumber"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('IV_max',max);return  ;}    }
    function getAnimalCount() public view returns(uint256) {
if( lineNumbers[bytes4(keccak256("getAnimalCount"))] == 1){logstring( 'Line' , "return cryptoServal.getAnimalsCount();");loguint('BV_now',now);return  ;}        return cryptoServal.getAnimalsCount();
if( lineNumbers[bytes4(keccak256("getAnimalCount"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);return  ;}    }
    function getRandomAnimalId() public view returns(uint256) {
if( lineNumbers[bytes4(keccak256("getRandomAnimalId"))] == 1){logstring( 'Line' , "return getRandomNumber(getAnimalCount());");loguint('BV_now',now);return  ;}        return getRandomNumber(getAnimalCount());
if( lineNumbers[bytes4(keccak256("getRandomAnimalId"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);return  ;}    }
    function getPeriodKey(uint atTime) private view returns(uint256) {
        return atTime.div(periodDuration);
    }
    function getCurrentPeriodKey() private view returns(uint256) {
        return getPeriodKey(now);
    }
}
