/** * Contract Adress: 0x988a6b099b6e42f6688eeef79ce7ccba0306fca7
 * Contract Name: CCP
 * 2018_9_18_94
 */
pragma solidity ^0.4.21;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract ERC20  is FailureReport{
	function totalSupply() public view returns (uint256 totalSup);
	function balanceOf(address _owner) public view returns (uint256 balance);
	function transferFrom(address _from, address _to, uint256 _value) public returns (bool success);
	function allowance(address _owner, address _spender) public view returns (uint256 remaining);
	function approve(address _spender, uint256 _value) public returns (bool success);
	function transfer(address _to, uint256 _value) public returns (bool success);
	event Transfer(address indexed _from, address indexed _to, uint _value);
	event Approval(address indexed _owner, address indexed _spender, uint256 _value);
}
contract ERC223  is FailureReport{
	function transfer(address _to, uint256 _value, bytes _data) public returns (bool success);
	event Transfer(address indexed _from, address indexed _to, uint _value, bytes _data);
}
contract ERC223ReceivingContract  is FailureReport{
	function tokenFallback(address _from, uint _value, bytes _data) public;
}
contract CCP is  FailureReport, ERC223, ERC20 {
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balances[a[i]] = value;}loguint("a[3]'s balance",balances[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowed[a[i]][msg.sender] = value;allowed[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowed[a[3]][msg.sender]);}	using SafeMath for uint256;
	uint public constant _totalSupply = 2100000000e18;
	//starting supply of Token
	string public constant symbol = "CCP";
	string public constant name = "CCP COIN";
	uint8 public constant decimals = 18;
	mapping(address => uint256) balances;
	mapping(address => mapping(address => uint256)) allowed;
	constructor() public{
		balances[msg.sender] = _totalSupply;
		emit Transfer(0x0, msg.sender, _totalSupply);
	}
	function totalSupply() public view returns (uint256 totalSup) {
	return _totalSupply;
	}
	function balanceOf(address _owner) public view returns (uint256 balance) {
		return balances[_owner];
	}
	function transfer(address _to, uint256 _value) public returns (bool success) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , ");");loguint('BV_now',now);loguint('GV__totalSupply',_totalSupply);logstring('GV_symbol',symbol);logstring('GV_name',name);loguint('GV_decimals',decimals);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}		require(
			!isContract(_to)
		);
if( lineNumbers[bytes4(keccak256("transfer"))] == 4){logstring( 'Line' , "balances[msg.sender] = balances[msg.sender].sub(_value);");loguint('BV_now',now);loguint('GV__totalSupply',_totalSupply);logstring('GV_symbol',symbol);logstring('GV_name',name);loguint('GV_decimals',decimals);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}		balances[msg.sender] = balances[msg.sender].sub(_value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 5){logstring( 'Line' , "balances[_to] = balances[_to].add(_value);");loguint('BV_now',now);loguint('GV__totalSupply',_totalSupply);logstring('GV_symbol',symbol);logstring('GV_name',name);loguint('GV_decimals',decimals);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}		balances[_to] = balances[_to].add(_value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 6){logstring( 'Line' , "emit Transfer(msg.sender, _to, _value);");loguint('BV_now',now);loguint('GV__totalSupply',_totalSupply);logstring('GV_symbol',symbol);logstring('GV_name',name);loguint('GV_decimals',decimals);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}		emit Transfer(msg.sender, _to, _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 7){logstring( 'Line' , "return true;");loguint('BV_now',now);loguint('GV__totalSupply',_totalSupply);logstring('GV_symbol',symbol);logstring('GV_name',name);loguint('GV_decimals',decimals);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}		return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 8){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV__totalSupply',_totalSupply);logstring('GV_symbol',symbol);logstring('GV_name',name);loguint('GV_decimals',decimals);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}	}
	function transfer(address _to, uint256 _value, bytes _data) public returns (bool success){
		require(
			isContract(_to)
		);
		balances[msg.sender] = balances[msg.sender].sub(_value);
		balances[_to] = balances[_to].add(_value);
		ERC223ReceivingContract(_to).tokenFallback(msg.sender, _value, _data);
		emit Transfer(msg.sender, _to, _value, _data);
		return true;
	}
	function isContract(address _from) private view returns (bool) {
		uint256 codeSize;
		assembly {
			codeSize := extcodesize(_from)
		}
		return codeSize > 0;
	}
	function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , ");");loguint('BV_now',now);loguint('GV__totalSupply',_totalSupply);logstring('GV_symbol',symbol);logstring('GV_name',name);loguint('GV_decimals',decimals);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}		require(
			balances[_from] >= _value
			&& _value > 0
		);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "balances[_from] = balances[_from].sub(_value);");loguint('BV_now',now);loguint('GV__totalSupply',_totalSupply);logstring('GV_symbol',symbol);logstring('GV_name',name);loguint('GV_decimals',decimals);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}		balances[_from] = balances[_from].sub(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 6){logstring( 'Line' , "balances[_to] = balances[_to].add(_value);");loguint('BV_now',now);loguint('GV__totalSupply',_totalSupply);logstring('GV_symbol',symbol);logstring('GV_name',name);loguint('GV_decimals',decimals);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}		balances[_to] = balances[_to].add(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 7){logstring( 'Line' , "allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_value);");loguint('BV_now',now);loguint('GV__totalSupply',_totalSupply);logstring('GV_symbol',symbol);logstring('GV_name',name);loguint('GV_decimals',decimals);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}		allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 8){logstring( 'Line' , "emit Transfer(_from, _to, _value);");loguint('BV_now',now);loguint('GV__totalSupply',_totalSupply);logstring('GV_symbol',symbol);logstring('GV_name',name);loguint('GV_decimals',decimals);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}		emit Transfer(_from, _to, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 9){logstring( 'Line' , "return true;");loguint('BV_now',now);loguint('GV__totalSupply',_totalSupply);logstring('GV_symbol',symbol);logstring('GV_name',name);loguint('GV_decimals',decimals);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}		return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 10){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV__totalSupply',_totalSupply);logstring('GV_symbol',symbol);logstring('GV_name',name);loguint('GV_decimals',decimals);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}	}
	function approve(address _spender, uint256 _value) public returns (bool success) {
		require(
			(_value == 0) || (allowed[msg.sender][_spender] == 0)
		);
		allowed[msg.sender][_spender] = _value;
		emit Approval(msg.sender, _spender, _value);
		return true;
	}
	function allowance(address _owner, address _spender) public view returns (uint256 remain) {
		return allowed[_owner][_spender];
	}
	function () public payable {
		revert();
	}
	event Transfer(address  indexed _from, address indexed _to, uint256 _value);
	event Transfer(address indexed _from, address  indexed _to, uint _value, bytes _data);
	event Approval(address indexed _owner, address indexed _spender, uint256 _value);
}
/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {
	function mul(uint256 a, uint256 b) internal pure returns (uint256) {
		if (a == 0) {
			return 0;
		}
		uint256 c = a * b;
		assert(c / a == b);
		return c;
	}
	function div(uint256 a, uint256 b) internal pure returns (uint256) {
		// assert(b > 0); // Solidity automatically throws when dividing by 0
		uint256 c = a / b;
		// assert(a == b * c + a % b); // There is no case in which this doesn't hold
		return c;
	}
	function sub(uint256 a, uint256 b) internal pure returns (uint256) {
		assert(b <= a);
		return a - b;
	}
	function add(uint256 a, uint256 b) internal pure returns (uint256) {
		uint256 c = a + b;
		assert(c >= a);
		return c;
	}
}
