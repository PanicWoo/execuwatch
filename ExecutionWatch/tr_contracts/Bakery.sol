/** * Contract Adress: 0x6ba2fa9f8ad7b7c376210c0fb4fd311a66d98e8e
 * Contract Name: Bakery
 * 2018_11_4_22
 */
pragma solidity ^0.4.25;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract Bakery  is FailureReport{

event logcontracts(string,address[]);  // index of created contracts
  address[] public contracts;
  // useful to know the row count in contracts index
  function getContractCount()
    public
    constant
    returns(uint contractCount)
  {
if( lineNumbers[bytes4(keccak256("getContractCount"))] == 1){logstring( 'Line' , "return contracts.length;");loguint('BV_now',now);emit logcontracts('GV_contracts',contracts);return  ;}    return contracts.length;
if( lineNumbers[bytes4(keccak256("getContractCount"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);emit logcontracts('GV_contracts',contracts);return  ;}  }
  // deploy a new contract
  function newCookie()
    public
    returns(address newContract)
  {
if( lineNumbers[bytes4(keccak256("newCookie"))] == 1){logstring( 'Line' , "Cookie c = new Cookie();");loguint('BV_now',now);emit logcontracts('GV_contracts',contracts);return  ;}    Cookie c = new Cookie();
if( lineNumbers[bytes4(keccak256("newCookie"))] == 2){logstring( 'Line' , "contracts.push(c);");loguint('BV_now',now);emit logcontracts('GV_contracts',contracts);return  ;}    contracts.push(c);
if( lineNumbers[bytes4(keccak256("newCookie"))] == 3){logstring( 'Line' , "return c;");loguint('BV_now',now);emit logcontracts('GV_contracts',contracts);return  ;}    return c;
if( lineNumbers[bytes4(keccak256("newCookie"))] == 4){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);emit logcontracts('GV_contracts',contracts);return  ;}  }
}
contract Cookie {
  function () public payable {}
  // suppose the deployed contract has a purpose
  function getFlavor()
    public
    constant
    returns (string flavor)
  {
    return "mmm ... chocolate chip";
  }
}
