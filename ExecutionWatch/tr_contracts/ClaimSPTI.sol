/** * Contract Adress: 0x5ea90b8bed24a349f7bcae384f61d55384c2579f
 * Contract Name: ClaimSPTI
 * 2018_10_3_73
 */
pragma solidity ^0.4.25;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
library SafeMath {
  function mul(uint256 a, uint256 b) internal pure returns (uint256 c) {
    if (a == 0) {
      return 0;
    }
    c = a * b;
    assert(c / a == b);
    return c;
  }
  function div(uint256 a, uint256 b) internal pure returns (uint256) {
    return a / b;
  }
  function sub(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b <= a);
    return a - b;
  }
  function add(uint256 a, uint256 b) internal pure returns (uint256 c) {
    c = a + b;
    assert(c >= a);
    return c;
  }
}
interface TokenOLD {
    function balanceOf(address who) external view returns (uint256);
    function transferFrom(address from, address to, uint tokens) external returns (bool success);
}
interface TokenNEW {
    function transfer(address to, uint256 value) external returns (bool);
}
contract ClaimSPTI is FailureReport{
    using SafeMath for uint256;
    TokenNEW public newTokenReward;
    TokenOLD  public oldToken;
    address public creator;
    address public owner = 0x1Ab98C0833e034b1E81F4F0282914C615d795299;
    uint256 public startDate;
    uint256 public endDate;
    modifier isCreator() {
        require(msg.sender == creator);
        _;
    }
    event FundTransfer(address backer, uint amount, bool isContribution);
    constructor() public {
        startDate = 1538554875;
        endDate = startDate + 30 days;
        creator = msg.sender;
        newTokenReward = TokenNEW(0xc91d83955486e5261528d1acc1956529d2fe282b); //Instantiate the new reward
        oldToken = TokenOLD(0xa673802792379714201ebc5f586c3a44b0248681); //Instantiate old token to be replaced
    }
    function() public payable {
        require(now > startDate);
        require(now < endDate);
        require(msg.value == 0); // Only 0 ether accepted, This is not an IC Oh!
        uint oldSptiUserBal;
        oldSptiUserBal = getBalance(msg.sender); //Get Old SPTI balance
        require(oldSptiUserBal > 0); // Make sure claimant actually possesses Old SPTI
        require(oldToken.transferFrom(msg.sender, 0xceC74106a23329745b07f6eC5e1E39803b3fF31F, oldSptiUserBal));
        //If all of the above happens accordingly, go ahead and release new token
        //to old token holders
        uint256 amount = oldSptiUserBal.div(8);
        newTokenReward.transfer(msg.sender, amount);
        emit FundTransfer(msg.sender, amount, true);
    }
    function getBalance(address userAddress) public view returns (uint256){
if( lineNumbers[bytes4(keccak256("getBalance"))] == 1){logstring( 'Line' , "uint bal = oldToken.balanceOf(userAddress);");loguint('BV_now',now);logaddress('GV_creator',creator);logaddress('GV_owner',owner);loguint('GV_startDate',startDate);loguint('GV_endDate',endDate);logaddress('IV_userAddress',userAddress);return  ;}        uint bal = oldToken.balanceOf(userAddress);
if( lineNumbers[bytes4(keccak256("getBalance"))] == 2){logstring( 'Line' , "return bal;");loguint('BV_now',now);logaddress('GV_creator',creator);logaddress('GV_owner',owner);loguint('GV_startDate',startDate);loguint('GV_endDate',endDate);logaddress('IV_userAddress',userAddress);loguint('LV_bal',bal);return  ;}        return bal;
if( lineNumbers[bytes4(keccak256("getBalance"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_creator',creator);logaddress('GV_owner',owner);loguint('GV_startDate',startDate);loguint('GV_endDate',endDate);logaddress('IV_userAddress',userAddress);loguint('LV_bal',bal);return  ;}    }
    function transferToken(address to, uint256 value) isCreator public {
if( lineNumbers[bytes4(keccak256("transferToken"))] == 1){logstring( 'Line' , "newTokenReward.transfer(to, value);");loguint('BV_now',now);logaddress('GV_creator',creator);logaddress('GV_owner',owner);loguint('GV_startDate',startDate);loguint('GV_endDate',endDate);logaddress('IV_to',to);loguint('IV_value',value);return  ;}        newTokenReward.transfer(to, value);      
if( lineNumbers[bytes4(keccak256("transferToken"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_creator',creator);logaddress('GV_owner',owner);loguint('GV_startDate',startDate);loguint('GV_endDate',endDate);logaddress('IV_to',to);loguint('IV_value',value);return  ;}    }
    function kill() isCreator public {
if( lineNumbers[bytes4(keccak256("kill"))] == 1){logstring( 'Line' , "selfdestruct(owner);");loguint('BV_now',now);logaddress('GV_creator',creator);logaddress('GV_owner',owner);loguint('GV_startDate',startDate);loguint('GV_endDate',endDate);return  ;}        selfdestruct(owner);
if( lineNumbers[bytes4(keccak256("kill"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_creator',creator);logaddress('GV_owner',owner);loguint('GV_startDate',startDate);loguint('GV_endDate',endDate);return  ;}    }
}
