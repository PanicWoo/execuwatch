/** * Contract Adress: 0xe136756b749740a23e726932fb66e49b57e4705f
 * Contract Name: BlockVibranium
 * 2018_11_4_72
 */
pragma solidity ^0.4.21;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
/**
 * @title SafeMaths
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {
  function mul(uint256 a, uint256 b) internal constant returns (uint256) {
    uint256 c = a * b;
    assert(a == 0 || c / a == b);
    return c;
  }
  function div(uint256 a, uint256 b) internal constant returns (uint256) {
    // assert(b > 0); // Solidity automatically throws when dividing by 0
    uint256 c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn't hold
    return c;
  }
  function sub(uint256 a, uint256 b) internal constant returns (uint256) {
    assert(b <= a);
    return a - b;
  }
  function add(uint256 a, uint256 b) internal constant returns (uint256) {
    uint256 c = a + b;
    assert(c >= a);
    return c;
  }
}
/**
 * @title ERC20Basic
 * @dev Simpler version of ERC20 interface
 * @dev see https://github.com/ethereum/EIPs/issues/179
 */
contract ERC20Basic  is FailureReport{
  uint256 public totalSupply;
  function balanceOf(address who) constant returns (uint256);
  function transfer(address to, uint256 value) returns (bool);
  event Transfer(address indexed from, address indexed to, uint256 value);
}
/**
 * @title Basic token
 * @dev Basic version of StandardToken, with no allowances.
 */
contract BasicToken is  FailureReport, ERC20Basic {
  using SafeMath for uint256;
  mapping(address => uint256) balances;
  /**
  * @dev transfer token for a specified address
  * @param _to The address to transfer to.
  * @param _value The amount to be transferred.
  */
  function transfer(address _to, uint256 _value) returns (bool) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "balances[msg.sender] = balances[msg.sender].sub(_value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    balances[msg.sender] = balances[msg.sender].sub(_value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 2){logstring( 'Line' , "balances[_to] = balances[_to].add(_value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    balances[_to] = balances[_to].add(_value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 3){logstring( 'Line' , "Transfer(msg.sender, _to, _value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    Transfer(msg.sender, _to, _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 4){logstring( 'Line' , "return true;");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}  }
  /**
  * @dev Gets the balance of the specified address.
  * @param _owner The address to query the the balance of.
  * @return An uint256 representing the amount owned by the passed address.
  */
  function balanceOf(address _owner) constant returns (uint256 balance) {
    return balances[_owner];
  }
}
/**
 * @title ERC20 interface
 * @dev see https://github.com/ethereum/EIPs/issues/20
 */
contract ERC20 is  FailureReport, ERC20Basic {
  function allowance(address owner, address spender) constant returns (uint256);
  function transferFrom(address from, address to, uint256 value) returns (bool);
  function approve(address spender, uint256 value) returns (bool);
  event Approval(address indexed owner, address indexed spender, uint256 value);
}
/**
 * @title Standard ERC20 token
 *
 * @dev Implementation of the basic standard token.
 * @dev https://github.com/ethereum/EIPs/issues/20
 * @dev Based on code by FirstBlood: https://github.com/Firstbloodio/token/blob/master/smart_contract/FirstBloodToken.sol
 */
contract StandardToken is  FailureReport, ERC20, BasicToken {
  mapping (address => mapping (address => uint256)) allowed;
  /**
   * @dev Transfer tokens from one address to another
   * @param _from address The address which you want to send tokens from
   * @param _to address The address which you want to transfer to
   * @param _value uint256 the amout of tokens to be transfered
   */
  function transferFrom(address _from, address _to, uint256 _value) returns (bool) {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "var _allowance = allowed[_from][msg.sender];");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    var _allowance = allowed[_from][msg.sender];
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 2){logstring( 'Line' , "balances[_to] = balances[_to].add(_value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    // Check is not needed because sub(_allowance, _value) will already throw if this condition is not met
    // require (_value <= _allowance);
    balances[_to] = balances[_to].add(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "balances[_from] = balances[_from].sub(_value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    balances[_from] = balances[_from].sub(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 6){logstring( 'Line' , "allowed[_from][msg.sender] = _allowance.sub(_value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    allowed[_from][msg.sender] = _allowance.sub(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 7){logstring( 'Line' , "Transfer(_from, _to, _value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    Transfer(_from, _to, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 8){logstring( 'Line' , "return true;");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 9){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}  }
  /**
   * @dev Aprove the passed address to spend the specified amount of tokens on behalf of msg.sender.
   * @param _spender The address which will spend the funds.
   * @param _value The amount of tokens to be spent.
   */
  function approve(address _spender, uint256 _value) returns (bool) {
    // To change the approve amount you first have to reduce the addresses`
    //  allowance to zero by calling `approve(_spender, 0)` if it is not
    //  already 0 to mitigate the race condition described here:
    //  https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
    require((_value == 0) || (allowed[msg.sender][_spender] == 0));
    allowed[msg.sender][_spender] = _value;
    Approval(msg.sender, _spender, _value);
    return true;
  }
  /**
   * @dev Function to check the amount of tokens that an owner allowed to a spender.
   * @param _owner address The address which owns the funds.
   * @param _spender address The address which will spend the funds.
   * @return A uint256 specifing the amount of tokens still available for the spender.
   */
  function allowance(address _owner, address _spender) constant returns (uint256 remaining) {
    return allowed[_owner][_spender];
  }
}
/**
 * @title SimpleToken
 * @dev Very simple ERC20 Token example, where all tokens are pre-assigned to the creator.
 * Note they can later distribute these tokens as they wish using `transfer` and other
 * `StandardToken` functions.
 */
contract BlockVibranium is  FailureReport, StandardToken {
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balances[a[i]] = value;}loguint("a[3]'s balance",balances[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowed[a[i]][msg.sender] = value;allowed[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowed[a[3]][msg.sender]);}  string public constant name = "BlockVibranium";
  string public constant symbol = "VBR";
  uint256 public constant decimals = 18;
  uint256 public constant INITIAL_SUPPLY = 26800000 * 10**18;
  /**
   * @dev Contructor that gives msg.sender all of existing tokens.
   */
  function BlockVibranium() {
    totalSupply = INITIAL_SUPPLY;
    balances[msg.sender] = INITIAL_SUPPLY;
  }
}
