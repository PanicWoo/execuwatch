/** * Contract Adress: 0x0f2a1A06024f6d2ceb2aDf937732f9029CA97045
 * Contract Name: Capital
 * 2018_10_11_55
 */
pragma solidity ^0.4.24;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract Capital  is FailureReport{

event logaddresses(string,address[]);  uint constant public CASH_BACK_PERCENT = 3;
  uint constant public PROJECT_FEE_PERCENT = 20;
  uint constant public PER_BLOCK = 48;
  uint constant public MINIMUM_INVEST = 10000000000000000 wei;
  uint public wave;
  address public owner;
  address public admin;
  address[] public addresses;
  bool public pause;
  mapping(address => Investor) public investors;
  TheStrongest public boss;
  modifier onlyOwner {
    require(owner == msg.sender);
    _;
  }
  struct Investor {
    uint ID;
    uint deposit;
    uint depositCount;
    uint blockNumber;
    address referrer;
  }
  struct TheStrongest {
    address addr;
    uint deposit;
  }
  constructor () public {
    owner = msg.sender;
    admin = msg.sender;
    addresses.length = 1;
    wave = 1;
  }
  function() payable public {
    if(owner == msg.sender){
      return;
    }
    require(pause == false);
    require(msg.value == 0 || msg.value >= MINIMUM_INVEST);
    Investor storage user = investors[msg.sender];
    if(user.ID == 0){
      msg.sender.transfer(0 wei);
      user.ID = addresses.push(msg.sender);
      address referrer = bytesToAddress(msg.data);
      if (investors[referrer].deposit > 0 && referrer != msg.sender) {
        user.referrer = referrer;
      }
    }
    if(user.deposit != 0) {
      uint amount = getInvestorDividendsAmount(msg.sender);
      if(address(this).balance < amount){
        pause = true;
        return;
      }
      msg.sender.transfer(amount);
    }
    admin.transfer(msg.value * PROJECT_FEE_PERCENT / 100);
    user.deposit += msg.value;
    user.depositCount += 1;
    user.blockNumber = block.number;
    uint bonusAmount = msg.value * CASH_BACK_PERCENT / 100;
    if (user.referrer != 0x0) {
      user.referrer.transfer(bonusAmount);
      if (user.depositCount == 1) {
        msg.sender.transfer(bonusAmount);
      }
    } else if (boss.addr > 0x0) {
      if(msg.sender != boss.addr){
        if(user.deposit < boss.deposit){
          boss.addr.transfer(bonusAmount);
        }
      }
    }
    if(user.deposit > boss.deposit) {
      boss = TheStrongest(msg.sender, user.deposit);
    }
  }
  function getInvestorCount() public view returns (uint) {
if( lineNumbers[bytes4(keccak256("getInvestorCount"))] == 1){logstring( 'Line' , "return addresses.length - 1;");loguint('BV_now',now);loguint('GV_CASH_BACK_PERCENT',CASH_BACK_PERCENT);loguint('GV_PROJECT_FEE_PERCENT',PROJECT_FEE_PERCENT);loguint('GV_PER_BLOCK',PER_BLOCK);loguint('GV_MINIMUM_INVEST',MINIMUM_INVEST);loguint('GV_wave',wave);logaddress('GV_owner',owner);logaddress('GV_admin',admin);emit logaddresses('GV_addresses',addresses);logbool('GV_pause',pause);return  ;}    return addresses.length - 1;
if( lineNumbers[bytes4(keccak256("getInvestorCount"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_CASH_BACK_PERCENT',CASH_BACK_PERCENT);loguint('GV_PROJECT_FEE_PERCENT',PROJECT_FEE_PERCENT);loguint('GV_PER_BLOCK',PER_BLOCK);loguint('GV_MINIMUM_INVEST',MINIMUM_INVEST);loguint('GV_wave',wave);logaddress('GV_owner',owner);logaddress('GV_admin',admin);emit logaddresses('GV_addresses',addresses);logbool('GV_pause',pause);return  ;}  }
  function getInvestorDividendsAmount(address addr) public view returns (uint) {
if( lineNumbers[bytes4(keccak256("getInvestorDividendsAmount"))] == 1){logstring( 'Line' , "uint amount = ((investors[addr].deposit * ((block.number - investors[addr].blockNumber) * PER_BLOCK)) / 10000000);");loguint('BV_now',now);loguint('GV_CASH_BACK_PERCENT',CASH_BACK_PERCENT);loguint('GV_PROJECT_FEE_PERCENT',PROJECT_FEE_PERCENT);loguint('GV_PER_BLOCK',PER_BLOCK);loguint('GV_MINIMUM_INVEST',MINIMUM_INVEST);loguint('GV_wave',wave);logaddress('GV_owner',owner);logaddress('GV_admin',admin);emit logaddresses('GV_addresses',addresses);logbool('GV_pause',pause);logaddress('IV_addr',addr);return  ;}    uint amount = ((investors[addr].deposit * ((block.number - investors[addr].blockNumber) * PER_BLOCK)) / 10000000);
if( lineNumbers[bytes4(keccak256("getInvestorDividendsAmount"))] == 2){logstring( 'Line' , "return amount;");loguint('BV_now',now);loguint('GV_CASH_BACK_PERCENT',CASH_BACK_PERCENT);loguint('GV_PROJECT_FEE_PERCENT',PROJECT_FEE_PERCENT);loguint('GV_PER_BLOCK',PER_BLOCK);loguint('GV_MINIMUM_INVEST',MINIMUM_INVEST);loguint('GV_wave',wave);logaddress('GV_owner',owner);logaddress('GV_admin',admin);emit logaddresses('GV_addresses',addresses);logbool('GV_pause',pause);logaddress('IV_addr',addr);loguint('LV_amount',amount);return  ;}    return amount;
if( lineNumbers[bytes4(keccak256("getInvestorDividendsAmount"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_CASH_BACK_PERCENT',CASH_BACK_PERCENT);loguint('GV_PROJECT_FEE_PERCENT',PROJECT_FEE_PERCENT);loguint('GV_PER_BLOCK',PER_BLOCK);loguint('GV_MINIMUM_INVEST',MINIMUM_INVEST);loguint('GV_wave',wave);logaddress('GV_owner',owner);logaddress('GV_admin',admin);emit logaddresses('GV_addresses',addresses);logbool('GV_pause',pause);logaddress('IV_addr',addr);loguint('LV_amount',amount);return  ;}  }
  function Restart() private {
    address addr;
    for (uint256 i = addresses.length - 1; i > 0; i--) {
      addr = addresses[i];
      addresses.length -= 1;
      delete investors[addr];
    }
    pause = false;
    wave += 1;
    delete boss;
  }
  function payout() public {
if( lineNumbers[bytes4(keccak256("payout"))] == 1){logstring( 'Line' , "Restart();");loguint('BV_now',now);loguint('GV_CASH_BACK_PERCENT',CASH_BACK_PERCENT);loguint('GV_PROJECT_FEE_PERCENT',PROJECT_FEE_PERCENT);loguint('GV_PER_BLOCK',PER_BLOCK);loguint('GV_MINIMUM_INVEST',MINIMUM_INVEST);loguint('GV_wave',wave);logaddress('GV_owner',owner);logaddress('GV_admin',admin);emit logaddresses('GV_addresses',addresses);logbool('GV_pause',pause);return  ;}    if (pause) {
      Restart();
if( lineNumbers[bytes4(keccak256("payout"))] == 3){logstring( 'Line' , "return;");loguint('BV_now',now);loguint('GV_CASH_BACK_PERCENT',CASH_BACK_PERCENT);loguint('GV_PROJECT_FEE_PERCENT',PROJECT_FEE_PERCENT);loguint('GV_PER_BLOCK',PER_BLOCK);loguint('GV_MINIMUM_INVEST',MINIMUM_INVEST);loguint('GV_wave',wave);logaddress('GV_owner',owner);logaddress('GV_admin',admin);emit logaddresses('GV_addresses',addresses);logbool('GV_pause',pause);return  ;}      return;
if( lineNumbers[bytes4(keccak256("payout"))] == 4){logstring( 'Line' , "uint amount;");loguint('BV_now',now);loguint('GV_CASH_BACK_PERCENT',CASH_BACK_PERCENT);loguint('GV_PROJECT_FEE_PERCENT',PROJECT_FEE_PERCENT);loguint('GV_PER_BLOCK',PER_BLOCK);loguint('GV_MINIMUM_INVEST',MINIMUM_INVEST);loguint('GV_wave',wave);logaddress('GV_owner',owner);logaddress('GV_admin',admin);emit logaddresses('GV_addresses',addresses);logbool('GV_pause',pause);return  ;}    }
    uint amount;
if( lineNumbers[bytes4(keccak256("payout"))] == 6){logstring( 'Line' , "address addr = addresses[i];");loguint('BV_now',now);loguint('GV_CASH_BACK_PERCENT',CASH_BACK_PERCENT);loguint('GV_PROJECT_FEE_PERCENT',PROJECT_FEE_PERCENT);loguint('GV_PER_BLOCK',PER_BLOCK);loguint('GV_MINIMUM_INVEST',MINIMUM_INVEST);loguint('GV_wave',wave);logaddress('GV_owner',owner);logaddress('GV_admin',admin);emit logaddresses('GV_addresses',addresses);logbool('GV_pause',pause);loguint('LV_amount',amount);return  ;}    for(uint256 i = addresses.length - 1; i >= 1; i--){
      address addr = addresses[i];
if( lineNumbers[bytes4(keccak256("payout"))] == 8){logstring( 'Line' , "amount = getInvestorDividendsAmount(addr);");loguint('BV_now',now);loguint('GV_CASH_BACK_PERCENT',CASH_BACK_PERCENT);loguint('GV_PROJECT_FEE_PERCENT',PROJECT_FEE_PERCENT);loguint('GV_PER_BLOCK',PER_BLOCK);loguint('GV_MINIMUM_INVEST',MINIMUM_INVEST);loguint('GV_wave',wave);logaddress('GV_owner',owner);logaddress('GV_admin',admin);emit logaddresses('GV_addresses',addresses);logbool('GV_pause',pause);loguint('LV_amount',amount);logaddress('LV_addr',addr);return  ;}      amount = getInvestorDividendsAmount(addr);
if( lineNumbers[bytes4(keccak256("payout"))] == 9){logstring( 'Line' , "investors[addr].blockNumber = block.number;");loguint('BV_now',now);loguint('GV_CASH_BACK_PERCENT',CASH_BACK_PERCENT);loguint('GV_PROJECT_FEE_PERCENT',PROJECT_FEE_PERCENT);loguint('GV_PER_BLOCK',PER_BLOCK);loguint('GV_MINIMUM_INVEST',MINIMUM_INVEST);loguint('GV_wave',wave);logaddress('GV_owner',owner);logaddress('GV_admin',admin);emit logaddresses('GV_addresses',addresses);logbool('GV_pause',pause);loguint('LV_amount',amount);logaddress('LV_addr',addr);return  ;}      investors[addr].blockNumber = block.number;
if( lineNumbers[bytes4(keccak256("payout"))] == 10){logstring( 'Line' , "pause = true;");loguint('BV_now',now);loguint('GV_CASH_BACK_PERCENT',CASH_BACK_PERCENT);loguint('GV_PROJECT_FEE_PERCENT',PROJECT_FEE_PERCENT);loguint('GV_PER_BLOCK',PER_BLOCK);loguint('GV_MINIMUM_INVEST',MINIMUM_INVEST);loguint('GV_wave',wave);logaddress('GV_owner',owner);logaddress('GV_admin',admin);emit logaddresses('GV_addresses',addresses);logbool('GV_pause',pause);loguint('LV_amount',amount);logaddress('LV_addr',addr);return  ;}      if (address(this).balance < amount) {
        pause = true;
if( lineNumbers[bytes4(keccak256("payout"))] == 12){logstring( 'Line' , "return;");loguint('BV_now',now);loguint('GV_CASH_BACK_PERCENT',CASH_BACK_PERCENT);loguint('GV_PROJECT_FEE_PERCENT',PROJECT_FEE_PERCENT);loguint('GV_PER_BLOCK',PER_BLOCK);loguint('GV_MINIMUM_INVEST',MINIMUM_INVEST);loguint('GV_wave',wave);logaddress('GV_owner',owner);logaddress('GV_admin',admin);emit logaddresses('GV_addresses',addresses);logbool('GV_pause',pause);loguint('LV_amount',amount);logaddress('LV_addr',addr);return  ;}        return;
if( lineNumbers[bytes4(keccak256("payout"))] == 13){logstring( 'Line' , "addr.transfer(amount);");loguint('BV_now',now);loguint('GV_CASH_BACK_PERCENT',CASH_BACK_PERCENT);loguint('GV_PROJECT_FEE_PERCENT',PROJECT_FEE_PERCENT);loguint('GV_PER_BLOCK',PER_BLOCK);loguint('GV_MINIMUM_INVEST',MINIMUM_INVEST);loguint('GV_wave',wave);logaddress('GV_owner',owner);logaddress('GV_admin',admin);emit logaddresses('GV_addresses',addresses);logbool('GV_pause',pause);loguint('LV_amount',amount);logaddress('LV_addr',addr);return  ;}      }
      addr.transfer(amount);
if( lineNumbers[bytes4(keccak256("payout"))] == 15){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_CASH_BACK_PERCENT',CASH_BACK_PERCENT);loguint('GV_PROJECT_FEE_PERCENT',PROJECT_FEE_PERCENT);loguint('GV_PER_BLOCK',PER_BLOCK);loguint('GV_MINIMUM_INVEST',MINIMUM_INVEST);loguint('GV_wave',wave);logaddress('GV_owner',owner);logaddress('GV_admin',admin);emit logaddresses('GV_addresses',addresses);logbool('GV_pause',pause);loguint('LV_amount',amount);logaddress('LV_addr',addr);return  ;}    }
  }
  function transferOwnership(address addr) onlyOwner public {
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 1){logstring( 'Line' , "owner = addr;");loguint('BV_now',now);loguint('GV_CASH_BACK_PERCENT',CASH_BACK_PERCENT);loguint('GV_PROJECT_FEE_PERCENT',PROJECT_FEE_PERCENT);loguint('GV_PER_BLOCK',PER_BLOCK);loguint('GV_MINIMUM_INVEST',MINIMUM_INVEST);loguint('GV_wave',wave);logaddress('GV_owner',owner);logaddress('GV_admin',admin);emit logaddresses('GV_addresses',addresses);logbool('GV_pause',pause);logaddress('IV_addr',addr);return  ;}    owner = addr;
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_CASH_BACK_PERCENT',CASH_BACK_PERCENT);loguint('GV_PROJECT_FEE_PERCENT',PROJECT_FEE_PERCENT);loguint('GV_PER_BLOCK',PER_BLOCK);loguint('GV_MINIMUM_INVEST',MINIMUM_INVEST);loguint('GV_wave',wave);logaddress('GV_owner',owner);logaddress('GV_admin',admin);emit logaddresses('GV_addresses',addresses);logbool('GV_pause',pause);logaddress('IV_addr',addr);return  ;}  }
  function bytesToAddress(bytes bys) private pure returns (address addr) {
    assembly {
      addr := mload(add(bys, 20))
    }
  }
}
