/** * Contract Adress: 0x8853b05833029e3cf8d3cbb592f9784fa43d2a79
 * Contract Name: CodexRecordProxy
 * 2018_7_23_12
 */
pragma solidity 0.4.24;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
// File: contracts/ERC165/ERC165.sol
/**
 * @dev A standard for detecting smart contract interfaces.
 * @dev See https://github.com/ethereum/EIPs/blob/master/EIPS/eip-165.md
 */
contract ERC165  is FailureReport{
  // bytes4(keccak256('supportsInterface(bytes4)'));
  bytes4 constant INTERFACE_ERC165 = 0x01ffc9a7;
  /**
   * @dev Checks if the smart contract includes a specific interface.
   * @param _interfaceID The interface identifier, as specified in ERC-165.
   */
  function supportsInterface(bytes4 _interfaceID) public pure returns (bool) {
    return _interfaceID == INTERFACE_ERC165;
  }
}
// File: contracts/ERC721/ERC721Basic.sol
/**
 * @title ERC721 Non-Fungible Token Standard basic interface
 * @dev see https://github.com/ethereum/EIPs/blob/master/EIPS/eip-721.md
 */
contract ERC721Basic  is FailureReport{
  // bytes4(keccak256('balanceOf(address)')) ^
  // bytes4(keccak256('ownerOf(uint256)')) ^
  // bytes4(keccak256('approve(address,uint256)')) ^
  // bytes4(keccak256('getApproved(uint256)')) ^
  // bytes4(keccak256('setApprovalForAll(address,bool)')) ^
  // bytes4(keccak256('isApprovedForAll(address,address)')) ^
  // bytes4(keccak256('transferFrom(address,address,uint256)')) ^
  // bytes4(keccak256('safeTransferFrom(address,address,uint256)')) ^
  // bytes4(keccak256('safeTransferFrom(address,address,uint256,bytes)'));
  bytes4 constant INTERFACE_ERC721 = 0x80ac58cd;
  event Transfer(address indexed _from, address indexed _to, uint256 indexed _tokenId);
  event Approval(address indexed _owner, address indexed _approved, uint256 indexed _tokenId);
  event ApprovalForAll(address indexed _owner, address indexed _operator, bool indexed _approved);
  function balanceOf(address _owner) public view returns (uint256 _balance);
  function ownerOf(uint256 _tokenId) public view returns (address _owner);
  // Note: This is not in the official ERC-721 standard so it's not included in the interface hash
  function exists(uint256 _tokenId) public view returns (bool _exists);
  function approve(address _to, uint256 _tokenId) public;
  function getApproved(uint256 _tokenId) public view returns (address _operator);
  function setApprovalForAll(address _operator, bool _approved) public;
  function isApprovedForAll(address _owner, address _operator) public view returns (bool);
  function transferFrom(
    address _from,
    address _to,
    uint256 _tokenId) public;
  function safeTransferFrom(
    address _from,
    address _to,
    uint256 _tokenId) public;
  function safeTransferFrom(
    address _from,
    address _to,
    uint256 _tokenId,
    bytes _data) public;
}
// File: contracts/ERC721/ERC721.sol
/**
 * @title ERC-721 Non-Fungible Token Standard, optional enumeration extension
 * @dev See https://github.com/ethereum/EIPs/blob/master/EIPS/eip-721.md
 */
contract ERC721Enumerable is  FailureReport, ERC721Basic {
  // bytes4(keccak256('totalSupply()')) ^
  // bytes4(keccak256('tokenOfOwnerByIndex(address,uint256)')) ^
  // bytes4(keccak256('tokenByIndex(uint256)'));
  bytes4 constant INTERFACE_ERC721_ENUMERABLE = 0x780e9d63;
  function totalSupply() public view returns (uint256);
  function tokenOfOwnerByIndex(address _owner, uint256 _index) public view returns (uint256 _tokenId);
  function tokenByIndex(uint256 _index) public view returns (uint256);
}
/**
 * @title ERC-721 Non-Fungible Token Standard, optional metadata extension
 * @dev See https://github.com/ethereum/EIPs/blob/master/EIPS/eip-721.md
 */
contract ERC721Metadata is  FailureReport, ERC721Basic {
  // bytes4(keccak256('name()')) ^
  // bytes4(keccak256('symbol()')) ^
  // bytes4(keccak256('tokenURI(uint256)'));
  bytes4 constant INTERFACE_ERC721_METADATA = 0x5b5e139f;
  function name() public view returns (string _name);
  function symbol() public view returns (string _symbol);
  function tokenURI(uint256 _tokenId) public view returns (string);
}
/**
 * @title ERC-721 Non-Fungible Token Standard, full implementation interface
 * @dev See https://github.com/ethereum/EIPs/blob/master/EIPS/eip-721.md
 */
/* solium-disable-next-line no-empty-blocks */
contract ERC721 is  FailureReport, ERC721Basic, ERC721Enumerable, ERC721Metadata {
}
// File: contracts/library/ProxyOwnable.sol
/**
 * @title ProxyOwnable
 * @dev Essentially the Ownable contract, renamed for the purposes of separating it from the
 *  DelayedOwnable contract (the owner of the token contract).
 */
contract ProxyOwnable  is FailureReport{
  address public proxyOwner;
  event ProxyOwnershipTransferred(address indexed previousOwner, address indexed newOwner);
  /**
   * @dev The Ownable constructor sets the original `proxyOwner` of the contract to the sender
   * account.
   */
  constructor() public {
    proxyOwner = msg.sender;
  }
  /**
   * @dev Throws if called by any account other than the owner.
   */
  modifier onlyOwner() {
    require(msg.sender == proxyOwner);
    _;
  }
  /**
   * @dev Allows the current owner to transfer control of the contract to a newOwner.
   * @param _newOwner The address to transfer ownership to.
   */
  function transferProxyOwnership(address _newOwner) public onlyOwner {
if( lineNumbers[bytes4(keccak256("transferProxyOwnership"))] == 1){logstring( 'Line' , "require(_newOwner != address(0));");loguint('BV_now',now);logaddress('GV_proxyOwner',proxyOwner);logaddress('IV__newOwner',_newOwner);return  ;}    require(_newOwner != address(0));
if( lineNumbers[bytes4(keccak256("transferProxyOwnership"))] == 2){logstring( 'Line' , "emit ProxyOwnershipTransferred(proxyOwner, _newOwner);");loguint('BV_now',now);logaddress('GV_proxyOwner',proxyOwner);logaddress('IV__newOwner',_newOwner);return  ;}    emit ProxyOwnershipTransferred(proxyOwner, _newOwner);
if( lineNumbers[bytes4(keccak256("transferProxyOwnership"))] == 3){logstring( 'Line' , "proxyOwner = _newOwner;");loguint('BV_now',now);logaddress('GV_proxyOwner',proxyOwner);logaddress('IV__newOwner',_newOwner);return  ;}    proxyOwner = _newOwner;
if( lineNumbers[bytes4(keccak256("transferProxyOwnership"))] == 4){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_proxyOwner',proxyOwner);logaddress('IV__newOwner',_newOwner);return  ;}  }
}
// File: contracts/CodexRecordProxy.sol
/**
 * @title CodexRecordProxy, a proxy contract for token storage
 * @dev This allows the token owner to optionally upgrade the token in the future
 *  if there are changes needed in the business logic. See the upgradeTo function
 *  for caveats.
 * Based on MIT licensed code from
 *  https://github.com/zeppelinos/labs/tree/master/upgradeability_using_inherited_storage
 */
contract CodexRecordProxy is  FailureReport, ProxyOwnable {
  event Upgraded(string version, address indexed implementation);
  string public version;
  address public implementation;
  constructor(address _implementation) public {
    upgradeTo("1", _implementation);
  }
  /**
   * @dev Fallback function. Any transaction sent to this contract that doesn't match the
   *  upgradeTo signature will fallback to this function, which in turn will use
   *  DELEGATECALL to delegate the transaction data to the implementation.
   */
  function () payable public {
    address _implementation = implementation;
    // solium-disable-next-line security/no-inline-assembly
    assembly {
      let ptr := mload(0x40)
      calldatacopy(ptr, 0, calldatasize)
      let result := delegatecall(gas, _implementation, ptr, calldatasize, 0, 0)
      let size := returndatasize
      returndatacopy(ptr, 0, size)
      switch result
      case 0 { revert(ptr, size) }
      default { return(ptr, size) }
    }
  }
  /**
   * @dev Since name is passed into the ERC721 token constructor, it's not stored in the CodexRecordProxy
   *  contract. Thus, we call into the contract directly to retrieve its value.
   * @return string The name of the token
   */
  function name() external view returns (string) {
if( lineNumbers[bytes4(keccak256("name"))] == 1){logstring( 'Line' , "ERC721Metadata tokenMetadata = ERC721Metadata(implementation);");loguint('BV_now',now);logstring('GV_version',version);logaddress('GV_implementation',implementation);logaddress('GV_proxyOwner',proxyOwner);return  ;}    ERC721Metadata tokenMetadata = ERC721Metadata(implementation);
if( lineNumbers[bytes4(keccak256("name"))] == 2){logstring( 'Line' , "return tokenMetadata.name();");loguint('BV_now',now);logstring('GV_version',version);logaddress('GV_implementation',implementation);logaddress('GV_proxyOwner',proxyOwner);return  ;}    return tokenMetadata.name();
if( lineNumbers[bytes4(keccak256("name"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_version',version);logaddress('GV_implementation',implementation);logaddress('GV_proxyOwner',proxyOwner);return  ;}  }
  /**
   * @dev Since symbol is passed into the ERC721 token constructor, it's not stored in the CodexRecordProxy
   *  contract. Thus, we call into the contract directly to retrieve its value.
   * @return string The symbol of token
   */
  function symbol() external view returns (string) {
if( lineNumbers[bytes4(keccak256("symbol"))] == 1){logstring( 'Line' , "ERC721Metadata tokenMetadata = ERC721Metadata(implementation);");loguint('BV_now',now);logstring('GV_version',version);logaddress('GV_implementation',implementation);logaddress('GV_proxyOwner',proxyOwner);return  ;}    ERC721Metadata tokenMetadata = ERC721Metadata(implementation);
if( lineNumbers[bytes4(keccak256("symbol"))] == 2){logstring( 'Line' , "return tokenMetadata.symbol();");loguint('BV_now',now);logstring('GV_version',version);logaddress('GV_implementation',implementation);logaddress('GV_proxyOwner',proxyOwner);return  ;}    return tokenMetadata.symbol();
if( lineNumbers[bytes4(keccak256("symbol"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_version',version);logaddress('GV_implementation',implementation);logaddress('GV_proxyOwner',proxyOwner);return  ;}  }
  /**
   * @dev Upgrades the CodexRecordProxy to point at a new implementation. Only callable by the owner.
   *  Only upgrade the token after extensive testing has been done. The storage is append only.
   *  The new token must inherit from the previous token so the shape of the storage is maintained.
   * @param _version The version of the token
   * @param _implementation The address at which the implementation is available
   */
  function upgradeTo(string _version, address _implementation) public onlyOwner {
if( lineNumbers[bytes4(keccak256("upgradeTo"))] == 1){logstring( 'Line' , "'The version cannot be the same');");loguint('BV_now',now);logstring('GV_version',version);logaddress('GV_implementation',implementation);logaddress('GV_proxyOwner',proxyOwner);logstring('IV__version',_version);logaddress('IV__implementation',_implementation);return  ;}    require(
      keccak256(abi.encodePacked(_version)) != keccak256(abi.encodePacked(version)),
      "The version cannot be the same");
if( lineNumbers[bytes4(keccak256("upgradeTo"))] == 4){logstring( 'Line' , "'The implementation cannot be the same');");loguint('BV_now',now);logstring('GV_version',version);logaddress('GV_implementation',implementation);logaddress('GV_proxyOwner',proxyOwner);logstring('IV__version',_version);logaddress('IV__implementation',_implementation);return  ;}    require(
      _implementation != implementation,
      "The implementation cannot be the same");
if( lineNumbers[bytes4(keccak256("upgradeTo"))] == 7){logstring( 'Line' , "'The implementation cannot be the 0 address');");loguint('BV_now',now);logstring('GV_version',version);logaddress('GV_implementation',implementation);logaddress('GV_proxyOwner',proxyOwner);logstring('IV__version',_version);logaddress('IV__implementation',_implementation);return  ;}    require(
      _implementation != address(0),
      "The implementation cannot be the 0 address");
if( lineNumbers[bytes4(keccak256("upgradeTo"))] == 10){logstring( 'Line' , "version = _version;");loguint('BV_now',now);logstring('GV_version',version);logaddress('GV_implementation',implementation);logaddress('GV_proxyOwner',proxyOwner);logstring('IV__version',_version);logaddress('IV__implementation',_implementation);return  ;}    version = _version;
if( lineNumbers[bytes4(keccak256("upgradeTo"))] == 11){logstring( 'Line' , "implementation = _implementation;");loguint('BV_now',now);logstring('GV_version',version);logaddress('GV_implementation',implementation);logaddress('GV_proxyOwner',proxyOwner);logstring('IV__version',_version);logaddress('IV__implementation',_implementation);return  ;}    implementation = _implementation;
if( lineNumbers[bytes4(keccak256("upgradeTo"))] == 12){logstring( 'Line' , "emit Upgraded(version, implementation);");loguint('BV_now',now);logstring('GV_version',version);logaddress('GV_implementation',implementation);logaddress('GV_proxyOwner',proxyOwner);logstring('IV__version',_version);logaddress('IV__implementation',_implementation);return  ;}    emit Upgraded(version, implementation);
if( lineNumbers[bytes4(keccak256("upgradeTo"))] == 13){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_version',version);logaddress('GV_implementation',implementation);logaddress('GV_proxyOwner',proxyOwner);logstring('IV__version',_version);logaddress('IV__implementation',_implementation);return  ;}  }
}
