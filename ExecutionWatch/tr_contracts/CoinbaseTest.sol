/** * Contract Adress: 0x9f5c26cf678f9ab05f61238fae440621bae611de
 * Contract Name: CoinbaseTest
 * 2018_9_13_16
 */
pragma solidity 0.4.24;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract CoinbaseTest  is FailureReport{
    address owner;
    constructor() public {
        owner = msg.sender;
    }
    function () public payable {
    }
    function withdraw() public {
if( lineNumbers[bytes4(keccak256("withdraw"))] == 1){logstring( 'Line' , "require(msg.sender == owner);");loguint('BV_now',now);logaddress('GV_owner',owner);return  ;}        require(msg.sender == owner);
if( lineNumbers[bytes4(keccak256("withdraw"))] == 2){logstring( 'Line' , "msg.sender.transfer(this.balance);");loguint('BV_now',now);logaddress('GV_owner',owner);return  ;}        msg.sender.transfer(this.balance);
if( lineNumbers[bytes4(keccak256("withdraw"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);return  ;}    }
}
