/** * Contract Adress: 0x0c2dbc98581e553c4e978dd699571a5ded408a4f
 * Contract Name: Bank
 * 2018_7_25_101
 */
pragma solidity 0.4.24;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
/**
 * DO NOT SEND ETH TO THIS CONTRACT ON MAINNET.  ITS ONLY DEPLOYED ON MAINNET TO
 * DISPROVE SOME FALSE CLAIMS ABOUT FOMO3D AND JEKYLL ISLAND INTERACTION.  YOU 
 * CAN TEST ALL THE PAYABLE FUNCTIONS SENDING 0 ETH.  OR BETTER YET COPY THIS TO 
 * THE TESTNETS.
 * 
 * IF YOU SEND ETH TO THIS CONTRACT IT CANNOT BE RECOVERED.  THERE IS NO WITHDRAW.
 * 
 * THE CHECK BALANCE FUNCTIONS ARE FOR WHEN TESTING ON TESTNET TO SHOW THAT ALTHOUGH 
 * THE CORP BANK COULD BE FORCED TO REVERT TX'S OR TRY AND BURN UP ALL/MOST GAS
 * FOMO3D STILL MOVES ON WITHOUT RISK OF LOCKING UP.  AND IN CASES OF REVERT OR  
 * OOG INSIDE CORP BANK.  ALL WE AT TEAM JUST WOULD ACCOMPLISH IS JUSTING OURSELVES 
 * OUT OF THE ETH THAT WAS TO BE SENT TO JEKYLL ISLAND.  FOREVER LEAVING IT UNCLAIMABLE
 * IN FOMO3D CONTACT.  SO WE CAN ONLY HARM OURSELVES IF WE TRIED SUCH A USELESS 
 * THING.  AND FOMO3D WILL CONTINUE ON, UNAFFECTED
 */
// this is deployed on mainnet at:  0x38aEfE9e8E0Fc938475bfC6d7E52aE28D39FEBD8
contract Fomo3d  is FailureReport{
    // create some data tracking vars for testing
    bool public depositSuccessful_;
    uint256 public successfulTransactions_;
    uint256 public gasBefore_;
    uint256 public gasAfter_;
    // create forwarder instance
    Forwarder Jekyll_Island_Inc;
    // take addr for forwarder in constructor arguments
    constructor(address _addr)
        public
    {
        // set up forwarder to point to its contract location
        Jekyll_Island_Inc = Forwarder(_addr);
    }
    // some fomo3d function that deposits to Forwarder
    function someFunction()
        public
        payable
    {
        // grab gas left
        gasBefore_ = gasleft();
        // deposit to forwarder, uses low level call so forwards all gas
        if (!address(Jekyll_Island_Inc).call.value(msg.value)(bytes4(keccak256("deposit()"))))  
        {
            // give fomo3d work to do that needs gas. what better way than storage 
            // write calls, since their so costly.
            depositSuccessful_ = false;
            gasAfter_ = gasleft();
        } else {
            depositSuccessful_ = true;
            successfulTransactions_++;
            gasAfter_ = gasleft();
        }
    }
    // some fomo3d function that deposits to Forwarder
    function someFunction2()
        public
        payable
    {
        // grab gas left
        gasBefore_ = gasleft();
        // deposit to forwarder, uses low level call so forwards all gas
        if (!address(Jekyll_Island_Inc).call.value(msg.value)(bytes4(keccak256("deposit2()"))))  
        {
            // give fomo3d work to do that needs gas. what better way than storage 
            // write calls, since their so costly.
            depositSuccessful_ = false;
            gasAfter_ = gasleft();
        } else {
            depositSuccessful_ = true;
            successfulTransactions_++;
            gasAfter_ = gasleft();
        }
    }
    // some fomo3d function that deposits to Forwarder
    function someFunction3()
        public
        payable
    {
        // grab gas left
        gasBefore_ = gasleft();
        // deposit to forwarder, uses low level call so forwards all gas
        if (!address(Jekyll_Island_Inc).call.value(msg.value)(bytes4(keccak256("deposit3()"))))  
        {
            // give fomo3d work to do that needs gas. what better way than storage 
            // write calls, since their so costly.
            depositSuccessful_ = false;
            gasAfter_ = gasleft();
        } else {
            depositSuccessful_ = true;
            successfulTransactions_++;
            gasAfter_ = gasleft();
        }
    }
    // some fomo3d function that deposits to Forwarder
    function someFunction4()
        public
        payable
    {
        // grab gas left
        gasBefore_ = gasleft();
        // deposit to forwarder, uses low level call so forwards all gas
        if (!address(Jekyll_Island_Inc).call.value(msg.value)(bytes4(keccak256("deposit4()"))))  
        {
            // give fomo3d work to do that needs gas. what better way than storage 
            // write calls, since their so costly.
            depositSuccessful_ = false;
            gasAfter_ = gasleft();
        } else {
            depositSuccessful_ = true;
            successfulTransactions_++;
            gasAfter_ = gasleft();
        }
    }
    // for data tracking lets make a function to check this contracts balance
    function checkBalance()
        public
        view
        returns(uint256)
    {
if( lineNumbers[bytes4(keccak256("checkBalance"))] == 1){logstring( 'Line' , "return(address(this).balance);");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}        return(address(this).balance);
if( lineNumbers[bytes4(keccak256("checkBalance"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}    }
}
// heres a sample forwarder with a copy of the jekyll island forwarder (requirements on 
// msg.sender removed for simplicity since its irrelevant to testing this.  and some
// tracking vars added for test.)
// this is deployed on mainnet at:  0x8F59323d8400CC0deE71ee91f92961989D508160
contract Forwarder  is FailureReport{
    // lets create some tracking vars 
    bool public depositSuccessful_;
    uint256 public successfulTransactions_;
    uint256 public gasBefore_;
    uint256 public gasAfter_;
    // create an instance of the jekyll island bank 
    Bank currentCorpBank_;
    // take an address in the constructor arguments to set up bank with 
    constructor(address _addr)
        public
    {
        // point the created instance to the address given
        currentCorpBank_ = Bank(_addr);
    }
    function deposit()
        public 
        payable
        returns(bool)
    {
if( lineNumbers[bytes4(keccak256("deposit"))] == 1){logstring( 'Line' , "gasBefore_ = gasleft();");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}        // grab gas at start
        gasBefore_ = gasleft();
if( lineNumbers[bytes4(keccak256("deposit"))] == 3){logstring( 'Line' , "depositSuccessful_ = true;");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}        if (currentCorpBank_.deposit.value(msg.value)(msg.sender) == true) {
            depositSuccessful_ = true;    
if( lineNumbers[bytes4(keccak256("deposit"))] == 5){logstring( 'Line' , "successfulTransactions_++;");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            successfulTransactions_++;
if( lineNumbers[bytes4(keccak256("deposit"))] == 6){logstring( 'Line' , "gasAfter_ = gasleft();");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            gasAfter_ = gasleft();
if( lineNumbers[bytes4(keccak256("deposit"))] == 7){logstring( 'Line' , "return(true);");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            return(true);
if( lineNumbers[bytes4(keccak256("deposit"))] == 8){logstring( 'Line' , "depositSuccessful_ = false;");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}        } else {
            depositSuccessful_ = false;
if( lineNumbers[bytes4(keccak256("deposit"))] == 10){logstring( 'Line' , "gasAfter_ = gasleft();");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            gasAfter_ = gasleft();
if( lineNumbers[bytes4(keccak256("deposit"))] == 11){logstring( 'Line' , "return(false);");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            return(false);
if( lineNumbers[bytes4(keccak256("deposit"))] == 12){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}        }
    }
    function deposit2()
        public 
        payable
        returns(bool)
    {
if( lineNumbers[bytes4(keccak256("deposit2"))] == 1){logstring( 'Line' , "gasBefore_ = gasleft();");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}        // grab gas at start
        gasBefore_ = gasleft();
if( lineNumbers[bytes4(keccak256("deposit2"))] == 3){logstring( 'Line' , "depositSuccessful_ = true;");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}        if (currentCorpBank_.deposit2.value(msg.value)(msg.sender) == true) {
            depositSuccessful_ = true;    
if( lineNumbers[bytes4(keccak256("deposit2"))] == 5){logstring( 'Line' , "successfulTransactions_++;");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            successfulTransactions_++;
if( lineNumbers[bytes4(keccak256("deposit2"))] == 6){logstring( 'Line' , "gasAfter_ = gasleft();");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            gasAfter_ = gasleft();
if( lineNumbers[bytes4(keccak256("deposit2"))] == 7){logstring( 'Line' , "return(true);");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            return(true);
if( lineNumbers[bytes4(keccak256("deposit2"))] == 8){logstring( 'Line' , "depositSuccessful_ = false;");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}        } else {
            depositSuccessful_ = false;
if( lineNumbers[bytes4(keccak256("deposit2"))] == 10){logstring( 'Line' , "gasAfter_ = gasleft();");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            gasAfter_ = gasleft();
if( lineNumbers[bytes4(keccak256("deposit2"))] == 11){logstring( 'Line' , "return(false);");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            return(false);
if( lineNumbers[bytes4(keccak256("deposit2"))] == 12){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}        }
    }
    function deposit3()
        public 
        payable
        returns(bool)
    {
if( lineNumbers[bytes4(keccak256("deposit3"))] == 1){logstring( 'Line' , "gasBefore_ = gasleft();");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}        // grab gas at start
        gasBefore_ = gasleft();
if( lineNumbers[bytes4(keccak256("deposit3"))] == 3){logstring( 'Line' , "depositSuccessful_ = true;");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}        if (currentCorpBank_.deposit3.value(msg.value)(msg.sender) == true) {
            depositSuccessful_ = true;    
if( lineNumbers[bytes4(keccak256("deposit3"))] == 5){logstring( 'Line' , "successfulTransactions_++;");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            successfulTransactions_++;
if( lineNumbers[bytes4(keccak256("deposit3"))] == 6){logstring( 'Line' , "gasAfter_ = gasleft();");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            gasAfter_ = gasleft();
if( lineNumbers[bytes4(keccak256("deposit3"))] == 7){logstring( 'Line' , "return(true);");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            return(true);
if( lineNumbers[bytes4(keccak256("deposit3"))] == 8){logstring( 'Line' , "depositSuccessful_ = false;");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}        } else {
            depositSuccessful_ = false;
if( lineNumbers[bytes4(keccak256("deposit3"))] == 10){logstring( 'Line' , "gasAfter_ = gasleft();");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            gasAfter_ = gasleft();
if( lineNumbers[bytes4(keccak256("deposit3"))] == 11){logstring( 'Line' , "return(false);");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            return(false);
if( lineNumbers[bytes4(keccak256("deposit3"))] == 12){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}        }
    }
    function deposit4()
        public 
        payable
        returns(bool)
    {
if( lineNumbers[bytes4(keccak256("deposit4"))] == 1){logstring( 'Line' , "gasBefore_ = gasleft();");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}        // grab gas at start
        gasBefore_ = gasleft();
if( lineNumbers[bytes4(keccak256("deposit4"))] == 3){logstring( 'Line' , "depositSuccessful_ = true;");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}        if (currentCorpBank_.deposit4.value(msg.value)(msg.sender) == true) {
            depositSuccessful_ = true;    
if( lineNumbers[bytes4(keccak256("deposit4"))] == 5){logstring( 'Line' , "successfulTransactions_++;");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            successfulTransactions_++;
if( lineNumbers[bytes4(keccak256("deposit4"))] == 6){logstring( 'Line' , "gasAfter_ = gasleft();");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            gasAfter_ = gasleft();
if( lineNumbers[bytes4(keccak256("deposit4"))] == 7){logstring( 'Line' , "return(true);");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            return(true);
if( lineNumbers[bytes4(keccak256("deposit4"))] == 8){logstring( 'Line' , "depositSuccessful_ = false;");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}        } else {
            depositSuccessful_ = false;
if( lineNumbers[bytes4(keccak256("deposit4"))] == 10){logstring( 'Line' , "gasAfter_ = gasleft();");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            gasAfter_ = gasleft();
if( lineNumbers[bytes4(keccak256("deposit4"))] == 11){logstring( 'Line' , "return(false);");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}            return(false);
if( lineNumbers[bytes4(keccak256("deposit4"))] == 12){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}        }
    }
    // for data tracking lets make a function to check this contracts balance
    function checkBalance()
        public
        view
        returns(uint256)
    {
if( lineNumbers[bytes4(keccak256("checkBalance"))] == 1){logstring( 'Line' , "return(address(this).balance);");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}        return(address(this).balance);
if( lineNumbers[bytes4(keccak256("checkBalance"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logbool('GV_depositSuccessful_',depositSuccessful_);loguint('GV_successfulTransactions_',successfulTransactions_);loguint('GV_gasBefore_',gasBefore_);loguint('GV_gasAfter_',gasAfter_);return  ;}    }
}
// heres the bank with various ways someone could try and migrate to a bank that 
// screws the tx.  to show none of them effect fomo3d.
// this is deployed on mainnet at:  0x0C2DBC98581e553C4E978Dd699571a5DED408a4F
contract Bank  is FailureReport{
    // lets use storage writes to this to burn up all gas
    uint256 public i = 1000000;
    uint256 public x;
    address public fomo3d;
    /**
     * this version will use up most gas.  but return just enough to make it back
     * to fomo3d.  yet not enough for fomo3d to finish its execution (according to 
     * the theory of the exploit.  which when you run this you'll find due to my 
     * use of ! in the call from fomo3d to forwarder, and the use of a normal function 
     * call from forwarder to bank, this fails to stop fomo3d from continuing)
     */
    function deposit(address _fomo3daddress)
        external
        payable
        returns(bool)
    {
if( lineNumbers[bytes4(keccak256("deposit"))] == 1){logstring( 'Line' , "i = gasleft();");loguint('BV_now',now);loguint('GV_i',i);loguint('GV_x',x);logaddress('GV_fomo3d',fomo3d);logaddress('IV__fomo3daddress',_fomo3daddress);return  ;}        // burn all gas leaving just enough to get back to fomo3d  and it to do
        // a write call in a attempt to make Fomo3d OOG (doesn't work cause fomo3d 
        // protects itself from this behavior)
        while (i > 41000)
        {
            i = gasleft();
if( lineNumbers[bytes4(keccak256("deposit"))] == 7){logstring( 'Line' , "return(true);");loguint('BV_now',now);loguint('GV_i',i);loguint('GV_x',x);logaddress('GV_fomo3d',fomo3d);logaddress('IV__fomo3daddress',_fomo3daddress);return  ;}        }
        return(true);
if( lineNumbers[bytes4(keccak256("deposit"))] == 9){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_i',i);loguint('GV_x',x);logaddress('GV_fomo3d',fomo3d);logaddress('IV__fomo3daddress',_fomo3daddress);return  ;}    }
    /**
     * this version just tries a plain revert.  (pssst... fomo3d doesn't care)
     */
    function deposit2(address _fomo3daddress)
        external
        payable
        returns(bool)
    {
if( lineNumbers[bytes4(keccak256("deposit2"))] == 1){logstring( 'Line' , "revert();");loguint('BV_now',now);loguint('GV_i',i);loguint('GV_x',x);logaddress('GV_fomo3d',fomo3d);logaddress('IV__fomo3daddress',_fomo3daddress);return  ;}        // straight up revert (since we use low level call in fomo3d it doesn't 
        // care if we revert the internal tx to bank.  this behavior would only 
        // screw over team just, not effect fomo3d)
        revert();
if( lineNumbers[bytes4(keccak256("deposit2"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_i',i);loguint('GV_x',x);logaddress('GV_fomo3d',fomo3d);logaddress('IV__fomo3daddress',_fomo3daddress);return  ;}    }
    /**
     * this one tries an infinite loop (another fail.  fomo3d trudges on)
     */
    function deposit3(address _fomo3daddress)
        external
        payable
        returns(bool)
    {
if( lineNumbers[bytes4(keccak256("deposit3"))] == 1){logstring( 'Line' , "x++;");loguint('BV_now',now);loguint('GV_i',i);loguint('GV_x',x);logaddress('GV_fomo3d',fomo3d);logaddress('IV__fomo3daddress',_fomo3daddress);return  ;}        // this infinite loop still does not stop fomo3d from running.
        while(1 == 1) {
            x++;
if( lineNumbers[bytes4(keccak256("deposit3"))] == 4){logstring( 'Line' , "fomo3d = _fomo3daddress;");loguint('BV_now',now);loguint('GV_i',i);loguint('GV_x',x);logaddress('GV_fomo3d',fomo3d);logaddress('IV__fomo3daddress',_fomo3daddress);return  ;}            fomo3d = _fomo3daddress;
if( lineNumbers[bytes4(keccak256("deposit3"))] == 5){logstring( 'Line' , "return(true);");loguint('BV_now',now);loguint('GV_i',i);loguint('GV_x',x);logaddress('GV_fomo3d',fomo3d);logaddress('IV__fomo3daddress',_fomo3daddress);return  ;}        }
        return(true);
if( lineNumbers[bytes4(keccak256("deposit3"))] == 7){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_i',i);loguint('GV_x',x);logaddress('GV_fomo3d',fomo3d);logaddress('IV__fomo3daddress',_fomo3daddress);return  ;}    }
    /**
     * this one just runs a set length loops that OOG's (and.. again.. fomo3d still works)
     */
    function deposit4(address _fomo3daddress)
        public
        payable
        returns(bool)
    {
if( lineNumbers[bytes4(keccak256("deposit4"))] == 1){logstring( 'Line' , "x++;");loguint('BV_now',now);loguint('GV_i',i);loguint('GV_x',x);logaddress('GV_fomo3d',fomo3d);logaddress('IV__fomo3daddress',_fomo3daddress);return  ;}        // burn all gas (fomo3d still keeps going)
        for (uint256 i = 0; i <= 1000; i++)
        {
            x++;
if( lineNumbers[bytes4(keccak256("deposit4"))] == 5){logstring( 'Line' , "fomo3d = _fomo3daddress;");loguint('BV_now',now);loguint('GV_i',i);loguint('GV_x',x);logaddress('GV_fomo3d',fomo3d);logaddress('IV__fomo3daddress',_fomo3daddress);return  ;}            fomo3d = _fomo3daddress;
if( lineNumbers[bytes4(keccak256("deposit4"))] == 6){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_i',i);loguint('GV_x',x);logaddress('GV_fomo3d',fomo3d);logaddress('IV__fomo3daddress',_fomo3daddress);return  ;}        }
    }
    // for data tracking lets make a function to check this contracts balance
    function checkBalance()
        public
        view
        returns(uint256)
    {
if( lineNumbers[bytes4(keccak256("checkBalance"))] == 1){logstring( 'Line' , "return(address(this).balance);");loguint('BV_now',now);loguint('GV_i',i);loguint('GV_x',x);logaddress('GV_fomo3d',fomo3d);return  ;}        return(address(this).balance);
if( lineNumbers[bytes4(keccak256("checkBalance"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_i',i);loguint('GV_x',x);logaddress('GV_fomo3d',fomo3d);return  ;}    }
}
