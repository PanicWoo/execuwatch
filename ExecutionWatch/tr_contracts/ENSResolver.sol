/** * Contract Adress: 0x8c7e4de7bb0293da4fb43747832d4b865b48348a
 * Contract Name: ENSResolver
 * 2018_7_5_7
 */
pragma solidity ^0.4.21;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
/**
 * @title Ownable
 * @dev The Ownable contract has an owner address, and provides basic authorization control
 * functions, this simplifies the implementation of "user permissions".
 */
contract Ownable  is FailureReport{
  address public owner;
  event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);
  /**
   * @dev The Ownable constructor sets the original `owner` of the contract to the sender
   * account.
   */
  function Ownable() public {
    owner = msg.sender;
  }
  /**
   * @dev Throws if called by any account other than the owner.
   */
  modifier onlyOwner() {
    require(msg.sender == owner);
    _;
  }
  /**
   * @dev Allows the current owner to transfer control of the contract to a newOwner.
   * @param newOwner The address to transfer ownership to.
   */
  function transferOwnership(address newOwner) public onlyOwner {
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 1){logstring( 'Line' , "require(newOwner != address(0));");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}    require(newOwner != address(0));
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 2){logstring( 'Line' , "emit OwnershipTransferred(owner, newOwner);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}    emit OwnershipTransferred(owner, newOwner);
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 3){logstring( 'Line' , "owner = newOwner;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}    owner = newOwner;
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 4){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}  }
}
contract ENS  is FailureReport{
    function owner(bytes32 node) constant returns (address);
    function resolver(bytes32 node) constant returns (Resolver);
    function ttl(bytes32 node) constant returns (uint64);
    function setOwner(bytes32 node, address owner);
    function setSubnodeOwner(bytes32 node, bytes32 label, address owner);
    function setResolver(bytes32 node, address resolver);
    function setTTL(bytes32 node, uint64 ttl);
}
contract Resolver  is FailureReport{
    function addr(bytes32 node) constant returns (address);
}
contract ENSResolver is  FailureReport, Ownable {
    ENS public ens;
    function ENSResolver(address ensAddress) public {
        require(ensAddress != address(0));
        ens = ENS(ensAddress);
    }
    function setENS(address ensAddress) public onlyOwner {
if( lineNumbers[bytes4(keccak256("setENS"))] == 1){logstring( 'Line' , "require(ensAddress != address(0));");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_ensAddress',ensAddress);return  ;}        require(ensAddress != address(0));
if( lineNumbers[bytes4(keccak256("setENS"))] == 2){logstring( 'Line' , "ens = ENS(ensAddress);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_ensAddress',ensAddress);return  ;}        ens = ENS(ensAddress);
if( lineNumbers[bytes4(keccak256("setENS"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_ensAddress',ensAddress);return  ;}    }
    function resolve(bytes32 node) public view returns (address) {
if( lineNumbers[bytes4(keccak256("resolve"))] == 1){logstring( 'Line' , "Resolver resolver = Resolver(ens.resolver(node));");loguint('BV_now',now);logaddress('GV_owner',owner);return  ;}        Resolver resolver = Resolver(ens.resolver(node));
if( lineNumbers[bytes4(keccak256("resolve"))] == 2){logstring( 'Line' , "return resolver.addr(node);");loguint('BV_now',now);logaddress('GV_owner',owner);return  ;}        return resolver.addr(node);
if( lineNumbers[bytes4(keccak256("resolve"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);return  ;}    }
}
