/** * Contract Adress: 0x1e182d0d805c0314c01182d08abb0aeff7d7d658
 * Contract Name: DepositWalletImpl
 * 2018_12_13_26
 */
pragma solidity ^0.4.24;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract ERC20  is FailureReport{
    function totalSupply() public view returns (uint);
    function balanceOf(address tokenOwner) public view returns (uint balance);
    function allowance(address tokenOwner, address spender) public view returns (uint remaining);
    function transfer(address to, uint tokens) public returns (bool success);
    function approve(address spender, uint tokens) public returns (bool success);
    function transferFrom(address from, address to, uint tokens) public returns (bool success);
    event Transfer(address indexed from, address indexed to, uint tokens);
    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);
}
contract DepositRegistry  is FailureReport{
    function getTokenContract() public view returns(address);
    function getDepositAdmin() public view returns(address);
    function getColdWallet() public view returns(address);
    function changeColdWallet(address _newWallet) public;
    event ColdWalletChanged(address previousWallet, address newWallet);
    event TokenChanged(address previousToken, address newToken);
}
contract DepositWallet  is FailureReport{
    function sweepTokens() public;
    event DepositProcessed(address indexed coldWallet, uint amount);
}
contract DepositWalletImpl is  FailureReport, DepositWallet {
    DepositRegistry public depositRegistry;
    constructor(DepositRegistry _depositRegistry) public {
        depositRegistry = _depositRegistry;
    }
    function sweepTokens() public {
if( lineNumbers[bytes4(keccak256("sweepTokens"))] == 1){logstring( 'Line' , "address tokenContractAddress = depositRegistry.getTokenContract();");loguint('BV_now',now);return  ;}        address tokenContractAddress = depositRegistry.getTokenContract();
if( lineNumbers[bytes4(keccak256("sweepTokens"))] == 2){logstring( 'Line' , "ERC20 tokenContract = ERC20(tokenContractAddress);");loguint('BV_now',now);logaddress('LV_tokenContractAddress',tokenContractAddress);return  ;}        ERC20 tokenContract = ERC20(tokenContractAddress);
if( lineNumbers[bytes4(keccak256("sweepTokens"))] == 3){logstring( 'Line' , "tokenContract.balanceOf(address(this));");loguint('BV_now',now);logaddress('LV_tokenContractAddress',tokenContractAddress);return  ;}        tokenContract.balanceOf(address(this));
if( lineNumbers[bytes4(keccak256("sweepTokens"))] == 4){logstring( 'Line' , "uint currentBalance = tokenContract.balanceOf(address(this));");loguint('BV_now',now);logaddress('LV_tokenContractAddress',tokenContractAddress);return  ;}        uint currentBalance = tokenContract.balanceOf(address(this));
if( lineNumbers[bytes4(keccak256("sweepTokens"))] == 5){logstring( 'Line' , "address coldWallet = depositRegistry.getColdWallet();");loguint('BV_now',now);logaddress('LV_tokenContractAddress',tokenContractAddress);loguint('LV_currentBalance',currentBalance);return  ;}        if (currentBalance > 0) {
            address coldWallet = depositRegistry.getColdWallet();
if( lineNumbers[bytes4(keccak256("sweepTokens"))] == 7){logstring( 'Line' , "require(tokenContract.transfer(coldWallet, currentBalance), 'Failed to transfer tokens');");loguint('BV_now',now);logaddress('LV_tokenContractAddress',tokenContractAddress);loguint('LV_currentBalance',currentBalance);logaddress('LV_coldWallet',coldWallet);return  ;}            require(tokenContract.transfer(coldWallet, currentBalance), "Failed to transfer tokens");
if( lineNumbers[bytes4(keccak256("sweepTokens"))] == 8){logstring( 'Line' , "emit DepositProcessed(coldWallet, currentBalance);");loguint('BV_now',now);logaddress('LV_tokenContractAddress',tokenContractAddress);loguint('LV_currentBalance',currentBalance);logaddress('LV_coldWallet',coldWallet);return  ;}            emit DepositProcessed(coldWallet, currentBalance);
if( lineNumbers[bytes4(keccak256("sweepTokens"))] == 9){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('LV_tokenContractAddress',tokenContractAddress);loguint('LV_currentBalance',currentBalance);logaddress('LV_coldWallet',coldWallet);return  ;}        }
    }
}
