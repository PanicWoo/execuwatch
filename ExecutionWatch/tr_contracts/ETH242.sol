/** * Contract Adress: 0x6c29b601a00798d9f13d9959f6d156be5032168e
 * Contract Name: ETH242
 * 2018_10_24_99
 */
pragma solidity ^0.4.24;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
/**
 * Website: www.eth242.com
 *
 *
 * RECOMMENDED GAS LIMIT: 200000
 * RECOMMENDED GAS PRICE: https://ethgasstation.info/
 */
library SafeMath {
    function mul(uint256 _a, uint256 _b) internal pure returns (uint256) {
        if (_a == 0) {
            return 0;
        }
        uint256 c = _a * _b;
        require(c / _a == _b);
        return c;
    }
    function div(uint256 _a, uint256 _b) internal pure returns (uint256) {
        require(_b > 0);
        uint256 c = _a / _b;
        return c;
    }
    function sub(uint256 _a, uint256 _b) internal pure returns (uint256) {
        require(_b <= _a);
        uint256 c = _a - _b;
        return c;
    }
    function add(uint256 _a, uint256 _b) internal pure returns (uint256) {
        uint256 c = _a + _b;
        require(c >= _a);
        return c;
    }
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b != 0);
        return a % b;
    }
}
contract InvestorsStorage  is FailureReport{
    address private owner;
    mapping (address => Investor) private investors;
    struct Investor {
        uint deposit;
        uint checkpoint;
    }
    constructor() public {
        owner = msg.sender;
    }
    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }
    function updateInfo(address _address, uint _value) external onlyOwner {
        investors[_address].deposit += _value;
        investors[_address].checkpoint = block.timestamp;
    }
    function updateCheckpoint(address _address) external onlyOwner {
        investors[_address].checkpoint = block.timestamp;
    }
    function d(address _address) external view onlyOwner returns(uint) {
        return investors[_address].deposit;
    }
    function c(address _address) external view onlyOwner returns(uint) {
        return investors[_address].checkpoint;
    }
    function getInterest(address _address) external view onlyOwner returns(uint) {
        if (investors[_address].deposit < 2420000000000000000) {
            return 242;
        } else {
            return 300;
        }
    }
}
contract ETH242  is FailureReport{
    using SafeMath for uint;
    address public owner;
    address admin;
    address marketing;
    uint waveStartUp;
    uint nextPayDay;
    event LogInvestment(address indexed _addr, uint _value);
    event LogPayment(address indexed _addr, uint _value);
    event LogReferralInvestment(address indexed _referral, address indexed _referrer, uint _value);
    event LogNewWave(uint _waveStartUp);
    InvestorsStorage private x;
    modifier notOnPause() {
        require(waveStartUp <= block.timestamp);
        _;
    }
    function renounceOwnership() external {
if( lineNumbers[bytes4(keccak256("renounceOwnership"))] == 1){logstring( 'Line' , "require(msg.sender == owner);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);return  ;}        require(msg.sender == owner);
if( lineNumbers[bytes4(keccak256("renounceOwnership"))] == 2){logstring( 'Line' , "owner = 0x0;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);return  ;}        owner = 0x0;
if( lineNumbers[bytes4(keccak256("renounceOwnership"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);return  ;}    }
    function bytesToAddress(bytes _source) internal pure returns(address parsedReferrer) {
        assembly {
            parsedReferrer := mload(add(_source,0x14))
        }
        return parsedReferrer;
    }
    function toReferrer(uint _value) internal {
        address _referrer = bytesToAddress(bytes(msg.data));
        if (_referrer != msg.sender) {
            _referrer.transfer(_value / 20);
            emit LogReferralInvestment(msg.sender, _referrer, _value);
        }
    }
    constructor() public {
        owner = msg.sender;
        admin = address(0xd38a013D7730564584C9aDBEeA83C1007E12E038);
        marketing = address(0x81eCf0979668D3C6a812B404215B53310f14f451);
        x = new InvestorsStorage();
    }
    function getInfo(address _address) external view returns(uint deposit, uint amountToWithdraw) {
if( lineNumbers[bytes4(keccak256("getInfo"))] == 1){logstring( 'Line' , "deposit = x.d(_address);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);logaddress('IV__address',_address);return  ;}        deposit = x.d(_address);
if( lineNumbers[bytes4(keccak256("getInfo"))] == 2){logstring( 'Line' , "amountToWithdraw = block.timestamp.sub(x.c(_address)).div(1 days).mul(x.d(_address).mul(x.getInterest(_address)).div(10000));");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);logaddress('IV__address',_address);return  ;}        amountToWithdraw = block.timestamp.sub(x.c(_address)).div(1 days).mul(x.d(_address).mul(x.getInterest(_address)).div(10000));
if( lineNumbers[bytes4(keccak256("getInfo"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);logaddress('IV__address',_address);return  ;}    }
    function() external payable {
        if (msg.value == 0) {
            withdraw();
        } else {
            invest();
        }
    }
    function invest() notOnPause public payable {
if( lineNumbers[bytes4(keccak256("invest"))] == 1){logstring( 'Line' , "admin.transfer(msg.value * 8 / 100);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);return  ;}        admin.transfer(msg.value * 8 / 100);
if( lineNumbers[bytes4(keccak256("invest"))] == 2){logstring( 'Line' , "marketing.transfer(msg.value * 5 / 100);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);return  ;}        marketing.transfer(msg.value * 5 / 100);
if( lineNumbers[bytes4(keccak256("invest"))] == 3){logstring( 'Line' , "withdraw();");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);return  ;}        if (x.d(msg.sender) > 0) {
            withdraw();
if( lineNumbers[bytes4(keccak256("invest"))] == 5){logstring( 'Line' , "x.updateInfo(msg.sender, msg.value);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);return  ;}        }
        x.updateInfo(msg.sender, msg.value);
if( lineNumbers[bytes4(keccak256("invest"))] == 7){logstring( 'Line' , "toReferrer(msg.value);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);return  ;}        if (msg.data.length == 20) {
            toReferrer(msg.value);
if( lineNumbers[bytes4(keccak256("invest"))] == 9){logstring( 'Line' , "emit LogInvestment(msg.sender, msg.value);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);return  ;}        }
        emit LogInvestment(msg.sender, msg.value);
if( lineNumbers[bytes4(keccak256("invest"))] == 11){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);return  ;}    }
    function withdraw() notOnPause public {
if( lineNumbers[bytes4(keccak256("withdraw"))] == 1){logstring( 'Line' , "nextWave();");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);return  ;}        if (address(this).balance < 100000000000000000) {
            nextWave();
if( lineNumbers[bytes4(keccak256("withdraw"))] == 3){logstring( 'Line' , "return;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);return  ;}            return;
if( lineNumbers[bytes4(keccak256("withdraw"))] == 4){logstring( 'Line' , "uint _payout = block.timestamp.sub(x.c(msg.sender)).div(1 days).mul(x.d(msg.sender).mul(x.getInterest(msg.sender)).div(10000));");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);return  ;}        }
        uint _payout = block.timestamp.sub(x.c(msg.sender)).div(1 days).mul(x.d(msg.sender).mul(x.getInterest(msg.sender)).div(10000));
if( lineNumbers[bytes4(keccak256("withdraw"))] == 6){logstring( 'Line' , "x.updateCheckpoint(msg.sender);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);loguint('LV__payout',_payout);return  ;}        x.updateCheckpoint(msg.sender);
if( lineNumbers[bytes4(keccak256("withdraw"))] == 7){logstring( 'Line' , "msg.sender.transfer(_payout);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);loguint('LV__payout',_payout);return  ;}        if (_payout > 0) {
            msg.sender.transfer(_payout);
if( lineNumbers[bytes4(keccak256("withdraw"))] == 9){logstring( 'Line' , "emit LogPayment(msg.sender, _payout);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);loguint('LV__payout',_payout);return  ;}            emit LogPayment(msg.sender, _payout);
if( lineNumbers[bytes4(keccak256("withdraw"))] == 10){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_admin',admin);logaddress('GV_marketing',marketing);loguint('GV_waveStartUp',waveStartUp);loguint('GV_nextPayDay',nextPayDay);loguint('LV__payout',_payout);return  ;}        }
    }
    function nextWave() private {
        x = new InvestorsStorage();
        waveStartUp = block.timestamp + 7 days;
        emit LogNewWave(waveStartUp);
    }
}
