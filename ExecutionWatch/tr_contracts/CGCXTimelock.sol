/** * Contract Adress: 0x91163883ec62811551e864d07e2881e8cdb69ce2
 * Contract Name: CGCXTimelock
 * 2018_9_13_51
 */
pragma solidity 0.4.24;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract ERC20  is FailureReport{
  function totalSupply() public view returns (uint256);
  function balanceOf(address who) public view returns (uint256);
  function transfer(address to, uint256 value) public returns (bool);
  function allowance(address owner, address spender)
    public view returns (uint256);
  function transferFrom(address from, address to, uint256 value)
    public returns (bool);
  function approve(address spender, uint256 value) public returns (bool);
  event Transfer(address indexed from, address indexed to, uint256 value);
  event Approval(
    address indexed owner,
    address indexed spender,
    uint256 value
  );
}
library SafeERC20 {
  function safeTransfer(ERC20 token, address to, uint256 value) internal {
    require(token.transfer(to, value));
  }
  function safeTransferFrom(
    ERC20 token,
    address from,
    address to,
    uint256 value
  )
    internal
  {
    require(token.transferFrom(from, to, value));
  }
  function safeApprove(ERC20 token, address spender, uint256 value) internal {
    require(token.approve(spender, value));
  }
}
/**
 * @title TokenTimelock
 * @dev TokenTimelock is a token holder contract that will allow a
 * beneficiary to extract the tokens after a given release time
 */
contract CGCXTimelock  is FailureReport{
  using SafeERC20 for ERC20;
  // ERC20 basic token contract being held
  ERC20 public token;
  // beneficiary of tokens after they are released
  address public beneficiary;
  // timestamp when token release is enabled
  uint256 public firstReleaseTime;
  uint256 public secondReleaseTime;
  uint256 public thirdReleaseTime;
  uint256 public fourthReleaseTime;
  constructor(
    address _token,
    address _beneficiary,
    uint256 _firstLockupInDays,
    uint256 _secondLockupInDays,
    uint256 _thirdLockupInDays,
    uint256 _fourthLockupInDays
  )
    public
  {
    require(_beneficiary != address(0));
    // solium-disable-next-line security/no-block-members
    require(_firstLockupInDays > 0);
    require(_secondLockupInDays > 0);
    require(_thirdLockupInDays > 0);
    require(_fourthLockupInDays > 0);
    token = ERC20(_token);
    beneficiary = _beneficiary;
    firstReleaseTime = now + _firstLockupInDays * 1 days;
    secondReleaseTime = now + _secondLockupInDays * 1 days;
    thirdReleaseTime = now + _thirdLockupInDays * 1 days;
    fourthReleaseTime = now + _fourthLockupInDays * 1 days;
  }
  /**
   * @notice Transfers tokens held by timelock to beneficiary.
   */
  function release() public {
if( lineNumbers[bytes4(keccak256("release"))] == 1){logstring( 'Line' , "uint256 amount;");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);return  ;}    uint256 amount;
if( lineNumbers[bytes4(keccak256("release"))] == 2){logstring( 'Line' , "amount = token.balanceOf(this);");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);loguint('LV_amount',amount);return  ;}    // solium-disable-next-line security/no-block-members
    if (fourthReleaseTime != 0 && block.timestamp >= fourthReleaseTime) {
      amount = token.balanceOf(this);
if( lineNumbers[bytes4(keccak256("release"))] == 5){logstring( 'Line' , "require(amount > 0);");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);loguint('LV_amount',amount);return  ;}      require(amount > 0);
if( lineNumbers[bytes4(keccak256("release"))] == 6){logstring( 'Line' , "token.safeTransfer(beneficiary, amount);");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);loguint('LV_amount',amount);return  ;}      token.safeTransfer(beneficiary, amount);
if( lineNumbers[bytes4(keccak256("release"))] == 7){logstring( 'Line' , "fourthReleaseTime = 0;");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);loguint('LV_amount',amount);return  ;}      fourthReleaseTime = 0;
if( lineNumbers[bytes4(keccak256("release"))] == 8){logstring( 'Line' , "amount = token.balanceOf(this);");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);loguint('LV_amount',amount);return  ;}    } else if (thirdReleaseTime != 0 && block.timestamp >= thirdReleaseTime) {
      amount = token.balanceOf(this);
if( lineNumbers[bytes4(keccak256("release"))] == 10){logstring( 'Line' , "require(amount > 0);");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);loguint('LV_amount',amount);return  ;}      require(amount > 0);
if( lineNumbers[bytes4(keccak256("release"))] == 11){logstring( 'Line' , "token.safeTransfer(beneficiary, amount / 2);");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);loguint('LV_amount',amount);return  ;}      token.safeTransfer(beneficiary, amount / 2);
if( lineNumbers[bytes4(keccak256("release"))] == 12){logstring( 'Line' , "thirdReleaseTime = 0;");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);loguint('LV_amount',amount);return  ;}      thirdReleaseTime = 0;
if( lineNumbers[bytes4(keccak256("release"))] == 13){logstring( 'Line' , "amount = token.balanceOf(this);");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);loguint('LV_amount',amount);return  ;}    } else if (secondReleaseTime != 0 && block.timestamp >= secondReleaseTime) {
      amount = token.balanceOf(this);
if( lineNumbers[bytes4(keccak256("release"))] == 15){logstring( 'Line' , "require(amount > 0);");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);loguint('LV_amount',amount);return  ;}      require(amount > 0);
if( lineNumbers[bytes4(keccak256("release"))] == 16){logstring( 'Line' , "token.safeTransfer(beneficiary, amount / 3);");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);loguint('LV_amount',amount);return  ;}      token.safeTransfer(beneficiary, amount / 3);
if( lineNumbers[bytes4(keccak256("release"))] == 17){logstring( 'Line' , "secondReleaseTime = 0;");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);loguint('LV_amount',amount);return  ;}      secondReleaseTime = 0;
if( lineNumbers[bytes4(keccak256("release"))] == 18){logstring( 'Line' , "amount = token.balanceOf(this);");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);loguint('LV_amount',amount);return  ;}    } else if (firstReleaseTime != 0 && block.timestamp >= firstReleaseTime) {
      amount = token.balanceOf(this);
if( lineNumbers[bytes4(keccak256("release"))] == 20){logstring( 'Line' , "require(amount > 0);");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);loguint('LV_amount',amount);return  ;}      require(amount > 0);
if( lineNumbers[bytes4(keccak256("release"))] == 21){logstring( 'Line' , "token.safeTransfer(beneficiary, amount / 4);");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);loguint('LV_amount',amount);return  ;}      token.safeTransfer(beneficiary, amount / 4);
if( lineNumbers[bytes4(keccak256("release"))] == 22){logstring( 'Line' , "firstReleaseTime = 0;");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);loguint('LV_amount',amount);return  ;}      firstReleaseTime = 0;
if( lineNumbers[bytes4(keccak256("release"))] == 23){logstring( 'Line' , "revert();");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);loguint('LV_amount',amount);return  ;}    } else {
      revert();
if( lineNumbers[bytes4(keccak256("release"))] == 25){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_beneficiary',beneficiary);loguint('GV_firstReleaseTime',firstReleaseTime);loguint('GV_secondReleaseTime',secondReleaseTime);loguint('GV_thirdReleaseTime',thirdReleaseTime);loguint('GV_fourthReleaseTime',fourthReleaseTime);loguint('LV_amount',amount);return  ;}    }
  }
}
