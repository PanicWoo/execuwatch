/** * Contract Adress: 0xb850067b93312028f821b0c772dd2199383d8d8f
 * Contract Name: ETHERFLEXCoin
 * 2018_6_27_24
 */
pragma solidity ^0.4.24;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {
  function mul(uint256 a, uint256 b) internal pure returns (uint256) {
    if (a == 0) {
      return 0;
    }
    uint256 c = a * b;
    assert(c / a == b);
    return c;
  }
  function div(uint256 a, uint256 b) internal pure returns (uint256) {
    // assert(b > 0); // Solidity automatically throws when dividing by 0
    uint256 c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn't hold
    return c;
  }
  function sub(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b <= a);
    return a - b;
  }
  function add(uint256 a, uint256 b) internal pure returns (uint256) {
    uint256 c = a + b;
    assert(c >= a);
    return c;
  }
}
/**
 * @title Ownable
 * @dev The Ownable contract has an owner address, and provides basic authorization control
 * functions, this simplifies the implementation of "user permissions".
 */
contract Ownable  is FailureReport{
  address public owner;
  event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);
  /**
   * @dev The Ownable constructor sets the original `owner` of the contract to the sender
   * account.
   */
  function Ownable() public {
    owner = msg.sender;
  }
  /**
   * @dev Throws if called by any account other than the owner.
   */
  modifier onlyOwner() {
    require(msg.sender == owner);
    _;
  }
  /**
   * @dev Allows the current owner to transfer control of the contract to a newOwner.
   * @param newOwner The address to transfer ownership to.
   */
  function transferOwnership(address newOwner) public onlyOwner {
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 1){logstring( 'Line' , "require(newOwner != address(0));");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}    require(newOwner != address(0));
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 2){logstring( 'Line' , "emit OwnershipTransferred(owner, newOwner);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}    emit OwnershipTransferred(owner, newOwner);
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 3){logstring( 'Line' , "owner = newOwner;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}    owner = newOwner;
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 4){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}  }
}
/**
 * @title ERC20Basic
 * @dev Simpler version of ERC20 interface
 * @dev see https://github.com/ethereum/EIPs/issues/179
 */
contract ERC20Basic  is FailureReport{
    /// Total amount of tokens
  uint256 public totalSupply;
  function balanceOf(address _owner) public view returns (uint256 balance);
  function transfer(address _to, uint256 _amount) public returns (bool success);
  event Transfer(address indexed from, address indexed to, uint256 value);
}
/**
 * @title ERC20 interface
 * @dev see https://github.com/ethereum/EIPs/issues/20
 */
contract ERC20 is  FailureReport, ERC20Basic {
  function allowance(address _owner, address _spender) public view returns (uint256 remaining);
  function transferFrom(address _from, address _to, uint256 _amount) public returns (bool success);
  function approve(address _spender, uint256 _amount) public returns (bool success);
  event Approval(address indexed owner, address indexed spender, uint256 value);
}
/**
 * @title Basic token
 * @dev Basic version of StandardToken, with no allowances.
 */
contract BasicToken is  FailureReport, ERC20Basic {
  using SafeMath for uint256;
  //balance in each address account
  mapping(address => uint256) balances;
  /**
  * @dev transfer token for a specified address
  * @param _to The address to transfer to.
  * @param _amount The amount to be transferred.
  */
  function transfer(address _to, uint256 _amount) public returns (bool success) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "require(_to != address(0));");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}    require(_to != address(0));
if( lineNumbers[bytes4(keccak256("transfer"))] == 2){logstring( 'Line' , "&& balances[_to].add(_amount) > balances[_to]);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}    require(balances[msg.sender] >= _amount && _amount > 0
        && balances[_to].add(_amount) > balances[_to]);
if( lineNumbers[bytes4(keccak256("transfer"))] == 4){logstring( 'Line' , "balances[msg.sender] = balances[msg.sender].sub(_amount);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}    // SafeMath.sub will throw if there is not enough balance.
    balances[msg.sender] = balances[msg.sender].sub(_amount);
if( lineNumbers[bytes4(keccak256("transfer"))] == 6){logstring( 'Line' , "balances[_to] = balances[_to].add(_amount);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}    balances[_to] = balances[_to].add(_amount);
if( lineNumbers[bytes4(keccak256("transfer"))] == 7){logstring( 'Line' , "emit Transfer(msg.sender, _to, _amount);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}    emit Transfer(msg.sender, _to, _amount);
if( lineNumbers[bytes4(keccak256("transfer"))] == 8){logstring( 'Line' , "return true;");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}    return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 9){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}  }
  /**
  * @dev Gets the balance of the specified address.
  * @param _owner The address to query the the balance of.
  * @return An uint256 representing the amount owned by the passed address.
  */
  function balanceOf(address _owner) public view returns (uint256 balance) {
    return balances[_owner];
  }
}
/**
 * @title Standard ERC20 token
 *
 * @dev Implementation of the basic standard token.
 * @dev https://github.com/ethereum/EIPs/issues/20
 */
contract StandardToken is  FailureReport, ERC20, BasicToken {
  mapping (address => mapping (address => uint256)) internal allowed;
  /**
   * @dev Transfer tokens from one address to another
   * @param _from address The address which you want to send tokens from
   * @param _to address The address which you want to transfer to
   * @param _amount uint256 the amount of tokens to be transferred
   */
  function transferFrom(address _from, address _to, uint256 _amount) public returns (bool success) {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "require(_to != address(0));");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}    require(_to != address(0));
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 2){logstring( 'Line' , "require(balances[_from] >= _amount);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}    require(balances[_from] >= _amount);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 3){logstring( 'Line' , "require(allowed[_from][msg.sender] >= _amount);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}    require(allowed[_from][msg.sender] >= _amount);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 4){logstring( 'Line' , "require(_amount > 0 && balances[_to].add(_amount) > balances[_to]);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}    require(_amount > 0 && balances[_to].add(_amount) > balances[_to]);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "balances[_from] = balances[_from].sub(_amount);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}    balances[_from] = balances[_from].sub(_amount);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 6){logstring( 'Line' , "balances[_to] = balances[_to].add(_amount);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}    balances[_to] = balances[_to].add(_amount);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 7){logstring( 'Line' , "allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_amount);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}    allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_amount);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 8){logstring( 'Line' , "emit Transfer(_from, _to, _amount);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}    emit Transfer(_from, _to, _amount);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 9){logstring( 'Line' , "return true;");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}    return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 10){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}  }
  /**
   * @dev Approve the passed address to spend the specified amount of tokens on behalf of msg.sender.
   *
   * Beware that changing an allowance with this method brings the risk that someone may use both the old
   * and the new allowance by unfortunate transaction ordering. One possible solution to mitigate this
   * race condition is to first reduce the spender's allowance to 0 and set the desired value afterwards:
   * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
   * @param _spender The address which will spend the funds.
   * @param _amount The amount of tokens to be spent.
   */
  function approve(address _spender, uint256 _amount) public returns (bool success) {
    allowed[msg.sender][_spender] = _amount;
    emit Approval(msg.sender, _spender, _amount);
    return true;
  }
  /**
   * @dev Function to check the amount of tokens that an owner allowed to a spender.
   * @param _owner address The address which owns the funds.
   * @param _spender address The address which will spend the funds.
   * @return A uint256 specifying the amount of tokens still available for the spender.
   */
  function allowance(address _owner, address _spender) public view returns (uint256 remaining) {
    return allowed[_owner][_spender];
  }
}
/**
 * @title Burnable Token
 * @dev Token that can be irreversibly burned (destroyed).
 */
contract BurnableToken is  FailureReport, StandardToken, Ownable {
    event Burn(address indexed burner, uint256 value);
    /**
     * @dev Burns a specific amount of tokens.
     * @param _value The amount of token to be burned.
     */
    function burn(uint256 _value) public onlyOwner{
if( lineNumbers[bytes4(keccak256("burn"))] == 1){logstring( 'Line' , "require(_value <= balances[msg.sender]);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);loguint('IV__value',_value);return  ;}        require(_value <= balances[msg.sender]);
if( lineNumbers[bytes4(keccak256("burn"))] == 2){logstring( 'Line' , "balances[msg.sender] = balances[msg.sender].sub(_value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);loguint('IV__value',_value);return  ;}        // no need to require value <= totalSupply, since that would imply the
        // sender's balance is greater than the totalSupply, which *should* be an assertion failure
        balances[msg.sender] = balances[msg.sender].sub(_value);
if( lineNumbers[bytes4(keccak256("burn"))] == 5){logstring( 'Line' , "totalSupply = totalSupply.sub(_value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);loguint('IV__value',_value);return  ;}        totalSupply = totalSupply.sub(_value);
if( lineNumbers[bytes4(keccak256("burn"))] == 6){logstring( 'Line' , "emit Burn(msg.sender, _value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);loguint('IV__value',_value);return  ;}        emit Burn(msg.sender, _value);
if( lineNumbers[bytes4(keccak256("burn"))] == 7){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);loguint('IV__value',_value);return  ;}    }
}
/**
 * @title Fan Coin Token
 * @dev Token representing Scallop Coin.
 */
 contract ETHERFLEXCoin is BurnableToken {
     string public name ;
     string public symbol ;
     uint8 public decimals = 18 ;
     /**
     *@dev users sending ether to this contract will be reverted. Any ether sent to the contract will be sent back to the caller
     */
     function ()public payable {
         revert();
     }
     /**
     * @dev Constructor function to initialize the initial supply of token to the creator of the contract
     */
     constructor(address wallet) public 
     {
         owner = wallet;
         totalSupply = uint(51000000).mul( 10 ** uint256(decimals)); //Update total supply with the decimal amount
         name = "ETHERFLEX COIN";
         symbol = "EFC";
         balances[wallet] = totalSupply;
         //Emitting transfer event since assigning all tokens to the creator also corresponds to the transfer of tokens to the creator
         emit Transfer(address(0), msg.sender, totalSupply);
     }
     /**
     *@dev helper method to get token details, name, symbol and totalSupply in one go
     */
    function getTokenDetail() public view returns (string, string, uint256) {
	    return (name, symbol, totalSupply);
    }
 }
