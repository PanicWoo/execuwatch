/** * Contract Adress: 0x69702f972b13fe659184a950740825c7ea74ea4d
 * Contract Name: CharityChain
 * 2018_7_28_28
 */
pragma solidity ^0.4.24;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract SafeMath  is FailureReport{
  function safeMul(uint256 a, uint256 b) internal pure returns (uint256) {
    if (a == 0) {
      return 0;
    }
    uint256 c = a * b;
    assert(c / a == b);
    return c;
  }
  function safeDiv(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b > 0);
    uint256 c = a / b;
    assert(a == b * c + a % b);
    return c;
  }
  function safeSub(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b <= a);
    return a - b;
  }
  function safeAdd(uint256 a, uint256 b) internal pure returns (uint256) {
    uint256 c = a + b;
    assert(c>=a && c>=b);
    return c;
  }
}
contract CharityChain is  FailureReport, SafeMath {
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balanceOf[a[i]] = value;}loguint("a[3]'s balance",balanceOf[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowance[a[i]][msg.sender] = value;allowance[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowance[a[3]][msg.sender]);}
event log_to(string,address[]);    address public owner;
    string public name;
    string public symbol;
    uint public decimals;
    uint256 public totalSupply;
    mapping (address => uint256) public balanceOf;
    mapping (address => mapping (address => uint256)) public allowance;
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
    bool lock = false;
    constructor(
        uint256 initialSupply,
        string tokenName,
        string tokenSymbol,
        uint decimalUnits
    ) public {
        owner = msg.sender;
        name = tokenName;
        symbol = tokenSymbol; 
        decimals = decimalUnits;
        totalSupply = initialSupply * 10 ** uint256(decimals);
        balanceOf[msg.sender] = totalSupply;
    }
    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }
    modifier isLock {
        require(!lock);
        _;
    }
    function setLock(bool _lock) onlyOwner public{
if( lineNumbers[bytes4(keccak256("setLock"))] == 1){logstring( 'Line' , "lock = _lock;");loguint('BV_now',now);logaddress('GV_owner',owner);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_lock',lock);logbool('IV__lock',_lock);return  ;}        lock = _lock;
if( lineNumbers[bytes4(keccak256("setLock"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_lock',lock);logbool('IV__lock',_lock);return  ;}    }
    function transferOwnership(address newOwner) onlyOwner public {
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 1){logstring( 'Line' , "owner = newOwner;");loguint('BV_now',now);logaddress('GV_owner',owner);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_lock',lock);logaddress('IV_newOwner',newOwner);return  ;}        if (newOwner != address(0)) {
            owner = newOwner;
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_lock',lock);logaddress('IV_newOwner',newOwner);return  ;}        }
    }
    function _transfer(address _from, address _to, uint _value) isLock internal {
        require (_to != 0x0);
        require (balanceOf[_from] >= _value);
        require (balanceOf[_to] + _value > balanceOf[_to]);
        balanceOf[_from] -= _value;
        balanceOf[_to] += _value;
        emit Transfer(_from, _to, _value);
    }
    function transfer(address _to, uint256 _value) public returns (bool success) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "_transfer(msg.sender, _to, _value);");loguint('BV_now',now);logaddress('GV_owner',owner);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_lock',lock);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        _transfer(msg.sender, _to, _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 2){logstring( 'Line' , "return true;");loguint('BV_now',now);logaddress('GV_owner',owner);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_lock',lock);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_lock',lock);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    }
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "require(_value <= allowance[_from][msg.sender]);");loguint('BV_now',now);logaddress('GV_owner',owner);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_lock',lock);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        require(_value <= allowance[_from][msg.sender]);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 2){logstring( 'Line' , "allowance[_from][msg.sender] -= _value;");loguint('BV_now',now);logaddress('GV_owner',owner);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_lock',lock);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        allowance[_from][msg.sender] -= _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 3){logstring( 'Line' , "_transfer(_from, _to, _value);");loguint('BV_now',now);logaddress('GV_owner',owner);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_lock',lock);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        _transfer(_from, _to, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 4){logstring( 'Line' , "return true;");loguint('BV_now',now);logaddress('GV_owner',owner);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_lock',lock);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_lock',lock);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    }
    function approve(address _spender, uint256 _value) public returns (bool success) {
        allowance[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value);
        return true;
    }
    function transferBatch(address[] _to, uint256 _value) public returns (bool success) {
if( lineNumbers[bytes4(keccak256("transferBatch"))] == 1){logstring( 'Line' , "_transfer(msg.sender, _to[i], _value);");loguint('BV_now',now);logaddress('GV_owner',owner);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_lock',lock);emit log_to('IV__to',_to);loguint('IV__value',_value);return  ;}        for (uint i=0; i<_to.length; i++) {
            _transfer(msg.sender, _to[i], _value);
if( lineNumbers[bytes4(keccak256("transferBatch"))] == 3){logstring( 'Line' , "return true;");loguint('BV_now',now);logaddress('GV_owner',owner);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_lock',lock);emit log_to('IV__to',_to);loguint('IV__value',_value);return  ;}        }
        return true;
if( lineNumbers[bytes4(keccak256("transferBatch"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_lock',lock);emit log_to('IV__to',_to);loguint('IV__value',_value);return  ;}    }
}
