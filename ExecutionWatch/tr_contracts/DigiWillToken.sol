/** * Contract Adress: 0xda8a2e989d4c4142d53339c0fcf0e6ea346061ce
 * Contract Name: DigiWillToken
 * 2018_6_12_66
 */
pragma solidity ^0.4.21;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract Ownable  is FailureReport{
    address public owner;
    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);
    /**
     * @dev The Ownable constructor sets the original `owner` of the contract to the sender
     * account.
     */
    function Ownable() public {
        owner = msg.sender;
    }
    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }
    /**
     * @dev Allows the current owner to transfer control of the contract to a newOwner.
     * @param newOwner The address to transfer ownership to.
     */
    function transferOwnership(address newOwner) public onlyOwner {
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 1){logstring( 'Line' , "require(newOwner != address(0));");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}        require(newOwner != address(0));
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 2){logstring( 'Line' , "emit OwnershipTransferred(owner, newOwner);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}        emit OwnershipTransferred(owner, newOwner);
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 3){logstring( 'Line' , "owner = newOwner;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}        owner = newOwner;
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 4){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}    }
}
/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {
  /**
  * @dev Multiplies two numbers, throws on overflow.
  */
  function mul(uint256 a, uint256 b) internal pure returns (uint256) {
    if (a == 0) {
      return 0;
    }
    uint256 c = a * b;
    assert(c / a == b);
    return c;
  }
  /**
  * @dev Integer division of two numbers, truncating the quotient.
  */
  function div(uint256 a, uint256 b) internal pure returns (uint256) {
    // assert(b > 0); // Solidity automatically throws when dividing by 0
    uint256 c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn't hold
    return c;
  }
  /**
  * @dev Subtracts two numbers, throws on overflow (i.e. if subtrahend is greater than minuend).
  */
  function sub(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b <= a);
    return a - b;
  }
  /**
  * @dev Adds two numbers, throws on overflow.
  */
  function add(uint256 a, uint256 b) internal pure returns (uint256) {
    uint256 c = a + b;
    assert(c >= a);
    return c;
  }
}
interface tokenRecipient {
    function receiveApproval(address _from, uint256 _value, address _token, bytes _extraData) external;
}
contract DigiWillToken is  FailureReport, Ownable {
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balances[a[i]] = value;}loguint("a[3]'s balance",balances[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowed[a[i]][msg.sender] = value;allowed[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowed[a[3]][msg.sender]);}    using SafeMath for uint256;
    string public name = "DigiWillToken";           //The Token's name: e.g. DigixDAO Tokens
    uint8 public decimals = 18;             //Number of decimals of the smallest unit
    string public symbol = "DGW";         //An identifier: e.g. REP
    uint public totalSupply;
    mapping (address => uint256) public balances;
    // `allowed` tracks any extra transfer rights as in all ERC20 tokens
    mapping (address => mapping (address => uint256)) public allowed;
////////////////
// Constructor
////////////////
    /// @notice Constructor to create a DigiWillToken
    function DigiWillToken() public {
        totalSupply = 200000100 * 10**18;
        // Give the creator all initial tokens
        balances[msg.sender] = totalSupply;
    }
///////////////////
// ERC20 Methods
///////////////////
    /// @notice Send `_amount` tokens to `_to` from `msg.sender`
    /// @param _to The address of the recipient
    /// @param _amount The amount of tokens to be transferred
    /// @return Whether the transfer was successful or not
    function transfer(address _to, uint256 _amount) public returns (bool success) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "doTransfer(msg.sender, _to, _amount);");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}        doTransfer(msg.sender, _to, _amount);
if( lineNumbers[bytes4(keccak256("transfer"))] == 2){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}        return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}    }
    /// @notice Send `_amount` tokens to `_to` from `_from` on the condition it
    ///  is approved by `_from`
    /// @param _from The address holding the tokens being transferred
    /// @param _to The address of the recipient
    /// @param _amount The amount of tokens to be transferred
    /// @return True if the transfer was successful
    function transferFrom(address _from, address _to, uint256 _amount
    ) public returns (bool success) {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "require(allowed[_from][msg.sender] >= _amount);");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}        // The standard ERC 20 transferFrom functionality
        require(allowed[_from][msg.sender] >= _amount);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 3){logstring( 'Line' , "allowed[_from][msg.sender] -= _amount;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}        allowed[_from][msg.sender] -= _amount;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 4){logstring( 'Line' , "doTransfer(_from, _to, _amount);");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}        doTransfer(_from, _to, _amount);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}        return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 6){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__amount',_amount);return  ;}    }
    /// @dev This is the actual transfer function in the token contract, it can
    ///  only be called by other functions in this contract.
    /// @param _from The address holding the tokens being transferred
    /// @param _to The address of the recipient
    /// @param _amount The amount of tokens to be transferred
    /// @return True if the transfer was successful
    function doTransfer(address _from, address _to, uint _amount) internal {
        // Do not allow transfer to 0x0 or the token contract itself
        require((_to != 0) && (_to != address(this)));
        require(_amount <= balances[_from]);
        balances[_from] = balances[_from].sub(_amount);
        balances[_to] = balances[_to].add(_amount);
        emit Transfer(_from, _to, _amount);
		}
    /// @return The balance of `_owner`
    function balanceOf(address _owner) public constant returns (uint256 balance) {
        return balances[_owner];
    }
    /// @notice `msg.sender` approves `_spender` to spend `_amount` tokens on
    ///  its behalf. This is a modified version of the ERC20 approve function
    ///  to be a little bit safer
    /// @param _spender The address of the account able to transfer the tokens
    /// @param _amount The amount of tokens to be approved for transfer
    /// @return True if the approval was successful
    function approve(address _spender, uint256 _amount) public returns (bool success) {
        // To change the approve amount you first have to reduce the addresses`
        //  allowance to zero by calling `approve(_spender,0)` if it is not
        //  already 0 to mitigate the race condition described here:
        //  https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
        require((_amount == 0) || (allowed[msg.sender][_spender] == 0));
        allowed[msg.sender][_spender] = _amount;
        emit Approval(msg.sender, _spender, _amount);
        return true;
    }
    function approveAndCall(address _spender, uint256 _value, bytes _extraData) public returns (bool success) {
if( lineNumbers[bytes4(keccak256("approveAndCall"))] == 1){logstring( 'Line' , "tokenRecipient spender = tokenRecipient(_spender);");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);logaddress('IV__spender',_spender);loguint('IV__value',_value);return  ;}        tokenRecipient spender = tokenRecipient(_spender);
if( lineNumbers[bytes4(keccak256("approveAndCall"))] == 2){logstring( 'Line' , "spender.receiveApproval(msg.sender, _value, this, _extraData);");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);logaddress('IV__spender',_spender);loguint('IV__value',_value);return  ;}        if (approve(_spender, _value)) {
            spender.receiveApproval(msg.sender, _value, this, _extraData);
if( lineNumbers[bytes4(keccak256("approveAndCall"))] == 4){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);logaddress('IV__spender',_spender);loguint('IV__value',_value);return  ;}            return true;
if( lineNumbers[bytes4(keccak256("approveAndCall"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);logaddress('IV__spender',_spender);loguint('IV__value',_value);return  ;}        }
    }
    function burn(uint256 _value) public onlyOwner {
if( lineNumbers[bytes4(keccak256("burn"))] == 1){logstring( 'Line' , "require(balances[msg.sender] >= _value);");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);loguint('IV__value',_value);return  ;}        require(balances[msg.sender] >= _value);
if( lineNumbers[bytes4(keccak256("burn"))] == 2){logstring( 'Line' , "balances[msg.sender] = balances[msg.sender].sub(_value);");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);loguint('IV__value',_value);return  ;}        balances[msg.sender] = balances[msg.sender].sub(_value);
if( lineNumbers[bytes4(keccak256("burn"))] == 3){logstring( 'Line' , "totalSupply = totalSupply.sub(_value);");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);loguint('IV__value',_value);return  ;}        totalSupply = totalSupply.sub(_value);
if( lineNumbers[bytes4(keccak256("burn"))] == 4){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);loguint('GV_totalSupply',totalSupply);logaddress('GV_owner',owner);loguint('IV__value',_value);return  ;}    }
    /// @dev This function makes it easy to read the `allowed[]` map
    /// @param _owner The address of the account that owns the token
    /// @param _spender The address of the account able to transfer the tokens
    /// @return Amount of remaining tokens of _owner that _spender is allowed
    ///  to spend
    function allowance(address _owner, address _spender
    ) public constant returns (uint256 remaining) {
        return allowed[_owner][_spender];
    }
    /// @dev This function makes it easy to get the total number of tokens
    /// @return The total number of tokens
    function totalSupply() public constant returns (uint) {
        return totalSupply;
    }
    event Transfer(
        address indexed _from,
        address indexed _to,
        uint256 _amount
        );
    event Approval(
        address indexed _owner,
        address indexed _spender,
        uint256 _amount
        );
    event Burn(
        address indexed _burner,
        uint256 _amount
        );
}
