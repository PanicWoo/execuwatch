/** * Contract Adress: 0x9E55cA0690Ac83dCB4D675873c0e344c848DBbf2
 * Contract Name: BBODServiceRegistry
 * 2018_7_27_118
 */
pragma solidity ^0.4.23;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
/**
 * @title Ownable
 * @dev The Ownable contract has an owner address, and provides basic authorization control
 * functions, this simplifies the implementation of "user permissions".
 */
contract Ownable  is FailureReport{
  address public owner;
  event OwnershipRenounced(address indexed previousOwner);
  event OwnershipTransferred(
    address indexed previousOwner,
    address indexed newOwner
  );
  /**
   * @dev The Ownable constructor sets the original `owner` of the contract to the sender
   * account.
   */
  constructor() public {
    owner = msg.sender;
  }
  /**
   * @dev Throws if called by any account other than the owner.
   */
  modifier onlyOwner() {
    require(msg.sender == owner);
    _;
  }
  /**
   * @dev Allows the current owner to relinquish control of the contract.
   * @notice Renouncing to ownership will leave the contract without an owner.
   * It will not be possible to call the functions with the `onlyOwner`
   * modifier anymore.
   */
  function renounceOwnership() public onlyOwner {
if( lineNumbers[bytes4(keccak256("renounceOwnership"))] == 1){logstring( 'Line' , "emit OwnershipRenounced(owner);");loguint('BV_now',now);logaddress('GV_owner',owner);return  ;}    emit OwnershipRenounced(owner);
if( lineNumbers[bytes4(keccak256("renounceOwnership"))] == 2){logstring( 'Line' , "owner = address(0);");loguint('BV_now',now);logaddress('GV_owner',owner);return  ;}    owner = address(0);
if( lineNumbers[bytes4(keccak256("renounceOwnership"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);return  ;}  }
  /**
   * @dev Allows the current owner to transfer control of the contract to a newOwner.
   * @param _newOwner The address to transfer ownership to.
   */
  function transferOwnership(address _newOwner) public onlyOwner {
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 1){logstring( 'Line' , "_transferOwnership(_newOwner);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV__newOwner',_newOwner);return  ;}    _transferOwnership(_newOwner);
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV__newOwner',_newOwner);return  ;}  }
  /**
   * @dev Transfers control of the contract to a newOwner.
   * @param _newOwner The address to transfer ownership to.
   */
  function _transferOwnership(address _newOwner) internal {
    require(_newOwner != address(0));
    emit OwnershipTransferred(owner, _newOwner);
    owner = _newOwner;
  }
}
contract BBODServiceRegistry is  FailureReport, Ownable {
  //1. Manager
  //2. CustodyStorage
  mapping(uint => address) public registry;
    constructor(address _owner) {
        owner = _owner;
    }
  function setServiceRegistryEntry (uint key, address entry) external onlyOwner {
if( lineNumbers[bytes4(keccak256("setServiceRegistryEntry"))] == 1){logstring( 'Line' , "registry[key] = entry;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('IV_key',key);logaddress('IV_entry',entry);return  ;}    registry[key] = entry;
if( lineNumbers[bytes4(keccak256("setServiceRegistryEntry"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('IV_key',key);logaddress('IV_entry',entry);return  ;}  }
}
