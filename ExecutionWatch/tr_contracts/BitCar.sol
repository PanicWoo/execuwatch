/** * Contract Adress: 0x08b4c866ae9d1be56a06e0c302054b4ffe067b43
 * Contract Name: BitCar
 * 2018_5_18_76
 */
pragma solidity ^0.4.23;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract ERC20TokenInterface  is FailureReport{
    /// @return The total amount of tokens
    function totalSupply() public constant returns (uint256 supply);
    /// @param _owner The address from which the balance will be retrieved
    /// @return The balance
    function balanceOf(address _owner) constant public returns (uint256 balance);
    /// @notice send `_value` token to `_to` from `msg.sender`
    /// @param _to The address of the recipient
    /// @param _value The amount of token to be transferred
    /// @return Whether the transfer was successful or not
    function transfer(address _to, uint256 _value) public returns (bool success);
    /// @notice send `_value` token to `_to` from `_from` on the condition it is approved by `_from`
    /// @param _from The address of the sender
    /// @param _to The address of the recipient
    /// @param _value The amount of token to be transferred
    /// @return Whether the transfer was successful or not
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success);
    /// @notice `msg.sender` approves `_spender` to spend `_value` tokens
    /// @param _spender The address of the account able to transfer the tokens
    /// @param _value The amount of tokens to be approved for transfer
    /// @return Whether the approval was successful or not
    function approve(address _spender, uint256 _value) public returns (bool success);
    /// @param _owner The address of the account owning tokens
    /// @param _spender The address of the account able to transfer the tokens
    /// @return Amount of remaining tokens allowed to spent
    function allowance(address _owner, address _spender) constant public returns (uint256 remaining);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
contract BitCar is  FailureReport, ERC20TokenInterface {
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balances[a[i]] = value;}loguint("a[3]'s balance",balances[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowed[a[i]][msg.sender] = value;allowed[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowed[a[3]][msg.sender]);}  function () public {
    //if ether is sent to this address, send it back.
    revert();
  }
  //// Constants ////
  string public constant name = 'BitCar';
  uint256 public constant decimals = 8;
  string public constant symbol = 'BITCAR';
  string public constant version = '1.0';
  string public constant note = 'If you can dream it, you can do it. Enzo Ferrari';
  // 500 million coins, each divided to up to 10^decimals units.
  uint256 private constant totalTokens = 500000000 * (10 ** decimals);
  mapping (address => uint256) public balances; // (ERC20)
  // A mapping from an account owner to a map from approved spender to their allowances.
  // (see ERC20 for details about allowances).
  mapping (address => mapping (address => uint256)) public allowed; // (ERC20)
  //// Events ////
  event MigrationInfoSet(string newMigrationInfo);
  // This is to be used when migration to a new contract starts.
  // This string can be used for any authorative information re the migration
  // (e.g. address to use for migration, or URL to explain where to find more info)
  string public migrationInfo = "";
  // The only address that can set migrationContractAddress, a secure multisig.
  address public migrationInfoSetter;
  //// Modifiers ////
  modifier onlyFromMigrationInfoSetter {
    if (msg.sender != migrationInfoSetter) {
      revert();
    }
    _;
  }
  //// Public functions ////
  constructor(address _migrationInfoSetter) public {
    if (_migrationInfoSetter == 0) revert();
    migrationInfoSetter = _migrationInfoSetter;
    // Upon creation, all tokens belong to the deployer.
    balances[msg.sender] = totalTokens;
  }
  // See ERC20
  function totalSupply() public constant returns (uint256) {
    return totalTokens;
  }
  // See ERC20
  // WARNING: If you call this with the address of a contract, the contract will receive the
  // funds, but will have no idea where they came from. Furthermore, if the contract is
  // not aware of POWR, the tokens will remain locked away in the contract forever.
  // It is always recommended to call instead compareAndApprove() (or approve()) and have the
  // receiving contract withdraw the money using transferFrom().
  function transfer(address _to, uint256 _value) public returns (bool) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "balances[msg.sender] -= _value;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    if (balances[msg.sender] >= _value) {
      balances[msg.sender] -= _value;
if( lineNumbers[bytes4(keccak256("transfer"))] == 3){logstring( 'Line' , "balances[_to] += _value;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}      balances[_to] += _value;
if( lineNumbers[bytes4(keccak256("transfer"))] == 4){logstring( 'Line' , "emit Transfer(msg.sender, _to, _value);");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}      emit Transfer(msg.sender, _to, _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 5){logstring( 'Line' , "return true;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}      return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 6){logstring( 'Line' , "return false;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    }
    return false;
if( lineNumbers[bytes4(keccak256("transfer"))] == 8){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}  }
  // See ERC20
  function transferFrom(address _from, address _to, uint256 _value) public returns (bool) {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "balances[_from] -= _value;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value) {
      balances[_from] -= _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 3){logstring( 'Line' , "allowed[_from][msg.sender] -= _value;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}      allowed[_from][msg.sender] -= _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 4){logstring( 'Line' , "balances[_to] += _value;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}      balances[_to] += _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "emit Transfer(_from, _to, _value);");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}      emit Transfer(_from, _to, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 6){logstring( 'Line' , "return true;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}      return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 7){logstring( 'Line' , "return false;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    }
    return false;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 9){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}  }
  // See ERC20
  function balanceOf(address _owner) constant public returns (uint256) {
    return balances[_owner];
  }
  // See ERC20
  // NOTE: this method is vulnerable and is placed here only to follow the ERC20 standard.
  // Before using, please take a look at the better compareAndApprove below.
  function approve(address _spender, uint256 _value) public returns (bool) {
    allowed[msg.sender][_spender] = _value;
    emit Approval(msg.sender, _spender, _value);
    return true;
  }
  // A vulernability of the approve method in the ERC20 standard was identified by
  // Mikhail Vladimirov and Dmitry Khovratovich here:
  // https://docs.google.com/document/d/1YLPtQxZu1UAvO9cZ1O2RPXBbT0mooh4DYKjA_jp-RLM
  // It's better to use this method which is not susceptible to over-withdrawing by the approvee.
  /// @param _spender The address to approve
  /// @param _currentValue The previous value approved, which can be retrieved with allowance(msg.sender, _spender)
  /// @param _newValue The new value to approve, this will replace the _currentValue
  /// @return bool Whether the approval was a success (see ERC20's `approve`)
  function compareAndApprove(address _spender, uint256 _currentValue, uint256 _newValue) public returns(bool) {
if( lineNumbers[bytes4(keccak256("compareAndApprove"))] == 1){logstring( 'Line' , "return false;");loguint('BV_now',now);logaddress('IV__spender',_spender);loguint('IV__currentValue',_currentValue);loguint('IV__newValue',_newValue);return  ;}    if (allowed[msg.sender][_spender] != _currentValue) {
      return false;
if( lineNumbers[bytes4(keccak256("compareAndApprove"))] == 3){logstring( 'Line' , "return approve(_spender, _newValue);");loguint('BV_now',now);logaddress('IV__spender',_spender);loguint('IV__currentValue',_currentValue);loguint('IV__newValue',_newValue);return  ;}    }
    return approve(_spender, _newValue);
if( lineNumbers[bytes4(keccak256("compareAndApprove"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__spender',_spender);loguint('IV__currentValue',_currentValue);loguint('IV__newValue',_newValue);return  ;}  }
  // See ERC20
  function allowance(address _owner, address _spender) constant public returns (uint256 remaining) {
    return allowed[_owner][_spender];
  }
  // Allows setting a descriptive string, which will aid any users in migrating their token
  // to a newer version of the contract. This field provides a kind of 'double-layer' of
  // authentication for any migration announcement, as it can only be set by BitCar.
  /// @param _migrationInfo The information string to be stored on the contract
  function setMigrationInfo(string _migrationInfo) onlyFromMigrationInfoSetter public {
if( lineNumbers[bytes4(keccak256("setMigrationInfo"))] == 1){logstring( 'Line' , "migrationInfo = _migrationInfo;");loguint('BV_now',now);logstring('IV__migrationInfo',_migrationInfo);return  ;}    migrationInfo = _migrationInfo;
if( lineNumbers[bytes4(keccak256("setMigrationInfo"))] == 2){logstring( 'Line' , "emit MigrationInfoSet(_migrationInfo);");loguint('BV_now',now);logstring('IV__migrationInfo',_migrationInfo);return  ;}    emit MigrationInfoSet(_migrationInfo);
if( lineNumbers[bytes4(keccak256("setMigrationInfo"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('IV__migrationInfo',_migrationInfo);return  ;}  }
  // To be used if the migrationInfoSetter wishes to transfer the migrationInfoSetter
  // permission to a new account, e.g. because of change in personnel, a concern that account
  // may have been compromised etc.
  /// @param _newMigrationInfoSetter The address of the new Migration Info Setter
  function changeMigrationInfoSetter(address _newMigrationInfoSetter) onlyFromMigrationInfoSetter public {
if( lineNumbers[bytes4(keccak256("changeMigrationInfoSetter"))] == 1){logstring( 'Line' , "migrationInfoSetter = _newMigrationInfoSetter;");loguint('BV_now',now);logaddress('IV__newMigrationInfoSetter',_newMigrationInfoSetter);return  ;}    migrationInfoSetter = _newMigrationInfoSetter;
if( lineNumbers[bytes4(keccak256("changeMigrationInfoSetter"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__newMigrationInfoSetter',_newMigrationInfoSetter);return  ;}  }
}
