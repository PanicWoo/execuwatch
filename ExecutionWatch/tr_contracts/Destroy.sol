/** * Contract Adress: 0x5ecdaf39eb3228c634943e591eb08f3a66eae2ba
 * Contract Name: Destroy
 * 2018_8_10_27
 */
pragma solidity ^0.4.23;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract Destroy is FailureReport{
      function delegatecall_selfdestruct(address _target) external returns (bool _ans) {
if( lineNumbers[bytes4(keccak256("delegatecall_selfdestruct"))] == 1){logstring( 'Line' , "_ans = _target.delegatecall(bytes4(sha3('address)')), this);");loguint('BV_now',now);logaddress('IV__target',_target);return  ;}          _ans = _target.delegatecall(bytes4(sha3("address)")), this); 
if( lineNumbers[bytes4(keccak256("delegatecall_selfdestruct"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__target',_target);return  ;}      }
}
