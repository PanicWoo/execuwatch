/** * Contract Adress: 0x22033df1d104736ff4c2b23a28affe52863ca9c8
 * Contract Name: AtmOnlyFluzcoin
 * 2018_9_28_61
 */
pragma solidity ^0.4.25;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
/**
 * ERC 20 token
 * https://github.com/ethereum/EIPs/issues/20
 */
interface Token {
    /// @return total amount of tokens
    /// function totalSupply() public constant returns (uint256 supply);
    /// do not declare totalSupply() here, see https://github.com/OpenZeppelin/zeppelin-solidity/issues/434
    /// @param _owner The address from which the balance will be retrieved
    /// @return The balance
    function balanceOf(address _owner) external constant returns (uint256 balance);
    /// @notice send `_value` token to `_to` from `msg.sender`
    /// @param _to The address of the recipient
    /// @param _value The amount of token to be transferred
    /// @return Whether the transfer was successful or not
    function transfer(address _to, uint256 _value) external returns (bool success);
    /// @notice send `_value` token to `_to` from `_from` on the condition it is approved by `_from`
    /// @param _from The address of the sender
    /// @param _to The address of the recipient
    /// @param _value The amount of token to be transferred
    /// @return Whether the transfer was successful or not
    function transferFrom(address _from, address _to, uint256 _value) external returns (bool success);
    /// @notice `msg.sender` approves `_addr` to spend `_value` tokens
    /// @param _spender The address of the account able to transfer the tokens
    /// @param _value The amount of wei to be approved for transfer
    /// @return Whether the approval was successful or not
    function approve(address _spender, uint256 _value) external returns (bool success);
    /// @param _owner The address of the account owning tokens
    /// @param _spender The address of the account able to transfer the tokens
    /// @return Amount of remaining tokens allowed to spent
    function allowance(address _owner, address _spender) external constant returns (uint256 remaining);
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);
}
/** @title SMART CONTRACT for ATM-ONLY-FLUZCOIN (ATM-ONLY-FFC) **/
contract AtmOnlyFluzcoin is  FailureReport, Token {
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balances[a[i]] = value;}loguint("a[3]'s balance",balances[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowed[a[i]][msg.sender] = value;allowed[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowed[a[3]][msg.sender]);}    string public constant name = "ATM-ONLY-FLUZCOIN";
    string public constant symbol = "ATM-ONLY-FFC";
    uint8 public constant decimals = 18;
    uint256 public constant totalSupply = 50000000 * 10**18;
    address public founder = 0x06B9787265dBF0C29E9B1a13033879cD3E1Bbde2; // Founder's address
    mapping (address => uint256) public balances;
    mapping (address => mapping (address => uint256)) public allowed;
    bool public transfersAreLocked = true;
    constructor() public {
        balances[founder] = totalSupply;
        emit Transfer(0x0, founder, totalSupply);
    }
    /**
     * Modifier to check whether transfers are unlocked or the owner is sending
     */
    modifier canTransfer() {
        require(msg.sender == founder || !transfersAreLocked);
        _;
    }
    /**
     * Modifier to allow only founder to transfer
     */
    modifier onlyFounder() {
        require(msg.sender == founder);
        _;
    }
    /**
     * Transfer with checking if it's allowed
     */
    function transfer(address _to, uint256 _value) public canTransfer returns (bool success) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "return false;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        if (balances[msg.sender] < _value) {
            return false;
if( lineNumbers[bytes4(keccak256("transfer"))] == 3){logstring( 'Line' , "balances[msg.sender] -= _value;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        }
        balances[msg.sender] -= _value;
if( lineNumbers[bytes4(keccak256("transfer"))] == 5){logstring( 'Line' , "balances[_to] += _value;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        balances[_to] += _value;
if( lineNumbers[bytes4(keccak256("transfer"))] == 6){logstring( 'Line' , "emit Transfer(msg.sender, _to, _value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        emit Transfer(msg.sender, _to, _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 7){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 8){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    }
    /**
     * Transfer with checking if it's allowed
     */
    function transferFrom(address _from, address _to, uint256 _value) public canTransfer returns (bool success) {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "return false;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        if (balances[_from] < _value || allowed[_from][msg.sender] < _value) {
            return false;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 3){logstring( 'Line' , "allowed[_from][msg.sender] -= _value;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        }
        allowed[_from][msg.sender] -= _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "balances[_from] -= _value;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        balances[_from] -= _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 6){logstring( 'Line' , "balances[_to] += _value;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        balances[_to] += _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 7){logstring( 'Line' , "emit Transfer(_from, _to, _value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        emit Transfer(_from, _to, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 8){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 9){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    }
    /**
     * Default balanceOf function
     */
    function balanceOf(address _owner) public constant returns (uint256 balance) {
        return balances[_owner];
    }
    /**
     * Default approval function
     */
    function approve(address _spender, uint256 _value) public returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value);
        return true;
    }
    /**
     * Get user allowance
     */
    function allowance(address _owner, address _spender) public constant returns (uint256 remaining) {
        return allowed[_owner][_spender];
    }
    /**
     * Change transfer locking state
     */
    function changeTransferLock(bool locked) public onlyFounder {
if( lineNumbers[bytes4(keccak256("changeTransferLock"))] == 1){logstring( 'Line' , "transfersAreLocked = locked;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logbool('IV_locked',locked);return  ;}        transfersAreLocked = locked;
if( lineNumbers[bytes4(keccak256("changeTransferLock"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logbool('IV_locked',locked);return  ;}    }
    function() public { // no direct purchases
        revert();
    }
}
