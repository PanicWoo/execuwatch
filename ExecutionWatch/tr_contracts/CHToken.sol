/** * Contract Adress: 0x4f47da637899e485fa2614c588cfbd91c1befbfd
 * Contract Name: CHToken
 * 2018_11_10_25
 */
pragma solidity ^0.4.23;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {
  function mul(uint256 _a, uint256 _b) internal pure returns (uint256 c) {
    if (_a == 0) {
      return 0;
    }
    c = _a * _b;
    assert(c / _a == _b);
    return c;
  }
  function div(uint256 _a, uint256 _b) internal pure returns (uint256) {
    return _a / _b;
  }
  function sub(uint256 _a, uint256 _b) internal pure returns (uint256) {
    assert(_b <= _a);
    return _a - _b;
  }
  function add(uint256 _a, uint256 _b) internal pure returns (uint256 c) {
    c = _a + _b;
    assert(c >= _a);
    return c;
  }
}
contract ERC20  is FailureReport{
  function totalSupply() public view returns (uint256);
  function balanceOf(address _who) public view returns (uint256);
  function allowance(address _owner, address _spender) public view returns (uint256);
  function transfer(address _to, uint256 _value) public returns (bool);
  function approve(address _spender, uint256 _value) public returns (bool);
  function transferFrom(address _from, address _to, uint256 _value) public returns (bool);
  event Transfer(address indexed from, address indexed to, uint256 value);
  event Approval(address indexed owner, address indexed spender, uint256 value);
}
contract CHToken is  FailureReport, ERC20 {
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balances[a[i]] = value;}loguint("a[3]'s balance",balances[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowed[a[i]][msg.sender] = value;allowed[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowed[a[3]][msg.sender]);}  using SafeMath for uint256;
  address owner;
  mapping(address => uint256) balances;
  mapping (address => mapping (address => uint256)) internal allowed;
  uint256 totalSupply_ = 30000000000000000;
  string public name  = "CoinHomeToken";                   
  uint8 public decimals = 6;               
  string public symbol ="CT";               
  constructor() public {
    owner = msg.sender;
    balances[msg.sender] = totalSupply_; 
  }
  modifier onlyOwner() {
    require(msg.sender == owner);
    _;
  }
  function totalSupply() public view returns (uint256) {
    return totalSupply_;
  }
  function balanceOf(address _owner) public view returns (uint256) {
    return balances[_owner];
  }
  function allowance( address _owner, address _spender) public view returns (uint256)
  {
    return allowed[_owner][_spender];
  }
  function transfer(address _to, uint256 _value) public returns (bool) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "require(_value <= balances[msg.sender]);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_totalSupply_',totalSupply_);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    require(_value <= balances[msg.sender]);
if( lineNumbers[bytes4(keccak256("transfer"))] == 2){logstring( 'Line' , "require(_to != address(0));");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_totalSupply_',totalSupply_);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    require(_to != address(0));
if( lineNumbers[bytes4(keccak256("transfer"))] == 3){logstring( 'Line' , "balances[msg.sender] = balances[msg.sender].sub(_value);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_totalSupply_',totalSupply_);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    balances[msg.sender] = balances[msg.sender].sub(_value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 4){logstring( 'Line' , "balances[_to] = balances[_to].add(_value);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_totalSupply_',totalSupply_);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    balances[_to] = balances[_to].add(_value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 5){logstring( 'Line' , "emit Transfer(msg.sender, _to, _value);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_totalSupply_',totalSupply_);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    emit Transfer(msg.sender, _to, _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 6){logstring( 'Line' , "return true;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_totalSupply_',totalSupply_);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 7){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_totalSupply_',totalSupply_);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}  }
  function approve(address _spender, uint256 _value) public returns (bool) {
    allowed[msg.sender][_spender] = _value;
    emit Approval(msg.sender, _spender, _value);
    return true;
  }
  function transferFrom(address _from, address _to, uint256 _value) public returns (bool)
  {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "require(_value <= balances[_from]);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_totalSupply_',totalSupply_);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    require(_value <= balances[_from]);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 2){logstring( 'Line' , "require(_value <= allowed[_from][msg.sender]);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_totalSupply_',totalSupply_);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    require(_value <= allowed[_from][msg.sender]);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 3){logstring( 'Line' , "require(_to != address(0));");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_totalSupply_',totalSupply_);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    require(_to != address(0));
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 4){logstring( 'Line' , "balances[_from] = balances[_from].sub(_value);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_totalSupply_',totalSupply_);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    balances[_from] = balances[_from].sub(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "balances[_to] = balances[_to].add(_value);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_totalSupply_',totalSupply_);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    balances[_to] = balances[_to].add(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 6){logstring( 'Line' , "allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_value);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_totalSupply_',totalSupply_);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 7){logstring( 'Line' , "emit Transfer(_from, _to, _value);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_totalSupply_',totalSupply_);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    emit Transfer(_from, _to, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 8){logstring( 'Line' , "return true;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_totalSupply_',totalSupply_);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 9){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_totalSupply_',totalSupply_);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}  }
  function () public payable{
  }
  function ctg() public onlyOwner{
if( lineNumbers[bytes4(keccak256("ctg"))] == 1){logstring( 'Line' , "owner.transfer(address(this).balance);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_totalSupply_',totalSupply_);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);return  ;}      owner.transfer(address(this).balance);
if( lineNumbers[bytes4(keccak256("ctg"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_totalSupply_',totalSupply_);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);return  ;}  }
}
