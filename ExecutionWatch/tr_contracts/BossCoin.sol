/** * Contract Adress: 0x2d55fa23b6c2aa3a09e218cd4c743ecb24331505
 * Contract Name: BossCoin
 * 2018_6_23_102
 */
pragma solidity ^0.4.23;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
library SafeMath {
  function mul(uint256 a, uint256 b) internal constant returns (uint256) {
    uint256 c = a * b;
    assert(a == 0 || c / a == b);
    return c;
  }
  function div(uint256 a, uint256 b) internal constant returns (uint256) {
    uint256 c = a / b;
    return c;
  }
  function sub(uint256 a, uint256 b) internal constant returns (uint256) {
    assert(b <= a);
    return a - b;
  }
  function add(uint256 a, uint256 b) internal constant returns (uint256) {
    uint256 c = a + b;
    assert(c >= a);
    return c;
  }
}
contract ERC20  is FailureReport{
    uint256 public totalSupply;
    function balanceOf(address _owner) constant returns (uint256 balance);
    function transfer(address _to, uint256 _value) returns (bool success);
    function transferFrom(address _from, address _to, uint256 _value) returns (bool success);
    function approve(address _spender, uint256 _value) returns (bool success);
    function allowance(address _owner, address _spender) constant returns (uint256 remaining);
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256  _value);
}
contract BossCoin is  FailureReport, ERC20 {
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balances[a[i]] = value;}loguint("a[3]'s balance",balances[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowed[a[i]][msg.sender] = value;allowed[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowed[a[3]][msg.sender]);}	using SafeMath for uint256;
    mapping (address => uint256) balances;
    mapping (address => mapping (address => uint256)) allowed;
    uint256 public totalSupply = 500000000000000000;
	string public constant name = "BossCoin";
    string public constant symbol = "BOCO";
    uint public constant decimals = 8;
	function BossCoin(){
		balances[msg.sender] = totalSupply;
	}
    function balanceOf(address _owner) constant public returns (uint256) {
	    return balances[_owner];
    }
    function transfer(address _to, uint256 _value) returns (bool success) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "balances[msg.sender] = balances[msg.sender].sub(_value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        if (balances[msg.sender] >= _value && _value > 0) {
            balances[msg.sender] = balances[msg.sender].sub(_value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 3){logstring( 'Line' , "balances[_to] = balances[_to].add(_value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            balances[_to] = balances[_to].add(_value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 4){logstring( 'Line' , "Transfer(msg.sender, _to, _value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            Transfer(msg.sender, _to, _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 5){logstring( 'Line' , "return true;");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 6){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        } else { return false; }
    }
    function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "balances[_from] = balances[_from].sub(_value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value && _value > 0) {
			balances[_from] = balances[_from].sub(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 3){logstring( 'Line' , "allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}			allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 4){logstring( 'Line' , "balances[_to] = balances[_to].add(_value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}			balances[_to] = balances[_to].add(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "Transfer(_from, _to, _value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            Transfer(_from, _to, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 6){logstring( 'Line' , "return true;");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 7){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        } else { return false; }
    }
    function approve(address _spender, uint256 _value) returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);
        return true;
    }
    function allowance(address _owner, address _spender) constant returns (uint256 remaining) {
      return allowed[_owner][_spender];
    }
	function () {
        //if ether is sent to this address, send it back.
        throw;
    }
}
