/** * Contract Adress: 0xbad8fa6da1017b3e3ea3c7dd7d66615f739e6300
 * Contract Name: CheersWangToken
 * 2018_8_21_86
 */
pragma solidity 0.4.24;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract Token  is FailureReport{
    /// @return total amount of tokens
    function totalSupply() constant returns (uint supply) {}
    /// @param _owner The address from which the balance will be retrieved
    /// @return The balance
    function balanceOf(address _owner) constant returns (uint balance) {}
    /// @notice send `_value` token to `_to` from `msg.sender`
    /// @param _to The address of the recipient
    /// @param _value The amount of token to be transferred
    /// @return Whether the transfer was successful or not
    function transfer(address _to, uint _value) returns (bool success) {}
    /// @notice send `_value` token to `_to` from `_from` on the condition it is approved by `_from`
    /// @param _from The address of the sender
    /// @param _to The address of the recipient
    /// @param _value The amount of token to be transferred
    /// @return Whether the transfer was successful or not
    function transferFrom(address _from, address _to, uint _value) returns (bool success) {}
    /// @notice `msg.sender` approves `_addr` to spend `_value` tokens
    /// @param _spender The address of the account able to transfer the tokens
    /// @param _value The amount of wei to be approved for transfer
    /// @return Whether the approval was successful or not
    function approve(address _spender, uint _value) returns (bool success) {}
    /// @param _owner The address of the account owning tokens
    /// @param _spender The address of the account able to transfer the tokens
    /// @return Amount of remaining tokens allowed to spent
    function allowance(address _owner, address _spender) constant returns (uint remaining) {}
    event Transfer(address indexed _from, address indexed _to, uint _value);
    event Approval(address indexed _owner, address indexed _spender, uint _value);
}
contract RegularToken is  FailureReport, Token {
    function transfer(address _to, uint _value) returns (bool) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "balances[msg.sender] -= _value;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        //Default assumes totalSupply can't be over max (2^256 - 1).
        if (balances[msg.sender] >= _value && balances[_to] + _value >= balances[_to]) {
            balances[msg.sender] -= _value;
if( lineNumbers[bytes4(keccak256("transfer"))] == 4){logstring( 'Line' , "balances[_to] += _value;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            balances[_to] += _value;
if( lineNumbers[bytes4(keccak256("transfer"))] == 5){logstring( 'Line' , "Transfer(msg.sender, _to, _value);");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            Transfer(msg.sender, _to, _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 6){logstring( 'Line' , "return true;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 7){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        } else { return false; }
    }
    function transferFrom(address _from, address _to, uint _value) returns (bool) {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "balances[_to] += _value;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value && balances[_to] + _value >= balances[_to]) {
            balances[_to] += _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 3){logstring( 'Line' , "balances[_from] -= _value;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            balances[_from] -= _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 4){logstring( 'Line' , "allowed[_from][msg.sender] -= _value;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            allowed[_from][msg.sender] -= _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "Transfer(_from, _to, _value);");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            Transfer(_from, _to, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 6){logstring( 'Line' , "return true;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 7){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        } else { return false; }
    }
    function balanceOf(address _owner) constant returns (uint) {
        return balances[_owner];
    }
    function approve(address _spender, uint _value) returns (bool) {
        allowed[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);
        return true;
    }
    function allowance(address _owner, address _spender) constant returns (uint) {
        return allowed[_owner][_spender];
    }
    mapping (address => uint) balances;
    mapping (address => mapping (address => uint)) allowed;
    uint public totalSupply;
}
contract UnboundedRegularToken is  FailureReport, RegularToken {
    uint constant MAX_UINT = 2**256 - 1;
    /// @dev ERC20 transferFrom, modified such that an allowance of MAX_UINT represents an unlimited amount.
    /// @param _from Address to transfer from.
    /// @param _to Address to transfer to.
    /// @param _value Amount to transfer.
    /// @return Success of transfer.
    function transferFrom(address _from, address _to, uint _value)
        public
        returns (bool)
    {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "uint allowance = allowed[_from][msg.sender];");loguint('BV_now',now);loguint('GV_MAX_UINT',MAX_UINT);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        uint allowance = allowed[_from][msg.sender];
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 2){logstring( 'Line' , "balances[_to] += _value;");loguint('BV_now',now);loguint('GV_MAX_UINT',MAX_UINT);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);loguint('LV_allowance',allowance);return  ;}        if (balances[_from] >= _value
            && allowance >= _value
            && balances[_to] + _value >= balances[_to]
        ) {
            balances[_to] += _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 7){logstring( 'Line' , "balances[_from] -= _value;");loguint('BV_now',now);loguint('GV_MAX_UINT',MAX_UINT);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);loguint('LV_allowance',allowance);return  ;}            balances[_from] -= _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 8){logstring( 'Line' , "allowed[_from][msg.sender] -= _value;");loguint('BV_now',now);loguint('GV_MAX_UINT',MAX_UINT);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);loguint('LV_allowance',allowance);return  ;}            if (allowance < MAX_UINT) {
                allowed[_from][msg.sender] -= _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 10){logstring( 'Line' , "Transfer(_from, _to, _value);");loguint('BV_now',now);loguint('GV_MAX_UINT',MAX_UINT);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);loguint('LV_allowance',allowance);return  ;}            }
            Transfer(_from, _to, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 12){logstring( 'Line' , "return true;");loguint('BV_now',now);loguint('GV_MAX_UINT',MAX_UINT);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);loguint('LV_allowance',allowance);return  ;}            return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 13){logstring( 'Line' , "return false;");loguint('BV_now',now);loguint('GV_MAX_UINT',MAX_UINT);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);loguint('LV_allowance',allowance);return  ;}        } else {
            return false;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 15){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_MAX_UINT',MAX_UINT);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);loguint('LV_allowance',allowance);return  ;}        }
    }
}
contract CheersWangToken is  FailureReport, UnboundedRegularToken {
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balances[a[i]] = value;}loguint("a[3]'s balance",balances[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowed[a[i]][msg.sender] = value;allowed[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowed[a[3]][msg.sender]);}    uint public totalSupply = 7700000000000000000000000000;
    uint8 constant public decimals = 18;
    string constant public name = "CheersWangToken";
    string constant public symbol = "CWT";
    function CheersWangToken() {
        balances[msg.sender] = totalSupply;
        Transfer(address(0), msg.sender, totalSupply);
    }
}
