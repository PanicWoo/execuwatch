/** * Contract Adress: 0x5a37ae1b841c0cd4c394d6c58c81c1a166f54053
 * Contract Name: AddressrResolver
 * 2018_8_31_60
 */
pragma solidity 0.4.24;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract AddressrResolver  is FailureReport{
    address public addr;
    address owner;
    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }
    constructor() public {
        owner = msg.sender;
    }
    function changeOwner(address newowner) external onlyOwner {
if( lineNumbers[bytes4(keccak256("changeOwner"))] == 1){logstring( 'Line' , "owner = newowner;");loguint('BV_now',now);logaddress('GV_addr',addr);logaddress('GV_owner',owner);logaddress('IV_newowner',newowner);return  ;}        owner = newowner;
if( lineNumbers[bytes4(keccak256("changeOwner"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_addr',addr);logaddress('GV_owner',owner);logaddress('IV_newowner',newowner);return  ;}    }
    function setAddr(address newaddr) external onlyOwner {
if( lineNumbers[bytes4(keccak256("setAddr"))] == 1){logstring( 'Line' , "addr = newaddr;");loguint('BV_now',now);logaddress('GV_addr',addr);logaddress('GV_owner',owner);logaddress('IV_newaddr',newaddr);return  ;}        addr = newaddr;
if( lineNumbers[bytes4(keccak256("setAddr"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_addr',addr);logaddress('GV_owner',owner);logaddress('IV_newaddr',newaddr);return  ;}    }
}
