/** * Contract Adress: 0x56b2926ce99ce497183d26eb976b51f3bb4a81d9
 * Contract Name: BitcoinDominatorERC20Token
 * 2018_9_19_18
 */
pragma solidity ^0.4.21;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract BitcoinDominatorERC20Token  is FailureReport{
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balanceOf[a[i]] = value;}loguint("a[3]'s balance",balanceOf[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowance[a[i]][msg.sender] = value;allowance[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowance[a[3]][msg.sender]);}    // Track how many tokens are owned by each address.
    mapping (address => uint256) public balanceOf;
    string public name = "Bitcoin Dominator";
    string public symbol = "BTCDM";
    uint8 public decimals = 18;
    uint256 public totalSupply = 21000000 * (uint256(10) ** decimals);
    event Transfer(address indexed from, address indexed to, uint256 value);
    function BitcoinDominatorERC20Token() public {
        // Initially assign all tokens to the contract's creator.
        balanceOf[msg.sender] = totalSupply;
        emit Transfer(address(0), msg.sender, totalSupply);
    }
    function transfer(address to, uint256 value) public returns (bool success) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "require(balanceOf[msg.sender] >= value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV_to',to);loguint('IV_value',value);return  ;}        require(balanceOf[msg.sender] >= value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 2){logstring( 'Line' , "balanceOf[msg.sender] -= value;  // deduct from sender's balance");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV_to',to);loguint('IV_value',value);return  ;}        balanceOf[msg.sender] -= value;  // deduct from sender's balance
if( lineNumbers[bytes4(keccak256("transfer"))] == 3){logstring( 'Line' , "balanceOf[to] += value;          // add to recipient's balance");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV_to',to);loguint('IV_value',value);return  ;}        balanceOf[to] += value;          // add to recipient's balance
if( lineNumbers[bytes4(keccak256("transfer"))] == 4){logstring( 'Line' , "emit Transfer(msg.sender, to, value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV_to',to);loguint('IV_value',value);return  ;}        emit Transfer(msg.sender, to, value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 5){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV_to',to);loguint('IV_value',value);return  ;}        return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 6){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV_to',to);loguint('IV_value',value);return  ;}    }
    event Approval(address indexed owner, address indexed spender, uint256 value);
    mapping(address => mapping(address => uint256)) public allowance;
    function approve(address spender, uint256 value)
        public
        returns (bool success)
    {
        allowance[msg.sender][spender] = value;
        emit Approval(msg.sender, spender, value);
        return true;
    }
    function transferFrom(address from, address to, uint256 value)
        public
        returns (bool success)
    {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "require(value <= balanceOf[from]);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV_from',from);logaddress('IV_to',to);loguint('IV_value',value);return  ;}        require(value <= balanceOf[from]);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 2){logstring( 'Line' , "require(value <= allowance[from][msg.sender]);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV_from',from);logaddress('IV_to',to);loguint('IV_value',value);return  ;}        require(value <= allowance[from][msg.sender]);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 3){logstring( 'Line' , "balanceOf[from] -= value;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV_from',from);logaddress('IV_to',to);loguint('IV_value',value);return  ;}        balanceOf[from] -= value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 4){logstring( 'Line' , "balanceOf[to] += value;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV_from',from);logaddress('IV_to',to);loguint('IV_value',value);return  ;}        balanceOf[to] += value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "allowance[from][msg.sender] -= value;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV_from',from);logaddress('IV_to',to);loguint('IV_value',value);return  ;}        allowance[from][msg.sender] -= value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 6){logstring( 'Line' , "emit Transfer(from, to, value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV_from',from);logaddress('IV_to',to);loguint('IV_value',value);return  ;}        emit Transfer(from, to, value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 7){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV_from',from);logaddress('IV_to',to);loguint('IV_value',value);return  ;}        return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 8){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV_from',from);logaddress('IV_to',to);loguint('IV_value',value);return  ;}    }
}
