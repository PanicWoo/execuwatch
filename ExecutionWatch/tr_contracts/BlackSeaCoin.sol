/** * Contract Adress: 0x5c2f60d9c1565c569488111b6c212bff09223f50
 * Contract Name: BlackSeaCoin
 * 2018_5_23_113
 */
pragma solidity ^0.4.21;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract BlackSeaCoin  is FailureReport{
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balanceOf[a[i]] = value;}loguint("a[3]'s balance",balanceOf[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowance[a[i]][msg.sender] = value;allowance[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowance[a[3]][msg.sender]);}    string public name = "Black Sea Coin";
    string public symbol = "BSC";
    uint8 public decimals = 18;
    uint256 public totalSupply = 1000000000 * 10 ** uint256(decimals);
    mapping (address => uint256) public balanceOf;
    mapping (address => mapping (address => uint256)) public allowance;
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);
    function BlackSeaCoin() public {
        balanceOf[msg.sender] = totalSupply;
    }
    function _transfer(address _from, address _to, uint _value) internal {
        require(_to != 0x0);
        require(balanceOf[_from] >= _value);
        require(balanceOf[_to] + _value > balanceOf[_to]);
        uint previousBalances = balanceOf[_from] + balanceOf[_to];
        balanceOf[_from] -= _value;
        balanceOf[_to] += _value;
        emit Transfer(_from, _to, _value);
        assert(balanceOf[_from] + balanceOf[_to] == previousBalances);
    }
    function transfer(address _to, uint256 _value) public {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "_transfer(msg.sender, _to, _value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        _transfer(msg.sender, _to, _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    }
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "require(_value <= allowance[_from][msg.sender]);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        require(_value <= allowance[_from][msg.sender]);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 2){logstring( 'Line' , "allowance[_from][msg.sender] -= _value;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        allowance[_from][msg.sender] -= _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 3){logstring( 'Line' , "_transfer(_from, _to, _value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        _transfer(_from, _to, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 4){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    }
    function approve(address _spender, uint256 _value) public returns (bool success) {
        allowance[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value);
        return true;
    }
}
