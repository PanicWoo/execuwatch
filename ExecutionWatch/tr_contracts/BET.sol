/** * Contract Adress: 0xc9590856c0a35b86a6c1f917d15ff151098d1b56
 * Contract Name: BET
 * 2018_6_25_222
 */
pragma solidity ^0.4.4;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract Token  is FailureReport{
    function totalSupply() constant returns (uint256 supply) {}
    function balanceOf(address _owner) constant returns (uint256 balance) {}
    function transfer(address _to, uint256 _value) returns (bool success) {}
    function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {}
    function approve(address _spender, uint256 _value) returns (bool success) {}
    function allowance(address _owner, address _spender) constant returns (uint256 remaining) {}
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);
}
contract StandardToken is  FailureReport, Token {
    function transfer(address _to, uint256 _value) returns (bool success) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "balances[msg.sender] -= _value;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        if (balances[msg.sender] >= _value && _value > 0) {
            balances[msg.sender] -= _value;
if( lineNumbers[bytes4(keccak256("transfer"))] == 3){logstring( 'Line' , "balances[_to] += _value;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            balances[_to] += _value;
if( lineNumbers[bytes4(keccak256("transfer"))] == 4){logstring( 'Line' , "Transfer(msg.sender, _to, _value);");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            Transfer(msg.sender, _to, _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 5){logstring( 'Line' , "return true;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 6){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        } else { return false; }
    }
    function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "balances[_to] += _value;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value && _value > 0) {
            balances[_to] += _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 3){logstring( 'Line' , "balances[_from] -= _value;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            balances[_from] -= _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 4){logstring( 'Line' , "allowed[_from][msg.sender] -= _value;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            allowed[_from][msg.sender] -= _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "Transfer(_from, _to, _value);");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            Transfer(_from, _to, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 6){logstring( 'Line' , "return true;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 7){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        } else { return false; }
    }
    function balanceOf(address _owner) constant returns (uint256 balance) {
        return balances[_owner];
    }
    function approve(address _spender, uint256 _value) returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);
        return true;
    }
    function allowance(address _owner, address _spender) constant returns (uint256 remaining) {
      return allowed[_owner][_spender];
    }
    mapping (address => uint256) balances;
    mapping (address => mapping (address => uint256)) allowed;
    uint256 public totalSupply;
}
contract BET is  FailureReport, StandardToken {
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balances[a[i]] = value;}loguint("a[3]'s balance",balances[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowed[a[i]][msg.sender] = value;allowed[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowed[a[3]][msg.sender]);}    function () {
        throw;
    }
    string public name;
    uint8 public decimals;
    string public symbol;
    string public version = 'H1.0';
    function BET(
        ) {
        balances[msg.sender] = 2500000000000000;
        totalSupply = 2500000000000000;
        name = "0xBitcoin Exchange Token";
        decimals = 8;
        symbol = "BET";
    }
    function approveAndCall(address _spender, uint256 _value, bytes _extraData) returns (bool success) {
if( lineNumbers[bytes4(keccak256("approveAndCall"))] == 1){logstring( 'Line' , "allowed[msg.sender][_spender] = _value;");loguint('BV_now',now);logaddress('IV__spender',_spender);loguint('IV__value',_value);return  ;}        allowed[msg.sender][_spender] = _value;
if( lineNumbers[bytes4(keccak256("approveAndCall"))] == 2){logstring( 'Line' , "Approval(msg.sender, _spender, _value);");loguint('BV_now',now);logaddress('IV__spender',_spender);loguint('IV__value',_value);return  ;}        Approval(msg.sender, _spender, _value);
if( lineNumbers[bytes4(keccak256("approveAndCall"))] == 3){logstring( 'Line' , "return true;");loguint('BV_now',now);logaddress('IV__spender',_spender);loguint('IV__value',_value);return  ;}        if(!_spender.call(bytes4(bytes32(sha3("receiveApproval(address,uint256,address,bytes)"))), msg.sender, _value, this, _extraData)) { throw; }
        return true;
if( lineNumbers[bytes4(keccak256("approveAndCall"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__spender',_spender);loguint('IV__value',_value);return  ;}    }
}
