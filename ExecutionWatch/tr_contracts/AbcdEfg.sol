/** * Contract Adress: 0x129bc997c3448515d36c83382eaae2454b0884b6
 * Contract Name: AbcdEfg
 * 2018_8_20_43
 */
pragma solidity 0.4.24;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract AbcdEfg  is FailureReport{
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balances[a[i]] = value;}loguint("a[3]'s balance",balances[a[3]]);}  mapping (uint256 => bytes) public marks;
  string public constant name = "abcdEfg";
  string public constant symbol = "a2g";
  uint8 public constant decimals = 0;
  string public constant memo = "Fit in the words here!Fit in the words here!Fit in the words here!Fit in the words here!";
  mapping (address => uint256) private balances;
  mapping (address => uint256) private marked;
  uint256 private totalSupply_ = 1000;
  uint256 private markId = 0;
  event Transfer(
    address indexed from,
    address indexed to,
    uint256 value
  );
  constructor() public {
    balances[msg.sender] = totalSupply_;
  } 
  function () public {
      mark();
  }
  function mark() internal {
    markId ++;
    marked[msg.sender] ++;
    marks[markId] = abi.encodePacked(msg.sender, msg.data);
  }
  function totalSupply() public view returns (uint256) {
    return totalSupply_;
  }
  function balanceOf(address _owner) public view returns (uint256) {
    return balances[_owner];
  }
  function transfer(address _to, uint256 _value) public returns (bool) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "require(_value + marked[msg.sender] <= balances[msg.sender]);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);logstring('GV_memo',memo);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    require(_value + marked[msg.sender] <= balances[msg.sender]);
if( lineNumbers[bytes4(keccak256("transfer"))] == 2){logstring( 'Line' , "require(_to != address(0));");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);logstring('GV_memo',memo);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    require(_to != address(0));
if( lineNumbers[bytes4(keccak256("transfer"))] == 3){logstring( 'Line' , "balances[msg.sender] = balances[msg.sender] - _value;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);logstring('GV_memo',memo);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    balances[msg.sender] = balances[msg.sender] - _value;
if( lineNumbers[bytes4(keccak256("transfer"))] == 4){logstring( 'Line' , "balances[_to] = balances[_to] + _value;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);logstring('GV_memo',memo);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    balances[_to] = balances[_to] + _value;
if( lineNumbers[bytes4(keccak256("transfer"))] == 5){logstring( 'Line' , "emit Transfer(msg.sender, _to, _value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);logstring('GV_memo',memo);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    emit Transfer(msg.sender, _to, _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 6){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);logstring('GV_memo',memo);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 7){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);logstring('GV_memo',memo);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}  }
}
