/** * Contract Adress: 0xcaa18367c55c7fca5afeb71efa211eb1fbfa4ac0
 * Contract Name: BonusBank
 * 2018_12_4_90
 */
pragma solidity ^0.4.25;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract BonusBank  is FailureReport{
    address public owner;
    uint public limit;
    modifier isOwner() {
        require(msg.sender == owner);
        _;
    }
    event Deposit(address indexed from, uint value);
    function limitvalue(uint _limit) public {
if( lineNumbers[bytes4(keccak256("limitvalue"))] == 1){logstring( 'Line' , "require(_limit > 0);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_limit',limit);loguint('IV__limit',_limit);return  ;}        require(_limit > 0);
if( lineNumbers[bytes4(keccak256("limitvalue"))] == 2){logstring( 'Line' , "owner = msg.sender;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_limit',limit);loguint('IV__limit',_limit);return  ;}        owner = msg.sender;
if( lineNumbers[bytes4(keccak256("limitvalue"))] == 3){logstring( 'Line' , "limit = _limit ;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_limit',limit);loguint('IV__limit',_limit);return  ;}        limit = _limit ;
if( lineNumbers[bytes4(keccak256("limitvalue"))] == 4){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_limit',limit);loguint('IV__limit',_limit);return  ;}    }
    function deposit() public payable {
if( lineNumbers[bytes4(keccak256("deposit"))] == 1){logstring( 'Line' , "Deposit(msg.sender, msg.value);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_limit',limit);return  ;}        Deposit(msg.sender, msg.value);
if( lineNumbers[bytes4(keccak256("deposit"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_limit',limit);return  ;}    }
    function canDistribution() public constant returns (bool) {
if( lineNumbers[bytes4(keccak256("canDistribution"))] == 1){logstring( 'Line' , "return this.balance >= limit;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_limit',limit);return  ;}        return this.balance >= limit;
if( lineNumbers[bytes4(keccak256("canDistribution"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_limit',limit);return  ;}    }
    function getCurrentBalance() constant returns (uint) {
if( lineNumbers[bytes4(keccak256("getCurrentBalance"))] == 1){logstring( 'Line' , "return this.balance;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_limit',limit);return  ;}        return this.balance;
if( lineNumbers[bytes4(keccak256("getCurrentBalance"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_limit',limit);return  ;}    }
    function distribution() public isOwner {
if( lineNumbers[bytes4(keccak256("distribution"))] == 1){logstring( 'Line' , "require(canDistribution());");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_limit',limit);return  ;}        require(canDistribution());
if( lineNumbers[bytes4(keccak256("distribution"))] == 2){logstring( 'Line' , "owner.transfer(this.balance);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_limit',limit);return  ;}        owner.transfer(this.balance);
if( lineNumbers[bytes4(keccak256("distribution"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_limit',limit);return  ;}    }
}
