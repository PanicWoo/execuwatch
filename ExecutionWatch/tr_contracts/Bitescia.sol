/** * Contract Adress: 0x7eae79fa769bc57729739c42fcdb2076fdddd0a5
 * Contract Name: Bitescia
 * 2018_7_10_109
 */
pragma solidity ^0.4.4;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract Token  is FailureReport{
    /// @return total amount of tokens
    function totalSupply() constant returns (uint256 supply) {}
    /// @param _owner The address from which the balance will be retrieved
    /// @return The balance
    function balanceOf(address _owner) constant returns (uint256 balance) {}
    /// @notice send `_value` token to `_to` from `msg.sender`
    /// @param _to The address of the recipient
    /// @param _value The amount of token to be transferred
    /// @return Whether the transfer was successful or not
    function transfer(address _to, uint256 _value) returns (bool success) {}
    /// @notice send `_value` token to `_to` from `_from` on the condition it is approved by `_from`
    /// @param _from The address of the sender
    /// @param _to The address of the recipient
    /// @param _value The amount of token to be transferred
    /// @return Whether the transfer was successful or not
    function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {}
    /// @notice `msg.sender` approves `_addr` to spend `_value` tokens
    /// @param _spender The address of the account able to transfer the tokens
    /// @param _value The amount of wei to be approved for transfer
    /// @return Whether the approval was successful or not
    function approve(address _spender, uint256 _value) returns (bool success) {}
    /// @param _owner The address of the account owning tokens
    /// @param _spender The address of the account able to transfer the tokens
    /// @return Amount of remaining tokens allowed to spent
    function allowance(address _owner, address _spender) constant returns (uint256 remaining) {}
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);
}
contract StandardToken is  FailureReport, Token {
    function transfer(address _to, uint256 _value) returns (bool success) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "balances[msg.sender] -= _value;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        //Default assumes totalSupply can't be over max (2^256 - 1).
        //If your token leaves out totalSupply and can issue more tokens as time goes on, you need to check if it doesn't wrap.
        //Replace the if with this one instead.
        //if (balances[msg.sender] >= _value && balances[_to] + _value > balances[_to]) {
        if (balances[msg.sender] >= _value && _value > 0) {
            balances[msg.sender] -= _value;
if( lineNumbers[bytes4(keccak256("transfer"))] == 7){logstring( 'Line' , "balances[_to] += _value;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            balances[_to] += _value;
if( lineNumbers[bytes4(keccak256("transfer"))] == 8){logstring( 'Line' , "Transfer(msg.sender, _to, _value);");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            Transfer(msg.sender, _to, _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 9){logstring( 'Line' , "return true;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 10){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        } else { return false; }
    }
    function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "balances[_to] += _value;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        //same as above. Replace this line with the following if you want to protect against wrapping uints.
        //if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value && balances[_to] + _value > balances[_to]) {
        if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value && _value > 0) {
            balances[_to] += _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "balances[_from] -= _value;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            balances[_from] -= _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 6){logstring( 'Line' , "allowed[_from][msg.sender] -= _value;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            allowed[_from][msg.sender] -= _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 7){logstring( 'Line' , "Transfer(_from, _to, _value);");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            Transfer(_from, _to, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 8){logstring( 'Line' , "return true;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 9){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        } else { return false; }
    }
    function balanceOf(address _owner) constant returns (uint256 balance) {
        return balances[_owner];
    }
    function approve(address _spender, uint256 _value) returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);
        return true;
    }
    function allowance(address _owner, address _spender) constant returns (uint256 remaining) {
      return allowed[_owner][_spender];
    }
    mapping (address => uint256) balances;
    mapping (address => mapping (address => uint256)) allowed;
    uint256 public totalSupply;
}
contract Bitescia is  FailureReport, StandardToken { // CHANGE THIS. Update the contract name.
    /* Public variables of the token */
    /*
    NOTE:
    The following variables are OPTIONAL vanities. One does not have to include them.
    They allow one to customise the token contract & in no way influences the core functionality.
    Some wallets/interfaces might not even bother to look at this information.
    */
    string public name;                   // Token Name
    uint8 public decimals;                // How many decimals to show. To be standard complicant keep it 18
    string public symbol;                 // An identifier: eg SBX, XPR etc..
    string public version = 'H1.0'; 
    uint256 public unitsOneEthCanBuy;     // How many units of your coin can be bought by 1 ETH?
    uint256 public totalEthInWei;         // WEI is the smallest unit of ETH (the equivalent of cent in USD or satoshi in BTC). We'll store the total ETH raised via our ICO here.  
    address public fundsWallet;           // Where should the raised ETH go?
    // This is a constructor function 
    // which means the following function name has to match the contract name declared above
    function Bitescia() {
        balances[msg.sender] = 1000000000000000000000;               // Give the creator all initial tokens. This is set to 1000 for example. If you want your initial tokens to be X and your decimal is 5, set this value to X * 100000. (CHANGE THIS)
        totalSupply = 1000000000000000000000;                        // Update total supply (1000 for example) (CHANGE THIS)
        name = "Bitescia";                                   // Set the name for display purposes (CHANGE THIS)
        decimals = 10;                                               // Amount of decimals for display purposes (CHANGE THIS)
        symbol = "ITS";                                             // Set the symbol for display purposes (CHANGE THIS)
        unitsOneEthCanBuy = 10000000;                                      // Set the price of your token for the ICO (CHANGE THIS)
        fundsWallet = msg.sender;                                    // The owner of the contract gets ETH
    }
    function() payable{
        totalEthInWei = totalEthInWei + msg.value;
        uint256 amount = msg.value * unitsOneEthCanBuy;
        if (balances[fundsWallet] < amount) {
            return;
        }
        balances[fundsWallet] = balances[fundsWallet] - amount;
        balances[msg.sender] = balances[msg.sender] + amount;
        Transfer(fundsWallet, msg.sender, amount); // Broadcast a message to the blockchain
        //Transfer ether to fundsWallet
        fundsWallet.transfer(msg.value);                               
    }
    /* Approves and then calls the receiving contract */
    function approveAndCall(address _spender, uint256 _value, bytes _extraData) returns (bool success) {
if( lineNumbers[bytes4(keccak256("approveAndCall"))] == 1){logstring( 'Line' , "allowed[msg.sender][_spender] = _value;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logstring('GV_version',version);loguint('GV_unitsOneEthCanBuy',unitsOneEthCanBuy);loguint('GV_totalEthInWei',totalEthInWei);logaddress('GV_fundsWallet',fundsWallet);logaddress('IV__spender',_spender);loguint('IV__value',_value);return  ;}        allowed[msg.sender][_spender] = _value;
if( lineNumbers[bytes4(keccak256("approveAndCall"))] == 2){logstring( 'Line' , "Approval(msg.sender, _spender, _value);");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logstring('GV_version',version);loguint('GV_unitsOneEthCanBuy',unitsOneEthCanBuy);loguint('GV_totalEthInWei',totalEthInWei);logaddress('GV_fundsWallet',fundsWallet);logaddress('IV__spender',_spender);loguint('IV__value',_value);return  ;}        Approval(msg.sender, _spender, _value);
if( lineNumbers[bytes4(keccak256("approveAndCall"))] == 3){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logstring('GV_version',version);loguint('GV_unitsOneEthCanBuy',unitsOneEthCanBuy);loguint('GV_totalEthInWei',totalEthInWei);logaddress('GV_fundsWallet',fundsWallet);logaddress('IV__spender',_spender);loguint('IV__value',_value);return  ;}        //call the receiveApproval function on the contract you want to be notified. This crafts the function signature manually so one doesn't have to include a contract in here just for this.
        //receiveApproval(address _from, uint256 _value, address _tokenContract, bytes _extraData)
        //it is assumed that when does this that the call *should* succeed, otherwise one would use vanilla approve instead.
        if(!_spender.call(bytes4(bytes32(sha3("receiveApproval(address,uint256,address,bytes)"))), msg.sender, _value, this, _extraData)) { throw; }
        return true;
if( lineNumbers[bytes4(keccak256("approveAndCall"))] == 8){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logstring('GV_version',version);loguint('GV_unitsOneEthCanBuy',unitsOneEthCanBuy);loguint('GV_totalEthInWei',totalEthInWei);logaddress('GV_fundsWallet',fundsWallet);logaddress('IV__spender',_spender);loguint('IV__value',_value);return  ;}    }
}
