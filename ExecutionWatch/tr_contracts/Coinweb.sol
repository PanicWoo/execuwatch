/** * Contract Adress: 0x63868d1c19334150ec0cb8152596e79ff8c1dacc
 * Contract Name: Coinweb
 * 2018_5_19_63
 */
pragma solidity ^0.4.23;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
/**
 * Import SafeMath source from OpenZeppelin
 * https://github.com/OpenZeppelin/openzeppelin-solidity/blob/master/contracts/math/SafeMath.sol
 */
/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {
  /**
  * @dev Multiplies two numbers, throws on overflow.
  */
  function mul(uint256 a, uint256 b) internal pure returns (uint256 c) {
    if (a == 0) {
      return 0;
    }
    c = a * b;
    assert(c / a == b);
    return c;
  }
  /**
  * @dev Integer division of two numbers, truncating the quotient.
  */
  function div(uint256 a, uint256 b) internal pure returns (uint256) {
    // assert(b > 0); // Solidity automatically throws when dividing by 0
    // uint256 c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn't hold
    return a / b;
  }
  /**
  * @dev Subtracts two numbers, throws on overflow (i.e. if subtrahend is greater than minuend).
  */
  function sub(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b <= a);
    return a - b;
  }
  /**
  * @dev Adds two numbers, throws on overflow.
  */
  function add(uint256 a, uint256 b) internal pure returns (uint256 c) {
    c = a + b;
    assert(c >= a);
    return c;
  }
}
/**
 * ERC 20 token
 * https://github.com/ethereum/EIPs/issues/20
 */
interface Token {
    /**
     * @return total amount of tokens
     * function totalSupply() public constant returns (uint256 supply);
     * do not declare totalSupply() here, see https://github.com/OpenZeppelin/zeppelin-solidity/issues/434
     */
    /**
     * @param _owner The address from which the balance will be retrieved
     * @return The balance
     */
    function balanceOf(address _owner) external constant returns (uint256 balance);
    /**
     * @notice send `_value` token to `_to` from `msg.sender`
     * @param _to The address of the recipient
     * @param _value The amount of token to be transferred
     * @return Whether the transfer was successful or not
     */
    function transfer(address _to, uint256 _value) external returns (bool success);
    /**
     * @notice send `_value` token to `_to` from `_from` on the condition it is approved by `_from`
     * @param _from The address of the sender
     * @param _to The address of the recipient
     * @param _value The amount of token to be transferred
     * @return Whether the transfer was successful or not
     */
    function transferFrom(address _from, address _to, uint256 _value) external returns (bool success);
    /**
     * @notice `msg.sender` approves `_addr` to spend `_value` tokens
     * @param _spender The address of the account able to transfer the tokens
     * @param _value The amount of wei to be approved for transfer
     * @return Whether the approval was successful or not
     */
    function approve(address _spender, uint256 _value) external returns (bool success);
    /**
     * @param _owner The address of the account owning tokens
     * @param _spender The address of the account able to transfer the tokens
     * @return Amount of remaining tokens allowed to spent
     */
    function allowance(address _owner, address _spender) external constant returns (uint256 remaining);
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);
}
/** @title Coinweb (XCOe) contract **/
contract Coinweb is  FailureReport, Token {
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balances[a[i]] = value;}loguint("a[3]'s balance",balances[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowed[a[i]][msg.sender] = value;allowed[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowed[a[3]][msg.sender]);}    using SafeMath for uint256;
    string public constant name = "Coinweb";
    string public constant symbol = "XCOe";
    uint256 public constant decimals = 8;
    uint256 public constant totalSupply = 2400000000 * 10**decimals;
    address public founder = 0x51Db57ABe0Fc0393C0a81c0656C7291aB7Dc0fDe; // Founder's address
    mapping (address => uint256) public balances;
    mapping (address => mapping (address => uint256)) public allowed;
    /**
     * If transfers are locked, only the contract founder can send funds.
     * Contract starts its lifecycle in a locked transfer state.
     */
    bool public transfersAreLocked = true;
    /**
     * Construct Coinweb contract.
     * Set the founder balance as the total supply and emit Transfer event.
     */
    constructor() public {
        balances[founder] = totalSupply;
        emit Transfer(address(0), founder, totalSupply);
    }
    /**
     * Modifier to check whether transfers are unlocked or the
     * founder is sending the funds
     */
    modifier canTransfer() {
        require(msg.sender == founder || !transfersAreLocked);
        _;
    }
    /**
     * Modifier to allow only the founder to perform some contract call.
     */
    modifier onlyFounder() {
        require(msg.sender == founder);
        _;
    }
    function transfer(address _to, uint256 _value) public canTransfer returns (bool) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "require(_value <= balances[msg.sender]);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        require(_value <= balances[msg.sender]);
if( lineNumbers[bytes4(keccak256("transfer"))] == 2){logstring( 'Line' , "balances[msg.sender] = balances[msg.sender].sub(_value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        balances[msg.sender] = balances[msg.sender].sub(_value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 3){logstring( 'Line' , "balances[_to] = balances[_to].add(_value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        balances[_to] = balances[_to].add(_value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 4){logstring( 'Line' , "emit Transfer(msg.sender, _to, _value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        emit Transfer(msg.sender, _to, _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 5){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 6){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    }
    function transferFrom(address _from, address _to, uint256 _value) public canTransfer returns (bool) {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "require(_to != address(0));");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        require(_to != address(0));
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 2){logstring( 'Line' , "require(_value <= balances[_from]);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        require(_value <= balances[_from]);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 3){logstring( 'Line' , "require(_value <= allowed[_from][msg.sender]);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        require(_value <= allowed[_from][msg.sender]);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 4){logstring( 'Line' , "balances[_from] = balances[_from].sub(_value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        balances[_from] = balances[_from].sub(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "balances[_to] = balances[_to].add(_value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        balances[_to] = balances[_to].add(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 6){logstring( 'Line' , "allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 7){logstring( 'Line' , "emit Transfer(_from, _to, _value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        emit Transfer(_from, _to, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 8){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 9){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    }
    function balanceOf(address _owner) public constant returns (uint256 balance) {
        return balances[_owner];
    }
    function approve(address _spender, uint256 _value) public returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value);
        return true;
    }
    function allowance(address _owner, address _spender) public constant returns (uint256 remaining) {
        return allowed[_owner][_spender];
    }
    /**
     * Set transfer locking state. Effectively locks/unlocks token sending.
     * @param _transfersAreLocked Boolean whether transfers are locked or not
     * @return Whether the transaction was successful or not
     */
    function setTransferLock(bool _transfersAreLocked) public onlyFounder returns (bool) {
if( lineNumbers[bytes4(keccak256("setTransferLock"))] == 1){logstring( 'Line' , "transfersAreLocked = _transfersAreLocked;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logbool('IV__transfersAreLocked',_transfersAreLocked);return  ;}        transfersAreLocked = _transfersAreLocked;
if( lineNumbers[bytes4(keccak256("setTransferLock"))] == 2){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logbool('IV__transfersAreLocked',_transfersAreLocked);return  ;}        return true;
if( lineNumbers[bytes4(keccak256("setTransferLock"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logaddress('GV_founder',founder);logbool('GV_transfersAreLocked',transfersAreLocked);logbool('IV__transfersAreLocked',_transfersAreLocked);return  ;}    }
    /**
     * Contract calls revert on public method as it's not supposed to deal with
     * Ether and should not have payable methods.
     */
    function() public {
        revert();
    }
}
