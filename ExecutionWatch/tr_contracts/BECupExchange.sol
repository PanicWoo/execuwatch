/** * Contract Adress: 0x8240c54208b57df3aaf53476fb3752336dca53d3
 * Contract Name: BECupExchange
 * 2018_6_12_233
 */
pragma solidity ^0.4.21;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
// File: node_modules/openzeppelin-solidity/contracts/math/SafeMath.sol
/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {
  /**
  * @dev Multiplies two numbers, throws on overflow.
  */
  function mul(uint256 a, uint256 b) internal pure returns (uint256 c) {
    if (a == 0) {
      return 0;
    }
    c = a * b;
    assert(c / a == b);
    return c;
  }
  /**
  * @dev Integer division of two numbers, truncating the quotient.
  */
  function div(uint256 a, uint256 b) internal pure returns (uint256) {
    // assert(b > 0); // Solidity automatically throws when dividing by 0
    // uint256 c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn't hold
    return a / b;
  }
  /**
  * @dev Subtracts two numbers, throws on overflow (i.e. if subtrahend is greater than minuend).
  */
  function sub(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b <= a);
    return a - b;
  }
  /**
  * @dev Adds two numbers, throws on overflow.
  */
  function add(uint256 a, uint256 b) internal pure returns (uint256 c) {
    c = a + b;
    assert(c >= a);
    return c;
  }
}
// File: contracts/cupExchange/CupExchange.sol
interface token {
    function transfer(address receiver, uint amount) external returns(bool);
    function transferFrom(address from, address to, uint amount) external returns(bool);
    function allowance(address owner, address spender) external returns(uint256);
    function balanceOf(address owner) external returns(uint256);
}
contract CupExchange  is FailureReport{
    using SafeMath for uint256;
    using SafeMath for int256;
    address public owner;
    token internal teamCup;
    token internal cup;
    uint256 public exchangePrice; // with decimals
    bool public halting = true;
    event Halted(bool halting);
    event Exchange(address user, uint256 distributedAmount, uint256 collectedAmount);
    /**
     * Constructor function
     *
     * Setup the contract owner
     */
    constructor(address cupToken, address teamCupToken) public {
        owner = msg.sender;
        teamCup = token(teamCupToken);
        cup = token(cupToken);
    }
    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }
    /**
     * User exchange for team cup
     */
    function exchange() public {
if( lineNumbers[bytes4(keccak256("exchange"))] == 1){logstring( 'Line' , "require(msg.sender != address(0x0));");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);return  ;}        require(msg.sender != address(0x0));
if( lineNumbers[bytes4(keccak256("exchange"))] == 2){logstring( 'Line' , "require(msg.sender != address(this));");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);return  ;}        require(msg.sender != address(this));
if( lineNumbers[bytes4(keccak256("exchange"))] == 3){logstring( 'Line' , "require(!halting);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);return  ;}        require(!halting);
if( lineNumbers[bytes4(keccak256("exchange"))] == 4){logstring( 'Line' , "uint256 allowance = cup.allowance(msg.sender, this);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);return  ;}        // collect cup token
        uint256 allowance = cup.allowance(msg.sender, this);
if( lineNumbers[bytes4(keccak256("exchange"))] == 6){logstring( 'Line' , "require(allowance > 0);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);loguint('LV_allowance',allowance);return  ;}        require(allowance > 0);
if( lineNumbers[bytes4(keccak256("exchange"))] == 7){logstring( 'Line' , "require(cup.transferFrom(msg.sender, this, allowance));");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);loguint('LV_allowance',allowance);return  ;}        require(cup.transferFrom(msg.sender, this, allowance));
if( lineNumbers[bytes4(keccak256("exchange"))] == 8){logstring( 'Line' , "uint256 teamCupBalance = teamCup.balanceOf(address(this));");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);loguint('LV_allowance',allowance);return  ;}        // transfer team cup token
        uint256 teamCupBalance = teamCup.balanceOf(address(this));
if( lineNumbers[bytes4(keccak256("exchange"))] == 10){logstring( 'Line' , "uint256 teamCupAmount = allowance * exchangePrice;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);loguint('LV_allowance',allowance);loguint('LV_teamCupBalance',teamCupBalance);return  ;}        uint256 teamCupAmount = allowance * exchangePrice;
if( lineNumbers[bytes4(keccak256("exchange"))] == 11){logstring( 'Line' , "require(teamCupAmount <= teamCupBalance);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);loguint('LV_allowance',allowance);loguint('LV_teamCupBalance',teamCupBalance);loguint('LV_teamCupAmount',teamCupAmount);return  ;}        require(teamCupAmount <= teamCupBalance);
if( lineNumbers[bytes4(keccak256("exchange"))] == 12){logstring( 'Line' , "require(teamCup.transfer(msg.sender, teamCupAmount));");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);loguint('LV_allowance',allowance);loguint('LV_teamCupBalance',teamCupBalance);loguint('LV_teamCupAmount',teamCupAmount);return  ;}        require(teamCup.transfer(msg.sender, teamCupAmount));
if( lineNumbers[bytes4(keccak256("exchange"))] == 13){logstring( 'Line' , "emit Exchange(msg.sender, teamCupAmount, allowance);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);loguint('LV_allowance',allowance);loguint('LV_teamCupBalance',teamCupBalance);loguint('LV_teamCupAmount',teamCupAmount);return  ;}        emit Exchange(msg.sender, teamCupAmount, allowance);
if( lineNumbers[bytes4(keccak256("exchange"))] == 14){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);loguint('LV_allowance',allowance);loguint('LV_teamCupBalance',teamCupBalance);loguint('LV_teamCupAmount',teamCupAmount);return  ;}    }
    /**
     * Withdraw the funds
     */
    function safeWithdrawal(address safeAddress) public onlyOwner {
if( lineNumbers[bytes4(keccak256("safeWithdrawal"))] == 1){logstring( 'Line' , "require(safeAddress != address(0x0));");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);logaddress('IV_safeAddress',safeAddress);return  ;}        require(safeAddress != address(0x0));
if( lineNumbers[bytes4(keccak256("safeWithdrawal"))] == 2){logstring( 'Line' , "require(safeAddress != address(this));");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);logaddress('IV_safeAddress',safeAddress);return  ;}        require(safeAddress != address(this));
if( lineNumbers[bytes4(keccak256("safeWithdrawal"))] == 3){logstring( 'Line' , "uint256 balance = teamCup.balanceOf(address(this));");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);logaddress('IV_safeAddress',safeAddress);return  ;}        uint256 balance = teamCup.balanceOf(address(this));
if( lineNumbers[bytes4(keccak256("safeWithdrawal"))] == 4){logstring( 'Line' , "teamCup.transfer(safeAddress, balance);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);logaddress('IV_safeAddress',safeAddress);loguint('LV_balance',balance);return  ;}        teamCup.transfer(safeAddress, balance);
if( lineNumbers[bytes4(keccak256("safeWithdrawal"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);logaddress('IV_safeAddress',safeAddress);loguint('LV_balance',balance);return  ;}    }
    /**
    * Set finalPriceForThisCoin
    */
    function setExchangePrice(int256 price) public onlyOwner {
if( lineNumbers[bytes4(keccak256("setExchangePrice"))] == 1){logstring( 'Line' , "require(price > 0);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);logint('IV_price',price);return  ;}        require(price > 0);
if( lineNumbers[bytes4(keccak256("setExchangePrice"))] == 2){logstring( 'Line' , "exchangePrice = uint256(price);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);logint('IV_price',price);return  ;}        exchangePrice = uint256(price);
if( lineNumbers[bytes4(keccak256("setExchangePrice"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);logint('IV_price',price);return  ;}    }
    function halt() public onlyOwner {
if( lineNumbers[bytes4(keccak256("halt"))] == 1){logstring( 'Line' , "halting = true;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);return  ;}        halting = true;
if( lineNumbers[bytes4(keccak256("halt"))] == 2){logstring( 'Line' , "emit Halted(halting);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);return  ;}        emit Halted(halting);
if( lineNumbers[bytes4(keccak256("halt"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);return  ;}    }
    function unhalt() public onlyOwner {
if( lineNumbers[bytes4(keccak256("unhalt"))] == 1){logstring( 'Line' , "halting = false;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);return  ;}        halting = false;
if( lineNumbers[bytes4(keccak256("unhalt"))] == 2){logstring( 'Line' , "emit Halted(halting);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);return  ;}        emit Halted(halting);
if( lineNumbers[bytes4(keccak256("unhalt"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('GV_exchangePrice',exchangePrice);logbool('GV_halting',halting);return  ;}    }
}
// File: contracts/cupExchange/cupExchangeImpl/BECupExchange.sol
contract BECupExchange is CupExchange {
    address public cup = 0x0750167667190A7Cd06a1e2dBDd4006eD5b522Cc;
    address public teamCup = 0x5CEcFb553Be4f3B7695B7392D4fd4cDB999C1DeC;
    constructor() CupExchange(cup, teamCup) public {}
}
