/** * Contract Adress: 0xca4a7c3494761a22257b63beb160082ae048dd3d
 * Contract Name: Distribution
 * 2018_10_18_38
 */
pragma solidity ^0.4.24;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract ERC20  is FailureReport{
    function totalSupply() public view returns (uint256);
    function balanceOf(address _who) public view returns (uint256);
}
contract Distribution  is FailureReport{

event logholders(string,address[]);
event log_addresses(string,address[]);    address public owner;
    ERC20 public token;
    address[] holders;
    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }
    function transferOwnership(address _newOwner) external onlyOwner {
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 1){logstring( 'Line' , "require(_newOwner != address(0));");loguint('BV_now',now);logaddress('GV_owner',owner);emit logholders('GV_holders',holders);logaddress('IV__newOwner',_newOwner);return  ;}        require(_newOwner != address(0));
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 2){logstring( 'Line' , "owner = _newOwner;");loguint('BV_now',now);logaddress('GV_owner',owner);emit logholders('GV_holders',holders);logaddress('IV__newOwner',_newOwner);return  ;}        owner = _newOwner;
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);emit logholders('GV_holders',holders);logaddress('IV__newOwner',_newOwner);return  ;}    }
    constructor(address _ERC20) public {
        owner = msg.sender;
        token = ERC20(_ERC20);
    }
    function() external payable {
    }
    function payDividends(uint _value) external onlyOwner {
if( lineNumbers[bytes4(keccak256("payDividends"))] == 1){logstring( 'Line' , "holders[i].transfer(_value * token.balanceOf(holders[i]) / token.totalSupply());");loguint('BV_now',now);logaddress('GV_owner',owner);emit logholders('GV_holders',holders);loguint('IV__value',_value);return  ;}        for (uint i = 0; i < holders.length; i++) {
            holders[i].transfer(_value * token.balanceOf(holders[i]) / token.totalSupply());
if( lineNumbers[bytes4(keccak256("payDividends"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);emit logholders('GV_holders',holders);loguint('IV__value',_value);return  ;}        }
    }
    function addHolder(address _address) external onlyOwner {
if( lineNumbers[bytes4(keccak256("addHolder"))] == 1){logstring( 'Line' , "holders.push(_address);");loguint('BV_now',now);logaddress('GV_owner',owner);emit logholders('GV_holders',holders);logaddress('IV__address',_address);return  ;}        holders.push(_address);
if( lineNumbers[bytes4(keccak256("addHolder"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);emit logholders('GV_holders',holders);logaddress('IV__address',_address);return  ;}    }
    function addListOfHolders(address[] _addresses) external onlyOwner {
if( lineNumbers[bytes4(keccak256("addListOfHolders"))] == 1){logstring( 'Line' , "holders.push(_addresses[i]);");loguint('BV_now',now);logaddress('GV_owner',owner);emit logholders('GV_holders',holders);emit log_addresses('IV__addresses',_addresses);return  ;}        for (uint i = 0; i < _addresses.length; i++) {
            holders.push(_addresses[i]);
if( lineNumbers[bytes4(keccak256("addListOfHolders"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);emit logholders('GV_holders',holders);emit log_addresses('IV__addresses',_addresses);return  ;}        }
    }
    function emptyListOfHolders() external onlyOwner {
if( lineNumbers[bytes4(keccak256("emptyListOfHolders"))] == 1){logstring( 'Line' , "delete holders[i];");loguint('BV_now',now);logaddress('GV_owner',owner);emit logholders('GV_holders',holders);return  ;}        for (uint i = 0; i < holders.length; i++) {
            delete holders[i];
if( lineNumbers[bytes4(keccak256("emptyListOfHolders"))] == 3){logstring( 'Line' , "holders.length = 0;");loguint('BV_now',now);logaddress('GV_owner',owner);emit logholders('GV_holders',holders);return  ;}        }
        holders.length = 0;
if( lineNumbers[bytes4(keccak256("emptyListOfHolders"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);emit logholders('GV_holders',holders);return  ;}    }
    function getLengthOfList() external view returns(uint) {
if( lineNumbers[bytes4(keccak256("getLengthOfList"))] == 1){logstring( 'Line' , "return holders.length;");loguint('BV_now',now);logaddress('GV_owner',owner);emit logholders('GV_holders',holders);return  ;}        return holders.length;
if( lineNumbers[bytes4(keccak256("getLengthOfList"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);emit logholders('GV_holders',holders);return  ;}    }
    function getHolder(uint _number) external view returns(address) {
if( lineNumbers[bytes4(keccak256("getHolder"))] == 1){logstring( 'Line' , "return holders[_number];");loguint('BV_now',now);logaddress('GV_owner',owner);emit logholders('GV_holders',holders);loguint('IV__number',_number);return  ;}        return holders[_number];
if( lineNumbers[bytes4(keccak256("getHolder"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);emit logholders('GV_holders',holders);loguint('IV__number',_number);return  ;}    }
    function withdrawBalance() external onlyOwner {
if( lineNumbers[bytes4(keccak256("withdrawBalance"))] == 1){logstring( 'Line' , "owner.transfer(address(this).balance);");loguint('BV_now',now);logaddress('GV_owner',owner);emit logholders('GV_holders',holders);return  ;}        owner.transfer(address(this).balance);
if( lineNumbers[bytes4(keccak256("withdrawBalance"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);emit logholders('GV_holders',holders);return  ;}    }
}
