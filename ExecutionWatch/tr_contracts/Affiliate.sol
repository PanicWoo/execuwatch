/** * Contract Adress: 0x7172ca5b3764f61216b3280e63bbd6c3834aef73
 * Contract Name: Affiliate
 * 2018_6_25_96
 */
pragma solidity ^0.4.23;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
/**
 * @title Ownable
 * @dev The Ownable contract has an owner address, and provides basic authorization control
 * functions, this simplifies the implementation of "user permissions".
 */
contract Ownable  is FailureReport{
  address public owner;
  event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);
  /**
   * @dev The Ownable constructor sets the original `owner` of the contract to the sender
   * account.
   */
  constructor() public {
    owner = msg.sender;
  }
  /**
   * @dev Throws if called by any account other than the owner.
   */
  modifier onlyOwner() {
    require(msg.sender == owner);
    _;
  }
  /**
   * @dev Allows the current owner to transfer control of the contract to a newOwner.
   * @param newOwner The address to transfer ownership to.
   */
  function transferOwnership(address newOwner) public onlyOwner {
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 1){logstring( 'Line' , "require(newOwner != address(0));");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}    require(newOwner != address(0));
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 2){logstring( 'Line' , "emit OwnershipTransferred(owner, newOwner);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}    emit OwnershipTransferred(owner, newOwner);
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 3){logstring( 'Line' , "owner = newOwner;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}    owner = newOwner;
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 4){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}  }
}
/**
 * @title TokenContract
 * @dev Token contract interface with transfer and balanceOf functions which need to be implemented
 */
interface TokenContract {
  /**
   * @dev Transfer funds to recipient address
   * @param _recipient Recipients address
   * @param _amount Amount to transfer
   */
  function transfer(address _recipient, uint256 _amount) external returns (bool);
  /**
   * @dev Return balance of holders address
   * @param _holder Holders address
   */
  function balanceOf(address _holder) external view returns (uint256);
}
/**
 * @title Affiliate
 * @dev Affiliate contract collects and stores all affiliates and token earnings for each affiliate
 */
contract Affiliate is  FailureReport, Ownable {

event log_affiliates(string,address[]);
event log_amount(string,uint[]);  TokenContract public tkn;
  mapping (address => uint256) affiliates;
  /**
   * @dev Add affiliates in affiliate mapping
   * @param _affiliates List of all affiliates
   * @param _amount Amount earned
   */
  function addAffiliates(address[] _affiliates, uint256[] _amount) onlyOwner public {
if( lineNumbers[bytes4(keccak256("addAffiliates"))] == 1){logstring( 'Line' , "require(_affiliates.length > 0);");loguint('BV_now',now);logaddress('GV_owner',owner);emit log_affiliates('IV__affiliates',_affiliates);emit log_amount('IV__amount',_amount);return  ;}    require(_affiliates.length > 0);
if( lineNumbers[bytes4(keccak256("addAffiliates"))] == 2){logstring( 'Line' , "require(_affiliates.length == _amount.length);");loguint('BV_now',now);logaddress('GV_owner',owner);emit log_affiliates('IV__affiliates',_affiliates);emit log_amount('IV__amount',_amount);return  ;}    require(_affiliates.length == _amount.length);
if( lineNumbers[bytes4(keccak256("addAffiliates"))] == 3){logstring( 'Line' , "affiliates[_affiliates[i]] = _amount[i];");loguint('BV_now',now);logaddress('GV_owner',owner);emit log_affiliates('IV__affiliates',_affiliates);emit log_amount('IV__amount',_amount);return  ;}    for (uint256 i = 0; i < _affiliates.length; i++) {
      affiliates[_affiliates[i]] = _amount[i];
if( lineNumbers[bytes4(keccak256("addAffiliates"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);emit log_affiliates('IV__affiliates',_affiliates);emit log_amount('IV__amount',_amount);return  ;}    }
  }
  /**
   * @dev Claim reward collected through your affiliates
   */
  function claimReward() public {
if( lineNumbers[bytes4(keccak256("claimReward"))] == 1){logstring( 'Line' , "require(tkn.transfer(msg.sender, affiliates[msg.sender]));");loguint('BV_now',now);logaddress('GV_owner',owner);return  ;}    if (affiliates[msg.sender] > 0) {
      require(tkn.transfer(msg.sender, affiliates[msg.sender]));
if( lineNumbers[bytes4(keccak256("claimReward"))] == 3){logstring( 'Line' , "affiliates[msg.sender] = 0;");loguint('BV_now',now);logaddress('GV_owner',owner);return  ;}      affiliates[msg.sender] = 0;
if( lineNumbers[bytes4(keccak256("claimReward"))] == 4){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);return  ;}    }
  }
  /**
   * @dev Terminate the Affiliate contract and destroy it
   */
  function terminateContract() onlyOwner public {
if( lineNumbers[bytes4(keccak256("terminateContract"))] == 1){logstring( 'Line' , "uint256 amount = tkn.balanceOf(address(this));");loguint('BV_now',now);logaddress('GV_owner',owner);return  ;}    uint256 amount = tkn.balanceOf(address(this));
if( lineNumbers[bytes4(keccak256("terminateContract"))] == 2){logstring( 'Line' , "require(tkn.transfer(owner, amount));");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('LV_amount',amount);return  ;}    require(tkn.transfer(owner, amount));
if( lineNumbers[bytes4(keccak256("terminateContract"))] == 3){logstring( 'Line' , "selfdestruct(owner);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('LV_amount',amount);return  ;}    selfdestruct(owner);
if( lineNumbers[bytes4(keccak256("terminateContract"))] == 4){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('LV_amount',amount);return  ;}  }
}
