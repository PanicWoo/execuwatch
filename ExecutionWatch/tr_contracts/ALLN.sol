/** * Contract Adress: 0x28fb6531723ee54b1073f7f2beb7f2e3c74503bc
 * Contract Name: ALLN
 * 2018_10_17_64
 */
pragma solidity ^0.4.23;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
/**
 * @title SafeMath
 */
library SafeMath {
  function mul(uint256 a, uint256 b) internal pure returns (uint256) {
    if (a == 0) {
      return 0;
    }
    uint256 c = a * b;
    assert(c / a == b);
    return c;
  }
  function div(uint256 a, uint256 b) internal pure returns (uint256) {
    uint256 c = a / b;
    return c;
  }
  function sub(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b <= a);
    return a - b;
  }
  function add(uint256 a, uint256 b) internal pure returns (uint256) {
    uint256 c = a + b;
    assert(c >= a);
    return c;
  }
}
/**
 * @title ERC20 interface
 */
contract StandardToken  is FailureReport{
  function totalSupply() public view returns (uint256);
  function balanceOf(address _owner) public view returns (uint256);
  function transfer(address _to, uint256 _value) public returns (bool);
  function transferFrom(address _from, address _to, uint256 _value) public returns (bool);
  function approve(address _spender, uint256 _value) public returns (bool);
  function allowance(address _owner, address _spender) public view returns (uint256);
  event Transfer(address indexed _from, address indexed _to, uint256 _value);
  event Approval(address indexed _owner, address indexed _spender, uint256 _value);
}
/**
 * @title Owned
 */
contract Owned  is FailureReport{
  address public owner;
  constructor() public {
    owner = msg.sender;
  }
  modifier onlyOwner {
    require(msg.sender == owner);
    _;
  }
}
/**
 * @title ALLN Token
 */
contract ALLNToken is  FailureReport, StandardToken {
  using SafeMath for uint256;
  mapping(address => uint256) balances;
  mapping (address => mapping (address => uint256)) allowed;
  uint256 public totalToken;
  function transfer(address _to, uint256 _value) public returns (bool) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "require(balances[msg.sender] >= _value);");loguint('BV_now',now);loguint('GV_totalToken',totalToken);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    require(balances[msg.sender] >= _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 2){logstring( 'Line' , "balances[msg.sender] = balances[msg.sender].sub(_value);");loguint('BV_now',now);loguint('GV_totalToken',totalToken);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    balances[msg.sender] = balances[msg.sender].sub(_value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 3){logstring( 'Line' , "balances[_to] = balances[_to].add(_value);");loguint('BV_now',now);loguint('GV_totalToken',totalToken);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    balances[_to] = balances[_to].add(_value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 4){logstring( 'Line' , "emit Transfer(msg.sender, _to, _value);");loguint('BV_now',now);loguint('GV_totalToken',totalToken);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    emit Transfer(msg.sender, _to, _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 5){logstring( 'Line' , "return true;");loguint('BV_now',now);loguint('GV_totalToken',totalToken);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 6){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_totalToken',totalToken);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}  }
  function transferFrom(address _from, address _to, uint256 _value) public returns (bool) {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "require(balances[_from] >= _value);");loguint('BV_now',now);loguint('GV_totalToken',totalToken);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    require(balances[_from] >= _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 2){logstring( 'Line' , "require(allowed[_from][msg.sender] >= _value);");loguint('BV_now',now);loguint('GV_totalToken',totalToken);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    require(allowed[_from][msg.sender] >= _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 3){logstring( 'Line' , "balances[_from] = balances[_from].sub(_value);");loguint('BV_now',now);loguint('GV_totalToken',totalToken);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    balances[_from] = balances[_from].sub(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 4){logstring( 'Line' , "balances[_to] = balances[_to].add(_value);");loguint('BV_now',now);loguint('GV_totalToken',totalToken);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    balances[_to] = balances[_to].add(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_value);");loguint('BV_now',now);loguint('GV_totalToken',totalToken);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 6){logstring( 'Line' , "emit Transfer(_from, _to, _value);");loguint('BV_now',now);loguint('GV_totalToken',totalToken);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    emit Transfer(_from, _to, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 7){logstring( 'Line' , "return true;");loguint('BV_now',now);loguint('GV_totalToken',totalToken);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 8){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_totalToken',totalToken);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}  }
  function totalSupply() public view returns (uint256) {
    return totalToken;
  }
  function balanceOf(address _owner) public view returns (uint256) {
    return balances[_owner];
  }
  function approve(address _spender, uint256 _value) public returns (bool) {
    allowed[msg.sender][_spender] = _value;
    emit Approval(msg.sender, _spender, _value);
    return true;
  }
  function allowance(address _owner, address _spender) public view returns (uint256) {
    return allowed[_owner][_spender];
  }
}
/**
 * @title Airline & Life Networking Token
 */
contract ALLN is  FailureReport, ALLNToken, Owned {
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balances[a[i]] = value;}loguint("a[3]'s balance",balances[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowed[a[i]][msg.sender] = value;allowed[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowed[a[3]][msg.sender]);}  string  public constant name     = "Airline & Life Networking";
  string  public constant symbol   = "ALLN";
  uint256 public constant decimals = 18;
  uint256 public constant initialToken     = 3500000000 * (10 ** decimals);
  uint256 public constant publicToken      = initialToken * 100 / 100;
  address public constant lockoutAddress = 0x7B080EDe2f84240DE894fd2f35bCe464a5d67f4D;
  address public constant rescueAddress = 0xa1AA83108bf7B225b35260273899686eBF4839C7;
  uint256 public constant lockoutEndTime = 1569902400; // 2019-10-01 04:00:00 GMT
  mapping(address => bool) lockAddresses;
  constructor() public {
    totalToken     = initialToken;
    balances[msg.sender]         = publicToken;
    emit Transfer(0x0, msg.sender, publicToken);
    lockAddresses[lockoutAddress] = false;
  }
  modifier transferable(address _addr) {
    require(!lockAddresses[_addr]);
    _;
  }
  function unlock() public onlyOwner {
if( lineNumbers[bytes4(keccak256("unlock"))] == 1){logstring( 'Line' , "lockAddresses[lockoutAddress] = false;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_initialToken',initialToken);loguint('GV_publicToken',publicToken);logaddress('GV_lockoutAddress',lockoutAddress);logaddress('GV_rescueAddress',rescueAddress);loguint('GV_lockoutEndTime',lockoutEndTime);loguint('GV_totalToken',totalToken);logaddress('GV_owner',owner);return  ;}   //  if (lockAddresses[lockoutAddress] && now >= lockoutEndTime)
    if (lockAddresses[lockoutAddress])
      lockAddresses[lockoutAddress] = false;
if( lineNumbers[bytes4(keccak256("unlock"))] == 4){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_initialToken',initialToken);loguint('GV_publicToken',publicToken);logaddress('GV_lockoutAddress',lockoutAddress);logaddress('GV_rescueAddress',rescueAddress);loguint('GV_lockoutEndTime',lockoutEndTime);loguint('GV_totalToken',totalToken);logaddress('GV_owner',owner);return  ;}  }
  function transfer(address _to, uint256 _value) public transferable(msg.sender) returns (bool) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "return super.transfer(_to, _value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_initialToken',initialToken);loguint('GV_publicToken',publicToken);logaddress('GV_lockoutAddress',lockoutAddress);logaddress('GV_rescueAddress',rescueAddress);loguint('GV_lockoutEndTime',lockoutEndTime);loguint('GV_totalToken',totalToken);logaddress('GV_owner',owner);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    return super.transfer(_to, _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_initialToken',initialToken);loguint('GV_publicToken',publicToken);logaddress('GV_lockoutAddress',lockoutAddress);logaddress('GV_rescueAddress',rescueAddress);loguint('GV_lockoutEndTime',lockoutEndTime);loguint('GV_totalToken',totalToken);logaddress('GV_owner',owner);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}  }
  function approve(address _spender, uint256 _value) public transferable(msg.sender) returns (bool) {
    return super.approve(_spender, _value);
  }
  function transferFrom(address _from, address _to, uint256 _value) public transferable(_from) returns (bool) {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "return super.transferFrom(_from, _to, _value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_initialToken',initialToken);loguint('GV_publicToken',publicToken);logaddress('GV_lockoutAddress',lockoutAddress);logaddress('GV_rescueAddress',rescueAddress);loguint('GV_lockoutEndTime',lockoutEndTime);loguint('GV_totalToken',totalToken);logaddress('GV_owner',owner);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    return super.transferFrom(_from, _to, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_initialToken',initialToken);loguint('GV_publicToken',publicToken);logaddress('GV_lockoutAddress',lockoutAddress);logaddress('GV_rescueAddress',rescueAddress);loguint('GV_lockoutEndTime',lockoutEndTime);loguint('GV_totalToken',totalToken);logaddress('GV_owner',owner);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}  }
  function transferAnyERC20Token(address _tokenAddress, uint256 _value) public onlyOwner returns (bool) {
if( lineNumbers[bytes4(keccak256("transferAnyERC20Token"))] == 1){logstring( 'Line' , "return ALLNToken(_tokenAddress).transfer(rescueAddress, _value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_initialToken',initialToken);loguint('GV_publicToken',publicToken);logaddress('GV_lockoutAddress',lockoutAddress);logaddress('GV_rescueAddress',rescueAddress);loguint('GV_lockoutEndTime',lockoutEndTime);loguint('GV_totalToken',totalToken);logaddress('GV_owner',owner);logaddress('IV__tokenAddress',_tokenAddress);loguint('IV__value',_value);return  ;}    return ALLNToken(_tokenAddress).transfer(rescueAddress, _value);
if( lineNumbers[bytes4(keccak256("transferAnyERC20Token"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_initialToken',initialToken);loguint('GV_publicToken',publicToken);logaddress('GV_lockoutAddress',lockoutAddress);logaddress('GV_rescueAddress',rescueAddress);loguint('GV_lockoutEndTime',lockoutEndTime);loguint('GV_totalToken',totalToken);logaddress('GV_owner',owner);logaddress('IV__tokenAddress',_tokenAddress);loguint('IV__value',_value);return  ;}  }
}
