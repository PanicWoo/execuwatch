/** * Contract Adress: 0x7c6546bea18e7d3f734b16ceb76950cb47c55fc5
 * Contract Name: BatchAirDrop
 * 2018_5_15_10
 */
pragma solidity ^0.4.23;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract MintableTokenIface  is FailureReport{
    function mint(address beneficiary, uint256 amount) public returns (bool);
    function transfer(address to, uint256 value) public returns (bool);
}
contract BatchAirDrop  is FailureReport{

event logwallets(string,address[]);    MintableTokenIface public token;
    address public owner;
    constructor(MintableTokenIface _token) public {
        owner = msg.sender;
        token = _token;
    }
    function batchSend(uint256 amount, address[] wallets) public {
if( lineNumbers[bytes4(keccak256("batchSend"))] == 1){logstring( 'Line' , "require(msg.sender == owner);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('IV_amount',amount);emit logwallets('IV_wallets',wallets);return  ;}        require(msg.sender == owner);
if( lineNumbers[bytes4(keccak256("batchSend"))] == 2){logstring( 'Line' , "require(amount != 0);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('IV_amount',amount);emit logwallets('IV_wallets',wallets);return  ;}        require(amount != 0);
if( lineNumbers[bytes4(keccak256("batchSend"))] == 3){logstring( 'Line' , "token.transfer(wallets[i], amount);");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('IV_amount',amount);emit logwallets('IV_wallets',wallets);return  ;}        for (uint256 i = 0; i < wallets.length; i++) {
            token.transfer(wallets[i], amount);
if( lineNumbers[bytes4(keccak256("batchSend"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);loguint('IV_amount',amount);emit logwallets('IV_wallets',wallets);return  ;}        }
    }
}
