/** * Contract Adress: 0x0d689d1d3ef39e379329d9fc88eabceccc1215a0
 * Contract Name: BuyZygoma
 * 2018_9_21_99
 */
pragma solidity ^0.4.25;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
library SafeMath {
    function add(uint a, uint b) internal pure returns (uint c) {
        c = a + b;
        require(c >= a);
    }
    function sub(uint a, uint b) internal pure returns (uint c) {
        require(b <= a);
        c = a - b;
    }
    function mul(uint a, uint b) internal pure returns (uint c) {
        c = a * b;
        require(a == 0 || c / a == b);
    }
    function div(uint a, uint b) internal pure returns (uint c) {
        require(b > 0);
        c = a / b;
    }
}
contract ERC20Interface  is FailureReport{
    function name() public constant returns (string);
    function symbol() public constant returns (string);
    function decimals() public constant returns (uint8);
    function totalSupply() public constant returns (uint);
    function balanceOf(address tokenOwner) public constant returns (uint balance);
    function allowance(address tokenOwner, address spender) public constant returns (uint remaining);
    function transfer(address to, uint tokens) public returns (bool success);
    function approve(address spender, uint tokens) public returns (bool success);
    function transferFrom(address from, address to, uint tokens) public returns (bool success);
    event Transfer(address indexed from, address indexed to, uint tokens);
    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);
}
contract Owned  is FailureReport{
    address public owner;
    address public newOwner;
    event OwnershipTransferred(address indexed _from, address indexed _to);
    function Owned() public {
        owner = msg.sender;
    }
    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }
    function transferOwnership(address _newOwner) public onlyOwner {
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 1){logstring( 'Line' , "newOwner = _newOwner;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV__newOwner',_newOwner);return  ;}        newOwner = _newOwner;
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV__newOwner',_newOwner);return  ;}    }
    function acceptOwnership() public {
if( lineNumbers[bytes4(keccak256("acceptOwnership"))] == 1){logstring( 'Line' , "require(msg.sender == newOwner);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);return  ;}        require(msg.sender == newOwner);
if( lineNumbers[bytes4(keccak256("acceptOwnership"))] == 2){logstring( 'Line' , "OwnershipTransferred(owner, newOwner);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);return  ;}        OwnershipTransferred(owner, newOwner);
if( lineNumbers[bytes4(keccak256("acceptOwnership"))] == 3){logstring( 'Line' , "owner = newOwner;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);return  ;}        owner = newOwner;
if( lineNumbers[bytes4(keccak256("acceptOwnership"))] == 4){logstring( 'Line' , "newOwner = address(0);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);return  ;}        newOwner = address(0);
if( lineNumbers[bytes4(keccak256("acceptOwnership"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);return  ;}    }
}
contract ZygStop is  FailureReport, Owned {
    bool public stopped = false;
    modifier stoppable {
        assert (!stopped);
        _;
    }
    function stop() public onlyOwner {
if( lineNumbers[bytes4(keccak256("stop"))] == 1){logstring( 'Line' , "stopped = true;");loguint('BV_now',now);logbool('GV_stopped',stopped);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);return  ;}        stopped = true;
if( lineNumbers[bytes4(keccak256("stop"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logbool('GV_stopped',stopped);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);return  ;}    }
    function start() public onlyOwner {
if( lineNumbers[bytes4(keccak256("start"))] == 1){logstring( 'Line' , "stopped = false;");loguint('BV_now',now);logbool('GV_stopped',stopped);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);return  ;}        stopped = false;
if( lineNumbers[bytes4(keccak256("start"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logbool('GV_stopped',stopped);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);return  ;}    }
}
contract Utils  is FailureReport{
    function Utils() internal {
    }
    modifier validAddress(address _address) {
        require(_address != 0x0);
        _;
    }
    modifier notThis(address _address) {
        require(_address != address(this));
        _;
    }
}
contract BuyZygoma is  FailureReport, Owned, ZygStop, Utils {
    using SafeMath for uint;
    ERC20Interface public zygomaAddress;
    function BuyZygoma(ERC20Interface _zygomaAddress) public{
        zygomaAddress = _zygomaAddress;
    }
    function withdrawTo(address to, uint amount)
        public onlyOwner
        notThis(to)
    {   
if( lineNumbers[bytes4(keccak256("withdrawTo"))] == 1){logstring( 'Line' , "require(amount <= this.balance);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logbool('GV_stopped',stopped);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV_to',to);loguint('IV_amount',amount);return  ;}        require(amount <= this.balance);
if( lineNumbers[bytes4(keccak256("withdrawTo"))] == 2){logstring( 'Line' , "to.transfer(amount);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logbool('GV_stopped',stopped);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV_to',to);loguint('IV_amount',amount);return  ;}        to.transfer(amount);
if( lineNumbers[bytes4(keccak256("withdrawTo"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logbool('GV_stopped',stopped);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV_to',to);loguint('IV_amount',amount);return  ;}    }
    function withdrawERC20TokenTo(ERC20Interface token, address to, uint amount)
        public onlyOwner
        validAddress(token)
        validAddress(to)
        notThis(to)
    {
if( lineNumbers[bytes4(keccak256("withdrawERC20TokenTo"))] == 1){logstring( 'Line' , "assert(token.transfer(to, amount));");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logbool('GV_stopped',stopped);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV_to',to);loguint('IV_amount',amount);return  ;}        assert(token.transfer(to, amount));
if( lineNumbers[bytes4(keccak256("withdrawERC20TokenTo"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logbool('GV_stopped',stopped);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV_to',to);loguint('IV_amount',amount);return  ;}    }
    function buyToken() internal
    {
        require(!stopped && msg.value >= 0.001 ether);
        uint amount = msg.value * 350000;
        assert(zygomaAddress.transfer(msg.sender, amount));
    }
    function() public payable stoppable {
        buyToken();
    }
}
