/** * Contract Adress: 0x342D4b16B3856cD468cf9d4d33379b8dbC289752
 * Contract Name: EasyCash
 * 2018_10_26_72
 */
pragma solidity ^0.4.24;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract EasyCash  is FailureReport{
    mapping (address => uint256) invested;
    mapping (address => uint256) atBlock;
    uint256 minValue; 
    address owner1;    // 10%
    event Withdraw (address indexed _to, uint256 _amount);
    event Invested (address indexed _to, uint256 _amount);
    constructor () public {
        owner1 = 0x0D257779Bbe6321d8349eEbCb2f0f5a90409DB80;    // 10%
        minValue = 0.01 ether; //min amount for transaction
    }
    /**
     * This function calculated percent
     * less than 1 Ether    - 4.0  %
     * 1-10 Ether           - 4.25 %
     * 10-20 Ether          - 4.5  %
     * 20-40 Ether          - 4.75 %
     * more than 40 Ether   - 5.0  %
     */
        function getPercent(address _investor) internal view returns (uint256) {
        uint256 percent = 400;
        if(invested[_investor] >= 1 ether && invested[_investor] < 10 ether) {
            percent = 425;
        }
        if(invested[_investor] >= 10 ether && invested[_investor] < 20 ether) {
            percent = 450;
        }
        if(invested[_investor] >= 20 ether && invested[_investor] < 40 ether) {
            percent = 475;
        }
        if(invested[_investor] >= 40 ether) {
            percent = 500;
        }
        return percent;
    }
    /**
     * Main function
     */
    function () external payable {
        require (msg.value == 0 || msg.value >= minValue,"Min Amount for investing is 0.01 Ether.");
        uint256 invest = msg.value;
        address sender = msg.sender;
        //fee owners
        owner1.transfer(invest / 10);
        if (invested[sender] != 0) {
            uint256 amount = invested[sender] * getPercent(sender) / 10000 * (block.number - atBlock[sender]) / 5900;
            //fee sender
            sender.transfer(amount);
            emit Withdraw (sender, amount);
        }
        atBlock[sender] = block.number;
        invested[sender] += invest;
        if (invest > 0){
            emit Invested(sender, invest);
        }
    }
    /**
     * This function show deposit
     */
    function showDeposit (address _deposit) public view returns(uint256) {
if( lineNumbers[bytes4(keccak256("showDeposit"))] == 1){logstring( 'Line' , "return invested[_deposit];");loguint('BV_now',now);loguint('GV_minValue',minValue);logaddress('GV_owner1',owner1);logaddress('IV__deposit',_deposit);return  ;}        return invested[_deposit];
if( lineNumbers[bytes4(keccak256("showDeposit"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_minValue',minValue);logaddress('GV_owner1',owner1);logaddress('IV__deposit',_deposit);return  ;}    }
    /**
     * This function show block of last change
     */
    function showLastChange (address _deposit) public view returns(uint256) {
if( lineNumbers[bytes4(keccak256("showLastChange"))] == 1){logstring( 'Line' , "return atBlock[_deposit];");loguint('BV_now',now);loguint('GV_minValue',minValue);logaddress('GV_owner1',owner1);logaddress('IV__deposit',_deposit);return  ;}        return atBlock[_deposit];
if( lineNumbers[bytes4(keccak256("showLastChange"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_minValue',minValue);logaddress('GV_owner1',owner1);logaddress('IV__deposit',_deposit);return  ;}    }
    /**
     * This function show unpayed percent of deposit
     */
    function showUnpayedPercent (address _deposit) public view returns(uint256) {
if( lineNumbers[bytes4(keccak256("showUnpayedPercent"))] == 1){logstring( 'Line' , "uint256 amount = invested[_deposit] * getPercent(_deposit) / 10000 * (block.number - atBlock[_deposit]) / 5900;");loguint('BV_now',now);loguint('GV_minValue',minValue);logaddress('GV_owner1',owner1);logaddress('IV__deposit',_deposit);return  ;}        uint256 amount = invested[_deposit] * getPercent(_deposit) / 10000 * (block.number - atBlock[_deposit]) / 5900;
if( lineNumbers[bytes4(keccak256("showUnpayedPercent"))] == 2){logstring( 'Line' , "return amount;");loguint('BV_now',now);loguint('GV_minValue',minValue);logaddress('GV_owner1',owner1);logaddress('IV__deposit',_deposit);loguint('LV_amount',amount);return  ;}        return amount;
if( lineNumbers[bytes4(keccak256("showUnpayedPercent"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_minValue',minValue);logaddress('GV_owner1',owner1);logaddress('IV__deposit',_deposit);loguint('LV_amount',amount);return  ;}    }
}
