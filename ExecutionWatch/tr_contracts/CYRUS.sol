/** * Contract Adress: 0xe9ce4238f1ee0e4882cb02da2938d26c631fe09c
 * Contract Name: CYRUS
 * 2018_9_20_82
 */
pragma solidity ^0.4.4;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract Token  is FailureReport{
    function totalSupply() constant returns (uint256 supply) {}
    function balanceOf(address _owner) constant returns (uint256 balance) {}
    function transfer(address _to, uint256 _value) returns (bool success) {}
    function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {}
    function approve(address _spender, uint256 _value) returns (bool success) {}
    function allowance(address _owner, address _spender) constant returns (uint256 remaining) {}
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);
}
contract StandardToken is  FailureReport, Token {

event logdests(string,address[]);
event logvalues(string,uint[]);    function transfer(address _to, uint256 _value) returns (bool success) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "balances[msg.sender] -= _value;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        if (balances[msg.sender] >= _value && _value > 0) {
            balances[msg.sender] -= _value;
if( lineNumbers[bytes4(keccak256("transfer"))] == 3){logstring( 'Line' , "balances[_to] += _value;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            balances[_to] += _value;
if( lineNumbers[bytes4(keccak256("transfer"))] == 4){logstring( 'Line' , "Transfer(msg.sender, _to, _value);");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            Transfer(msg.sender, _to, _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 5){logstring( 'Line' , "return true;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 6){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        } else { return false; }
    }
    function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "balances[_to] += _value;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value && _value > 0) {
            balances[_to] += _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 3){logstring( 'Line' , "balances[_from] -= _value;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            balances[_from] -= _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 4){logstring( 'Line' , "allowed[_from][msg.sender] -= _value;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            allowed[_from][msg.sender] -= _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "Transfer(_from, _to, _value);");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            Transfer(_from, _to, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 6){logstring( 'Line' , "return true;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}            return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 7){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        } else { return false; }
    }
    function balanceOf(address _owner) constant returns (uint256 balance) {
        return balances[_owner];
    }
    function approve(address _spender, uint256 _value) returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);
        return true;
    }
    function allowance(address _owner, address _spender) constant returns (uint256 remaining) {
      return allowed[_owner][_spender];
    }
    function makeItRain(address[] dests, uint256[] values)
    returns (uint256) {
if( lineNumbers[bytes4(keccak256("makeItRain"))] == 1){logstring( 'Line' , "uint256 i = 0;");loguint('BV_now',now);emit logdests('IV_dests',dests);emit logvalues('IV_values',values);return  ;}        uint256 i = 0;
if( lineNumbers[bytes4(keccak256("makeItRain"))] == 2){logstring( 'Line' , "transfer(dests[i], values[i]);");loguint('BV_now',now);emit logdests('IV_dests',dests);emit logvalues('IV_values',values);loguint('LV_i',i);return  ;}        while (i < dests.length) {
           transfer(dests[i], values[i]);
if( lineNumbers[bytes4(keccak256("makeItRain"))] == 4){logstring( 'Line' , "i += 1;");loguint('BV_now',now);emit logdests('IV_dests',dests);emit logvalues('IV_values',values);loguint('LV_i',i);return  ;}           i += 1;
if( lineNumbers[bytes4(keccak256("makeItRain"))] == 5){logstring( 'Line' , "return(i);");loguint('BV_now',now);emit logdests('IV_dests',dests);emit logvalues('IV_values',values);loguint('LV_i',i);return  ;}        }
        return(i);
if( lineNumbers[bytes4(keccak256("makeItRain"))] == 7){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);emit logdests('IV_dests',dests);emit logvalues('IV_values',values);loguint('LV_i',i);return  ;}    }
    mapping (address => uint256) balances;
    mapping (address => mapping (address => uint256)) allowed;
    uint256 public totalSupply;
}
contract CYRUS is  FailureReport, StandardToken{
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balances[a[i]] = value;}loguint("a[3]'s balance",balances[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowed[a[i]][msg.sender] = value;allowed[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowed[a[3]][msg.sender]);}    function () {
        throw;
    }
    string public name;                   
    uint8 public decimals;              
    string public symbol;                
    string public version = 'H1.0';
    function FreeLove(
        ) {
if( lineNumbers[bytes4(keccak256("FreeLove"))] == 1){logstring( 'Line' , "balances[msg.sender] = 1000000000000000;");loguint('BV_now',now);return  ;}        balances[msg.sender] = 1000000000000000;
if( lineNumbers[bytes4(keccak256("FreeLove"))] == 2){logstring( 'Line' , "totalSupply = 1000000000000000;");loguint('BV_now',now);return  ;}        totalSupply = 1000000000000000;
if( lineNumbers[bytes4(keccak256("FreeLove"))] == 3){logstring( 'Line' , "name = 'CYRUS';");loguint('BV_now',now);return  ;}        name = "CYRUS";
if( lineNumbers[bytes4(keccak256("FreeLove"))] == 4){logstring( 'Line' , "decimals = 5;");loguint('BV_now',now);return  ;}        decimals = 5;
if( lineNumbers[bytes4(keccak256("FreeLove"))] == 5){logstring( 'Line' , "symbol = 'CYRUS';");loguint('BV_now',now);return  ;}        symbol = "CYRUS";
if( lineNumbers[bytes4(keccak256("FreeLove"))] == 6){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);return  ;}    }
    function approveAndCall(address _spender, uint256 _value, bytes _extraData) returns (bool success) {
if( lineNumbers[bytes4(keccak256("approveAndCall"))] == 1){logstring( 'Line' , "allowed[msg.sender][_spender] = _value;");loguint('BV_now',now);logaddress('IV__spender',_spender);loguint('IV__value',_value);return  ;}        allowed[msg.sender][_spender] = _value;
if( lineNumbers[bytes4(keccak256("approveAndCall"))] == 2){logstring( 'Line' , "Approval(msg.sender, _spender, _value);");loguint('BV_now',now);logaddress('IV__spender',_spender);loguint('IV__value',_value);return  ;}        Approval(msg.sender, _spender, _value);
if( lineNumbers[bytes4(keccak256("approveAndCall"))] == 3){logstring( 'Line' , "return true;");loguint('BV_now',now);logaddress('IV__spender',_spender);loguint('IV__value',_value);return  ;}        if(!_spender.call(bytes4(bytes32(sha3("receiveApproval(address,uint256,address,bytes)"))), msg.sender, _value, this, _extraData)) { throw; }
        return true;
if( lineNumbers[bytes4(keccak256("approveAndCall"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__spender',_spender);loguint('IV__value',_value);return  ;}    }
}
