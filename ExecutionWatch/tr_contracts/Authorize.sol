/** * Contract Adress: 0x4a6ead96974679957a17d2f9c7835a3da7ddf91d
 * Contract Name: Authorize
 * 2018_12_1_6
 */
pragma solidity ^0.4.25;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract Ownable  is FailureReport{
    address public owner;
    constructor() public {
        owner = msg.sender;
    }
    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }
}
contract Authorize is  FailureReport, Ownable {
    /* Define variable owner of the type address */
    address public backEndOperator = msg.sender;
    mapping(address=>bool) public whitelist;
    event Authorized(address wlCandidate, uint timestamp);
    event Revoked(address wlCandidate, uint timestamp);
    modifier backEnd() {
        require(msg.sender == backEndOperator || msg.sender == owner);
        _;
    }
    function setBackEndAddress(address newBackEndOperator) public onlyOwner {
if( lineNumbers[bytes4(keccak256("setBackEndAddress"))] == 1){logstring( 'Line' , "backEndOperator = newBackEndOperator;");loguint('BV_now',now);logaddress('GV_backEndOperator',backEndOperator);logaddress('GV_owner',owner);logaddress('IV_newBackEndOperator',newBackEndOperator);return  ;}        backEndOperator = newBackEndOperator;
if( lineNumbers[bytes4(keccak256("setBackEndAddress"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_backEndOperator',backEndOperator);logaddress('GV_owner',owner);logaddress('IV_newBackEndOperator',newBackEndOperator);return  ;}    }
    function authorize(address wlCandidate) public backEnd  {
if( lineNumbers[bytes4(keccak256("authorize"))] == 1){logstring( 'Line' , "require(wlCandidate != address(0x0));");loguint('BV_now',now);logaddress('GV_backEndOperator',backEndOperator);logaddress('GV_owner',owner);logaddress('IV_wlCandidate',wlCandidate);return  ;}        require(wlCandidate != address(0x0));
if( lineNumbers[bytes4(keccak256("authorize"))] == 2){logstring( 'Line' , "require(!isWhitelisted(wlCandidate));");loguint('BV_now',now);logaddress('GV_backEndOperator',backEndOperator);logaddress('GV_owner',owner);logaddress('IV_wlCandidate',wlCandidate);return  ;}        require(!isWhitelisted(wlCandidate));
if( lineNumbers[bytes4(keccak256("authorize"))] == 3){logstring( 'Line' , "whitelist[wlCandidate] = true;");loguint('BV_now',now);logaddress('GV_backEndOperator',backEndOperator);logaddress('GV_owner',owner);logaddress('IV_wlCandidate',wlCandidate);return  ;}        whitelist[wlCandidate] = true;
if( lineNumbers[bytes4(keccak256("authorize"))] == 4){logstring( 'Line' , "emit Authorized(wlCandidate, now);");loguint('BV_now',now);logaddress('GV_backEndOperator',backEndOperator);logaddress('GV_owner',owner);logaddress('IV_wlCandidate',wlCandidate);return  ;}        emit Authorized(wlCandidate, now);
if( lineNumbers[bytes4(keccak256("authorize"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_backEndOperator',backEndOperator);logaddress('GV_owner',owner);logaddress('IV_wlCandidate',wlCandidate);return  ;}    }
    function revoke(address wlCandidate) public  onlyOwner {
if( lineNumbers[bytes4(keccak256("revoke"))] == 1){logstring( 'Line' , "whitelist[wlCandidate] = false;");loguint('BV_now',now);logaddress('GV_backEndOperator',backEndOperator);logaddress('GV_owner',owner);logaddress('IV_wlCandidate',wlCandidate);return  ;}        whitelist[wlCandidate] = false;
if( lineNumbers[bytes4(keccak256("revoke"))] == 2){logstring( 'Line' , "emit Revoked(wlCandidate, now);");loguint('BV_now',now);logaddress('GV_backEndOperator',backEndOperator);logaddress('GV_owner',owner);logaddress('IV_wlCandidate',wlCandidate);return  ;}        emit Revoked(wlCandidate, now);
if( lineNumbers[bytes4(keccak256("revoke"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_backEndOperator',backEndOperator);logaddress('GV_owner',owner);logaddress('IV_wlCandidate',wlCandidate);return  ;}    }
    function isWhitelisted(address wlCandidate) public view returns(bool) {
if( lineNumbers[bytes4(keccak256("isWhitelisted"))] == 1){logstring( 'Line' , "return whitelist[wlCandidate];");loguint('BV_now',now);logaddress('GV_backEndOperator',backEndOperator);logaddress('GV_owner',owner);logaddress('IV_wlCandidate',wlCandidate);return  ;}        return whitelist[wlCandidate];
if( lineNumbers[bytes4(keccak256("isWhitelisted"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_backEndOperator',backEndOperator);logaddress('GV_owner',owner);logaddress('IV_wlCandidate',wlCandidate);return  ;}    }
}
