/** * Contract Adress: 0x0daf6d0c0651ef0d638223efd1d4e0ae039f3801
 * Contract Name: AporooToken
 * 2018_6_26_142
 */
pragma solidity ^0.4.21;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract EIP20Interface  is FailureReport{
    /* This is a slight change to the ERC20 base standard.
    function totalSupply() constant returns (uint256 supply);
    is replaced with:
    uint256 public totalSupply;
    This automatically creates a getter function for the totalSupply.
    This is moved to the base contract since public getter functions are not
    currently recognised as an implementation of the matching abstract
    function by the compiler.
    */
    /// total amount of tokens
    uint256 public totalSupply;
    /// @param _owner The address from which the balance will be retrieved
    /// @return The balance
    function balanceOf(address _owner) public view returns (uint256 balance);
    /// @notice send `_value` token to `_to` from `msg.sender`
    /// @param _to The address of the recipient
    /// @param _value The amount of token to be transferred
    /// @return Whether the transfer was successful or not
    function transfer(address _to, uint256 _value) public returns (bool success);
    /// @notice send `_value` token to `_to` from `_from` on the condition it is approved by `_from`
    /// @param _from The address of the sender
    /// @param _to The address of the recipient
    /// @param _value The amount of token to be transferred
    /// @return Whether the transfer was successful or not
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success);
    /// @notice `msg.sender` approves `_spender` to spend `_value` tokens
    /// @param _spender The address of the account able to transfer the tokens
    /// @param _value The amount of tokens to be approved for transfer
    /// @return Whether the approval was successful or not
    function approve(address _spender, uint256 _value) public returns (bool success);
    /// @param _owner The address of the account owning tokens
    /// @param _spender The address of the account able to transfer the tokens
    /// @return Amount of remaining tokens allowed to spent
    function allowance(address _owner, address _spender) public view returns (uint256 remaining);
    // solhint-disable-next-line no-simple-event-func-name
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);
}
contract AporooToken is  FailureReport, EIP20Interface {
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balances[a[i]] = value;}loguint("a[3]'s balance",balances[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowed[a[i]][msg.sender] = value;allowed[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowed[a[3]][msg.sender]);}    uint256 constant private MAX_UINT256 = 2**256 - 1;
    mapping (address => uint256) public balances;
    mapping (address => mapping (address => uint256)) public allowed;
    /*
    NOTE:
    The following variables are OPTIONAL vanities. One does not have to include them.
    They allow one to customise the token contract & in no way influences the core functionality.
    Some wallets/interfaces might not even bother to look at this information.
    */
    string public name;                   //fancy name: eg Simon Bucks
    uint8 public decimals;               //How many decimals to show.
    string public symbol;                 //An identifier: eg SBX
    function AporooToken(
        uint256 _initialAmount,
        string _tokenName,
        uint8 _decimalUnits,
        string _tokenSymbol
    ) public {
        balances[msg.sender] = _initialAmount;               // Give the creator all initial tokens
        totalSupply = _initialAmount;                        // Update total supply
        name = _tokenName;                                   // Set the name for display purposes
        decimals = _decimalUnits;                            // Amount of decimals for display purposes
        symbol = _tokenSymbol;                               // Set the symbol for display purposes
    }
    function transfer(address _to, uint256 _value) public returns (bool success) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "require(balances[msg.sender] >= _value);");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        require(balances[msg.sender] >= _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 2){logstring( 'Line' , "balances[msg.sender] -= _value;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        balances[msg.sender] -= _value;
if( lineNumbers[bytes4(keccak256("transfer"))] == 3){logstring( 'Line' , "balances[_to] += _value;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        balances[_to] += _value;
if( lineNumbers[bytes4(keccak256("transfer"))] == 4){logstring( 'Line' , "emit Transfer(msg.sender, _to, _value); //solhint-disable-line indent, no-unused-vars");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        emit Transfer(msg.sender, _to, _value); //solhint-disable-line indent, no-unused-vars
if( lineNumbers[bytes4(keccak256("transfer"))] == 5){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 6){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    }
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "uint256 allowance = allowed[_from][msg.sender];");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        uint256 allowance = allowed[_from][msg.sender];
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 2){logstring( 'Line' , "require(balances[_from] >= _value && allowance >= _value);");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);loguint('LV_allowance',allowance);return  ;}        require(balances[_from] >= _value && allowance >= _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 3){logstring( 'Line' , "balances[_to] += _value;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);loguint('LV_allowance',allowance);return  ;}        balances[_to] += _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 4){logstring( 'Line' , "balances[_from] -= _value;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);loguint('LV_allowance',allowance);return  ;}        balances[_from] -= _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "allowed[_from][msg.sender] -= _value;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);loguint('LV_allowance',allowance);return  ;}        if (allowance < MAX_UINT256) {
            allowed[_from][msg.sender] -= _value;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 7){logstring( 'Line' , "emit Transfer(_from, _to, _value); //solhint-disable-line indent, no-unused-vars");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);loguint('LV_allowance',allowance);return  ;}        }
        emit Transfer(_from, _to, _value); //solhint-disable-line indent, no-unused-vars
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 9){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);loguint('LV_allowance',allowance);return  ;}        return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 10){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);loguint('GV_decimals',decimals);logstring('GV_symbol',symbol);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);loguint('LV_allowance',allowance);return  ;}    }
    function balanceOf(address _owner) public view returns (uint256 balance) {
        return balances[_owner];
    }
    function approve(address _spender, uint256 _value) public returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value); //solhint-disable-line indent, no-unused-vars
        return true;
    }
    function allowance(address _owner, address _spender) public view returns (uint256 remaining) {
        return allowed[_owner][_spender];
    }
}
