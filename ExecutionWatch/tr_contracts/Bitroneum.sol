/** * Contract Adress: 0xde744f433567d45701e7d6963f799e5cdda12f5e
 * Contract Name: Bitroneum
 * 2018_5_19_25
 */
pragma solidity ^0.4.23;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract ERC20Basic  is FailureReport{
  function totalSupply() public view returns (uint256);
  function balanceOf(address who) public view returns (uint256);
  function transfer(address to, uint256 value) public returns (bool);
  event Transfer(address indexed from, address indexed to, uint256 value);
}
library SafeMath {
  function mul(uint256 a, uint256 b) internal pure returns (uint256 c) {
    if (a == 0) {
      return 0;
    }
    c = a * b;
    assert(c / a == b);
    return c;
  }
  function div(uint256 a, uint256 b) internal pure returns (uint256) {
    return a / b;
  }
  function sub(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b <= a);
    return a - b;
  }
  function add(uint256 a, uint256 b) internal pure returns (uint256 c) {
    c = a + b;
    assert(c >= a);
    return c;
  }
}
contract BasicToken is  FailureReport, ERC20Basic {
  using SafeMath for uint256;
  mapping(address => uint256) balances;
  uint256 totalSupply_;
  function totalSupply() public view returns (uint256) {
    return totalSupply_;
  }
  function transfer(address _to, uint256 _value) public returns (bool) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "require(_to != address(0));");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    require(_to != address(0));
if( lineNumbers[bytes4(keccak256("transfer"))] == 2){logstring( 'Line' , "require(_value <= balances[msg.sender]);");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    require(_value <= balances[msg.sender]);
if( lineNumbers[bytes4(keccak256("transfer"))] == 3){logstring( 'Line' , "balances[msg.sender] = balances[msg.sender].sub(_value);");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    balances[msg.sender] = balances[msg.sender].sub(_value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 4){logstring( 'Line' , "balances[_to] = balances[_to].add(_value);");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    balances[_to] = balances[_to].add(_value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 5){logstring( 'Line' , "emit Transfer(msg.sender, _to, _value);");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    emit Transfer(msg.sender, _to, _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 6){logstring( 'Line' , "return true;");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 7){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}  }
  function balanceOf(address _owner) public view returns (uint256) {
    return balances[_owner];
  }
}
contract ERC20 is  FailureReport, ERC20Basic {
  function allowance(address owner, address spender)
    public view returns (uint256);
  function transferFrom(address from, address to, uint256 value)
    public returns (bool);
  function approve(address spender, uint256 value) public returns (bool);
  event Approval(
    address indexed owner,
    address indexed spender,
    uint256 value
  );
}
contract StandardToken is  FailureReport, ERC20, BasicToken {
  mapping (address => mapping (address => uint256)) internal allowed;
  function transferFrom(
    address _from,
    address _to,
    uint256 _value
  )
    public
    returns (bool)
  {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "require(_to != address(0));");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    require(_to != address(0));
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 2){logstring( 'Line' , "require(_value <= balances[_from]);");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    require(_value <= balances[_from]);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 3){logstring( 'Line' , "require(_value <= allowed[_from][msg.sender]);");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    require(_value <= allowed[_from][msg.sender]);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 4){logstring( 'Line' , "balances[_from] = balances[_from].sub(_value);");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    balances[_from] = balances[_from].sub(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "balances[_to] = balances[_to].add(_value);");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    balances[_to] = balances[_to].add(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 6){logstring( 'Line' , "allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_value);");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 7){logstring( 'Line' , "emit Transfer(_from, _to, _value);");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    emit Transfer(_from, _to, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 8){logstring( 'Line' , "return true;");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 9){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}  }
  function approve(address _spender, uint256 _value) public returns (bool) {
    allowed[msg.sender][_spender] = _value;
    emit Approval(msg.sender, _spender, _value);
    return true;
  }
  function allowance(
    address _owner,
    address _spender
   )
    public
    view
    returns (uint256)
  {
    return allowed[_owner][_spender];
  }
  function increaseApproval(
    address _spender,
    uint _addedValue
  )
    public
    returns (bool)
  {
if( lineNumbers[bytes4(keccak256("increaseApproval"))] == 1){logstring( 'Line' , "allowed[msg.sender][_spender].add(_addedValue));");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__spender',_spender);loguint('IV__addedValue',_addedValue);return  ;}    allowed[msg.sender][_spender] = (
      allowed[msg.sender][_spender].add(_addedValue));
if( lineNumbers[bytes4(keccak256("increaseApproval"))] == 3){logstring( 'Line' , "emit Approval(msg.sender, _spender, allowed[msg.sender][_spender]);");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__spender',_spender);loguint('IV__addedValue',_addedValue);return  ;}    emit Approval(msg.sender, _spender, allowed[msg.sender][_spender]);
if( lineNumbers[bytes4(keccak256("increaseApproval"))] == 4){logstring( 'Line' , "return true;");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__spender',_spender);loguint('IV__addedValue',_addedValue);return  ;}    return true;
if( lineNumbers[bytes4(keccak256("increaseApproval"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__spender',_spender);loguint('IV__addedValue',_addedValue);return  ;}  }
  function decreaseApproval(
    address _spender,
    uint _subtractedValue
  )
    public
    returns (bool)
  {
if( lineNumbers[bytes4(keccak256("decreaseApproval"))] == 1){logstring( 'Line' , "uint oldValue = allowed[msg.sender][_spender];");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__spender',_spender);loguint('IV__subtractedValue',_subtractedValue);return  ;}    uint oldValue = allowed[msg.sender][_spender];
if( lineNumbers[bytes4(keccak256("decreaseApproval"))] == 2){logstring( 'Line' , "allowed[msg.sender][_spender] = 0;");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__spender',_spender);loguint('IV__subtractedValue',_subtractedValue);loguint('LV_oldValue',oldValue);return  ;}    if (_subtractedValue > oldValue) {
      allowed[msg.sender][_spender] = 0;
if( lineNumbers[bytes4(keccak256("decreaseApproval"))] == 4){logstring( 'Line' , "allowed[msg.sender][_spender] = oldValue.sub(_subtractedValue);");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__spender',_spender);loguint('IV__subtractedValue',_subtractedValue);loguint('LV_oldValue',oldValue);return  ;}    } else {
      allowed[msg.sender][_spender] = oldValue.sub(_subtractedValue);
if( lineNumbers[bytes4(keccak256("decreaseApproval"))] == 6){logstring( 'Line' , "emit Approval(msg.sender, _spender, allowed[msg.sender][_spender]);");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__spender',_spender);loguint('IV__subtractedValue',_subtractedValue);loguint('LV_oldValue',oldValue);return  ;}    }
    emit Approval(msg.sender, _spender, allowed[msg.sender][_spender]);
if( lineNumbers[bytes4(keccak256("decreaseApproval"))] == 8){logstring( 'Line' , "return true;");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__spender',_spender);loguint('IV__subtractedValue',_subtractedValue);loguint('LV_oldValue',oldValue);return  ;}    return true;
if( lineNumbers[bytes4(keccak256("decreaseApproval"))] == 9){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);logaddress('IV__spender',_spender);loguint('IV__subtractedValue',_subtractedValue);loguint('LV_oldValue',oldValue);return  ;}  }
}
contract BurnableToken is  FailureReport, BasicToken {
  event Burn(address indexed burner, uint256 value);
  function burn(uint256 _value) public {
if( lineNumbers[bytes4(keccak256("burn"))] == 1){logstring( 'Line' , "_burn(msg.sender, _value);");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);loguint('IV__value',_value);return  ;}    _burn(msg.sender, _value);
if( lineNumbers[bytes4(keccak256("burn"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_totalSupply_',totalSupply_);loguint('IV__value',_value);return  ;}  }
  function _burn(address _who, uint256 _value) internal {
    require(_value <= balances[_who]);
    balances[_who] = balances[_who].sub(_value);
    totalSupply_ = totalSupply_.sub(_value);
    emit Burn(_who, _value);
    emit Transfer(_who, address(0), _value);
  }
}
contract Bitroneum is StandardToken, BurnableToken {
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balances[a[i]] = value;}loguint("a[3]'s balance",balances[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowed[a[i]][msg.sender] = value;allowed[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowed[a[3]][msg.sender]);}  string public constant name = "Bitroneum";
  string public constant symbol = "BITRO";
  uint8 public constant decimals = 18;
  uint256 public constant INITIAL_SUPPLY = 300000000 * (10 ** uint256(decimals));
  constructor() public {
    totalSupply_ = INITIAL_SUPPLY;
    balances[msg.sender] = INITIAL_SUPPLY;
    emit Transfer(0x0, msg.sender, INITIAL_SUPPLY);
  }
}
