/** * Contract Adress: 0x563172281800b139f69fb038cc2c08cad56ce699
 * Contract Name: ContractFeatures
 * 2018_6_6_32
 */
pragma solidity ^0.4.21;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
/*
    Contract Features interface
*/
contract IContractFeatures  is FailureReport{
    function isSupported(address _contract, uint256 _features) public view returns (bool);
    function enableFeatures(uint256 _features, bool _enable) public;
}
/**
    Contract Features
    Generic contract that allows every contract on the blockchain to define which features it supports.
    Other contracts can query this contract to find out whether a given contract on the
    blockchain supports a certain feature.
    Each contract type can define its own list of feature flags.
    Features can be only enabled/disabled by the contract they are defined for.
    Features should be defined by each contract type as bit flags, e.g. -
    uint256 public constant FEATURE1 = 1 << 0;
    uint256 public constant FEATURE2 = 1 << 1;
    uint256 public constant FEATURE3 = 1 << 2;
    ...
*/
contract ContractFeatures is  FailureReport, IContractFeatures {
    mapping (address => uint256) private featureFlags;
    event FeaturesAddition(address indexed _address, uint256 _features);
    event FeaturesRemoval(address indexed _address, uint256 _features);
    /**
        @dev constructor
    */
    function ContractFeatures() public {
    }
    /**
        @dev returns true if a given contract supports the given feature(s), false if not
        @param _contract    contract address to check support for
        @param _features    feature(s) to check for
        @return true if the contract supports the feature(s), false if not
    */
    function isSupported(address _contract, uint256 _features) public view returns (bool) {
if( lineNumbers[bytes4(keccak256("isSupported"))] == 1){logstring( 'Line' , "return (featureFlags[_contract] & _features) == _features;");loguint('BV_now',now);logaddress('IV__contract',_contract);loguint('IV__features',_features);return  ;}        return (featureFlags[_contract] & _features) == _features;
if( lineNumbers[bytes4(keccak256("isSupported"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__contract',_contract);loguint('IV__features',_features);return  ;}    }
    /**
        @dev allows a contract to enable/disable certain feature(s)
        @param _features    feature(s) to enable/disable
        @param _enable      true to enable the feature(s), false to disabled them
    */
    function enableFeatures(uint256 _features, bool _enable) public {
if( lineNumbers[bytes4(keccak256("enableFeatures"))] == 1){logstring( 'Line' , "return;");loguint('BV_now',now);loguint('IV__features',_features);logbool('IV__enable',_enable);return  ;}        if (_enable) {
            if (isSupported(msg.sender, _features))
                return;
if( lineNumbers[bytes4(keccak256("enableFeatures"))] == 4){logstring( 'Line' , "featureFlags[msg.sender] |= _features;");loguint('BV_now',now);loguint('IV__features',_features);logbool('IV__enable',_enable);return  ;}            featureFlags[msg.sender] |= _features;
if( lineNumbers[bytes4(keccak256("enableFeatures"))] == 5){logstring( 'Line' , "emit FeaturesAddition(msg.sender, _features);");loguint('BV_now',now);loguint('IV__features',_features);logbool('IV__enable',_enable);return  ;}            emit FeaturesAddition(msg.sender, _features);
if( lineNumbers[bytes4(keccak256("enableFeatures"))] == 6){logstring( 'Line' , "return;");loguint('BV_now',now);loguint('IV__features',_features);logbool('IV__enable',_enable);return  ;}        } else {
            if (!isSupported(msg.sender, _features))
                return;
if( lineNumbers[bytes4(keccak256("enableFeatures"))] == 9){logstring( 'Line' , "featureFlags[msg.sender] &= ~_features;");loguint('BV_now',now);loguint('IV__features',_features);logbool('IV__enable',_enable);return  ;}            featureFlags[msg.sender] &= ~_features;
if( lineNumbers[bytes4(keccak256("enableFeatures"))] == 10){logstring( 'Line' , "emit FeaturesRemoval(msg.sender, _features);");loguint('BV_now',now);loguint('IV__features',_features);logbool('IV__enable',_enable);return  ;}            emit FeaturesRemoval(msg.sender, _features);
if( lineNumbers[bytes4(keccak256("enableFeatures"))] == 11){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('IV__features',_features);logbool('IV__enable',_enable);return  ;}        }
    }
}
