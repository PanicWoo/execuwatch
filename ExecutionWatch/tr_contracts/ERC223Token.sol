/** * Contract Adress: 0x297cb49f672a7e728385ed7f273972fbcc496604
 * Contract Name: ERC223Token
 * 2018_10_1_113
 */
pragma solidity ^0.4.23;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
/**
 * @title Ownable
 * @dev The Ownable contract has an owner address, and provides basic authorization control
 * functions, this simplifies the implementation of "user permissions".
 */
contract Ownable  is FailureReport{
  address public owner;
  event OwnershipRenounced(address indexed previousOwner);
  event OwnershipTransferred(
    address indexed previousOwner,
    address indexed newOwner
  );
  /**
   * @dev The Ownable constructor sets the original `owner` of the contract to the sender
   * account.
   */
  constructor() public {
    owner = msg.sender;
  }
  /**
   * @dev Throws if called by any account other than the owner.
   */
  modifier onlyOwner() {
    require(msg.sender == owner);
    _;
  }
  /**
   * @dev Allows the current owner to transfer control of the contract to a newOwner.
   * @param newOwner The address to transfer ownership to.
   */
  function transferOwnership(address newOwner) public onlyOwner {
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 1){logstring( 'Line' , "require(newOwner != address(0));");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}    require(newOwner != address(0));
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 2){logstring( 'Line' , "emit OwnershipTransferred(owner, newOwner);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}    emit OwnershipTransferred(owner, newOwner);
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 3){logstring( 'Line' , "owner = newOwner;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}    owner = newOwner;
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 4){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}  }
  /**
   * @dev Allows the current owner to relinquish control of the contract.
   */
  function renounceOwnership() public onlyOwner {
if( lineNumbers[bytes4(keccak256("renounceOwnership"))] == 1){logstring( 'Line' , "emit OwnershipRenounced(owner);");loguint('BV_now',now);logaddress('GV_owner',owner);return  ;}    emit OwnershipRenounced(owner);
if( lineNumbers[bytes4(keccak256("renounceOwnership"))] == 2){logstring( 'Line' , "owner = address(0);");loguint('BV_now',now);logaddress('GV_owner',owner);return  ;}    owner = address(0);
if( lineNumbers[bytes4(keccak256("renounceOwnership"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);return  ;}  }
}
/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {
  /**
  * @dev Multiplies two numbers, throws on overflow.
  */
  function mul(uint256 a, uint256 b) internal pure returns (uint256 c) {
    if (a == 0) {
      return 0;
    }
    c = a * b;
    assert(c / a == b);
    return c;
  }
  /**
  * @dev Integer division of two numbers, truncating the quotient.
  */
  function div(uint256 a, uint256 b) internal pure returns (uint256) {
    // assert(b > 0); // Solidity automatically throws when dividing by 0
    // uint256 c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn't hold
    return a / b;
  }
  /**
  * @dev Subtracts two numbers, throws on overflow (i.e. if subtrahend is greater than minuend).
  */
  function sub(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b <= a);
    return a - b;
  }
  /**
  * @dev Adds two numbers, throws on overflow.
  */
  function add(uint256 a, uint256 b) internal pure returns (uint256 c) {
    c = a + b;
    assert(c >= a);
    return c;
  }
}
/**
 * @title Pausable
 * @dev Base contract which allows children to implement an emergency stop mechanism.
 */
contract Pausable is  FailureReport, Ownable {
  event Pause();
  event Unpause();
  bool public paused = false;
  /**
   * @dev Modifier to make a function callable only when the contract is not paused.
   */
  modifier whenNotPaused() {
    require(!paused);
    _;
  }
  /**
   * @dev Modifier to make a function callable only when the contract is paused.
   */
  modifier whenPaused() {
    require(paused);
    _;
  }
  /**
   * @dev called by the owner to pause, triggers stopped state
   */
  function pause() onlyOwner whenNotPaused public {
if( lineNumbers[bytes4(keccak256("pause"))] == 1){logstring( 'Line' , "paused = true;");loguint('BV_now',now);logbool('GV_paused',paused);logaddress('GV_owner',owner);return  ;}    paused = true;
if( lineNumbers[bytes4(keccak256("pause"))] == 2){logstring( 'Line' , "emit Pause();");loguint('BV_now',now);logbool('GV_paused',paused);logaddress('GV_owner',owner);return  ;}    emit Pause();
if( lineNumbers[bytes4(keccak256("pause"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logbool('GV_paused',paused);logaddress('GV_owner',owner);return  ;}  }
  /**
   * @dev called by the owner to unpause, returns to normal state
   */
  function unpause() onlyOwner whenPaused public {
if( lineNumbers[bytes4(keccak256("unpause"))] == 1){logstring( 'Line' , "paused = false;");loguint('BV_now',now);logbool('GV_paused',paused);logaddress('GV_owner',owner);return  ;}    paused = false;
if( lineNumbers[bytes4(keccak256("unpause"))] == 2){logstring( 'Line' , "emit Unpause();");loguint('BV_now',now);logbool('GV_paused',paused);logaddress('GV_owner',owner);return  ;}    emit Unpause();
if( lineNumbers[bytes4(keccak256("unpause"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logbool('GV_paused',paused);logaddress('GV_owner',owner);return  ;}  }
}
contract ContractReceiver  is FailureReport{
    function tokenFallback(address _from, uint _value, bytes _data);
}
contract ERC223Token is  FailureReport, Pausable {

event logaddresses(string,address[]);
event logaddresses(string,address[]);
event logamounts(string,uint[]);  using SafeMath for uint256;
  mapping (address => uint) balances;
  mapping (address => mapping (address => uint256)) internal allowed;
  mapping (address => bool) public frozenAccount;
  event Transfer(address indexed _from, address indexed _to, uint256 _value, bytes _data);
  event FrozenFunds(address target, bool frozen);
  event Approval(address indexed owner, address indexed spender, uint256 value);
  string public name;
  string public symbol;
  uint8 public decimals;
  uint256 public totalSupply;
  constructor(string _name, string _symbol, uint8 _decimals, uint256 _supply)
  {
      name = _name;
      symbol = _symbol;
      decimals = _decimals;
      totalSupply = _supply * (10 ** decimals);
      balances[msg.sender] = totalSupply;
  }
  // Function to access name of token .
  function name() constant returns (string _name) {
if( lineNumbers[bytes4(keccak256("name"))] == 1){logstring( 'Line' , "return name;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);return  ;}      return name;
if( lineNumbers[bytes4(keccak256("name"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);return  ;}  }
  // Function to access symbol of token .
  function symbol() constant returns (string _symbol) {
if( lineNumbers[bytes4(keccak256("symbol"))] == 1){logstring( 'Line' , "return symbol;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);return  ;}      return symbol;
if( lineNumbers[bytes4(keccak256("symbol"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);return  ;}  }
  // Function to access decimals of token .
  function decimals() constant returns (uint8 _decimals) {
if( lineNumbers[bytes4(keccak256("decimals"))] == 1){logstring( 'Line' , "return decimals;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);return  ;}      return decimals;
if( lineNumbers[bytes4(keccak256("decimals"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);return  ;}  }
  // Function to access total supply of tokens .
  function totalSupply() constant returns (uint256 _totalSupply) {
      return totalSupply;
  }
  function whoIsOwner() constant returns (address _owner) {
if( lineNumbers[bytes4(keccak256("whoIsOwner"))] == 1){logstring( 'Line' , "return owner;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);return  ;}      return owner;
if( lineNumbers[bytes4(keccak256("whoIsOwner"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);return  ;}  }
  function freezeAccount(address target, bool freeze) onlyOwner public {
if( lineNumbers[bytes4(keccak256("freezeAccount"))] == 1){logstring( 'Line' , "frozenAccount[target] = freeze;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV_target',target);logbool('IV_freeze',freeze);return  ;}    frozenAccount[target] = freeze;
if( lineNumbers[bytes4(keccak256("freezeAccount"))] == 2){logstring( 'Line' , "emit FrozenFunds(target, freeze);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV_target',target);logbool('IV_freeze',freeze);return  ;}    emit FrozenFunds(target, freeze);
if( lineNumbers[bytes4(keccak256("freezeAccount"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV_target',target);logbool('IV_freeze',freeze);return  ;}  }
  // Function that is called when a user or another contract wants to transfer funds .
  function transfer(address _to, uint _value, bytes _data, string _custom_fallback)
  whenNotPaused
  returns (bool success)
  {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "require(_to != address(0));");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__to',_to);loguint('IV__value',_value);logstring('IV__custom_fallback',_custom_fallback);return  ;}    require(_to != address(0));
if( lineNumbers[bytes4(keccak256("transfer"))] == 2){logstring( 'Line' , "require(!frozenAccount[_to]);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__to',_to);loguint('IV__value',_value);logstring('IV__custom_fallback',_custom_fallback);return  ;}    require(!frozenAccount[_to]);
if( lineNumbers[bytes4(keccak256("transfer"))] == 3){logstring( 'Line' , "require(!frozenAccount[msg.sender]);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__to',_to);loguint('IV__value',_value);logstring('IV__custom_fallback',_custom_fallback);return  ;}    require(!frozenAccount[msg.sender]);
if( lineNumbers[bytes4(keccak256("transfer"))] == 4){logstring( 'Line' , "require(balanceOf(msg.sender) >= _value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__to',_to);loguint('IV__value',_value);logstring('IV__custom_fallback',_custom_fallback);return  ;}    if(isContract(_to)) {
      require(balanceOf(msg.sender) >= _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 6){logstring( 'Line' , "balances[_to] = balanceOf(_to).sub(_value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__to',_to);loguint('IV__value',_value);logstring('IV__custom_fallback',_custom_fallback);return  ;}        balances[_to] = balanceOf(_to).sub(_value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 7){logstring( 'Line' , "balances[_to] = balanceOf(_to).add(_value);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__to',_to);loguint('IV__value',_value);logstring('IV__custom_fallback',_custom_fallback);return  ;}        balances[_to] = balanceOf(_to).add(_value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 8){logstring( 'Line' , "assert(_to.call.value(0)(bytes4(sha3(_custom_fallback)), msg.sender, _value, _data));");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__to',_to);loguint('IV__value',_value);logstring('IV__custom_fallback',_custom_fallback);return  ;}        assert(_to.call.value(0)(bytes4(sha3(_custom_fallback)), msg.sender, _value, _data));
if( lineNumbers[bytes4(keccak256("transfer"))] == 9){logstring( 'Line' , "emit Transfer(msg.sender, _to, _value, _data);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__to',_to);loguint('IV__value',_value);logstring('IV__custom_fallback',_custom_fallback);return  ;}        emit Transfer(msg.sender, _to, _value, _data);
if( lineNumbers[bytes4(keccak256("transfer"))] == 10){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__to',_to);loguint('IV__value',_value);logstring('IV__custom_fallback',_custom_fallback);return  ;}        return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 11){logstring( 'Line' , "return transferToAddress(_to, _value, _data);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__to',_to);loguint('IV__value',_value);logstring('IV__custom_fallback',_custom_fallback);return  ;}    }
    else {
        return transferToAddress(_to, _value, _data);
if( lineNumbers[bytes4(keccak256("transfer"))] == 14){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__to',_to);loguint('IV__value',_value);logstring('IV__custom_fallback',_custom_fallback);return  ;}    }
}
  // Function that is called when a user or another contract wants to transfer funds .
  function transfer(address _to, uint _value, bytes _data)
  whenNotPaused
  returns (bool success) {
    require(_to != address(0));
    require(!frozenAccount[_to]);
    require(!frozenAccount[msg.sender]);
    if(isContract(_to)) {
        return transferToContract(_to, _value, _data);
    }
    else {
        return transferToAddress(_to, _value, _data);
    }
}
  // Standard function transfer similar to ERC20 transfer with no _data .
  // Added due to backwards compatibility reasons .
  function transfer(address _to, uint _value)
  whenNotPaused
  returns (bool success) {
    require(_to != address(0));
    require(!frozenAccount[_to]);
    require(!frozenAccount[msg.sender]);
    //standard function transfer similar to ERC20 transfer with no _data
    //added due to backwards compatibility reasons
    bytes memory empty;
    if(isContract(_to)) {
        return transferToContract(_to, _value, empty);
    }
    else {
        return transferToAddress(_to, _value, empty);
    }
}
//assemble the given address bytecode. If bytecode exists then the _addr is a contract.
  function isContract(address _addr) private returns (bool is_contract) {
      uint length;
      assembly {
            //retrieve the size of the code on target address, this needs assembly
            length := extcodesize(_addr)
      }
      return (length>0);
    }
  //function that is called when transaction target is an address
  function transferToAddress(address _to, uint _value, bytes _data) private returns (bool success) {
    require(_to != address(0));
    require(!frozenAccount[_to]);
    require(balanceOf(msg.sender) >= _value);
    require(!frozenAccount[msg.sender]);
    balances[msg.sender] = balanceOf(msg.sender).sub(_value);
    balances[_to] = balanceOf(_to).add(_value);
    emit Transfer(msg.sender, _to, _value, _data);
    return true;
  }
  //function that is called when transaction target is a contract
  function transferToContract(address _to, uint _value, bytes _data) private returns (bool success) {
    require(_to != address(0));
    require(!frozenAccount[_to]);
    require(balanceOf(msg.sender) >= _value);
    require(!frozenAccount[msg.sender]);
    balances[msg.sender] = balanceOf(msg.sender).sub(_value);
    balances[_to] = balanceOf(_to).add(_value);
    ContractReceiver receiver = ContractReceiver(_to);
    receiver.tokenFallback(msg.sender, _value, _data);
    emit Transfer(msg.sender, _to, _value, _data);
    return true;
  }
  function balanceOf(address _owner) constant returns (uint balance) {
    return balances[_owner];
  }
  /**
   * @dev Approve the passed address to spend the specified amount of tokens on behalf of msg.sender.
   *
   * Beware that changing an allowance with this method brings the risk that someone may use both the old
   * and the new allowance by unfortunate transaction ordering. One possible solution to mitigate this
   * race condition is to first reduce the spender's allowance to 0 and set the desired value afterwards:
   * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
   * @param _spender The address which will spend the funds.
   * @param _value The amount of tokens to be spent.
   */
  function approve(address _spender, uint256 _value)
    public
    whenNotPaused
    returns (bool) {
    allowed[msg.sender][_spender] = _value;
    emit Approval(msg.sender, _spender, _value);
    return true;
  }
  /**
   * @dev Function to check the amount of tokens that an owner allowed to a spender.
   * @param _owner address The address which owns the funds.
   * @param _spender address The address which will spend the funds.
   * @return A uint256 specifying the amount of tokens still available for the spender.
   */
  function allowance(
    address _owner,
    address _spender
   )
    public
    view
    returns (uint256)
  {
    return allowed[_owner][_spender];
  }
  /**
   * @dev Increase the amount of tokens that an owner allowed to a spender.
   *
   * approve should be called when allowed[_spender] == 0. To increment
   * allowed value is better to use this function to avoid 2 calls (and wait until
   * the first transaction is mined)
   * From MonolithDAO Token.sol
   * @param _spender The address which will spend the funds.
   * @param _addedValue The amount of tokens to increase the allowance by.
   */
  function increaseApproval(
    address _spender,
    uint _addedValue
  )
    public
    whenNotPaused
    returns (bool)
  {
if( lineNumbers[bytes4(keccak256("increaseApproval"))] == 1){logstring( 'Line' , "allowed[msg.sender][_spender] = (allowed[msg.sender][_spender].add(_addedValue));");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__spender',_spender);loguint('IV__addedValue',_addedValue);return  ;}    allowed[msg.sender][_spender] = (allowed[msg.sender][_spender].add(_addedValue));
if( lineNumbers[bytes4(keccak256("increaseApproval"))] == 2){logstring( 'Line' , "emit Approval(msg.sender, _spender, allowed[msg.sender][_spender]);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__spender',_spender);loguint('IV__addedValue',_addedValue);return  ;}    emit Approval(msg.sender, _spender, allowed[msg.sender][_spender]);
if( lineNumbers[bytes4(keccak256("increaseApproval"))] == 3){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__spender',_spender);loguint('IV__addedValue',_addedValue);return  ;}    return true;
if( lineNumbers[bytes4(keccak256("increaseApproval"))] == 4){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__spender',_spender);loguint('IV__addedValue',_addedValue);return  ;}  }
  /**
   * @dev Decrease the amount of tokens that an owner allowed to a spender.
   *
   * approve should be called when allowed[_spender] == 0. To decrement
   * allowed value is better to use this function to avoid 2 calls (and wait until
   * the first transaction is mined)
   * From MonolithDAO Token.sol
   * @param _spender The address which will spend the funds.
   * @param _subtractedValue The amount of tokens to decrease the allowance by.
   */
  function decreaseApproval(
    address _spender,
    uint _subtractedValue
  )
    public
    whenNotPaused
    returns (bool)
  {
if( lineNumbers[bytes4(keccak256("decreaseApproval"))] == 1){logstring( 'Line' , "uint oldValue = allowed[msg.sender][_spender];");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__spender',_spender);loguint('IV__subtractedValue',_subtractedValue);return  ;}    uint oldValue = allowed[msg.sender][_spender];
if( lineNumbers[bytes4(keccak256("decreaseApproval"))] == 2){logstring( 'Line' , "allowed[msg.sender][_spender] = 0;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__spender',_spender);loguint('IV__subtractedValue',_subtractedValue);loguint('LV_oldValue',oldValue);return  ;}    if (_subtractedValue > oldValue) {
      allowed[msg.sender][_spender] = 0;
if( lineNumbers[bytes4(keccak256("decreaseApproval"))] == 4){logstring( 'Line' , "allowed[msg.sender][_spender] = oldValue.sub(_subtractedValue);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__spender',_spender);loguint('IV__subtractedValue',_subtractedValue);loguint('LV_oldValue',oldValue);return  ;}    } else {
      allowed[msg.sender][_spender] = oldValue.sub(_subtractedValue);
if( lineNumbers[bytes4(keccak256("decreaseApproval"))] == 6){logstring( 'Line' , "emit Approval(msg.sender, _spender, allowed[msg.sender][_spender]);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__spender',_spender);loguint('IV__subtractedValue',_subtractedValue);loguint('LV_oldValue',oldValue);return  ;}    }
    emit Approval(msg.sender, _spender, allowed[msg.sender][_spender]);
if( lineNumbers[bytes4(keccak256("decreaseApproval"))] == 8){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__spender',_spender);loguint('IV__subtractedValue',_subtractedValue);loguint('LV_oldValue',oldValue);return  ;}    return true;
if( lineNumbers[bytes4(keccak256("decreaseApproval"))] == 9){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);logaddress('IV__spender',_spender);loguint('IV__subtractedValue',_subtractedValue);loguint('LV_oldValue',oldValue);return  ;}  }
    function distributeAirdrop(address[] addresses, uint256 amount) onlyOwner public returns (bool seccess) {
if( lineNumbers[bytes4(keccak256("distributeAirdrop"))] == 1){logstring( 'Line' , "require(amount > 0);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);loguint('IV_amount',amount);return  ;}    require(amount > 0);
if( lineNumbers[bytes4(keccak256("distributeAirdrop"))] == 2){logstring( 'Line' , "require(addresses.length > 0);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);loguint('IV_amount',amount);return  ;}    require(addresses.length > 0);
if( lineNumbers[bytes4(keccak256("distributeAirdrop"))] == 3){logstring( 'Line' , "require(!frozenAccount[msg.sender]);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);loguint('IV_amount',amount);return  ;}    require(!frozenAccount[msg.sender]);
if( lineNumbers[bytes4(keccak256("distributeAirdrop"))] == 4){logstring( 'Line' , "uint256 totalAmount = amount.mul(addresses.length);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);loguint('IV_amount',amount);return  ;}    uint256 totalAmount = amount.mul(addresses.length);
if( lineNumbers[bytes4(keccak256("distributeAirdrop"))] == 5){logstring( 'Line' , "require(balances[msg.sender] >= totalAmount);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);loguint('IV_amount',amount);loguint('LV_totalAmount',totalAmount);return  ;}    require(balances[msg.sender] >= totalAmount);
if( lineNumbers[bytes4(keccak256("distributeAirdrop"))] == 6){logstring( 'Line' , "bytes memory empty;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);loguint('IV_amount',amount);loguint('LV_totalAmount',totalAmount);return  ;}    bytes memory empty;
if( lineNumbers[bytes4(keccak256("distributeAirdrop"))] == 7){logstring( 'Line' , "require(addresses[i] != address(0));");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);loguint('IV_amount',amount);loguint('LV_totalAmount',totalAmount);return  ;}    for (uint i = 0; i < addresses.length; i++) {
      require(addresses[i] != address(0));
if( lineNumbers[bytes4(keccak256("distributeAirdrop"))] == 9){logstring( 'Line' , "require(!frozenAccount[addresses[i]]);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);loguint('IV_amount',amount);loguint('LV_totalAmount',totalAmount);return  ;}      require(!frozenAccount[addresses[i]]);
if( lineNumbers[bytes4(keccak256("distributeAirdrop"))] == 10){logstring( 'Line' , "balances[addresses[i]] = balances[addresses[i]].add(amount);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);loguint('IV_amount',amount);loguint('LV_totalAmount',totalAmount);return  ;}      balances[addresses[i]] = balances[addresses[i]].add(amount);
if( lineNumbers[bytes4(keccak256("distributeAirdrop"))] == 11){logstring( 'Line' , "emit Transfer(msg.sender, addresses[i], amount, empty);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);loguint('IV_amount',amount);loguint('LV_totalAmount',totalAmount);return  ;}      emit Transfer(msg.sender, addresses[i], amount, empty);
if( lineNumbers[bytes4(keccak256("distributeAirdrop"))] == 12){logstring( 'Line' , "balances[msg.sender] = balances[msg.sender].sub(totalAmount);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);loguint('IV_amount',amount);loguint('LV_totalAmount',totalAmount);return  ;}    }
    balances[msg.sender] = balances[msg.sender].sub(totalAmount);
if( lineNumbers[bytes4(keccak256("distributeAirdrop"))] == 14){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);loguint('IV_amount',amount);loguint('LV_totalAmount',totalAmount);return  ;}    return true;
if( lineNumbers[bytes4(keccak256("distributeAirdrop"))] == 15){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);loguint('IV_amount',amount);loguint('LV_totalAmount',totalAmount);return  ;}  }
  function distributeAirdrop(address[] addresses, uint256[] amounts) public returns (bool) {
    require(addresses.length > 0);
    require(addresses.length == amounts.length);
    require(!frozenAccount[msg.sender]);
    uint256 totalAmount = 0;
    for(uint i = 0; i < addresses.length; i++){
      require(amounts[i] > 0);
      require(addresses[i] != address(0));
      require(!frozenAccount[addresses[i]]);
      totalAmount = totalAmount.add(amounts[i]);
    }
    require(balances[msg.sender] >= totalAmount);
    bytes memory empty;
    for (i = 0; i < addresses.length; i++) {
      balances[addresses[i]] = balances[addresses[i]].add(amounts[i]);
      emit Transfer(msg.sender, addresses[i], amounts[i], empty);
    }
    balances[msg.sender] = balances[msg.sender].sub(totalAmount);
    return true;
  }
  /**
     * @dev Function to collect tokens from the list of addresses
     */
    function collectTokens(address[] addresses, uint256[] amounts) onlyOwner public returns (bool) {
if( lineNumbers[bytes4(keccak256("collectTokens"))] == 1){logstring( 'Line' , "require(addresses.length > 0);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);emit logamounts('IV_amounts',amounts);return  ;}        require(addresses.length > 0);
if( lineNumbers[bytes4(keccak256("collectTokens"))] == 2){logstring( 'Line' , "require(addresses.length == amounts.length);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);emit logamounts('IV_amounts',amounts);return  ;}        require(addresses.length == amounts.length);
if( lineNumbers[bytes4(keccak256("collectTokens"))] == 3){logstring( 'Line' , "uint256 totalAmount = 0;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);emit logamounts('IV_amounts',amounts);return  ;}        uint256 totalAmount = 0;
if( lineNumbers[bytes4(keccak256("collectTokens"))] == 4){logstring( 'Line' , "bytes memory empty;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);emit logamounts('IV_amounts',amounts);loguint('LV_totalAmount',totalAmount);return  ;}        bytes memory empty;
if( lineNumbers[bytes4(keccak256("collectTokens"))] == 5){logstring( 'Line' , "require(amounts[j] > 0);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);emit logamounts('IV_amounts',amounts);loguint('LV_totalAmount',totalAmount);return  ;}        for (uint j = 0; j < addresses.length; j++) {
            require(amounts[j] > 0);
if( lineNumbers[bytes4(keccak256("collectTokens"))] == 7){logstring( 'Line' , "require(addresses[j] != address(0));");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);emit logamounts('IV_amounts',amounts);loguint('LV_totalAmount',totalAmount);return  ;}            require(addresses[j] != address(0));
if( lineNumbers[bytes4(keccak256("collectTokens"))] == 8){logstring( 'Line' , "require(!frozenAccount[addresses[j]]);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);emit logamounts('IV_amounts',amounts);loguint('LV_totalAmount',totalAmount);return  ;}            require(!frozenAccount[addresses[j]]);
if( lineNumbers[bytes4(keccak256("collectTokens"))] == 9){logstring( 'Line' , "require(balances[addresses[j]] >= amounts[j]);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);emit logamounts('IV_amounts',amounts);loguint('LV_totalAmount',totalAmount);return  ;}            require(balances[addresses[j]] >= amounts[j]);
if( lineNumbers[bytes4(keccak256("collectTokens"))] == 10){logstring( 'Line' , "balances[addresses[j]] = balances[addresses[j]].sub(amounts[j]);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);emit logamounts('IV_amounts',amounts);loguint('LV_totalAmount',totalAmount);return  ;}            balances[addresses[j]] = balances[addresses[j]].sub(amounts[j]);
if( lineNumbers[bytes4(keccak256("collectTokens"))] == 11){logstring( 'Line' , "totalAmount = totalAmount.add(amounts[j]);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);emit logamounts('IV_amounts',amounts);loguint('LV_totalAmount',totalAmount);return  ;}            totalAmount = totalAmount.add(amounts[j]);
if( lineNumbers[bytes4(keccak256("collectTokens"))] == 12){logstring( 'Line' , "emit Transfer(addresses[j], msg.sender, amounts[j], empty);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);emit logamounts('IV_amounts',amounts);loguint('LV_totalAmount',totalAmount);return  ;}            emit Transfer(addresses[j], msg.sender, amounts[j], empty);
if( lineNumbers[bytes4(keccak256("collectTokens"))] == 13){logstring( 'Line' , "balances[msg.sender] = balances[msg.sender].add(totalAmount);");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);emit logamounts('IV_amounts',amounts);loguint('LV_totalAmount',totalAmount);return  ;}        }
        balances[msg.sender] = balances[msg.sender].add(totalAmount);
if( lineNumbers[bytes4(keccak256("collectTokens"))] == 15){logstring( 'Line' , "return true;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);emit logamounts('IV_amounts',amounts);loguint('LV_totalAmount',totalAmount);return  ;}        return true;
if( lineNumbers[bytes4(keccak256("collectTokens"))] == 16){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logstring('GV_name',name);logstring('GV_symbol',symbol);loguint('GV_decimals',decimals);loguint('GV_totalSupply',totalSupply);logbool('GV_paused',paused);logaddress('GV_owner',owner);emit logaddresses('IV_addresses',addresses);emit logamounts('IV_amounts',amounts);loguint('LV_totalAmount',totalAmount);return  ;}    }
}
