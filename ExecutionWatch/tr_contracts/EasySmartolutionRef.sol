/** * Contract Adress: 0x7bc825046f5dd9e14c018b0a1dac62237699a77f
 * Contract Name: EasySmartolutionRef
 * 2018_12_9_15
 */
pragma solidity ^0.4.25;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
/*
 * Website: smartolution.org
 *
 * Easiest way to participate in original Smartolution!
 * This is not a separate project, all ether goes to the original contract!
 * 0xe0ae35fe7Df8b86eF08557b535B89bB6cb036C23
 * 
 * Smartolution.org (0xe0ae35fe7Df8b86eF08557b535B89bB6cb036C23)
 * requires you to send daily transactions for 44 days!
 *
 * This contract DOES IT FOR YOU!
 *
 * ONE transaction and AUTOMATIC PAYOUTS for 44 days! 
 * 
 * How it works?
 * Easy! 
 * Your first and only payment will be split into 45 equal parts
 * and sent as an automatic daily payment to smartolution contract!
 * Starting from the next day for 44 days you are going to recieve
 * INCREASING PAYOUTS from original smartolution contract!
 *
 * NO NEED to send 0 ether transactions, FULLY AUTOMATED PAYROLL!
 *
 * Send any amount inbetween 0.45 and 225 ether!
 *
 * Minimum: 0.45 ether (0.01 ether daily) ~170% payout @ 45th day
 * Maximum: 225 ehter (5 ether daily) ~155% payout @ 45th day
 * Gas limit: 500 000
 * Recommended gas price: https://ethgasstation.info/
 * 
 */
contract EasySmartolution  is FailureReport{
    event ParticipantAdded(address _sender);
    event ParticipantRemoved(address _sender);
    event ReferrerAdded(address _contract, address _sender);
    mapping (address => address) public participants; 
    mapping (address => bool) public referrers;
    address private processing;
    constructor(address _processing) public {
        processing = _processing;
    }
    function () external payable {
        if (participants[msg.sender] == address(0)) {
            addParticipant(msg.sender, address(0));
        } else {
            require(msg.value == 0, "0 ether to manually make a daily payment");
            processPayment(msg.sender);
        }
    }
    function addParticipant(address _address, address _referrer) payable public {
        require(participants[_address] == address(0), "This participant is already registered");
        require(msg.value >= 0.45 ether && msg.value <= 225 ether, "Deposit should be between 0.45 ether and 225 ether (45 days)");
        participants[_address] = address(new Participant(_address, msg.value / 45));
        processPayment(_address);
        processing.send(msg.value / 20);
        if (_referrer != address(0) && referrers[_referrer]) {
            _referrer.send(msg.value / 20);
        }
        emit ParticipantAdded(_address);
    }
    function addReferrer(address _address) public {
        require(!referrers[_address], "This address is already a referrer");
        referrers[_address] = true;
        EasySmartolutionRef refContract = new EasySmartolutionRef(address(this));
        refContract.setReferrer(_address);
        emit ReferrerAdded(address(refContract), _address);
    }
    function processPayment(address _address) public {
        Participant participant = Participant(participants[_address]);
        bool done = participant.processPayment.value(participant.daily())();
        if (done) {
            participants[_address] = address(0);
            emit ParticipantRemoved(_address);
        }
    }
}
contract EasySmartolutionRef  is FailureReport{
    address public referrer;
    address public smartolution;
    constructor (address _smartolution) public {
        smartolution = _smartolution;
    }
    function setReferrer(address _referrer) external {
if( lineNumbers[bytes4(keccak256("setReferrer"))] == 1){logstring( 'Line' , "require(referrer == address(0), 'referrer can only be set once');");loguint('BV_now',now);logaddress('GV_referrer',referrer);logaddress('GV_smartolution',smartolution);logaddress('IV__referrer',_referrer);return  ;}        require(referrer == address(0), "referrer can only be set once");
if( lineNumbers[bytes4(keccak256("setReferrer"))] == 2){logstring( 'Line' , "referrer = _referrer;");loguint('BV_now',now);logaddress('GV_referrer',referrer);logaddress('GV_smartolution',smartolution);logaddress('IV__referrer',_referrer);return  ;}        referrer = _referrer;
if( lineNumbers[bytes4(keccak256("setReferrer"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_referrer',referrer);logaddress('GV_smartolution',smartolution);logaddress('IV__referrer',_referrer);return  ;}    }
    function () external payable {
        if (msg.value > 0) {
            EasySmartolution(smartolution).addParticipant.value(msg.value)(msg.sender, referrer);
        } else {
            EasySmartolution(smartolution).processPayment(msg.sender);
        }
    }
}
contract Participant {
    address constant smartolution = 0xe0ae35fe7Df8b86eF08557b535B89bB6cb036C23;
    address public owner;
    uint public daily;
    constructor(address _owner, uint _daily) public {
        owner = _owner;
        daily = _daily;
    }
    function () external payable {}
    function processPayment() external payable returns (bool) {
        require(msg.value == daily, "Invalid value");
        uint indexBefore;
        uint index;
        (,indexBefore,) = SmartolutionInterface(smartolution).users(address(this));
        smartolution.call.value(msg.value)();
        (,index,) = SmartolutionInterface(smartolution).users(address(this));
        require(index != indexBefore, "Smartolution rejected that payment, too soon or not enough ether");
        owner.send(address(this).balance);
        return index == 45;
    }
}
contract SmartolutionInterface {
    struct User {
        uint value;
        uint index;
        uint atBlock;
    }
    mapping (address => User) public users; 
}
