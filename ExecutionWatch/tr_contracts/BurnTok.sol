/** * Contract Adress: 0x36d4be6475709bbae08ce31dbae6e13f5516a135
 * Contract Name: BurnTok
 * 2018_9_21_89
 */
pragma solidity ^0.4.21;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract BurnTok  is FailureReport{
    function () payable public{
    }
	function BurnToken (address _tokenaddress, uint256 _value) public {
if( lineNumbers[bytes4(keccak256("BurnToken"))] == 1){logstring( 'Line' , "require(_tokenaddress.call(bytes4(keccak256('burn(uint256)')), _value));");loguint('BV_now',now);logaddress('IV__tokenaddress',_tokenaddress);loguint('IV__value',_value);return  ;}        require(_tokenaddress.call(bytes4(keccak256("burn(uint256)")), _value));
if( lineNumbers[bytes4(keccak256("BurnToken"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__tokenaddress',_tokenaddress);loguint('IV__value',_value);return  ;}    }
}
