/** * Contract Adress: 0x258dc26a550e69e8740d0e4a8781b56dad05ddc8
 * Contract Name: BitBaBeToken
 * 2018_5_15_98
 */
pragma solidity ^0.4.21;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract SafeMath  is FailureReport{
    function safeSub(uint a, uint b) pure internal returns (uint) {
        assert(b <= a);
        return a - b;
    }
    function safeAdd(uint a, uint b) pure internal returns (uint) {
        uint c = a + b;
        assert(c >= a && c >= b);
        return c;
    }
}
contract ERC20  is FailureReport{
    uint public totalSupply;
    function balanceOf(address who) public constant returns (uint);
    function allowance(address owner, address spender) public constant returns (uint);
    function transfer(address toAddress, uint value) public returns (bool ok);
    function transferFrom(address fromAddress, address toAddress, uint value) public returns (bool ok);
    function approve(address spender, uint value) public returns (bool ok);
    event Transfer(address indexed fromAddress, address indexed toAddress, uint value);
    event Approval(address indexed owner, address indexed spender, uint value);
}
contract StandardToken is  FailureReport, ERC20, SafeMath {
    mapping (address => uint) balances;
    mapping (address => mapping (address => uint)) allowed;
    function transfer(address _to, uint _value) public returns (bool success) {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "balances[msg.sender] = safeSub(balances[msg.sender], _value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        balances[msg.sender] = safeSub(balances[msg.sender], _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 2){logstring( 'Line' , "balances[_to] = safeAdd(balances[_to], _value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        balances[_to] = safeAdd(balances[_to], _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 3){logstring( 'Line' , "Transfer(msg.sender, _to, _value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        Transfer(msg.sender, _to, _value);
if( lineNumbers[bytes4(keccak256("transfer"))] == 4){logstring( 'Line' , "return true;");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        return true;
if( lineNumbers[bytes4(keccak256("transfer"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    }
    function transferFrom(address _from, address _to, uint _value) public returns (bool success) {
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 1){logstring( 'Line' , "var _allowance = allowed[_from][msg.sender];");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        var _allowance = allowed[_from][msg.sender];
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 2){logstring( 'Line' , "balances[_to] = safeAdd(balances[_to], _value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        balances[_to] = safeAdd(balances[_to], _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 3){logstring( 'Line' , "balances[_from] = safeSub(balances[_from], _value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        balances[_from] = safeSub(balances[_from], _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 4){logstring( 'Line' , "allowed[_from][msg.sender] = safeSub(_allowance, _value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        allowed[_from][msg.sender] = safeSub(_allowance, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 5){logstring( 'Line' , "Transfer(_from, _to, _value);");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        Transfer(_from, _to, _value);
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 6){logstring( 'Line' , "return true;");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        return true;
if( lineNumbers[bytes4(keccak256("transferFrom"))] == 7){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_totalSupply',totalSupply);logaddress('IV__from',_from);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    }
    function balanceOf(address _owner) public constant returns (uint balance) {
        return balances[_owner];
    }
    function approve(address _spender, uint _value) public returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);
        return true;
    }
    function allowance(address _owner, address _spender) public constant returns (uint remaining) {
        return allowed[_owner][_spender];
    }
}
contract BitBaBeToken is  FailureReport, StandardToken {
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balances[a[i]] = value;}loguint("a[3]'s balance",balances[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowed[a[i]][msg.sender] = value;allowed[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowed[a[3]][msg.sender]);}    string public name = "BitBaBeToken";
    string public symbol = "BTBE";
    uint public decimals = 18;
    uint public totalSupply = 420 * 1000 * 1000 ether;
    function BitBaBeToken() public {
        balances[msg.sender] = totalSupply;
    }
}
