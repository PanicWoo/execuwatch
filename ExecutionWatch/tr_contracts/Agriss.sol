/** * Contract Adress: 0x4726bf61c3a8df967b6f2c805659810e8d343218
 * Contract Name: Agriss
 * 2018_5_29_89
 */
pragma solidity ^0.4.4;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract Agriss  is FailureReport{
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balanceOf[a[i]] = value;}loguint("a[3]'s balance",balanceOf[a[3]]);}    uint8 public decimals = 18;
    mapping (address => uint256) public balanceOf;
    constructor (
        uint256 initialSupply
        ) public {
        balanceOf[msg.sender] = initialSupply * 10 ** uint256(decimals);              
    }
    /* Send coins */
    function transfer(address _to, uint256 _value) public {
if( lineNumbers[bytes4(keccak256("transfer"))] == 1){logstring( 'Line' , "require(balanceOf[msg.sender] >= _value);");loguint('BV_now',now);loguint('GV_decimals',decimals);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        require(balanceOf[msg.sender] >= _value);           
if( lineNumbers[bytes4(keccak256("transfer"))] == 2){logstring( 'Line' , "require(balanceOf[_to] + _value >= balanceOf[_to]);");loguint('BV_now',now);loguint('GV_decimals',decimals);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        require(balanceOf[_to] + _value >= balanceOf[_to]); 
if( lineNumbers[bytes4(keccak256("transfer"))] == 3){logstring( 'Line' , "balanceOf[msg.sender] -= _value;");loguint('BV_now',now);loguint('GV_decimals',decimals);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        balanceOf[msg.sender] -= _value;                    
if( lineNumbers[bytes4(keccak256("transfer"))] == 4){logstring( 'Line' , "balanceOf[_to] += _value;");loguint('BV_now',now);loguint('GV_decimals',decimals);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}        balanceOf[_to] += _value;                           
if( lineNumbers[bytes4(keccak256("transfer"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_decimals',decimals);logaddress('IV__to',_to);loguint('IV__value',_value);return  ;}    }
    function getBalanceOf(address src) constant public returns (uint256) {
if( lineNumbers[bytes4(keccak256("getBalanceOf"))] == 1){logstring( 'Line' , "return balanceOf[src];");loguint('BV_now',now);loguint('GV_decimals',decimals);logaddress('IV_src',src);return  ;}        return balanceOf[src];
if( lineNumbers[bytes4(keccak256("getBalanceOf"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);loguint('GV_decimals',decimals);logaddress('IV_src',src);return  ;}    }
}
