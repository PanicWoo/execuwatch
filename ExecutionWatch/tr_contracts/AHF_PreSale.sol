/** * Contract Adress: 0xd74ac22ffc06d6f96cb41eef4e0fdb836889c3ff
 * Contract Name: AHF_PreSale
 * 2018_5_19_29
 */
pragma solidity ^0.4.24;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
// ----------------------------------------------------------------------------
// Owned contract
// ----------------------------------------------------------------------------
contract Owned  is FailureReport{
    address public owner;
    address public newOwner;
    event OwnershipTransferred(address indexed _from, address indexed _to);
    constructor() public {
        owner = msg.sender;
    }
    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }
    function transferOwnership(address _newOwner) public onlyOwner {
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 1){logstring( 'Line' , "newOwner = _newOwner;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV__newOwner',_newOwner);return  ;}        newOwner = _newOwner;
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV__newOwner',_newOwner);return  ;}    }
    function acceptOwnership() public {
if( lineNumbers[bytes4(keccak256("acceptOwnership"))] == 1){logstring( 'Line' , "require(msg.sender == newOwner);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);return  ;}        require(msg.sender == newOwner);
if( lineNumbers[bytes4(keccak256("acceptOwnership"))] == 2){logstring( 'Line' , "emit OwnershipTransferred(owner, newOwner);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);return  ;}        emit OwnershipTransferred(owner, newOwner);
if( lineNumbers[bytes4(keccak256("acceptOwnership"))] == 3){logstring( 'Line' , "owner = newOwner;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);return  ;}        owner = newOwner;
if( lineNumbers[bytes4(keccak256("acceptOwnership"))] == 4){logstring( 'Line' , "newOwner = address(0);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);return  ;}        newOwner = address(0);
if( lineNumbers[bytes4(keccak256("acceptOwnership"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);return  ;}    }
}
// ----------------------------------------------------------------------------
// ERC Token Standard #20 Interface
// https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20-token-standard.md
// ----------------------------------------------------------------------------
contract ERC20Interface  is FailureReport{
    function totalSupply() public constant returns (uint);
    function balanceOf(address tokenOwner) public constant returns (uint balance);
    function allowance(address tokenOwner, address spender) public constant returns (uint remaining);
    function transfer(address to, uint tokens) public returns (bool success);
    function approve(address spender, uint tokens) public returns (bool success);
    function transferFrom(address from, address to, uint tokens) public returns (bool success);
    event Transfer(address indexed from, address indexed to, uint tokens);
    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);
}
// ----------------------------------------------------------------------------
// Dividends implementation interface
// ----------------------------------------------------------------------------
contract AHF_PreSale is  FailureReport, Owned {
    ERC20Interface public tokenContract;
    address public vaultAddress;
    bool public fundingEnabled;
    uint public totalCollected;         // In wei
    uint public tokenPrice;         // In wei
    function setTokenAddress(address _tokenAddress) public onlyOwner {
if( lineNumbers[bytes4(keccak256("setTokenAddress"))] == 1){logstring( 'Line' , "tokenContract = ERC20Interface(_tokenAddress);");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV__tokenAddress',_tokenAddress);return  ;}        tokenContract = ERC20Interface(_tokenAddress);
if( lineNumbers[bytes4(keccak256("setTokenAddress"))] == 2){logstring( 'Line' , "return;");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV__tokenAddress',_tokenAddress);return  ;}        return;
if( lineNumbers[bytes4(keccak256("setTokenAddress"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV__tokenAddress',_tokenAddress);return  ;}    }
    function setVaultAddress(address _vaultAddress) public onlyOwner {
if( lineNumbers[bytes4(keccak256("setVaultAddress"))] == 1){logstring( 'Line' , "vaultAddress = _vaultAddress;");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV__vaultAddress',_vaultAddress);return  ;}        vaultAddress = _vaultAddress;
if( lineNumbers[bytes4(keccak256("setVaultAddress"))] == 2){logstring( 'Line' , "return;");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV__vaultAddress',_vaultAddress);return  ;}        return;
if( lineNumbers[bytes4(keccak256("setVaultAddress"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV__vaultAddress',_vaultAddress);return  ;}    }
    function setFundingEnabled(bool _fundingEnabled) public onlyOwner {
if( lineNumbers[bytes4(keccak256("setFundingEnabled"))] == 1){logstring( 'Line' , "fundingEnabled = _fundingEnabled;");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logbool('IV__fundingEnabled',_fundingEnabled);return  ;}        fundingEnabled = _fundingEnabled;
if( lineNumbers[bytes4(keccak256("setFundingEnabled"))] == 2){logstring( 'Line' , "return;");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logbool('IV__fundingEnabled',_fundingEnabled);return  ;}        return;
if( lineNumbers[bytes4(keccak256("setFundingEnabled"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logbool('IV__fundingEnabled',_fundingEnabled);return  ;}    }
    function updateTokenPrice(uint _newTokenPrice) public onlyOwner {
if( lineNumbers[bytes4(keccak256("updateTokenPrice"))] == 1){logstring( 'Line' , "tokenPrice = _newTokenPrice;");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);loguint('IV__newTokenPrice',_newTokenPrice);return  ;}        tokenPrice = _newTokenPrice;
if( lineNumbers[bytes4(keccak256("updateTokenPrice"))] == 2){logstring( 'Line' , "return;");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);loguint('IV__newTokenPrice',_newTokenPrice);return  ;}        return;
if( lineNumbers[bytes4(keccak256("updateTokenPrice"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);loguint('IV__newTokenPrice',_newTokenPrice);return  ;}    }
    function () public payable {
        require (fundingEnabled && (tokenPrice > 0) && (msg.value >= tokenPrice));
        totalCollected += msg.value;
        //Send the ether to the vault
        vaultAddress.transfer(msg.value);
        uint tokens = (msg.value * 10**18) / tokenPrice;
        require (tokenContract.transfer(msg.sender, tokens));
        return;
    }
    /// @notice This method can be used by the owner to extract mistakenly
    ///  sent tokens to this contract.
    /// @param _token The address of the token contract that you want to recover
    ///  set to 0 in case you want to extract ether.
    function claimTokens(address _token) public onlyOwner {
if( lineNumbers[bytes4(keccak256("claimTokens"))] == 1){logstring( 'Line' , "owner.transfer(address(this).balance);");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV__token',_token);return  ;}        if (_token == 0x0) {
            owner.transfer(address(this).balance);
if( lineNumbers[bytes4(keccak256("claimTokens"))] == 3){logstring( 'Line' , "return;");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV__token',_token);return  ;}            return;
if( lineNumbers[bytes4(keccak256("claimTokens"))] == 4){logstring( 'Line' , "ERC20Interface token = ERC20Interface(_token);");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV__token',_token);return  ;}        }
        ERC20Interface token = ERC20Interface(_token);
if( lineNumbers[bytes4(keccak256("claimTokens"))] == 6){logstring( 'Line' , "uint balance = token.balanceOf(this);");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV__token',_token);return  ;}        uint balance = token.balanceOf(this);
if( lineNumbers[bytes4(keccak256("claimTokens"))] == 7){logstring( 'Line' , "token.transfer(owner, balance);");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV__token',_token);loguint('LV_balance',balance);return  ;}        token.transfer(owner, balance);
if( lineNumbers[bytes4(keccak256("claimTokens"))] == 8){logstring( 'Line' , "emit ClaimedTokens(_token, owner, balance);");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV__token',_token);loguint('LV_balance',balance);return  ;}        emit ClaimedTokens(_token, owner, balance);
if( lineNumbers[bytes4(keccak256("claimTokens"))] == 9){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_vaultAddress',vaultAddress);logbool('GV_fundingEnabled',fundingEnabled);loguint('GV_totalCollected',totalCollected);loguint('GV_tokenPrice',tokenPrice);logaddress('GV_owner',owner);logaddress('GV_newOwner',newOwner);logaddress('IV__token',_token);loguint('LV_balance',balance);return  ;}    }
    event ClaimedTokens(address indexed _token, address indexed _controller, uint _amount);
}
