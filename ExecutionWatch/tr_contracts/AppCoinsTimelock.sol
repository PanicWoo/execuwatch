/** * Contract Adress: 0xcaf12149a090c83836e1f07632b8c139bc0ef6c6
 * Contract Name: AppCoinsTimelock
 * 2018_10_25_67
 */
pragma solidity ^0.4.24;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
contract ERC20Interface  is FailureReport{
    function name() public view returns(bytes32);
    function symbol() public view returns(bytes32);
    function balanceOf (address _owner) public view returns(uint256 balance);
    function transfer(address _to, uint256 _value) public returns (bool success);
    function transferFrom(address _from, address _to, uint256 _value) public returns (uint);
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
}
contract AppCoins is  FailureReport, ERC20Interface{
    // Public variables of the token
    address public owner;
    bytes32 private token_name;
    bytes32 private token_symbol;
    uint8 public decimals = 18;
    // 18 decimals is the strongly suggested default, avoid changing it
    uint256 public totalSupply;
    // This creates an array with all balances
    mapping (address => uint256) public balances;
    mapping (address => mapping (address => uint256)) public allowance;
    // This generates a public event on the blockchain that will notify clients
    event Transfer(address indexed from, address indexed to, uint256 value);
    // This notifies clients about the amount burnt
    event Burn(address indexed from, uint256 value);
    function AppCoins() public {
        owner = msg.sender;
        token_name = "AppCoins";
        token_symbol = "APPC";
        uint256 _totalSupply = 1000000;
        totalSupply = _totalSupply * 10 ** uint256(decimals);  // Update total supply with the decimal amount
        balances[owner] = totalSupply;                // Give the creator all initial tokens
    }
    function name() public view returns(bytes32) {
        return token_name;
    }
    function symbol() public view returns(bytes32) {
        return token_symbol;
    }
    function balanceOf (address _owner) public view returns(uint256 balance) {
        return balances[_owner];
    }
    function _transfer(address _from, address _to, uint _value) internal returns (bool) {
        // Prevent transfer to 0x0 address. Use burn() instead
        require(_to != 0x0);
        // Check if the sender has enough
        require(balances[_from] >= _value);
        // Check for overflows
        require(balances[_to] + _value > balances[_to]);
        // Save this for an assertion in the future
        uint previousBalances = balances[_from] + balances[_to];
        // Subtract from the sender
        balances[_from] -= _value;
        // Add the same to the recipient
        balances[_to] += _value;
        emit Transfer(_from, _to, _value);
        // Asserts are used to use static analysis to find bugs in your code. They should never fail
        assert(balances[_from] + balances[_to] == previousBalances);
    }
    function transfer (address _to, uint256 _amount) public returns (bool success) {
        if( balances[msg.sender] >= _amount && _amount > 0 && balances[_to] + _amount > balances[_to]) {
            balances[msg.sender] -= _amount;
            balances[_to] += _amount;
            emit Transfer(msg.sender, _to, _amount);
            return true;
        } else {
            return false;
        }
    }
    function transferFrom(address _from, address _to, uint256 _value) public returns (uint) {
        require(_value <= allowance[_from][msg.sender]);     // Check allowance
        allowance[_from][msg.sender] -= _value;
        _transfer(_from, _to, _value);
        return allowance[_from][msg.sender];
    }
    function approve(address _spender, uint256 _value) public
        returns (bool success) {
        allowance[msg.sender][_spender] = _value;
        return true;
    }
    function burn(uint256 _value) public returns (bool success) {
        require(balances[msg.sender] >= _value);   // Check if the sender has enough
        balances[msg.sender] -= _value;            // Subtract from the sender
        totalSupply -= _value;                      // Updates totalSupply
        emit Burn(msg.sender, _value);
        return true;
    }
    function burnFrom(address _from, uint256 _value) public returns (bool success) {
        require(balances[_from] >= _value);                // Check if the targeted balance is enough
        require(_value <= allowance[_from][msg.sender]);    // Check allowance
        balances[_from] -= _value;                         // Subtract from the targeted balance
        allowance[_from][msg.sender] -= _value;             // Subtract from the sender's allowance
        totalSupply -= _value;                              // Update totalSupply
        emit Burn(_from, _value);
        return true;
    }
}
contract AppCoinsTimelock  is FailureReport{
function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){balances[a[i]] = value;}loguint("a[3]'s balance",balances[a[3]]);}function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){allowance[a[i]][msg.sender] = value;allowance[msg.sender][a[i]] = value;}loguint("a[3][msg.sender]'s allowance",allowance[a[3]][msg.sender]);}
event log_addresses(string,address[]);
event log_amounts(string,uint[]);    // AppCoins token
    AppCoins private appc;
    // beneficiary of tokens
    mapping (address => uint) balances;
    // timestamp when token release is enabled
    uint private releaseTime;
    event NewFundsAllocated(address _address,  uint _amount);
    event FundsReleased(address _address,  uint _amount);
    constructor(
        address _addrAppc,
        uint256 _releaseTime
    )
    public
    {
        appc = AppCoins(_addrAppc);
        releaseTime = _releaseTime;
    }
    function getReleaseTime() public view returns(uint256) {
if( lineNumbers[bytes4(keccak256("getReleaseTime"))] == 1){logstring( 'Line' , "return releaseTime;");loguint('BV_now',now);return  ;}        return releaseTime;
if( lineNumbers[bytes4(keccak256("getReleaseTime"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);return  ;}    }
    function getBalanceOf(address _address) public view returns(uint256){
if( lineNumbers[bytes4(keccak256("getBalanceOf"))] == 1){logstring( 'Line' , "return balances[_address];");loguint('BV_now',now);logaddress('IV__address',_address);return  ;}        return balances[_address];
if( lineNumbers[bytes4(keccak256("getBalanceOf"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__address',_address);return  ;}    }
    function allocateFunds(address _address, uint256 _amount) public {
if( lineNumbers[bytes4(keccak256("allocateFunds"))] == 1){logstring( 'Line' , "require(appc.allowance(msg.sender, address(this)) >= _amount);");loguint('BV_now',now);logaddress('IV__address',_address);loguint('IV__amount',_amount);return  ;}        require(appc.allowance(msg.sender, address(this)) >= _amount);
if( lineNumbers[bytes4(keccak256("allocateFunds"))] == 2){logstring( 'Line' , "appc.transferFrom(msg.sender, address(this), _amount);");loguint('BV_now',now);logaddress('IV__address',_address);loguint('IV__amount',_amount);return  ;}        appc.transferFrom(msg.sender, address(this), _amount);
if( lineNumbers[bytes4(keccak256("allocateFunds"))] == 3){logstring( 'Line' , "balances[_address] = balances[_address] + _amount;");loguint('BV_now',now);logaddress('IV__address',_address);loguint('IV__amount',_amount);return  ;}        balances[_address] = balances[_address] + _amount;
if( lineNumbers[bytes4(keccak256("allocateFunds"))] == 4){logstring( 'Line' , "emit NewFundsAllocated(_address, balances[_address]);");loguint('BV_now',now);logaddress('IV__address',_address);loguint('IV__amount',_amount);return  ;}        emit NewFundsAllocated(_address, balances[_address]);
if( lineNumbers[bytes4(keccak256("allocateFunds"))] == 5){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__address',_address);loguint('IV__amount',_amount);return  ;}    }
    function allocateFundsBulk(address[] _addresses, uint256[] _amounts) public {
if( lineNumbers[bytes4(keccak256("allocateFundsBulk"))] == 1){logstring( 'Line' , "require(_addresses.length == _amounts.length);");loguint('BV_now',now);emit log_addresses('IV__addresses',_addresses);emit log_amounts('IV__amounts',_amounts);return  ;}        require(_addresses.length == _amounts.length);
if( lineNumbers[bytes4(keccak256("allocateFundsBulk"))] == 2){logstring( 'Line' , "allocateFunds(_addresses[i], _amounts[i]);");loguint('BV_now',now);emit log_addresses('IV__addresses',_addresses);emit log_amounts('IV__amounts',_amounts);return  ;}        for(uint i = 0; i < _addresses.length; i++){
            allocateFunds(_addresses[i], _amounts[i]);
if( lineNumbers[bytes4(keccak256("allocateFundsBulk"))] == 4){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);emit log_addresses('IV__addresses',_addresses);emit log_amounts('IV__amounts',_amounts);return  ;}        }
    }
    function release(address _address) public {
if( lineNumbers[bytes4(keccak256("release"))] == 1){logstring( 'Line' , "require(balances[_address] > 0);");loguint('BV_now',now);logaddress('IV__address',_address);return  ;}        require(balances[_address] > 0);
if( lineNumbers[bytes4(keccak256("release"))] == 2){logstring( 'Line' , "uint nowInMilliseconds = block.timestamp * 1000;");loguint('BV_now',now);logaddress('IV__address',_address);return  ;}        uint nowInMilliseconds = block.timestamp * 1000;
if( lineNumbers[bytes4(keccak256("release"))] == 3){logstring( 'Line' , "require(nowInMilliseconds >= releaseTime);");loguint('BV_now',now);logaddress('IV__address',_address);loguint('LV_nowInMilliseconds',nowInMilliseconds);return  ;}        require(nowInMilliseconds >= releaseTime);
if( lineNumbers[bytes4(keccak256("release"))] == 4){logstring( 'Line' , "uint amount = balances[_address];");loguint('BV_now',now);logaddress('IV__address',_address);loguint('LV_nowInMilliseconds',nowInMilliseconds);return  ;}        uint amount = balances[_address];
if( lineNumbers[bytes4(keccak256("release"))] == 5){logstring( 'Line' , "balances[_address] = 0;");loguint('BV_now',now);logaddress('IV__address',_address);loguint('LV_nowInMilliseconds',nowInMilliseconds);loguint('LV_amount',amount);return  ;}        balances[_address] = 0;
if( lineNumbers[bytes4(keccak256("release"))] == 6){logstring( 'Line' , "appc.transfer(_address, amount);");loguint('BV_now',now);logaddress('IV__address',_address);loguint('LV_nowInMilliseconds',nowInMilliseconds);loguint('LV_amount',amount);return  ;}        appc.transfer(_address, amount);
if( lineNumbers[bytes4(keccak256("release"))] == 7){logstring( 'Line' , "emit FundsReleased(_address, amount);");loguint('BV_now',now);logaddress('IV__address',_address);loguint('LV_nowInMilliseconds',nowInMilliseconds);loguint('LV_amount',amount);return  ;}        emit FundsReleased(_address, amount);
if( lineNumbers[bytes4(keccak256("release"))] == 8){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__address',_address);loguint('LV_nowInMilliseconds',nowInMilliseconds);loguint('LV_amount',amount);return  ;}    }
}
