/** * Contract Adress: 0xb1f8e55c7f64d203c1400b9d8555d050f94adf39
 * Contract Name: BalanceChecker
 * 2018_12_8_2
 */
pragma solidity ^0.4.21;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
// ERC20 contract interface
contract Token  is FailureReport{
  function balanceOf(address) public view returns (uint);
}
contract BalanceChecker  is FailureReport{

event logaddrBalances(string,uint[]);
event logusers(string,address[]);
event logtokens(string,address[]);  /* Fallback function, don't accept any ETH */
  function() public payable {
    revert("BalanceChecker does not accept payments");
  }
  /*
    Check the token balance of a wallet in a token contract
    Returns the balance of the token for user. Avoids possible errors:
      - return 0 on non-contract address 
      - returns 0 if the contract doesn't implement balanceOf
  */
  function tokenBalance(address user, address token) public view returns (uint) {
if( lineNumbers[bytes4(keccak256("tokenBalance"))] == 1){logstring( 'Line' , "uint256 tokenCode;");loguint('BV_now',now);logaddress('IV_user',user);logaddress('IV_token',token);return  ;}    // check if token is actually a contract
    uint256 tokenCode;
if( lineNumbers[bytes4(keccak256("tokenBalance"))] == 3){logstring( 'Line' , "return Token(token).balanceOf(user);");loguint('BV_now',now);logaddress('IV_user',user);logaddress('IV_token',token);loguint('LV_tokenCode',tokenCode);return  ;}    assembly { tokenCode := extcodesize(token) } // contract code size
    // is it a contract and does it implement balanceOf 
    if (tokenCode > 0 && token.call(bytes4(0x70a08231), user)) {  
      return Token(token).balanceOf(user);
if( lineNumbers[bytes4(keccak256("tokenBalance"))] == 7){logstring( 'Line' , "return 0;");loguint('BV_now',now);logaddress('IV_user',user);logaddress('IV_token',token);loguint('LV_tokenCode',tokenCode);return  ;}    } else {
      return 0;
if( lineNumbers[bytes4(keccak256("tokenBalance"))] == 9){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV_user',user);logaddress('IV_token',token);loguint('LV_tokenCode',tokenCode);return  ;}    }
  }
  /*
    Check the token balances of a wallet for multiple tokens.
    Pass 0x0 as a "token" address to get ETH balance.
    Possible error throws:
      - extremely large arrays for user and or tokens (gas cost too high) 
    Returns a one-dimensional that's user.length * tokens.length long. The
    array is ordered by all of the 0th users token balances, then the 1th
    user, and so on.
  */
  function balances(address[] users, address[] tokens) external view returns (uint[]) {
if( lineNumbers[bytes4(keccak256("balances"))] == 1){logstring( 'Line' , "uint[] memory addrBalances = new uint[](tokens.length * users.length);");loguint('BV_now',now);emit logusers('IV_users',users);emit logtokens('IV_tokens',tokens);return  ;}    uint[] memory addrBalances = new uint[](tokens.length * users.length);
if( lineNumbers[bytes4(keccak256("balances"))] == 2){logstring( 'Line' , "uint addrIdx = j + tokens.length * i;");loguint('BV_now',now);emit logusers('IV_users',users);emit logtokens('IV_tokens',tokens);emit logaddrBalances('LV_addrBalances',addrBalances);return  ;}    for(uint i = 0; i < users.length; i++) {
      for (uint j = 0; j < tokens.length; j++) {
        uint addrIdx = j + tokens.length * i;
if( lineNumbers[bytes4(keccak256("balances"))] == 5){logstring( 'Line' , "addrBalances[addrIdx] = tokenBalance(users[i], tokens[j]);");loguint('BV_now',now);emit logusers('IV_users',users);emit logtokens('IV_tokens',tokens);emit logaddrBalances('LV_addrBalances',addrBalances);loguint('LV_addrIdx',addrIdx);return  ;}        if (tokens[j] != address(0x0)) { 
          addrBalances[addrIdx] = tokenBalance(users[i], tokens[j]);
if( lineNumbers[bytes4(keccak256("balances"))] == 7){logstring( 'Line' , "addrBalances[addrIdx] = users[i].balance; // ETH balance");loguint('BV_now',now);emit logusers('IV_users',users);emit logtokens('IV_tokens',tokens);emit logaddrBalances('LV_addrBalances',addrBalances);loguint('LV_addrIdx',addrIdx);return  ;}        } else {
          addrBalances[addrIdx] = users[i].balance; // ETH balance    
if( lineNumbers[bytes4(keccak256("balances"))] == 9){logstring( 'Line' , "return addrBalances;");loguint('BV_now',now);emit logusers('IV_users',users);emit logtokens('IV_tokens',tokens);emit logaddrBalances('LV_addrBalances',addrBalances);loguint('LV_addrIdx',addrIdx);return  ;}        }
      }  
    }
    return addrBalances;
if( lineNumbers[bytes4(keccak256("balances"))] == 13){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);emit logusers('IV_users',users);emit logtokens('IV_tokens',tokens);emit logaddrBalances('LV_addrBalances',addrBalances);loguint('LV_addrIdx',addrIdx);return  ;}  }
}
