/** * Contract Adress: 0x67d0983da2f672e689bd9d84364729bf4c32b0b3
 * Contract Name: CCAirdropper
 * 2018_5_15_66
 */
pragma solidity ^0.4.21;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
/// @title Ownable contract
contract Ownable  is FailureReport{
    address public owner;
    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);
    constructor() public {
        owner = msg.sender;
    }
    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }
    function transferOwnership(address newOwner) onlyOwner public {
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 1){logstring( 'Line' , "require(newOwner != address(0));");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}        require(newOwner != address(0));
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 2){logstring( 'Line' , "emit OwnershipTransferred(owner, newOwner);");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}        emit OwnershipTransferred(owner, newOwner);
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 3){logstring( 'Line' , "owner = newOwner;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}        owner = newOwner;
if( lineNumbers[bytes4(keccak256("transferOwnership"))] == 4){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);logaddress('IV_newOwner',newOwner);return  ;}    }
}
/// @title Mortal contract - used to selfdestruct once we have no use of this contract
contract Mortal is  FailureReport, Ownable {
    function executeSelfdestruct() public onlyOwner {
if( lineNumbers[bytes4(keccak256("executeSelfdestruct"))] == 1){logstring( 'Line' , "selfdestruct(owner);");loguint('BV_now',now);logaddress('GV_owner',owner);return  ;}        selfdestruct(owner);
if( lineNumbers[bytes4(keccak256("executeSelfdestruct"))] == 2){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);return  ;}    }
}
/// @title ERC20 contract
/// https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20-token-standard.md
contract ERC20  is FailureReport{
    uint public totalSupply;
    function balanceOf(address who) public view returns (uint);
    function transfer(address to, uint value) public returns (bool);
    event Transfer(address indexed from, address indexed to, uint value);
    function allowance(address owner, address spender) public view returns (uint);
    function transferFrom(address from, address to, uint value) public returns (bool);
    function approve(address spender, uint value) public returns (bool);
    event Approval(address indexed owner, address indexed spender, uint value);
}
/// @title WizzleInfinityHelper contract
contract CCAirdropper is  FailureReport, Mortal {

event logdests(string,address[]);
event logvalues(string,uint[]);    mapping (address => bool) public whitelisted;
    ERC20 public token;
    constructor(address _token) public {
        token = ERC20(_token);
    }
    /// @dev Transfer tokens to addresses registered for airdrop
    /// @param dests Array of addresses that have registered for airdrop
    /// @param values Array of token amount for each address that have registered for airdrop
    /// @return Number of transfers
    function airdrop(address[] dests, uint256[] values) public onlyOwner returns (uint256) {
if( lineNumbers[bytes4(keccak256("airdrop"))] == 1){logstring( 'Line' , "require(dests.length == values.length);");loguint('BV_now',now);logaddress('GV_owner',owner);emit logdests('IV_dests',dests);emit logvalues('IV_values',values);return  ;}        require(dests.length == values.length);
if( lineNumbers[bytes4(keccak256("airdrop"))] == 2){logstring( 'Line' , "uint256 i = 0;");loguint('BV_now',now);logaddress('GV_owner',owner);emit logdests('IV_dests',dests);emit logvalues('IV_values',values);return  ;}        uint256 i = 0;
if( lineNumbers[bytes4(keccak256("airdrop"))] == 3){logstring( 'Line' , "token.transfer(dests[i], values[i]);");loguint('BV_now',now);logaddress('GV_owner',owner);emit logdests('IV_dests',dests);emit logvalues('IV_values',values);loguint('LV_i',i);return  ;}        while (i < dests.length) {
            token.transfer(dests[i], values[i]);
if( lineNumbers[bytes4(keccak256("airdrop"))] == 5){logstring( 'Line' , "i += 1;");loguint('BV_now',now);logaddress('GV_owner',owner);emit logdests('IV_dests',dests);emit logvalues('IV_values',values);loguint('LV_i',i);return  ;}            i += 1;
if( lineNumbers[bytes4(keccak256("airdrop"))] == 6){logstring( 'Line' , "return (i);");loguint('BV_now',now);logaddress('GV_owner',owner);emit logdests('IV_dests',dests);emit logvalues('IV_values',values);loguint('LV_i',i);return  ;}        }
        return (i); 
if( lineNumbers[bytes4(keccak256("airdrop"))] == 8){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('GV_owner',owner);emit logdests('IV_dests',dests);emit logvalues('IV_values',values);loguint('LV_i',i);return  ;}    }
}
