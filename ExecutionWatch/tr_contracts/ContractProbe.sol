/** * Contract Adress: 0x4b6b2db16e3eee4ee52bc3888c38a76c20d375f1
 * Contract Name: ContractProbe
 * 2018_9_14_26
 */
pragma solidity ^0.4.23;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
/*
The MIT License (MIT)
Copyright (c) 2018 Murray Software, LLC.
Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:
The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
//solhint-disable max-line-length
contract ContractProbe  is FailureReport{
    function probe(address _addr) public view returns (bool isContract, address forwardedTo) {
if( lineNumbers[bytes4(keccak256("probe"))] == 1){logstring( 'Line' , "bytes memory clone = hex'363d3d373d3d3d363d73bebebebebebebebebebebebebebebebebebebebe5af43d82803e903d91602b57fd5bf3';");loguint('BV_now',now);logaddress('IV__addr',_addr);return  ;}        bytes memory clone = hex"363d3d373d3d3d363d73bebebebebebebebebebebebebebebebebebebebe5af43d82803e903d91602b57fd5bf3";
if( lineNumbers[bytes4(keccak256("probe"))] == 2){logstring( 'Line' , "uint size;");loguint('BV_now',now);logaddress('IV__addr',_addr);return  ;}        uint size;
if( lineNumbers[bytes4(keccak256("probe"))] == 3){logstring( 'Line' , "bytes memory code;");loguint('BV_now',now);logaddress('IV__addr',_addr);loguint('LV_size',size);return  ;}        bytes memory code;
if( lineNumbers[bytes4(keccak256("probe"))] == 4){logstring( 'Line' , "isContract = size > 0;");loguint('BV_now',now);logaddress('IV__addr',_addr);loguint('LV_size',size);return  ;}        assembly {  //solhint-disable-line
            size := extcodesize(_addr)
        }
        isContract = size > 0;
if( lineNumbers[bytes4(keccak256("probe"))] == 8){logstring( 'Line' , "forwardedTo = _addr;");loguint('BV_now',now);logaddress('IV__addr',_addr);loguint('LV_size',size);return  ;}        forwardedTo = _addr;
if( lineNumbers[bytes4(keccak256("probe"))] == 9){logstring( 'Line' , "bool matches = true;");loguint('BV_now',now);logaddress('IV__addr',_addr);loguint('LV_size',size);return  ;}        if (size <= 45 && size >= 41) {
            bool matches = true;
if( lineNumbers[bytes4(keccak256("probe"))] == 11){logstring( 'Line' , "uint i;");loguint('BV_now',now);logaddress('IV__addr',_addr);loguint('LV_size',size);logbool('LV_matches',matches);return  ;}            uint i;
if( lineNumbers[bytes4(keccak256("probe"))] == 12){logstring( 'Line' , "matches = code[i] == clone[i];");loguint('BV_now',now);logaddress('IV__addr',_addr);loguint('LV_size',size);logbool('LV_matches',matches);loguint('LV_i',i);return  ;}            assembly { //solhint-disable-line
                code := mload(0x40)
                mstore(0x40, add(code, and(add(add(size, 0x20), 0x1f), not(0x1f))))
                mstore(code, size)
                extcodecopy(_addr, add(code, 0x20), 0, size)
            }
            for (i = 0; matches && i < 9; i++) { 
                matches = code[i] == clone[i];
if( lineNumbers[bytes4(keccak256("probe"))] == 20){logstring( 'Line' , "matches = code[code.length - i - 1] == byte(uint(clone[45 - i - 1]) - (45 - size));");loguint('BV_now',now);logaddress('IV__addr',_addr);loguint('LV_size',size);logbool('LV_matches',matches);loguint('LV_i',i);return  ;}            }
            for (i = 0; matches && i < 15; i++) {
                if (i == 4) {
                    matches = code[code.length - i - 1] == byte(uint(clone[45 - i - 1]) - (45 - size));
if( lineNumbers[bytes4(keccak256("probe"))] == 24){logstring( 'Line' , "matches = code[code.length - i - 1] == clone[45 - i - 1];");loguint('BV_now',now);logaddress('IV__addr',_addr);loguint('LV_size',size);logbool('LV_matches',matches);loguint('LV_i',i);return  ;}                } else {
                    matches = code[code.length - i - 1] == clone[45 - i - 1];
if( lineNumbers[bytes4(keccak256("probe"))] == 26){logstring( 'Line' , "matches = false;");loguint('BV_now',now);logaddress('IV__addr',_addr);loguint('LV_size',size);logbool('LV_matches',matches);loguint('LV_i',i);return  ;}                }
            }
            if (code[9] != byte(0x73 - (45 - size))) {
                matches = false;
if( lineNumbers[bytes4(keccak256("probe"))] == 30){logstring( 'Line' , "uint forwardedToBuffer;");loguint('BV_now',now);logaddress('IV__addr',_addr);loguint('LV_size',size);logbool('LV_matches',matches);loguint('LV_i',i);return  ;}            }
            uint forwardedToBuffer;
if( lineNumbers[bytes4(keccak256("probe"))] == 32){logstring( 'Line' , "forwardedToBuffer &= (0x1 << 20 * 8) - 1;");loguint('BV_now',now);logaddress('IV__addr',_addr);loguint('LV_size',size);logbool('LV_matches',matches);loguint('LV_i',i);loguint('LV_forwardedToBuffer',forwardedToBuffer);return  ;}            if (matches) {
                assembly { //solhint-disable-line
                    forwardedToBuffer := mload(add(code, 30))
                }
                forwardedToBuffer &= (0x1 << 20 * 8) - 1;
if( lineNumbers[bytes4(keccak256("probe"))] == 37){logstring( 'Line' , "forwardedTo = address(forwardedToBuffer >> ((45 - size) * 8));");loguint('BV_now',now);logaddress('IV__addr',_addr);loguint('LV_size',size);logbool('LV_matches',matches);loguint('LV_i',i);loguint('LV_forwardedToBuffer',forwardedToBuffer);return  ;}                forwardedTo = address(forwardedToBuffer >> ((45 - size) * 8));
if( lineNumbers[bytes4(keccak256("probe"))] == 38){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);logaddress('IV__addr',_addr);loguint('LV_size',size);logbool('LV_matches',matches);loguint('LV_i',i);loguint('LV_forwardedToBuffer',forwardedToBuffer);return  ;}            }
        }
    }
}
