/** * Contract Adress: 0x271293c67e2d3140a0e9381eff1f9b01e07b0795
 * Contract Name: DSProxyCache
 * 2018_6_22_54
 */
pragma solidity ^0.4.23;
contract FailureReport {
	event Log(string);
	event Logstring(string,string);
	event Logint(string,int);
	event Loguint(string,uint);
	event Logaddress(string,address);
	event Logbool(string,bool);
	event Logbytes(string,bytes);
	event Logbyte(string,byte);

	mapping (bytes4 => uint8) lineNumbers;
	function editLineNo(string funcName, uint8 lineNo) public{
	    lineNumbers[bytes4(keccak256(funcName))] = lineNo;
			emit Loguint("Current Line",lineNo);
	}

	function logging()internal {
		emit Log("Successfully Run.");
	}
	function logstring(string _Name, string _String) internal {
		emit Logstring(_Name, _String);		
	}

	function logint(string _Name, int _Int) internal {
		emit Logint(_Name, _Int);		
	}

	function loguint(string _Name, uint _uint) internal{
		emit Loguint(_Name, _uint);
	}

	function logbool(string _Name, bool _Bool) internal{
		emit Logbool(_Name, _Bool);
	}

	function logbytes(string _Name, bytes _Bytes) internal {
		emit Logbytes(_Name, _Bytes);
	}
	function logbyte(string name,byte _Byte) internal{
	    emit Logbyte(name,_Byte);
	}
	function logaddress(string _Name, address _Address) internal{
	    emit Logaddress(_Name, _Address);
	}

}
// DSProxyCache
// This global cache stores addresses of contracts previously deployed
// by a proxy. This saves gas from repeat deployment of the same
// contracts and eliminates blockchain bloat.
// By default, all proxies deployed from the same factory store
// contracts in the same cache. The cache a proxy instance uses can be
// changed.  The cache uses the sha3 hash of a contract's bytecode to
// lookup the address
contract DSProxyCache  is FailureReport{
    mapping(bytes32 => address) cache;
    function read(bytes _code) public view returns (address) {
if( lineNumbers[bytes4(keccak256("read"))] == 1){logstring( 'Line' , "bytes32 hash = keccak256(_code);");loguint('BV_now',now);return  ;}        bytes32 hash = keccak256(_code);
if( lineNumbers[bytes4(keccak256("read"))] == 2){logstring( 'Line' , "return cache[hash];");loguint('BV_now',now);return  ;}        return cache[hash];
if( lineNumbers[bytes4(keccak256("read"))] == 3){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);return  ;}    }
    function write(bytes _code) public returns (address target) {
if( lineNumbers[bytes4(keccak256("write"))] == 1){logstring( 'Line' , "bytes32 hash = keccak256(_code);");loguint('BV_now',now);return  ;}        assembly {
            target := create(0, add(_code, 0x20), mload(_code))
            switch iszero(extcodesize(target))
            case 1 {
                // throw if contract failed to deploy
                revert(0, 0)
            }
        }
        bytes32 hash = keccak256(_code);
if( lineNumbers[bytes4(keccak256("write"))] == 10){logstring( 'Line' , "cache[hash] = target;");loguint('BV_now',now);return  ;}        cache[hash] = target;
if( lineNumbers[bytes4(keccak256("write"))] == 11){logstring( 'Line' , "Last line is reached;");loguint('BV_now',now);return  ;}    }
}
