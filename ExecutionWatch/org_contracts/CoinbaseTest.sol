/** * Contract Adress: 0x9f5c26cf678f9ab05f61238fae440621bae611de
 * Contract Name: CoinbaseTest
 * 2018_9_13_16
 */
pragma solidity 0.4.24;
contract CoinbaseTest {
    address owner;
    constructor() public {
        owner = msg.sender;
    }
    function () public payable {
    }
    function withdraw() public {
        require(msg.sender == owner);
        msg.sender.transfer(this.balance);
    }
}
