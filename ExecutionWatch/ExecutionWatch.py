import sys
import re
import json
import itertools
import numpy as np
import os

#set 'home' to the root folder path
home = os.getcwd()


##################################
# ANALYSE/EXTRACT VARIABLES
##################################
def AnalyseVariables(varContent):
	Vars = []
	isStruct = False
	StructCount = -1
	for line in varContent:
		#Check the content of the "struct"
		if isStruct:
			if "{" in line:
				if StructCount == -1:
					StructCount += 2
				else:
					StructCount += 1
			elif "}" in line:
				StructCount -= 1
			if StructCount == 0:
				isStruct = False
				StructCount = -1
			else:
				continue
		if line.strip().lower().startswith("struct "):
			isStruct = True
			if "{" in line:
				StructCount += 2
		# Analyse the line whetherh contains some new-defined global variables
		# eles = re.findall(r"^(string|uint|int|address|bytes|byte|bool)(\d+|)(\[]|)(.+?);",line.strip())
		eles = re.findall(r"^(string|uint|int|address|bool)(\d+|)(\[]|)\s(.+?);",line.strip())
		if len(eles) == 0:
			continue
		elif "private" in eles[0][-1]:
			continue
		if eles[0][2].startswith("["):
			ArrayType = eles[0][2]
			isArray = True
		else:
			ArrayType = None
			isArray = False
		typeName = eles[0][0]
		# (&@^*&^#>?@>:>@"!")!
		if eles[0][-1].split("=")[0].strip() == "":
			continue
		varName = eles[0][-1].split("=")[0].strip().split()[-1]
		Vars.append((typeName,varName,isArray,ArrayType))
	return Vars

##################################
# FIND parents contract 
##################################
def identifyParents(content,startPoint):
	stop = False
	lineNo = startPoint
	allParents = []
	while not stop:
		currentLine = content[lineNo].split("//")[0]
		if "{" in currentLine:
			stopPoint = lineNo
			titleContent = " ".join( content[startPoint:stopPoint + 1] )
			if " is " in titleContent:
				startEditPos = titleContent.find(" is ") + 4
				stopEdidPos = titleContent.find("{")
				allParents = titleContent[startEditPos:stopEdidPos].strip().split(",")
			stop = True
		lineNo += 1

	return allParents


#############################
# Inject the console contract for debugging purpose
#############################
def injectConsoleContractRelationship(content,StartLineNo):
	# inject the inheritence relationship
	is_Position = content[StartLineNo].find(" is ")
	if is_Position != -1: # there is inheritence
		content[StartLineNo] = content[StartLineNo][:is_Position + 4] + " FailureReport, " + content[StartLineNo][is_Position + 4:]
	else:
		editPosition = content[StartLineNo].find("{")
		if editPosition != -1: # e.g. contract A is FailureReport{ ...
			# "{" is on the line
			content[StartLineNo] = content[StartLineNo][:editPosition] + " is FailureReport" + content[StartLineNo][editPosition:]
		else:									# e.g. contract A  is FailureReport...
			# "{" is not on current line, so we just need to append the relationship "is FailureReport"
			content[StartLineNo] += " is FailureReport"



##################################
# FIND CONTRACT RANGES
##################################
def findContractRanges(content,mainContractName):
	lineNo = 0
	contractRanges = []
	contractStartPoint = None
	findContract = False
	currContractName = None

	# we should find all contract class and analysis all
	for line in content:
		#inject the console contract
		if line.startswith("pragma solidity"):
			with open(home + "/ExecutionWatch/consoleContract_LineMap.txt","r") as f:
				consoleContract = f.read()
				content[lineNo] += consoleContract +  "\n"
		# find MAIN CONTRACT
		if line.startswith("contract ") and (not findContract):
			contractStartPoint = lineNo
			#get the contract name 
			currContractName = line.split()[1]
			if currContractName.endswith("{"):
				currContractName = currContractName[:-1]

			findContract = True
		# record CONTRACTS
		elif findContract and ( line.strip().startswith("function ") or line.strip().startswith("modifier ") or line.strip().startswith("constructor ")): # too limited 
			contractRanges.append( (currContractName, (contractStartPoint,lineNo) ) )
			findContract = False
			# BREAK if the main contract has been found 
			if currContractName == mainContractName:
				break
		elif findContract and ( line.strip().startswith("contract ") or (lineNo == len(content)) ):
			# to handle the contract without the function to terminate 
			contractRanges.append( (currContractName , (contractStartPoint,lineNo)) )
			contractStartPoint = lineNo
			currContractName = line.split()[1]
		lineNo += 1

	return contractRanges
##################################
# FIND FUNCTION FIRST LINE
##################################
def findFuncStartPoint(content,startPoint):
	currPoint = startPoint
	while True:
		if content[currPoint].split("//")[0].strip().endswith("{"):
			return currPoint
		elif content[currPoint].split("//")[0].strip().endswith(";"):
			return False
		currPoint += 1

def isFunc(content,startPoint):
	while startPoint < len(content):
		funcLine = content[startPoint].split("//")[0].strip()
		if funcLine.endswith("{"):# or funcLine.endswith("}") :
			return True
		elif funcLine.endswith(";"):
			return False
		startPoint += 1

##################################
# FIND FUNCTION RANGES
##################################
def findFunctionRanges(content,contractStartLineNo):
	functionRanges = {} # name: [ (startLineNo,finishLineNo, funcNo), (.. , .. , ..), .. ] 
	lineNo = contractStartLineNo
	isFunction = False
	functionRangeCount = -1
	contractRangeCount = -1
	scanningStart = False
	for line in content[contractStartLineNo:]:
		#skip the comment line
		if line.strip().startswith("//") or line.strip().startswith("/*") or line.strip().startswith("*") or line.strip().startswith("*/"):
			lineNo += 1
			continue
		
		# working on current function
		if isFunction:
			if functionRangeCount == -1 and (len(re.findall(r"{",line)) > 0):
					functionRangeCount += 1
			functionRangeCount += len(re.findall(r"{",line))
			functionRangeCount -= len(re.findall(r"}",line)) 
			# check whether we should terminate
			if functionRangeCount == 0:
				isFunction = False
				functionRangeCount = -1
				functionFinishPointer = lineNo
				#insert the function range to the "functionRange"
				if functionName in functionRanges:
					funcNo = len(functionRanges[functionName]) + 1
					functionRanges[functionName].append( (functionStartPoint,functionFinishPointer,funcNo) )
				else:
					functionRanges[functionName] = [(functionStartPoint,functionFinishPointer,1)]
				lineNo += 1
				continue
			else:
				lineNo += 1
				continue
			
		#identify the function start point
		if line.strip().startswith("function"):
			if isFunc(content,lineNo):
				isFunction = True
			else:
				lineNo += 1
				continue
			
			functionStartPoint = lineNo
			# (&@^*&^#>?@>:>@"!")!
			functionName = line.strip().split("function")[-1].split()[0]
			if functionName != "()":
				functionName = functionName.split("(")[0]
			if len(re.findall(r"{",line)) > 0:
				functionRangeCount += len(re.findall(r"{",line)) + 1
				functionRangeCount -= len(re.findall(r"}",line)) 
		else:
			# working on only main contract range
			if "{" in line and (not scanningStart):
				contractRangeCount += 1
				scanningStart = True
			contractRangeCount += len(re.findall(r"{",line))
			contractRangeCount -= len(re.findall(r"}",line))

		if scanningStart and (contractRangeCount == 0) :
			return functionRanges
		
		lineNo += 1

	# worse case, if the range can not be recongnize
	return functionRanges


##################################
# GET THE PUBLIC FUNCTION NAMES (based on ABI)
##################################
def getFunctionNames(keyDate,contractName):
	abi_path = home + "/ExecutionWatch/ABI.npz"
	abi = np.load(abi_path)

	if keyDate in abi:
		current_ABI = abi[keyDate]
	else:
		abi_path = home + "/ContractTest/"  + contractName + ".json"
		with open(abi_path,"r") as json_file:
			info = json.load(json_file)
			current_ABI = info["ABI"]
			current_ABI = json.loads(current_ABI)
	

	funcNames = []
	for eachELE in current_ABI:
		if eachELE["type"] == "function":
			funcNames.append(eachELE["name"])
	
	# only the public function name will be presented
	return funcNames

##################################
# INSERT NEW EVENT FOR ARRAY TYPE
##################################
# Var: (type,name,isArray,ArrayType)
def createNewEvent(Var):
	varName = Var[1]
	varType = Var[0] + Var[3]
	newEvent = "\n" + "event " + "log" + varName +"(string,"+ varType + ");"
	return newEvent


##################################
# CONSTRUCT LOGGING LINE
##################################
def constructLoggingLine(Vars,label):
	if label == "GV" or label == "LV" or label == "IV":
		LoggingLine = ""
	else:
		return None,None

	newEventLine = ""
	for var in Vars:
		varType = var[0]
		varName = var[1]
		isArray	= var[2]
		varName_str = "'" + label +"_" + varName + "'"
		
		# for array type variables, we create/inject new event in the current contract
		if isArray: 
			# create new event
			newEventLine += createNewEvent(var)
			# update the logging line
			LoggingLine += "emit log" +  varName + "("  +varName_str+ "," + varName + ");"
		
		# for non-array type variables, we use the log func from the inherited contract
		else:
			LoggingLine += "log" + varType + "("  +varName_str+ "," + varName + ");"

	return LoggingLine,newEventLine

##################################
# CONSTRUCT return content
##################################
def editLine(funcName, LogContent,lineNo, returnType):
	returnLine = "if( " + "lineNumbers[bytes4(keccak256(\""+ funcName + "\"))] == " + str(lineNo + 1) + "){" + LogContent + "return  ;}"
	return returnLine

##################################
# edit public Func
##################################
def editPublicFunc(content,funcStart,funcEnd,logLine,funcName):
	def searchNextLine(content,nextLineMark,funcEnd):
		isNextLine = False
		nextLine = ""
		while (not isNextLine):
			nextLineMark += 1
			if nextLineMark > funcEnd:
				nextLine = "Last line is reached;"
				break
			else:
				nextLine = content[nextLineMark]

			# filter the fake next lien: which is end without ";"
			if nextLine.split("//")[0].strip().endswith(";"):
				isNextLine = True
			if nextLine.startswith("//") or nextLine.startswith("/*") or nextLine.startswith("*/")  or nextLine.startswith("*") :
				isNextLine = False
			
		next_line_Content = "logstring( 'Line' , \""  + nextLine.strip().replace("\"" , "'") + "\");" 
		return next_line_Content

	inputDetails = []
	# #back-up the original function
	# org_func = content[funcStart:funcEnd + 1]

	# first line
	funcContentStartPoint = findFuncStartPoint(content,funcStart)
	newFirstLine = "".join(content[funcStart:funcContentStartPoint + 1]).replace("\n","")

	# function return type
	if "returns" in newFirstLine:
		returnType = newFirstLine.split("returns")[-1].split("{")[0]
		if "(" in returnType:
			returnType = returnType.strip()[1:-1].split()[0]
	else:
		returnType = " "

	# input variables
	IV_LogLine = ""
	IV_newEvent = ""
	# print(newFirstLine)
	inputs = ( newFirstLine.split("(")[1].split(")")[0] ).strip().split(",")
	if not (len(inputs) == 1 and inputs[0] == ''):
		pretented_lines = [] 
		for eachInput in inputs:
			if eachInput == '':
				continue
			pretented_lines.append(eachInput + " ;") # the space before ';' is necessary for some input without name defined
			inputType = eachInput.strip().split()[0] # suppose to be a list of value: [type, name] or [type]
			inputDetails.append(inputType)

		# construct Input variable logging lines
		inputVars = AnalyseVariables(pretented_lines)
		IV_LogLine,IV_newEvent = constructLoggingLine(inputVars,"IV")
		# print("IV", IV_newEvent,inputVars)
	

	# CASE1: line 0 
	# print(newFirstLine)
	next_line_Content = searchNextLine(content,funcContentStartPoint,funcEnd)
	# print("Line 0 :" + next_line_Content)
	content[funcContentStartPoint] += editLine(funcName,next_line_Content + logLine + IV_LogLine,0,returnType)

	# CASE2: line 1 - *
	total_LV_LogLine = ""
	total_LV_newEvent = ""
	codeLineCounter = 0
	for lineNo in range(funcContentStartPoint+1,funcEnd):
		line = content[lineNo]
		# TO filter some lines
		if line.startswith("//") or line.startswith("/*")  or line.startswith("*/")  or line.startswith("*"):
			continue
		if line.strip() == "":
			continue
		if not line.split("//")[0].strip().endswith(";"):
			continue

		codeLineCounter += 1

		Vars = AnalyseVariables([line])
		if len(Vars) > 0:
			LV_LogLine,LV_newEvent = constructLoggingLine(Vars,"LV")
			total_LV_LogLine += LV_LogLine
			total_LV_newEvent += LV_newEvent
		
		# find next line
		next_line_Content = searchNextLine(content,lineNo,funcEnd)
		# print("Line "+str(lineNo - funcContentStartPoint)+" :" + next_line_Content)
		content[lineNo] +=  editLine(funcName,next_line_Content + logLine + IV_LogLine + total_LV_LogLine,lineNo - funcContentStartPoint,returnType)

		if next_line_Content == "Last line is reached;":
			break
	
	return total_LV_newEvent + IV_newEvent ,str(codeLineCounter) ,inputDetails

	
##################################
# MAIN
##################################
if __name__ == "__main__":
	inputContract = sys.argv[1]
	singleSRC_path = "./ExecutionWatch/org_contracts/" + inputContract + ".sol"
	with open(singleSRC_path,"r") as file:
		content = file.readlines()

	#Basic Details
	contractName = content[1].split(":")[-1].strip()
	keyDate = content[2].strip().split()[-1]
	functionNames = getFunctionNames(keyDate,contractName)

	# Find all ranges of contract class
	contractRanges = findContractRanges(content,contractName)

	#Reconstruct the each public function 
	# functionDetails = ""
	functionDetails = {}
	exitBalance = False
	exitAllowance = False
	if len(contractRanges) != 0:
		# commonLogLine = "loguint('BV_this.balance',this.balance);loguint('BV_msg.value',msg.value);logaddress('BV_msg.sender',msg.sender);loguint('BV_now',now);"
		commonLogLine = "loguint('BV_now',now);"
		# Get the global variables 
		allGlobalVars = {}
		all_GV_newEventLine = ""
		all_GV_LoggingLine = ""
		for eachContract in contractRanges:
			currContractStart = eachContract[1][0]
			currGlobalFinishPoint = eachContract[1][1]
			currAllParents = identifyParents(content,currContractStart)
			currGlobalVars = AnalyseVariables(content[currContractStart:currGlobalFinishPoint + 1])
			# if there is no inherited relationship
			if len(currAllParents) == 0:
				allGlobalVars[eachContract[0]] = currGlobalVars
			# Append  Parent's global variable 
			else:
				for eachParent in currAllParents:
					eachParent = eachParent.strip()
					# some parents might be the interface class which don't have any variables and they will not be analysed
					if eachParent in allGlobalVars: 
						currGlobalVars += allGlobalVars[eachParent]
					allGlobalVars[eachContract[0]] =  currGlobalVars
			
			# Make the logging lines for the global variables
			currGV_LoggingLine,currGV_newEventLine = constructLoggingLine(currGlobalVars,"GV")
			content[currContractStart] += currGV_newEventLine 
			# print("GV", currGV_newEventLine)
			currGlobalLogLine = commonLogLine + currGV_LoggingLine  # global variable belongs to the commone line too

			# Get the Ranges of all function of current contract
			functionRanges = findFunctionRanges(content,currContractStart)  
			# Get the local variables 
			LV_newEventLine = ""
			for funcName in functionRanges:
				# 	TO filter the non-public functions && some common functions
				if (funcName not in functionNames) or (funcName in ["balanceOf","allowance","totalSupply","approve"]):
					continue
				
				# funcRange: (functionStartPoint,functionFinishPointer,funcNo)
				funcRange = functionRanges[funcName][0]

				# edit the function content for failure detection
				if "pure" in content[funcRange[0]]:
					continue
				LV_newEventLine,lineCounter,inputDetails = editPublicFunc(content,funcRange[0],funcRange[1],currGlobalLogLine,funcName)

				functionDetails[funcName] =  [lineCounter,inputDetails]
				# create the new events
				content[currContractStart] += LV_newEventLine

			# inject the inherited relationship 
			injectConsoleContractRelationship(content,currContractStart)

	#Solc Version Control
	for line in content:
		version = re.findall(r"pragma solidity\s*(.*?)\s*;", line)
		if len(version) > 0:
			required_solc_version = "".join(version[0].split())
			break
	
	# Create .json files (for both org&tr contracts) to maintain:
	# 	1. Contract Name
	#		2. Required Solc Version for compilation
	#		3. (Deployed) Contract Address
	#		4. ABI
	#		5. ByteCode
	# 	6. Contract Info 
	contractDetails = {
		"name":  contractName,
		"solcVersion": required_solc_version,
		"Address": "None",
		"ABI": "None",
		"ByteCode": "None",
		"contructorInfo": "None"
	}
	with open(home + "/ExecutionWatch/tr_contracts/"+contractName+".json","w") as tr_json:
		json.dump(contractDetails, tr_json)
	with open(home + "/ExecutionWatch/org_contracts/"+contractName+".json","w") as org_json:
		json.dump(contractDetails, org_json)

	# "tr_contracts" (folder): 
	#		tranformed contracts (.sol + .json)
	with open(home + "/ExecutionWatch/tr_contracts/"+contractName+".sol","w") as f:
		f.writelines(content)
	

	# "FunctionDetails" (folder): 
	# 		One Contract(.json) -> 
	# 			One(Many) Function(s) -> 
	# 				1. Line numb; 2. Input Type
	with open(home + '/ExecutionWatch/FuncDetails/'+contractName+".json","w") as json_file:
		json.dump(functionDetails, json_file)
	
	