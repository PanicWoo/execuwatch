const home = process.cwd()
const fs = require('fs');
const Web3 = require('web3');
const provider = new Web3.providers.HttpProvider("http://localhost:8545");	
const web3 = new Web3(provider); 

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


(async ()=>{
	//1. Loading the contract
	let contractName = process.argv[2];
	console.log("CONTRACT NAME:" + contractName);
	//Load the data of deployed contract
	let data = fs.readFileSync(home + "/ExecutionWatch/tr_contracts/" + contractName + ".json");
	data = JSON.parse(data);
	let abi = JSON.parse(data["ABI"]);
	let ctrInfo = data["constructorInfo"]

	console.log("\nConstructing Info:")
	console.log(ctrInfo)
	const Accounts = await web3.eth.getAccounts();
	const OwnerAddress = Accounts[0]; 
	const ExternalAddress = Accounts[1];
	// const AttackAgentAddress = null;
	
	// To load the failure information from the first stage which is 'FailureDetecting'
	var FailureInfo = fs.readFileSync(home + "/ExecutionWatch/FailureInfo_envSet/" + contractName + "_FailInfo.json");
	FailureInfo = JSON.parse(FailureInfo);
	var fuzzing_Size = FailureInfo["fuzzing_Size"] 
	var testingDB = JSON.parse(FailureInfo["testingDB"]) 
	var	staticalResult = JSON.parse(FailureInfo["staticalResult"]) 
	var testing_result = FailureInfo["testing_result"] 
	//run sub public functions of all failed functions
	var funcStart = false
	var CallerAddress
	var CallerType
	for (var failureCase of testing_result){
		var failedFuncName = failureCase[0]
		var failedInput = failureCase[1]
		var currAddr = failureCase[2];
		var myContract = new web3.eth.Contract(abi,currAddr);
		var resultTable = {}
		// all failed function needs to be tested
		// if (staticalResult[failedFuncName]["FailureRate"][0] + staticalResult[failedFuncName]["FailureRate"][1] != fuzzing_Size){
		
		//Locate the failure for all-failed functions only
		if ((staticalResult[failedFuncName]["FailureRate"][0] == fuzzing_Size/2 && staticalResult[failedFuncName]["FailureRate"][1] == fuzzing_Size/2) || (staticalResult[failedFuncName]["FailureRate"][0] == 999 && staticalResult[failedFuncName]["FailureRate"][1] == 999)  ){
			// distinguish the caller address
			if (!funcStart){
				var currFuncTotal = staticalResult[failedFuncName]["FailureRate"][0] + staticalResult[failedFuncName]["FailureRate"][1]
				var addressMark = staticalResult[failedFuncName]["FailureRate"][0]
				funcStart = true
				var funcCount = 0
			}
			if (funcCount < addressMark){
				CallerAddress = OwnerAddress
				CallerType = "OwnerAddress"
				funcCount += 1
			}else if (funcCount < currFuncTotal){
				CallerAddress = ExternalAddress
				CallerType = "ExternalAddress"
				funcCount += 1
			}
			
			if(funcCount == currFuncTotal){
				funcStart = false
			}

			const count = testingDB[failedFuncName][0]
			// console.log("All-Failed Function: ",failedFuncName)
			console.log("\nFailed Function: ",failedFuncName)
			console.log("CallerAddress: ", CallerType)
			console.log("Inputs: ",failedInput)
			console.log("Total lines: ",count)
			try{
				for(theLineNo = 1; theLineNo <= Number(count) + 1; theLineNo++){
					// console.log(theLineNo,count)
				
					// edit the line no
					// console.log(failedFuncName)
					// console.log(failedFuncName,theLineNo)
					await eval("myContract.methods.editLineNo(\""+failedFuncName+"\"," + theLineNo+")").send({from: CallerAddress, gas: 470000000}).then(
						// rec => {console.log("Line Edit to: ", theLineNo-1);console.log(rec["events"])}
						()=>{}
						)
					
					// var subFuncSig = failedFuncName + lineNo +"(" + failedInput + ")"
					var subFuncSig =  failedFuncName +"(" + failedInput + ")"
					// var isFailure = false;
					// console.log(subFuncSig)
					await eval("myContract.methods." + subFuncSig).send({from: CallerAddress, gas: 470000000}).then(rec => {
						// console.log(theLineNo, "events: " + rec["events"])
						// console.log(rec)
						// console.log(keys)
						var keys = Object.keys(rec["events"]);
						for (var key of keys){
							// Only analyse logging events
							if (!(key.includes("Log")) ){
								continue;
							}
							// if there are many events(an array)
							if (Array.isArray(rec["events"][key])){
								for (itemNo = 0; itemNo < rec["events"][key].length; itemNo ++){
									var returnVarName = rec["events"][key][itemNo]["returnValues"][0];
									var returnVarValue = rec["events"][key][itemNo]["returnValues"][1];
									if (returnVarName in resultTable){
										resultTable[returnVarName][theLineNo] =  returnVarValue;
									}else{
										resultTable[returnVarName] = Array.apply("None", Array(count + 1))
										resultTable[returnVarName][theLineNo] =  returnVarValue;
									}
									
								}
							// if there is only one event
							}else{
								var returnVarName = rec["events"][key]["returnValues"][0];
								var returnVarValue = rec["events"][key]["returnValues"][1];
								if (returnVarName in resultTable){
									resultTable[returnVarName][theLineNo] =  returnVarValue;
								}else{
									resultTable[returnVarName] = Array.apply("None", Array(count + 1))
									resultTable[returnVarName][theLineNo] =  returnVarValue;
								}
							}
						} 
					})
				}	
			}catch(e){
				if (("line" + theLineNo) in staticalResult[failedFuncName]["FailureLine"]){
					staticalResult[failedFuncName]["FailureLine"]["line" + theLineNo] += 1
				}else{
					staticalResult[failedFuncName]["FailureLine"]["line" + theLineNo] = 1
				}
				// isFailure = true
				// console.log("sub function failed")
				// console.log(e)
			}
			console.log(resultTable);
			console.log("\n");
		}
	}

	console.log("\nStatistical Result:")
	console.log(staticalResult)
	console.log("\n")
})()