


function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


(async ()=>{
	const home = process.cwd()
	const arguFuzz = require(home + '/ExecutionWatch/ArguFuzzing.js')

	const fs = require('fs');
	// const keccak256 = require(keccak256);

	//Setting up all the variables/packages 
	const Web3 = require('web3');
	const provider = new Web3.providers.HttpProvider("http://localhost:8545");	
	const web3 = new Web3(provider); 



	let addrMapData = fs.readFileSync(home + '/ExecutionWatch/_AddressMap.json' );
	const addrMap = JSON.parse(addrMapData);
	const addresses = addrMap["Payable"]
	const funcSig_Balance = "preAllocatedBalance([" + addrMap["Payable"].join(",") +","+ addrMap["nonPayable"].join(",")  + "],50000)"
	const funcSig_Allowance = "preAllocatedAllowance([" + addrMap["Payable"].join(",") +","+ addrMap["nonPayable"].join(",")  + "],50000)"
	
	// unlock the account
	const Accounts = await web3.eth.getAccounts();
	const OwnerAddress = Accounts[0];
	const ExternalAddress = Accounts[1];
	await web3.eth.personal.unlockAccount(OwnerAddress, "123",15000)
	await web3.eth.personal.unlockAccount(ExternalAddress, "123",15000)

	//1. Loading the contract
	let contractName = process.argv[2];
	//Load the data of deployed contract
	let data = fs.readFileSync(home + "/ExecutionWatch/tr_contracts/" + contractName + ".json");
	data = JSON.parse(data);
	let abi = JSON.parse(data["ABI"]);
	let contractAddr = data["address"];

	let constraintsData = fs.readFileSync(home + "/ExecutionWatch/Constraints/" + contractName + ".json");
	let constraints = JSON.parse(constraintsData)
	let existBalance = constraints["preAllocate"][0]
	let existAllowance = constraints["preAllocate"][1]

	var myContract = new web3.eth.Contract(abi,contractAddr);
	// console.log(funcSig_Balance)
	if (existBalance){
		await eval("myContract.methods." + funcSig_Balance).send({from: OwnerAddress, gas: 470000000})
		.on("receipt", function(receipt){
			// console.log(receipt["events"]["Loguint"]["returnValues"]);
			console.log("Balance-Allocation Status: ", receipt['status'])
		})
	}else{
		console.log("There is no balances mapping.")
	}

	if( existAllowance){
		// Owner Address
		await eval("myContract.methods." + funcSig_Allowance).send({from: OwnerAddress, gas: 470000000})
		.on("receipt", function(receipt){
			// console.log(receipt["events"]["Loguint"]["returnValues"]);
			console.log("Allowance-Allocation Status: ", receipt['status'])
		})
		// External Address
		await eval("myContract.methods." + funcSig_Allowance).send({from: ExternalAddress, gas: 470000000})
		.on("receipt", function(receipt){
			// console.log(receipt["events"]["Loguint"]["returnValues"]);
			console.log("Allocation Status: ", receipt['status'])
		})
	}else{
		console.log("There is no allowance mapping.")
	}
})()