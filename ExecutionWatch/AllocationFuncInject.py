import re
import sys 
import json
import os


##################################
# FIND ALLOW* && BALANCE*
##################################
def findAllowanceBalance(content):
	exitBalance = False
	exitAllowance = False
	balanceVarName = None
	allowanceVarName = None
	for line in content:
		if not exitBalance:
			balanceContent = re.findall(r'mapping.+(balance.+);',line.strip())
			if len(balanceContent) > 0:
				balanceVarName = re.findall(r'(balance.+)[=]*.*',balanceContent[0].strip())
				balanceVarName = balanceVarName[0]
				exitBalance = True
		
		if not exitAllowance:
			allowanceContent = re.findall(r'mapping.+(allow.+);',line.strip())
			if len(allowanceContent) > 0:
				allowanceVarName = re.findall(r'(allow.+)[=]*.*',allowanceContent[0].strip())
				allowanceVarName = allowanceVarName[0]
				exitAllowance = True
		
		if exitAllowance and exitBalance:
			break
			
	return balanceVarName,allowanceVarName

##################################
# INJECT Pre-ALLOCATE FUNCTION(S)
##################################
def injectPreallocateFunc(content,contractName,VarNames): # VarName: (balanceName/None, allowanceName/None)
	def findMainContractStartPoint(content,contractName):
		startPoint = None
		lineNo = 0
		for line in content:
			if line.startswith("contract") and ( (contractName + " " in line) or(contractName + "{" in line)):
				if line.strip().endswith("{"):
					startPoint = lineNo
				else:
					lineNo += 1
					while lineNo < len(content):
						if line.strip().endswith("{"):
							startPoint = lineNo
							return startPoint
						lineNo += 1
			lineNo += 1
		return startPoint
	

	balanceName = VarNames[0]
	allowanceName = VarNames[1]
	mainContractStartPoint = findMainContractStartPoint(content,contractName)
	if balanceName != None:
		content[mainContractStartPoint] += "function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){"+ balanceName +"[a[i]] = value;}loguint(\"a[3]\'s balance\","+balanceName+"[a[3]]);}"
	
	if allowanceName != None:
		content[mainContractStartPoint] += "function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){"+ allowanceName +"[a[i]][msg.sender] = value;"+allowanceName+"[msg.sender][a[i]] = value;}loguint(\"a[3][msg.sender]\'s allowance\","+allowanceName+"[a[3]][msg.sender]);}"

	return content


##################################
# MAIN
##################################
if __name__ == "__main__":
	contractName = sys.argv[1]
	workPath = os.getcwd()
	
	print(workPath)

	with open(workPath + "/ExecutionWatch/tr_contracts/"+contractName+".sol","r") as f:
		content = f.readlines()
	
	VarNames = findAllowanceBalance(content)
	# print(VarNames[0],VarNames[1])

	with open(workPath + "/ExecutionWatch/Constraints/" + contractName + ".json","r") as read_json:
		constraints = json.load(read_json)
	
	with open(workPath + "/ExecutionWatch/Constraints/" + contractName + ".json","w") as write_json:
		constraints["preAllocate"] = []
		if VarNames[0] != None:
			constraints["preAllocate"].append(True)
		else:
			constraints["preAllocate"].append(False)
		if VarNames[1] != None:
			constraints["preAllocate"].append(True)
		else:
			constraints["preAllocate"].append(False)
		
		# print(constraints)
		json.dump(constraints,write_json)

	if VarNames[0] == None and VarNames[1] == None:
		pass
	else:
		newContent = injectPreallocateFunc(content,contractName,VarNames)

		with open(workPath + "/ExecutionWatch/tr_contracts/"+contractName+".sol","w") as f:
			f.writelines(newContent)

