const fs = require("fs");
const arguFuzz = require('./ArguFuzzing.js')
const home = process.cwd() // This home address should be root address

// This function is mainly doing comilation and Deployment
async function CompileSmartContract(contractName,contractPath){
	//1. Setting up for all moudles we needed
	const fs = require('fs');
	const solc = require('solc');
	const source = fs.readFileSync(contractPath);

	//2. Compile the contract
	var Bytecode; 
	var Abi; 
	try{
		const output = solc.compile(source.toString(), (res)=>{}) //(res)=>{console.log(res)})
		Bytecode = output.contracts[":" + contractName].bytecode;
		Abi = output.contracts[":" + contractName].interface;
		if ( (typeof Bytecode !== "undefined") && (typeof Abi !== "undefined") ){

			return [Abi,Bytecode]
		} else{
			console.log("Compilation Failed: " +contractName  + "(Undefined abi or bytecode)")
			return false
		}
	}catch(e){
		console.log("Compilation Failed: " +contractName , e.message)
		return false
	}
}
//randomize the inputs
async function randomizeInputs(abi,ctrInfo){
	var theRandomArguments = [];
	var isPayableConstructor = false;
	var abi_table = JSON.parse(abi)
	var checkConstructor = false;
	for (iter = 0; iter< abi_table.length;iter++){
		if (abi_table[iter]["type"] == "constructor" && abi_table[iter]['inputs'].length > 0){
			ctrInfo["isPayable"] = isPayableConstructor
			var inputs = abi_table[iter]['inputs']
			var inputTypes = []
			for (inputNo = 0; inputNo < inputs.length;inputNo ++){
				inputTypes.push(inputs[inputNo]["type"]);
				theRandomArgument = arguFuzz.fuzz([inputs[inputNo]["type"]],1) // fuzz only 1 random input for the constructor
				theRandomArguments.push(eval(theRandomArgument[0]))
				var inputNameKey = inputs[inputNo]["name"] + "(Input)"
				ctrInfo[inputNameKey] = [inputs[inputNo]["type"]]
				ctrInfo[inputNameKey].push(theRandomArgument[0])
			}
			checkConstructor = true
		}

		//terminate if fallback and constructor are both reached
		if (checkConstructor){
			return theRandomArguments
		}
	}

	// In case, if we can not find the constructor in the ABI
	return theRandomArguments
}

//Deploying Function 
async function DeploySmartContract(abi,bytecode,ctrInfo,theRandomArguments){

	//1. Setting up for all moudles we needed
	const Web3 = require('web3');
	const web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
	const Accounts = await web3.eth.getAccounts()
	const DefaultAccount = Accounts[0] 


	//2. Deploy the contract 
	ctrInfo['creator'] =  DefaultAccount
	try{
		//2.1 Checking the payable status
		var isPayableConstructor = false;
		var isPayableFallback = false;
		abi_table = JSON.parse(abi)
		var checkFallback = false;
		var checkConstructor =false;
		for (iter = 0; iter< abi_table.length;iter++){
			if (abi_table[iter]["type"] == "fallback"){
				isPayableFallback = abi_table[iter]["payable"]
				checkFallback =true;
			}
			if (abi_table[iter]["type"] == "constructor" && abi_table[iter]['inputs'].length > 0){
				isPayableConstructor = abi_table[iter]["payable"];
				checkConstructor = true
			}
			//terminate if fallback and constructor are both reached
			if (checkFallback&&checkConstructor){
				break
			}
		}


		//2.2 Unlock theAccount	
		await web3.eth.personal.unlockAccount(DefaultAccount, "123",15000)

		//2.3 Instantiate a contract with its ABI
		const contract = new web3.eth.Contract(JSON.parse(abi)); 
		//2.4 deploy the contract instance with its bytecode,sender,arguments
		if (isPayableConstructor){
			await contract.deploy({
				data: "0x" + bytecode,
				arguments: theRandomArguments
				})
				.send({
					from: DefaultAccount,
					gas: 470000000,//, // the gas need
					value: 10000000
					// gasPrice: '1'
			}, (error, transactionHash) => { 
				// console.log(error,transactionHash)
			}).on('receipt', (receipt) => {
				contract_final_address = receipt.contractAddress;
				}) 
		}else{
			await contract.deploy({
				data: "0x" + bytecode,
				arguments: theRandomArguments
				})
				.send({
					from: DefaultAccount,
					gas: 470000000//, // the gas need
					// value: 1123456789
					// gasPrice: '1'
			}, (error, transactionHash) => { 
				// console.log(error,transactionHash)
			}).on('receipt', (receipt) => {
				contract_final_address = receipt.contractAddress;
				// console.log(contract_final_address)
				})
		}
	return [contract_final_address,isPayableFallback]
	}catch(e){
		console.log("Deployment Error: ", e.message)
		return [false,false]
	}
}

//Version Swap 
async function versionSwitch(contractName){
	//Create shell obj for auto deployment
	var shell = require('shelljs');
	// find required solc version based on the contractName
	var contractDetails = fs.readFileSync(home + "/ExecutionWatch/org_contracts/" + contractName + ".json");
	contractDetails = JSON.parse(contractDetails)
	var solcVersion = contractDetails["solcVersion"];

	// over write the package.json file with new solc version
	var pakcageFile = fs.readFileSync(home + "/package.json");
	pakcageFile = JSON.parse(pakcageFile)
	pakcageFile["dependencies"]["solc"] = solcVersion;
	fs.writeFileSync(home + "/package.json",JSON.stringify(pakcageFile),err => {console.log(err)});

	// run "npm install" to renew the solc version
	shell.exec("npm install")

}

(async ()=>{
	//1. Setting up for all moudles we needed
	const Web3 = require('web3');
	const web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
	const Accounts = await web3.eth.getAccounts()
	try{
		var contractName = process.argv[2];
		// Solc Version Switch
		await versionSwitch(contractName);

		//Compile two contracts
		//Original Contract Source Code
		let orgContractPath = home + "/ExecutionWatch/org_contracts/" + contractName + ".sol"
		var org_compileResult = await CompileSmartContract(contractName,orgContractPath);
		if (!org_compileResult){throw new Error("Original Contract is failed to be compiled.");}
		var org_abi = org_compileResult[0];
		var org_bytecode = org_compileResult[1];
		//Transformed Contract Source Code
		let trContractPath = home + "/ExecutionWatch/tr_contracts/" + contractName + ".sol"
		var tr_compileResult = await CompileSmartContract(contractName,trContractPath);
		if (!tr_compileResult){throw new Error("Transformed Contract is failed to be compiled.");}
		var tr_abi = tr_compileResult[0];
		var tr_bytecode = tr_compileResult[1];

		var ctrInfo = {}
		var theRandomArguments = await randomizeInputs(org_abi,ctrInfo)
		var org_deployResults = await DeploySmartContract(org_abi,org_bytecode,ctrInfo,theRandomArguments);
		var tr_deployResults = await DeploySmartContract(tr_abi,tr_bytecode,ctrInfo,theRandomArguments);
		var org_contractAddr = org_deployResults[0]
		var tr_contractAddr = tr_deployResults[0]
		var isPayableFallback = org_deployResults[1]

		// console.log(org_contractAddr,tr_contractAddr)
		
		if (!org_contractAddr){
			throw new Error("Original Contract:Deploy failed.");
		}else if(!tr_contractAddr){
			throw new Error("Transformed Contract:Deploy failed.");
		}

		//read the exist contractDetails file (contractName.json)
		let org_data_src = fs.readFileSync(home + "/ExecutionWatch/org_contracts/" + contractName + ".json");
		org_data_src = JSON.parse(org_data_src)

		let tr_data_src = fs.readFileSync(home + "/ExecutionWatch/tr_contracts/" + contractName + ".json");
		tr_data_src = JSON.parse(tr_data_src)

		//update the corresponding contract details
		org_data_src["address"] = org_contractAddr;
		org_data_src["ABI"] = org_abi;
		org_data_src["ByteCode"] = org_bytecode;
		org_data_src["constructorInfo"] = ctrInfo;

		tr_data_src["address"] = tr_contractAddr;
		tr_data_src["ABI"] = tr_abi;
		tr_data_src["ByteCode"] = tr_bytecode;
		tr_data_src["constructorInfo"] = ctrInfo;
		// Save updated contractDetails
		fs.writeFileSync(home + "/ExecutionWatch/org_contracts/" + contractName + ".json",JSON.stringify(org_data_src) , err => {console.log(err);}  )
		fs.writeFileSync(home + "/ExecutionWatch/tr_contracts/" + contractName + ".json",JSON.stringify(tr_data_src) , err => {console.log(err);}  )

		console.log(contractName + ": Deploy Successfully && Function(s) exist")
		
		// Check the existence of "AddressMap.json"
		fs.exists(home + "/ExecutionWatch/AddressMap.json", (ex)=>{
			if (!ex){
				let addressMap ={
					"Payable" : ["'" + Accounts[0] + "'","'" + Accounts[1] + "'"],
					"nonPayable": []
				}
				fs.writeFileSync(home + "/ExecutionWatch/AddressMap.json",JSON.stringify(addressMap) , err => {console.log(err);}  )
			}
		})

		// Edit the AddressMap
		var currentAddreMap = fs.readFileSync(home + "/ExecutionWatch/AddressMap.json");
		currentAddreMap = JSON.parse(currentAddreMap)
		// If the addressMap is empty
		if (currentAddreMap["Payable"].length == 0){currentAddreMap["Payable"].push("'" + Accounts[0] + "'");currentAddreMap["Payable"].push("'" + Accounts[1] + "'")}
		// check whether the fall back function is payable 
		if (isPayableFallback){
			currentAddreMap["Payable"].push("'" + org_contractAddr + "'");
			currentAddreMap["Payable"].push("'" + tr_contractAddr + "'");
		}else{
			currentAddreMap["nonPayable"].push("'" + org_contractAddr + "'");
			currentAddreMap["nonPayable"].push("'" + tr_contractAddr + "'")
		}

		fs.writeFileSync(home + "/ExecutionWatch/AddressMap.json",  JSON.stringify(currentAddreMap) , err => {console.log(err);}  );

		console.log(contractName + ": The address is saved into AddressMap Successfully\n");

	}catch(e){
		console.log(contractName + ": Deploy Failed\n");
		// console.log(e)
	}
})()
