## Execution Files

## Folder Structure

	|--> org_contracts (folder)
		|--> contractA.json
		|--> contractA.sol
		|--> contractB.json
		|--> contractB.sol
		|--> ... 
	|--> tr_contracts (folder)
		|--> contractA.json (but transformed)
		|--> contractA.sol 
		|--> contractB.json
		|--> contractB.sol
		|--> ... 
	|--> _AddressMap.json
	|--> ArguFuzzing.js
	|--> AllocationFuncInject.py
	|--> DiscoverConstraints.py
	|--> ExecutionWatch.py
	|--> EW_Deploy.js
	|--> EW_FailureDetection_both.js
	|--> EW_FailureDetection_single.js
	|--> EW_FailureLocation.js
	|--> EW_preAllocate.js