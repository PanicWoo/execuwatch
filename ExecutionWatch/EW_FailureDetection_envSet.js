const home = process.cwd()
const arguFuzz = require(home + '/ExecutionWatch/ArguFuzzing.js')

const fs = require('fs');

//Setting up all the variables/packages 
const Web3 = require('web3');
const provider = new Web3.providers.HttpProvider("http://localhost:8545");	
const web3 = new Web3(provider); 


function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// This function is mainly doing comilation and Deployment
async function CompileSmartContract(contractName,contractPath){
	//1. Setting up for all moudles we needed
	const fs = require('fs');
	const solc = require('solc');
	const source = fs.readFileSync(contractPath);

	//2. Compile the contract
	var Bytecode; 
	var Abi; 
	try{
		const output = solc.compile(source.toString(), (res)=>{}) //(res)=>{console.log(res)})
		Bytecode = output.contracts[":" + contractName].bytecode;
		Abi = output.contracts[":" + contractName].interface;
		if ( (typeof Bytecode !== "undefined") && (typeof Abi !== "undefined") ){

			return [Abi,Bytecode]
		} else{
			console.log("Compilation Failed: " +contractName  + "(Undefined abi or bytecode)")
			return false
		}
	}catch(e){
		console.log("Compilation Failed: " +contractName , e.message)
		return false
	}
}
//randomize the inputs
async function randomizeInputs(abi,ctrInfo){
	var theRandomArguments = [];
	var isPayableConstructor = false;
	var abi_table = JSON.parse(abi)
	var checkConstructor = false;
	for (iter = 0; iter< abi_table.length;iter++){
		if (abi_table[iter]["type"] == "constructor" && abi_table[iter]['inputs'].length > 0){
			ctrInfo["isPayable"] = isPayableConstructor
			var inputs = abi_table[iter]['inputs']
			var inputTypes = []
			for (inputNo = 0; inputNo < inputs.length;inputNo ++){
				inputTypes.push(inputs[inputNo]["type"]);
				theRandomArgument = arguFuzz.fuzz([inputs[inputNo]["type"]],1) // fuzz only 1 random input for the constructor
				theRandomArguments.push(eval(theRandomArgument[0]))
				var inputNameKey = inputs[inputNo]["name"] + "(Input)"
				ctrInfo[inputNameKey] = [inputs[inputNo]["type"]]
				ctrInfo[inputNameKey].push(theRandomArgument[0])
			}
			checkConstructor = true
		}

		//terminate if fallback and constructor are both reached
		if (checkConstructor){
			return theRandomArguments
		}
	}

	// In case, if we can not find the constructor in the ABI
	return theRandomArguments
}

//Deploying Function 
async function DeploySmartContract(abi,bytecode,ctrInfo,theRandomArguments){

	//1. Setting up for all moudles we needed
	const Web3 = require('web3');
	const web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
	const Accounts = await web3.eth.getAccounts()
	const DefaultAccount = Accounts[0] 


	//2. Deploy the contract 
	ctrInfo['creator'] =  DefaultAccount
	try{
		//2.1 Checking the payable status
		var isPayableConstructor = false;
		var isPayableFallback = false;
		abi_table = JSON.parse(abi)
		var checkFallback = false;
		var checkConstructor =false;
		for (iter = 0; iter< abi_table.length;iter++){
			if (abi_table[iter]["type"] == "fallback"){
				isPayableFallback = abi_table[iter]["payable"]
				checkFallback =true;
			}
			if (abi_table[iter]["type"] == "constructor" && abi_table[iter]['inputs'].length > 0){
				isPayableConstructor = abi_table[iter]["payable"];
				checkConstructor = true
			}
			//terminate if fallback and constructor are both reached
			if (checkFallback&&checkConstructor){
				break
			}
		}


		//2.2 Unlock theAccount	
		await web3.eth.personal.unlockAccount(DefaultAccount, "123",15000)

		//2.3 Instantiate a contract with its ABI
		const contract = new web3.eth.Contract(JSON.parse(abi)); 
		//2.4 deploy the contract instance with its bytecode,sender,arguments
		if (isPayableConstructor){
			await contract.deploy({
				data: "0x" + bytecode,
				arguments: theRandomArguments
				})
				.send({
					from: DefaultAccount,
					gas: 470000000,//, // the gas need
					value: 10000000
					// gasPrice: '1'
			}, (error, transactionHash) => { 
				// console.log(error,transactionHash)
			}).on('receipt', (receipt) => {
				contract_final_address = receipt.contractAddress;
				}) 
		}else{
			await contract.deploy({
				data: "0x" + bytecode,
				arguments: theRandomArguments
				})
				.send({
					from: DefaultAccount,
					gas: 470000000//, // the gas need
					// value: 1123456789
					// gasPrice: '1'
			}, (error, transactionHash) => { 
				// console.log(error,transactionHash)
			}).on('receipt', (receipt) => {
				contract_final_address = receipt.contractAddress;
				// console.log(contract_final_address)
				})
		}
	return [contract_final_address,isPayableFallback]
	}catch(e){
		console.log("Deployment Error: ", e.message)
		return [false,false]
	}
}

// Utilize the argu fuzzing to create the inputs arguments
function createFuzzingArgu(funcDetails,testingDB,k,constraints = null){
	let funcNames = Object.keys(funcDetails)
	for (var key of funcNames){
		var lineCounter = funcDetails[key][0]
		var inputTypes = funcDetails[key][1]
		// Generate the function inputs
		if (inputTypes.length == 0){
			var inputs = [" "," "]
		}else{
			if (constraints !== null){
				var currConstraints = constraints[key]
				var inputs = arguFuzz.fuzz(inputTypes,k,currConstraints)
			}else{
				var inputs = arguFuzz.fuzz(inputTypes,k)
			}
		}
		testingDB[key] = [lineCounter,inputs]
	}
}

//edit address map
function editAddressMap(isPayableFallback,contractAddr){
	// Edit the AddressMap
	var currentAddreMap = fs.readFileSync(home + "/ExecutionWatch/AddressMap.json");
	currentAddreMap = JSON.parse(currentAddreMap)
	// If the addressMap is empty
	if (currentAddreMap["Payable"].length == 0){currentAddreMap["Payable"].push("'" + Accounts[0] + "'");currentAddreMap["Payable"].push("'" + Accounts[1] + "'")}

	// check whether the fall back function is payable 
	if (isPayableFallback){
		currentAddreMap["Payable"].push("'" + contractAddr + "'");
	}else{
		currentAddreMap["nonPayable"].push("'" + contractAddr + "'")
	}

	fs.writeFileSync(home + "/ExecutionWatch/AddressMap.json",  JSON.stringify(currentAddreMap) , err => {console.log(err);}  );
}


async function preAllocate(contractName,myContract){
	const arguFuzz = require(home + '/ExecutionWatch/ArguFuzzing.js')

	const fs = require('fs');
	// const keccak256 = require(keccak256);

	//Setting up all the variables/packages 
	const Web3 = require('web3');
	const provider = new Web3.providers.HttpProvider("http://localhost:8545");	
	const web3 = new Web3(provider); 

	let addrMapData = fs.readFileSync(home + '/ExecutionWatch/AddressMap.json' );
	const addrMap = JSON.parse(addrMapData);
	const funcSig_Balance = "preAllocatedBalance([" + addrMap["Payable"].join(",") +","+ addrMap["nonPayable"].join(",")  + "],50000)"
	const funcSig_Allowance = "preAllocatedAllowance([" + addrMap["Payable"].join(",") +","+ addrMap["nonPayable"].join(",")  + "],50000)"
	
	// unlock the account
	const Accounts = await web3.eth.getAccounts();
	const OwnerAddress = Accounts[0];
	const ExternalAddress = Accounts[1];

	//1. Loading the contract

	let constraintsData = fs.readFileSync(home + "/ExecutionWatch/Constraints/" + contractName + ".json");
	let constraints = JSON.parse(constraintsData)
	let existBalance = constraints["preAllocate"][0]
	let existAllowance = constraints["preAllocate"][1]

	// var myContract = new web3.eth.Contract(abi,contractAddr);
	// console.log(funcSig_Balance)
	if (existBalance){
		await eval("myContract.methods." + funcSig_Balance).send({from: OwnerAddress, gas: 470000000})
		.on("receipt", function(receipt){
			// console.log(receipt["events"]["Loguint"]["returnValues"]);
			console.log("Balance-Allocation Status: ", receipt['status'])
		})
	}else{
		console.log("There is no balances mapping.")
	}

	if( existAllowance){
		// Owner Address
		await eval("myContract.methods." + funcSig_Allowance).send({from: OwnerAddress, gas: 470000000})
		.on("receipt", function(receipt){
			// console.log(receipt["events"]["Loguint"]["returnValues"]);
			console.log("Allowance-Allocation Status: ", receipt['status'])
		})
		
		// External Address
		await eval("myContract.methods." + funcSig_Allowance).send({from: ExternalAddress, gas: 470000000})
		.on("receipt", function(receipt){
			// console.log(receipt["events"]["Loguint"]["returnValues"]);
			console.log("Allocation Status: ", receipt['status'])
		})
	}else{
		console.log("There is no allowance mapping.")
	}
}


// Entry Function (main function)
(async ()=>{
	//1. Loading the contract Name
	let contractName = process.argv[2];

	//Compile 
	let trContractPath = home + "/ExecutionWatch/tr_contracts/" + contractName + ".sol"
	var tr_compileResult = await CompileSmartContract(contractName,trContractPath);
	if (!tr_compileResult){throw new Error("Original Contract is failed to be compiled.");}
	var tr_abi = tr_compileResult[0];
	var tr_bytecode = tr_compileResult[1];
	var ctrInfo = {}
	var constructorArguments = await randomizeInputs(tr_abi,ctrInfo)
	console.log("\nConstructing Info:")
	console.log(ctrInfo)

	// Testing function details
	let funcDetailsData = fs.readFileSync(home + "/ExecutionWatch/FuncDetails/" + contractName + ".json");
	funcDetails = JSON.parse(funcDetailsData); // e.g. funcDetails: {funcName: [lineCounter, [inputType1,inputType2]]}
	console.log(funcDetails)

	// Get the caller address 
	const Accounts = await web3.eth.getAccounts();
	const OwnerAddress = Accounts[0]; 
	const ExternalAddress = Accounts[1];
	const AttackAgentAddress = null;

	//2.Fuzzing test case generation based on ContractFuzzer
	//		6*k or 6 cases will be generate based on the public function
	var testingDB = {}
	var fuzzing_Size = 10;
	
	//OPTION1: Fuzzing with constraints
	// i.e. Constraints => {input1: [0,'infinite'], input2: '!= address(msg.sender')} or {}
	// var constraints = fs.readFileSync(home + "/ExecutionWatch/Constraints/" + contractName + ".json")
	// constraints = JSON.parse(constraints);
	// createFuzzingArgu(funcDetails,testingDB,fuzzing_Size,constraints)

	//OPTION2: Fuzzing without constraints (Naive Fuzzing) 
	createFuzzingArgu(funcDetails,testingDB,fuzzing_Size)
	console.log("\nPublic Functions (with details): ")
	
	

	// 3.Testing all function in random order
	var	tr_staticalResult = {}
	var tr_testing_result = []
	var filter_funcs = ["approve","totalSupply","allowance","balanceOf"]
	for(var funcName in testingDB){
		if (filter_funcs.includes(funcName)) {
			continue
		}
		console.log(funcName)
		// Initial staticalResult
		tr_staticalResult[funcName] = {
			"FailureRate" : [0,0,0],
			"FailureLine" : {}
		}

		//deploy a new contract
		var deployResults = await DeploySmartContract(tr_abi,tr_bytecode,ctrInfo,constructorArguments);
		// console.log(deployResults)
		var currAddr = deployResults[0]
		console.log("ContractAddress: ",currAddr)
		// var isPayableFallback = deployResults[1]
		// editAddressMap(isPayableFallback,currAddr) //edit address map
		// var tr_Contract = new web3.eth.Contract(tr_abi,tr_contractAddr);
		var currContract = new web3.eth.Contract( JSON.parse(tr_abi),currAddr);
		//preallocate function 
		console.log("+++++++++++++++++++Pre-Allocate+++++++++++++++++++")
		await preAllocate(contractName,currContract)


		// load the random inputs
		var inputs = testingDB[funcName][1]
		// console.log("Fuzzing Inputs")
		// console.log(inputs)
		var callerAddr = OwnerAddress
		var noInput = false
		if (inputs.length == 2){
			noInput = true
		}

		for(inputNo = 0; inputNo < inputs.length; inputNo ++){
			//function signature 
			var input = inputs[inputNo]
			var func_sig = funcName + "(" + input + ")"
			//caller address
			if (noInput){
				if (inputNo == 0){
					callerAddr = OwnerAddress
				}else{
					callerAddr = ExternalAddress
				}
			}else{
				if (inputNo ==inputs.length/2){
					callerAddr = ExternalAddress;
				}else if(inputNo ==inputs.length){
					callerAddr = AttackAgentAddress;
				}
			}

			var tr_status =null;

			try{
				// function call on original contract 
				await eval("currContract.methods." + func_sig).send({from: callerAddr, gas: 40000000}, () => {})
				.on("receipt", function(receipt){
					tr_status = receipt['status'];
				})
				// .on("error",console.error);
			}catch(e){
				// console.log("general fail")
				// console.log(inputNo,"transformed-",funcName, "Status: false")
				tr_status = false;
				// console.log(e.message)
				if (noInput){
					if (inputNo == 0 ){
						// owner address
						tr_staticalResult[funcName]["FailureRate"][0] += 999
					}else if(inputNo == 1 ){
						// external user address
						tr_staticalResult[funcName]["FailureRate"][1] += 999
					}
				}else{
					if (inputNo < inputs.length/2 ){
						// owner address
						tr_staticalResult[funcName]["FailureRate"][0] += 1
					}else if(inputNo <inputs.length ){
						// external user address
						tr_staticalResult[funcName]["FailureRate"][1] += 1
					}
					// else if(inputNo < 15){ // contract address as caller
					// 	// agent address
					// 	staticalResult[funcName]["FailureRate"][2] += 1
					// }
				}
				tr_testing_result.push([funcName,input,currAddr])
			}
			console.log("singleFuncTest: ",inputNo, funcName , tr_status )
		}
	}

	console.log("Transformed Statistical Result:")
	console.log(tr_staticalResult)
	console.log("\n")

	var failureInfo = {
		"fuzzing_Size": fuzzing_Size,
		"testingDB" : JSON.stringify(testingDB),
		"staticalResult": JSON.stringify(tr_staticalResult),
		"testing_result": tr_testing_result
	}
	fs.writeFileSync(home + "/ExecutionWatch/FailureInfo_envSet/" + contractName + "_FailInfo.json",JSON.stringify(failureInfo) , err => {console.log(err)})

})()