import sys
import re
import json
import itertools
import numpy as np

# read the home address
with open("./Home.txt","r") as home_file:
	home = home_file.readlines()[0]


##################################
# ANALYSE/EXTRACT VARIABLES
##################################
def AnalyseVariables(varContent):
	Vars = []
	isStruct = False
	StructCount = -1
	exitBalances = False
	for line in varContent:
		#Check the content of the "struct"
		if isStruct:
			if "{" in line:
				if StructCount == -1:
					StructCount += 2
				else:
					StructCount += 1
			elif "}" in line:
				StructCount -= 1
			if StructCount == 0:
				isStruct = False
				StructCount = -1
			else:
				continue
		if line.strip().lower().startswith("struct "):
			isStruct = True
			if "{" in line:
				StructCount += 2
		# Analyse the line whetherh contains some new-defined global variables
		eles = re.findall(r"^(string|uint|int|address|bytes|byte|bool)(\d+|)(\[]|)(.+?);",line.strip())
		if len(eles) == 0:
			continue
		if eles[0][2].startswith("["):
			ArrayType = eles[0][2]
			isArray = True
		else:
			ArrayType = None
			isArray = False
		typeName = eles[0][0]
		# (&@^*&^#>?@>:>@"!")!
		if eles[0][-1].split("=")[0].strip() == "":
			continue
		varName = eles[0][-1].split("=")[0].strip().split()[-1]
		Vars.append((typeName,varName,isArray,ArrayType))
	return Vars

##################################
# FIND parents contract 
##################################
def identifyParents(content,startPoint):
	stop = False
	lineNo = startPoint
	allParents = []
	while not stop:
		currentLine = content[lineNo].split("//")[0]
		if "{" in currentLine:
			stopPoint = lineNo
			titleContent = " ".join( content[startPoint:stopPoint + 1] )
			if " is " in titleContent:
				startEditPos = titleContent.find(" is ") + 4
				stopEdidPos = titleContent.find("{")
				allParents = titleContent[startEditPos:stopEdidPos].strip().split(",")
			stop = True
		lineNo += 1

	return allParents


#############################
# Inject the console contract for debugging purpose
#############################
def injectConsoleContractRelationship(content,StartLineNo):
	# inject the inheritence relationship
	is_Position = content[StartLineNo].find(" is ")
	if is_Position != -1: # there is inheritence
		content[StartLineNo] = content[StartLineNo][:is_Position + 4] + " FailureReport, " + content[StartLineNo][is_Position + 4:]
	else:
		editPosition = content[StartLineNo].find("{")
		if editPosition != -1:
			# "{" is on the line
			content[StartLineNo] = content[StartLineNo][:editPosition] + " is FailureReport" + content[StartLineNo][editPosition:]
		else:
			# "{" is not on current line, so we just need to append the relationship "is FailureReport"
			content[StartLineNo] += " is FailureReport"



##################################
# FIND CONTRACT RANGES
##################################
def findContractRanges(content,mainContractName):
	lineNo = 0
	contractRanges = []
	contractStartPoint = None
	findContract = False
	currContractName = None

	# we should find all contract class and analysis all
	for line in content:
		#inject the console contract
		if line.startswith("pragma solidity"):
			with open(home + "/consoleContract.txt","r") as f:
				consoleContract = f.read()
				content[lineNo] += consoleContract +  "\n"
		# find MAIN CONTRACT
		if line.startswith("contract ") and (not findContract):
			contractStartPoint = lineNo
			#get the contract name 
			currContractName = line.split()[1]
			if currContractName.endswith("{"):
				currContractName = currContractName[:-1]

			findContract = True
		# record CONTRACTS
		elif findContract and ( line.strip().startswith("function ") or line.strip().startswith("modifier ") or line.strip().startswith("constructor ")): # too limited 
			contractRanges.append( (currContractName, (contractStartPoint,lineNo) ) )
			findContract = False
			# BREAK if the main contract has been found 
			if currContractName == mainContractName:
				break
		elif findContract and ( line.strip().startswith("contract ") or (lineNo == len(content)) ):
			# to handle the contract without the function to terminate 
			contractRanges.append( (currContractName , (contractStartPoint,lineNo)) )
			contractStartPoint = lineNo
			currContractName = line.split()[1]
		lineNo += 1

	return contractRanges
##################################
# FIND FUNCTION FIRST LINE
##################################
def findFuncStartPoint(content,startPoint):
	currPoint = startPoint
	while True:
		if content[currPoint].split("//")[0].strip().endswith("{"):
			return currPoint
		elif content[currPoint].split("//")[0].strip().endswith(";"):
			return False
		currPoint += 1

def isFunc(content,startPoint):
	while True:
		if content[startPoint].split("//")[0].strip().endswith("{"):
			return True
		elif content[startPoint].split("//")[0].strip().endswith(";"):
			return False
		startPoint += 1

##################################
# FIND FUNCTION RANGES
##################################
def findFunctionRanges(content,contractStartLineNo):
	functionRanges = {} # name: [ (startLineNo,finishLineNo, funcNo), (.. , .. , ..), .. ] 
	lineNo = contractStartLineNo
	isFunction = False
	functionRangeCount = -1
	contractRangeCount = -1
	scanningStart = False
	for line in content[contractStartLineNo:]:
		#skip the comment line
		if line.strip().startswith("//") or line.strip().startswith("/*") or line.strip().startswith("*") or line.strip().startswith("*/"):
			lineNo += 1
			continue
		
		# working on current function
		if isFunction:
			if functionRangeCount == -1 and (len(re.findall(r"{",line)) > 0):
					functionRangeCount += 1
			functionRangeCount += len(re.findall(r"{",line))
			functionRangeCount -= len(re.findall(r"}",line)) 
			# check whether we should terminate
			if functionRangeCount == 0:
				isFunction = False
				functionRangeCount = -1
				functionFinishPointer = lineNo
				#insert the function range to the "functionRange"
				if functionName in functionRanges:
					funcNo = len(functionRanges[functionName]) + 1
					functionRanges[functionName].append( (functionStartPoint,functionFinishPointer,funcNo) )
				else:
					functionRanges[functionName] = [(functionStartPoint,functionFinishPointer,1)]
				lineNo += 1
				continue
			else:
				lineNo += 1
				continue
			
		#identify the function start point
		if line.strip().startswith("function"):
			if isFunc(content,lineNo):
				isFunction = True
			else:
				continue
			
			functionStartPoint = lineNo
			# (&@^*&^#>?@>:>@"!")!
			functionName = line.strip().split("function")[-1].split()[0]
			if functionName != "()":
				functionName = functionName.split("(")[0]
			if len(re.findall(r"{",line)) > 0:
				functionRangeCount += len(re.findall(r"{",line)) + 1
				functionRangeCount -= len(re.findall(r"}",line)) 
		else:
			# working on only main contract range
			if "{" in line and (not scanningStart):
				contractRangeCount += 1
				scanningStart = True
			contractRangeCount += len(re.findall(r"{",line))
			contractRangeCount -= len(re.findall(r"}",line))

		if scanningStart and (contractRangeCount == 0) :
			return functionRanges
		
		lineNo += 1

	# worse case, if the range can not be recongnize
	return functionRanges


##################################
# GET THE PUBLIC FUNCTION NAMES (based on ABI)
##################################
def getFunctionNames(keyDate,contractName):
	abi_path = home + "/_Data/ABI.npz"
	abi = np.load(abi_path)

	if keyDate in abi:
		current_ABI = abi[keyDate]
	else:
		abi_path = home + "/ContractTest/"  + contractName + ".json"
		with open(abi_path,"r") as json_file:
			info = json.load(json_file)
			current_ABI = info["ABI"]
			current_ABI = json.loads(current_ABI)
	

	funcNames = []
	for eachELE in current_ABI:
		if eachELE["type"] == "function":
			funcNames.append(eachELE["name"])
	
	# only the public function name will be presented
	return funcNames

##################################
# INSERT NEW EVENT FOR ARRAY TYPE
##################################
# Var: (type,name,isArray,ArrayType)
def createNewEvent(Var):
	varName = Var[1]
	varType = Var[0] + Var[3]
	newEvent = "\n" + "event " + "log" + varName +"(string,"+ varType + ");"
	return newEvent


##################################
# CONSTRUCT LOGGING LINE
##################################
def constructLoggingLine(Vars,label):
	if label == "GV" or label == "LV" or label == "IV":
		LoggingLine = ""
	else:
		return None,None

	newEventLine = ""
	for var in Vars:
		varType = var[0]
		varName = var[1]
		isArray	= var[2]
		varName_str = "'" + label +"_" + varName + "'"
		
		# for array type variables, we create/inject new event in the current contract
		if isArray: 
			# create new event
			newEventLine += createNewEvent(var)
			# update the logging line
			LoggingLine += "emit log" +  varName + "("  +varName_str+ "," + varName + ");"
		
		# for non-array type variables, we use the log func from the inherited contract
		else:
			LoggingLine += "log" + varType + "("  +varName_str+ "," + varName + ");"

	return LoggingLine,newEventLine

##################################
# CONSTRUCT return content
##################################
def createReturnLine(LogContent,lineNo, returnType):
	# return value for each return ype
	if returnType.startswith("address") or returnType.startswith("byte"):
		returnContent = "0x001"
	elif returnType.startswith("uint") or returnType.startswith("int"):
		returnContent = "0"
	elif returnType.startswith("string"):
		returnContent = "'string'"
	elif returnType.startswith("bool"):
		returnContent = "true"
	else:
		returnContent = " "
	
	# if return type is array 
	if returnType.endswith("[]"):
		returnContent = "[" + returnContent + "]"
	elif returnType.endswith("]"):
		arraySize = returnType.split("[")[-1].split["]"].strip()
		returnContent = str([returnContent] * int(arraySize))

	returnLine = "if( lineNo == " + str(lineNo) + "){" + LogContent + "return " + returnContent +" ;}"
	return returnLine

##################################
# edit public Func
##################################
def editPublicFunc(content,funcStart,funcEnd,logLine):
	def searchNextLine(content,nextLineMark,funcEnd):
		isNextLine = False
		nextLine = ""
		while (not isNextLine):
			nextLineMark += 1
			if nextLineMark > funcEnd:
				nextLine = "Last line is reached;"
				break
			else:
				nextLine = content[nextLineMark]

			# filter the fake next lien: which is end without ";"
			if nextLine.split("//")[0].strip().endswith(";"):
				isNextLine = True
			if nextLine.startswith("//") or nextLine.startswith("*/")  or nextLine.startswith("*"):
				isNextLine = False
			
		next_line_Content = "logstring( 'Line' , '"  + nextLine.strip() + "');" 
		return next_line_Content

	functionDetail = "" # name,count,type1/type2/type3/..
	#back-up the original function
	org_func = content[funcStart:funcEnd + 1]

	# first line
	funcContentStartPoint = findFuncStartPoint(content,funcStart)
	newFirstLine = "".join(content[funcStart:funcContentStartPoint + 1]).replace("\n","")

	# function return type
	if "returns" in newFirstLine:
		returnType = newFirstLine.split("returns")[-1].split("{")[0]
		if "(" in returnType:
			returnType = returnType.strip()[1:-1].split()[0]
	else:
		returnType = " "

	# input variables
	inputTypes = ""
	IV_LogLine = ""
	IV_newEvent = ""
	inputs = ( newFirstLine.split("(")[1].split(")")[0] ).strip().split(",")
	if len(inputs) == 1 and inputs[0] == '':
		functionDetail += ""
	else:
		pretented_lines = [] 
		for eachInput in inputs:
			if eachInput == '':
				continue
			pretented_lines.append(eachInput + " ;") # the space before ';' is necessary for some input without name defined
			inputType = eachInput.strip().split()[0] # suppose to be a list of value: [type, name] or [type]
			inputTypes += inputType + "/"

		# provide the input types to complete the function details
		# for further testing
		functionDetail += inputTypes[:-1]

		# construct Input variable logging lines
		inputVars = AnalyseVariables(pretented_lines)
		IV_LogLine,IV_newEvent = constructLoggingLine(inputVars,"IV")
	

	# add new input: "lineNo" 
	insertPosition = newFirstLine.find(")")
	checkPosition = newFirstLine.find("(")
	if insertPosition != -1 and checkPosition != -1:
		if checkPosition == insertPosition - 1 or (newFirstLine[checkPosition : insertPosition + 1].strip() == ''):
			newFirstLine = newFirstLine[:insertPosition] + "uint lineNo" + newFirstLine[insertPosition:]
		else:
			newFirstLine = newFirstLine[:insertPosition] + ",uint lineNo" + newFirstLine[insertPosition:]

	# change the function Name
	allEle = newFirstLine.split()
	allEle[1] = "test_" + allEle[1]
	newFirstLine = " ".join(allEle)

	# replace the first line
	content[funcStart:funcContentStartPoint] = [""]*(funcContentStartPoint - funcStart)
	org_func[funcContentStartPoint - funcStart] += "logging();" # for detecting whether the original function whether run successfully 
	content[funcContentStartPoint] = "".join((org_func)) + "\n" + newFirstLine 

	# CASE1: line 0 
	# print(newFirstLine)
	next_line_Content = searchNextLine(content,funcContentStartPoint,funcEnd)
	# print("Line 0 :" + next_line_Content)
	content[funcContentStartPoint] += createReturnLine(next_line_Content + logLine + IV_LogLine,0,returnType)

	# CASE2: line 1 - *
	total_LV_LogLine = ""
	total_LV_newEvent = ""
	codeLineCounter = 0
	for lineNo in range(funcContentStartPoint+1,funcEnd):
		line = content[lineNo]
		# TO filter some lines
		if line.startswith("//") or line.startswith("*/")  or line.startswith("*"):
			continue
		if line.strip() == "":
			continue
		if not line.split("//")[0].strip().endswith(";"):
			continue

		codeLineCounter += 1

		Vars = AnalyseVariables([line])
		if len(Vars) > 0:
			LV_LogLine,LV_newEvent = constructLoggingLine(Vars,"LV")
			total_LV_LogLine += LV_LogLine
			total_LV_newEvent += LV_newEvent
		
		# find next line
		next_line_Content = searchNextLine(content,lineNo,funcEnd)
		# print("Line "+str(lineNo - funcContentStartPoint)+" :" + next_line_Content)
		content[lineNo] +=  createReturnLine(next_line_Content + logLine + IV_LogLine + total_LV_LogLine,lineNo - funcContentStartPoint,returnType)

		if next_line_Content == "Last line is reached;":
			break
	
	
	functionDetail = str(codeLineCounter) + "," + functionDetail
	return total_LV_newEvent + IV_newEvent ,functionDetail

def injectPreDesignFunc(currContractContent,currContractStart):
	exitBalances = False
	exitAllowance = False
	lineNo = 0
	for line in currContractContent:
		# check whether exits the balances map 
		if not exitBalances:
			balances = re.findall(r"mapping.+?=>.+?(balance.*?)[\s]*;",line)
			allowed = re.findall(r'mapping.+?address.+?=>.+?address.+?uint.+?(allow.*?)[\s]*;',line)

			if (len(balances) > 0) and (not exitBalances):
				exitBalances = True

				injectFuncContent = "function preAllocatedBalance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){"+ balances[0] +"[a[i]] = value;}loguint(\"a[3]\'s balance\","+balances[0]+"[a[3]]);}"

				content[currContractStart + lineNo] = injectFuncContent +  "\n" + content[currContractStart + lineNo]

			if (len(allowed) > 0) and (not exitAllowance):
				exitAllowance = True
				injectFuncContent = "function preAllocatedAllowance(address[] a, uint value) public{for (uint i = 0; i<a.length; i++){"+ allowed[0] +"[a[i]][msg.sender] = value;"+allowed[0]+"[msg.sender][a[i]] = value;}loguint(\"a[3][msg.sender]\'s allowance\","+allowed[0]+"[a[3]][msg.sender]);}"
				content[currContractStart + lineNo] = injectFuncContent +  "\n" + content[currContractStart + lineNo]
			
			if exitBalances and exitAllowance:
				break
			
			lineNo += 1

	return exitBalances, exitAllowance

##################################
# MAIN
##################################
if __name__ == "__main__":
	contractFolder = sys.argv[1]
	inputContract = sys.argv[2]
	singleSRC_path = home + "/" + contractFolder + "/" + inputContract + ".sol"
	with open(singleSRC_path,"r") as file:
		content = file.readlines()

	#Basic Details
	contractName = content[1].split(":")[-1].strip()
	keyDate = content[2].strip().split()[-1]
	functionNames = getFunctionNames(keyDate,contractName)

	# Find all ranges of contract class
	contractRanges = findContractRanges(content,contractName)

	#Reconstruct the each public function 
	functionDetails = ""
	exitBalance = False
	exitAllowance = False
	if len(contractRanges) != 0:
		commonLogLine = "loguint('BV_this.balance',this.balance);loguint('BV_msg.value',msg.value);logaddress('BV_msg.sender',msg.sender);loguint('BV_now',now);"
		# Get the global variables 
		allGlobalVars = {}
		all_GV_newEventLine = ""
		all_GV_LoggingLine = ""
		for eachContract in contractRanges:
			currContractStart = eachContract[1][0]
			currGlobalFinishPoint = eachContract[1][1]
			currAllParents = identifyParents(content,currContractStart)
			currGlobalVars = AnalyseVariables(content[currContractStart:currGlobalFinishPoint + 1])
			# if there is no inherited relationship
			if len(currAllParents) == 0:
				allGlobalVars[eachContract[0]] = currGlobalVars
			# Append  Parent's global variable 
			else:
				for eachParent in currAllParents:
					eachParent = eachParent.strip()
					currGlobalVars += allGlobalVars[eachParent]
					allGlobalVars[eachContract[0]] =  currGlobalVars
			
			# Make the logging lines for the global variables
			currGV_LoggingLine,currGV_newEventLine = constructLoggingLine(currGlobalVars,"GV")
			currGlobalLogLine = commonLogLine + currGV_LoggingLine  # global variable belongs to the commone line too

			# Get the Ranges of all function of current contract
			functionRanges = findFunctionRanges(content,currContractStart)  
			# Get the local variables 
			LV_newEventLine = ""
			for funcName in functionRanges:
				# 	TO filter the non-public functions
				if funcName not in functionNames:
					continue
				# funcRange: (functionStartPoint,functionFinishPointer,funcNo)
				funcRange = functionRanges[funcName][0]

				# edit the function content for failure detection
				LV_newEventLine,funcDetail = editPublicFunc(content,funcRange[0],funcRange[1],currGlobalLogLine)

				functionDetails += funcName + "," + funcDetail + ";"
				# create the new events
				content[currContractStart] += currGV_newEventLine + LV_newEventLine

			# inject the inherited relationship 
			injectConsoleContractRelationship(content,currContractStart)

			# inject the function which can pre allocate value for the balance map
			if (not exitBalance) and (not exitAllowance):
				temp_exitBalance,temp_exitAllowance = injectPreDesignFunc(content[currContractStart:currGlobalFinishPoint + 1],currContractStart)
				exitBalance = temp_exitBalance
				exitAllowance = temp_exitAllowance
			elif not exitBalance:
				temp_exitBalance,temp_exitAllowance = injectPreDesignFunc(content[currContractStart:currGlobalFinishPoint + 1],currContractStart)
				exitBalance = temp_exitBalance
			elif not exitAllowance:
				temp_exitBalance,temp_exitAllowance = injectPreDesignFunc(content[currContractStart:currGlobalFinishPoint + 1],currContractStart)
				exitAllowance = temp_exitAllowance
			
	with open(home + "/FS_results/"+contractName+".txt","w") as f:
		functionDetails += str(exitBalance) + "/" + str(exitAllowance)
		f.write(functionDetails)
	with open(home + "/testContracts/"+contractName+".sol","w") as f:
		f.writelines(content)