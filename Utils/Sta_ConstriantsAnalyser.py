#TO DO: analyse the constraints' Distribution based on the function

#1. Find the functions of the contract
#2. Locate the contraints
#		2.1 require/assert keyword
#		2.2 transfer()/send()/call() keyword
#   2.3 modifier
#3. Statistical Record
import re
import sys
import os

def isFunc(content,startPoint):
	while startPoint < len(content):
		funcLine = content[startPoint].split("//")[0].strip()
		if funcLine.endswith("{"):# or funcLine.endswith("}") :
			return True
		elif funcLine.endswith(";"):
			return False
		startPoint += 1
	return False

##################################
# FIND FUNCTION RANGES
##################################
def findFunctionRanges(content):
	functionRanges = {} # name: [ (startLineNo,finishLineNo, funcNo), (.. , .. , ..), .. ] 
	lineNo = 0
	isFunction = False
	functionRangeCount = -1
	for line in content:
		#skip the comment line
		if line.strip().startswith("//") or line.strip().startswith("/*") or line.strip().startswith("*") or line.strip().startswith("*/"):
			lineNo += 1
			continue
		
		# working on current function
		if isFunction:
			if functionRangeCount == -1 and (len(re.findall(r"{",line)) > 0):
					functionRangeCount += 1
			functionRangeCount += len(re.findall(r"{",line))
			functionRangeCount -= len(re.findall(r"}",line)) 
			# check whether we should terminate
			if functionRangeCount == 0:
				isFunction = False
				functionRangeCount = -1
				functionFinishPointer = lineNo
				#insert the function range to the "functionRange"
				if functionName in functionRanges:
					funcNo = len(functionRanges[functionName]) + 1
					functionRanges[functionName].append( (functionStartPoint,functionFinishPointer,funcNo) )
				else:
					functionRanges[functionName] = [(functionStartPoint,functionFinishPointer,1)]
				lineNo += 1
				continue
			else:
				lineNo += 1
				continue
			
		#identify the function start point
		if line.strip().startswith("function"):
			if isFunc(content,lineNo):
				isFunction = True
			else:
				lineNo += 1
				continue

			
			functionStartPoint = lineNo
			# (&@^*&^#>?@>:>@"!")!
			functionName = line.strip().split("function")[-1].split("(")[0]
			if functionName != "()":
				functionName = functionName.split("(")[0]
			if len(re.findall(r"{",line)) > 0:
				functionRangeCount += len(re.findall(r"{",line)) + 1
				functionRangeCount -= len(re.findall(r"}",line)) 
		
		lineNo += 1

	# worse case, if the range can not be recongnize
	return functionRanges


def findModifiers(content):
	def findName(content,StartPoint):
		lineNo = StartPoint
		onGoing = 0
		while onGoing < 20:
			line = content[lineNo].strip()
			if "{" in line:
				return "".join(content[StartPoint:lineNo+1]).strip().split("{")[0].split("(")[0].split("modifier")[1].strip()
			else:
				onGoing += 1
			lineNo += 1
		
		# has keyword "modifier", but can not find the name
		return "Nil"
	modifiers = []
	lineNo = 0
	for line in content:
		
		if line.strip().startswith("modifier"):
			modifierName = findName(content,lineNo)
			if modifierName != "Nil":
				modifiers.append(modifierName)
		lineNo += 1
	
	return modifiers


	return True

def findConstraints(content,functionRanges,modifierNames):
	RequireConst = False
	ModifierConst = False
	TransferConst = False
	# If there is no functions 
	if len(functionRanges) == 0:
		return [ModifierConst,RequireConst,TransferConst]
	

	for funcName in functionRanges:
		funcRange = functionRanges[funcName][0]
		funcContent = content[funcRange[0]:funcRange[1]]
	
	# Modifier constraints
	functionHead = "".join(funcContent).split("{")[0]
	for modifierName in modifierNames:
		if modifierName in functionHead:
			ModifierConst = True
			break

	#require/assert constraint
	for line in funcContent:
		if line.strip().startswith("require") or line.strip().startswith("assert"):
			RequireConst = True
			break
	
	#transfer ether constraints
	for line in funcContent:
		if ".transfer(" in line:
			transferContent = re.findall(r'.transfer\((.+)\)',line)
			if len(transferContent) > 0:
				transferContent = transferContent[0]
				if "," not in transferContent:
					transferConst = True
					break
		elif ".send(" in line:
			sendContent = re.findall(r'.send\((.+)\)',line)[0]
			if "," not in sendContent:
				transferConst = True
				break
		elif ".call(" in line and ".value(" in line:
			transferConst = True
			break
	
	return [ModifierConst,RequireConst,TransferConst]
	

	

# if __name__ == "__main__":
# 	totalFunc = 0
# 	requireFunc = 0
# 	transferFunc = 0
# 	modifierFunc = 0
# 	# with open() as f:
# 	# 	sourceCode = f.readlines()
# 	# 	functions = functionContent(sourceCode)
# 	# 	a = findConstraints(functions)
# 	# DataPath = "./ContractTest^0.4.4/USDCCoin.sol"
# 	DataPath	= "/Users/BAOandPAN/Desktop/SummerResearch/Solidity_Analyse/allContracts"
# 	totalContracts = 0
# 	for filename in os.listdir(DataPath):
# 		if not filename.strip().endswith(".sol"):
# 			continue
# 		filePath = DataPath + "/" + filename
# 		with open(filePath,"r") as dataF:
# 			content = dataF.readlines()

# 			funcRanges = findFunctionRanges(content)
# 			modifiers = findModifiers(content)

# 			[ModifierConst,RequireConst,TransferConst] = findConstraints(content,funcRanges,modifiers)

# 			totalFunc += len(funcRanges)
# 			if ModifierConst:
# 				modifierFunc += 1
# 			if RequireConst:
# 				requireFunc += 1
# 			if TransferConst:
# 				transferFunc += 1
	
# 		totalContracts += 1
# 		print("Total Contract: " + str(totalContracts) + ", Total Functions: " + str(totalFunc))
# 		print("M: " + str(modifierFunc) + ", R: " + str(requireFunc) + ", T: " + str(transferFunc))

# 	print("Total Functions: " + str(totalFunc))
# 	print("M: " + str(modifierFunc) + ", R: " + str(requireFunc) + ", T: " + str(transferFunc))	

if __name__ == "__main__":
	dataPath = "/Users/BAOandPAN/Desktop/transfer.txt"
	total = 0
	with open(dataPath,"r") as dataF:
		content = dataF.readlines()
		newContent = []
		# print(len(content))
		# print(len(set(content)) )
		for line in content:
			if ".delegatecall" in line and ".value(" in line:
				newContent.append(line.split()[0] + "\n")
				
		
		print(len(set(newContent)),len(newContent),len(content))

	

	# with open("/Users/BAOandPAN/Desktop/newCallValue.txt","w") as f:
	# 	f.writelines(newContent)