import os 
import sys
import json
import numpy as np



def commonFuncPresentation():
	src_path = "./DC_src_processed"
	# keys = ["balanceOf","allowance","totalSupply","approve"]
	dic = {
		"balanceOf" : 0,
		"allowance": 0,
		"totalSupply" : 0,
		"approve" : 0
	}
	for name in os.listdir(src_path):
		# print(name)
		with open(src_path + "/" + name,"r") as f_read:
			data = json.load(f_read)
			abi = data["ABI"]
			for key in dic:
				if "\"name\":\""+ key +"\"" in abi:
					dic[key] += 1
			# for eachFunc in data["ABI"]:
			# 	print(eachFunc)
			# 	funcName = eachFunc["name"] 
			# 	if funcName in dic:
			# 		dic[funcName] += 1

	print(dic)

def hardCodeContractAddress():
	with open("./FL_withContrains.txt","r") as f1:
		content1 = "".join(f1.readlines())
	with open("./hardCodeContracts.txt","r") as f2:
		content2 = "".join(f2.readlines())
		# print(content)
	
	count = 0
	for name in os.listdir("./DC_src_processed"):
		name = name[:-5]
		if name in content2:
			# if name in content1:
			print(name)
			count += 1
	
	print(count)

if __name__ == "__main__":
	# hardCodeContractAddress()
	home = "/Users/BAOandPAN/Desktop/HonoursProject"
	abi_path = home + "/_Data/ABI.npz"
	abi = np.load(abi_path)
	functionNum = []
	for keyDate in abi:
		curr_abi = abi[keyDate]
		# print(len(curr_abi))
		# break
		total = 0
		if curr_abi.all() == None:
			continue
		for eachELE in curr_abi:
			# print(eachELE)
			if (eachELE["type"] == "function") and ("stateMutability" in eachELE):
				if (eachELE["stateMutability"] != "view"):
					total	+= 1
		if total > 20:
			print(curr_abi)
			break
		functionNum.append(str(total) + "\n")
	
	# with open("./test_funcNum.txt","w") as f:
	# 	f.writelines(functionNum)
			