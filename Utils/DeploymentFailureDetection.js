const fs = require("fs");
const arguFuzz = require('./ArguFuzzing.js')

// This function is mainly doing comilation and Deployment
async function CompileSmartContract(contractName,contractPath){
	//1. Setting up for all moudles we needed
	const fs = require('fs');
	const solc = require('solc');
	const source = fs.readFileSync(contractPath);

	//2. Compile the contract
	var Bytecode; 
	var Abi; 
	try{
		const output = solc.compile(source.toString(), (res)=>{}) //(res)=>{console.log(res)})
		Bytecode = output.contracts[":" + contractName].bytecode;
		Abi = output.contracts[":" + contractName].interface;
		if ( (typeof Bytecode !== "undefined") && (typeof Abi !== "undefined") ){

			return [Abi,Bytecode]
		} else{
			console.log("Compilation Failed: " +contractName  + "(Undefined abi or bytecode)")
			return false
		}
	}catch(e){
		console.log("Compilation Failed: " +contractName , e.message)
		return false
	}
}

//Deploying Function 
async function DeploySmartContract(abi,bytecode){
	//1. Setting up for all moudles we needed
	const Web3 = require('web3');
	const web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
	const Accounts = await web3.eth.getAccounts()
	const DefaultAccount = Accounts[0] 


	//2. Deploy the contract 
	try{
		//2.1 Construct random input variables for constructor
		var theRandomArguments = [];
		var isPayable = false;
		abi_table = JSON.parse(abi)
		for (i = 0; i< abi_table.length;i++){
			if (abi_table[i]["type"] == "constructor" && abi_table[i]['inputs'].length > 0){
				var isPayable = abi_table[i]["payable"];
				var inputs = abi_table[i]['inputs']
				var inputTypes = []
				var theRandomArguments = []
				for (inputNo = 0; inputNo < inputs.length;inputNo ++){
					inputTypes.push(inputs[inputNo]["type"]);
					theRandomArgument = arguFuzz.fuzz(inputTypes,1) // fuzz only 1 random input for the constructor
					theRandomArguments.push(eval(theRandomArgument[0]))
				}
				break
			}
		}


		//2.2 Unlock theAccount	
		await web3.eth.personal.unlockAccount(DefaultAccount, "123456",15000)

		//2.3 Instantiate a contract with its ABI
		const contract = new web3.eth.Contract(JSON.parse(abi)); 

		//2.4 deploy the contract instance with its bytecode,sender,arguments
		console.log(isPayable, theRandomArguments)
		if (isPayable){
			await contract.deploy({
				data: "0x" + bytecode,
				arguments: theRandomArguments
				})
				.send({
					from: DefaultAccount,
					gas: 470000000,//, // the gas need
					value: 100000000
					// gasPrice: '1'
			}, (error, transactionHash) => { 
			}).on('receipt', (receipt) => {
				var rec = receipt;
				contract_final_address = receipt.contractAddress;
				}) 
		}else{
			await contract.deploy({
				data: "0x" + bytecode,
				arguments: theRandomArguments
				})
				.send({
					from: DefaultAccount,
					gas: 470000000//, // the gas need
					// value: 1123456789
					// gasPrice: '1'
			}, (error, transactionHash) => { 
				// console.log(error,transactionHash)
			}).on('receipt', (receipt) => {
				var rec = receipt;
				contract_final_address = receipt.contractAddress;
				})
		}
	return contract_final_address
	}catch(e){
		console.log("Deployment Error: ", e.message)
		// console.log(rec)
		return false
	}
}


(async ()=>{
	try{
		var contractName = process.argv[2];
		console.log("CONTRACT:  "+contractName)
		let contractPath = "./ContractTest/" + contractName + ".sol"
		var compileResult = await CompileSmartContract(contractName,contractPath);
		if (!compileResult){console.log(compileResult);throw new Error("Compile Failed");}
		var abi = compileResult[0];
		var bytecode = compileResult[1];
		var contractAddr = await DeploySmartContract(abi,bytecode);
		if (!contractAddr){
			throw new Error("Deploy failed.");
		}else{
			console.log("Deployment Successfully")
		}

	}catch(e){
		console.log("Deployment Failed");
		console.error(e);
	}
	
})()

