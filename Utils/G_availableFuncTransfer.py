import os

availablePath = "/Users/BAOandPAN/Desktop/HonoursProject/avaibaleContract.txt"
SourcePath = "/Users/BAOandPAN/Desktop/HonoursProject/ContractTest"
Versions = ["^0.4.4","^0.4.24","^0.4.21","^0.4.23","^0.4.25","0.4.24"]


with open(availablePath,'r') as f:
	availableContracts = []
	for line in f.readlines():
		availableContracts.append(line[:-6])

for eachVer in Versions:
	contracts = os.listdir(SourcePath + eachVer)
	contracts = [i[:-4] for i in contracts]
	# print(contracts)
	overLapContracts = list(set(availableContracts) & set(contracts))

	print(overLapContracts)
	print(eachVer)

	# shift current overlap contract to the destination folder
	toPath = "/Users/BAOandPAN/Desktop/HonoursProject/ExecutionWatch/org_contracts"
	for eachContract in overLapContracts:
		with open(SourcePath + eachVer + "/" + eachContract + ".sol","r") as f3:
			sourceCode = f3.readlines()
		with open(toPath + "/" + eachContract + ".sol","w") as f4:
			f4.writelines(sourceCode)