const fs = require("fs");
const arguFuzz = require('./ArguFuzzing.js')

// This function is mainly doing comilation and Deployment
async function CompileSmartContract(contractName,contractPath){
	//1. Setting up for all moudles we needed
	const fs = require('fs');
	const solc = require('solc');
	const source = fs.readFileSync(contractPath);

	//2. Compile the contract
	var Bytecode; 
	var Abi; 
	try{
		const output = solc.compile(source.toString(), (res)=>{}) //(res)=>{console.log(res)})
		Bytecode = output.contracts[":" + contractName].bytecode;
		Abi = output.contracts[":" + contractName].interface;
		if ( (typeof Bytecode !== "undefined") && (typeof Abi !== "undefined") ){

			return [Abi,Bytecode]
		} else{
			console.log("Compilation Failed: " +contractName  + "(Undefined abi or bytecode)")
			return false
		}
	}catch(e){
		console.log("Compilation Failed: " +contractName , e.message)
		return false
	}
}

//Deploying Function 
async function DeploySmartContract(abi,bytecode,ctrInfo){

	//1. Setting up for all moudles we needed
	const Web3 = require('web3');
	const web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
	const Accounts = await web3.eth.getAccounts()
	const DefaultAccount = Accounts[0] 


	//2. Deploy the contract 
	ctrInfo['creator'] =  DefaultAccount
	try{
		//2.1 Construct random input variables for constructor
		var theRandomArguments = [];
		var isPayableConstructor = false;
		var isPayableFallback = false;
		abi_table = JSON.parse(abi)
		var checkFallback = false;
		var checkConstructor = false;
		for (iter = 0; iter< abi_table.length;iter++){
			if (abi_table[iter]["type"] == "fallback"){
				isPayableFallback = abi_table[iter]["payable"]
				checkFallback =true;
			}
			if (abi_table[iter]["type"] == "constructor" && abi_table[iter]['inputs'].length > 0){
				isPayableConstructor = abi_table[iter]["payable"];
				ctrInfo["isPayable"] = isPayableConstructor
				var inputs = abi_table[iter]['inputs']
				var inputTypes = []
				for (inputNo = 0; inputNo < inputs.length;inputNo ++){
					inputTypes.push(inputs[inputNo]["type"]);
					theRandomArgument = arguFuzz.fuzz(inputTypes,1) // fuzz only 1 random input for the constructor
					theRandomArguments.push(eval(theRandomArgument[0]))
					var inputNameKey = inputs[inputNo]["name"] + "(Input)"
					ctrInfo[inputNameKey] = [inputs[inputNo]["type"]]
					ctrInfo[inputNameKey].push(theRandomArgument[0])
				}
				checkConstructor = true
			}

			//terminate if fallback and constructor are both reached
			if (checkConstructor && checkFallback){
				break
			}
		}


		//2.2 Unlock theAccount	
		await web3.eth.personal.unlockAccount(DefaultAccount, "123",15000)

		//2.3 Instantiate a contract with its ABI
		const contract = new web3.eth.Contract(JSON.parse(abi)); 
		// console.log(bytecode.length)
		// contract.deploy({
		// 	data: "0x" + bytecode,
		// 	arguments: theRandomArguments
		// 	}).estimateGas((err,gas) => {
		// 		console.log(err);
		// 		console.log(gas);
		// 	})
		//2.4 deploy the contract instance with its bytecode,sender,arguments
		if (isPayableConstructor){
			await contract.deploy({
				data: "0x" + bytecode,
				arguments: theRandomArguments
				})
				.send({
					from: DefaultAccount,
					gas: 470000000,//, // the gas need
					value: 10000000
					// gasPrice: '1'
			}, (error, transactionHash) => { 
				// console.log(error,transactionHash)
			}).on('receipt', (receipt) => {
				contract_final_address = receipt.contractAddress;
				}) 
		}else{
			await contract.deploy({
				data: "0x" + bytecode,
				arguments: theRandomArguments
				})
				.send({
					from: DefaultAccount,
					gas: 470000000//, // the gas need
					// value: 1123456789
					// gasPrice: '1'
			}, (error, transactionHash) => { 
				// console.log(error,transactionHash)
			}).on('receipt', (receipt) => {
				contract_final_address = receipt.contractAddress;
				// console.log(contract_final_address)
				})
		}
	return [contract_final_address,isPayableFallback]
	}catch(e){
		console.log("Deployment Error: ", e.message)
		return [false,false]
	}
}


(async ()=>{
	//1. Setting up for all moudles we needed
	const Web3 = require('web3');
	const web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
	const Accounts = await web3.eth.getAccounts()
	try{
		var contractName = process.argv[2];
		let contractPath = "./testContracts/" + contractName + ".sol"
		var compileResult = await CompileSmartContract(contractName,contractPath);
		if (!compileResult){throw new Error("Compile Failed");}
		var abi = compileResult[0];
		var bytecode = compileResult[1];
		var ctrInfo = {}
		var deployResult = await DeploySmartContract(abi,bytecode,ctrInfo);
		var contractAddr = deployResult[0]
		var isPayableFallback = deployResult[1]
		
		if (!contractAddr){
			throw new Error("Deploy failed.");
		}
		// collect all test functions
		let analyseResultPath = "./FS_results/" + contractName + ".txt";
		// collect all test functions
		let testFunc = fs.readFileSync(analyseResultPath).toString();
		testFunc = testFunc.toString().split(";");
		var existenceBA = testFunc.pop().split("/");
		var eBalance = existenceBA[0]
		var eAllowance = existenceBA[1]


		let data_src = {
			name: contractName,
			address: contractAddr,
			ABI: abi,
			ByteCode: bytecode,
			testFunc: testFunc,
			constructorInfo: ctrInfo,
			existBalance: eBalance,
			existAllowance: eAllowance
		};
		if (JSON.stringify(testFunc) !="[]"){
			console.log(contractName + ": Deploy Successfully\nFunction(s) exist\n")
			fs.writeFileSync("./DC_src"+ "/" + contractName + ".json",JSON.stringify(data_src),err => {console.log(err)});
			
			// Edit the _AddressMap
			var currentAddreMap = fs.readFileSync("./Utils/_AddressMap.json");
			currentAddreMap = JSON.parse(currentAddreMap)
			// If the addressMap is empty
			if (currentAddreMap["Payable"].length == 0){currentAddreMap["Payable"].push("'" + Accounts[0] + "'");currentAddreMap["Payable"].push("'" + Accounts[1] + "'")}

			// check whether the fall back function is payable 
			if (isPayableFallback){
				currentAddreMap["Payable"].push("'" + contractAddr + "'")
			}else{
				currentAddreMap["nonPayable"].push("'" + contractAddr + "'")
			}

			fs.writeFileSync("./Utils/_AddressMap.json",  JSON.stringify(currentAddreMap) , err => {console.log(err);}  );
		}else{
			console.log(contractName + ": Deploy Successfully\nNo Function(s)\n")
		}
	
	}catch(e){
		console.log(contractName + ": Deploy Failed\n");
		// console.log(e)
	}
})()

// module.exports = {
// 	CompileSmartContract, DeploySmartContract
// };

// (async ()=>{
// 	try{
// 		var contractName = process.argv[2];
// 		console.log("CONTRACT:  "+contractName)
// 		let contractPath = "./ContractTest/" + contractName + ".sol"
// 		var compileResult = await CompileSmartContract(contractName,contractPath);
// 		if (!compileResult){console.log(compileResult);throw new Error("Compile Failed");}
// 		var abi = compileResult[0];
// 		var bytecode = compileResult[1];
// 		var contractAddr = await DeploySmartContract(abi,bytecode);
// 		if (!contractAddr){
// 			throw new Error("Deploy failed.");
// 		}else{
// 			console.log("Deployment Successfully")
// 		}

// 	}catch(e){
// 		console.log("Deployment Failed");
// 		console.error(e);
// 	}
	
// })()
