import numpy as np
import json
from G_randomSelector import fileRandomSelector

CG_path = "/Users/BAOandPAN/Desktop/HonoursProject/_Data/all_CG.txt"
srcCode_path = "/Users/BAOandPAN/Desktop/SummerResearch/Solidity_Analyse/z_allContracts"



def CGtoJSON():
	src_path = CG_path
	CallGraph = {}
	with open(src_path,"r") as f:
		src = f.readlines()
		lineNo = 0
		startPoint = None
		endPoint = None
		total_line = len(src)

		for line in src:
			if lineNo + 1 >= total_line:
				break
			if line.startswith("2018_"):
				startPoint = lineNo + 1
				key_date = src[startPoint-1].strip()
				with open(srcCode_path + "/" + key_date + ".txt","r") as srcF:
					ContractName = srcF.readlines()[1].split(":")[-1].strip()
			if src[lineNo + 1].startswith("2018_"):
				endPoint = lineNo
				CallGraph[key_date] = {
					"NAME": ContractName,
					"CALLS": src[startPoint:endPoint+1]
				}

			lineNo += 1
			print(lineNo/total_line,end = "\r")
	
	with open("CallGraph.json","w") as json_file:
		json.dump(CallGraph,json_file)

# CGtoJSON()

def analyseIndependency():
	abi = np.load("/Users/BAOandPAN/Desktop/HonoursProject/analyseABI/ABI.npz")
	src_path = "/Users/BAOandPAN/Desktop/HonoursProject/_Data/CallGraph.json"
	with open(src_path,"r") as json_file:
		CallGraph = json.load(json_file)
	
	total_public = 0
	filtered_public = 0

	for _ in range(10000):
		key = fileRandomSelector()
		if key not in CallGraph:
			continue
		processedCG = {}
		mainContractName = CallGraph[key]["NAME"]
		currentABI = abi[key]

		#Public Function Names
		publicFuncs = []
		if currentABI.all() == None:
			continue
		for eachEle in currentABI:
			if (eachEle["type"] == "function") and ("stateMutability" in eachEle):
				if eachEle["stateMutability"] != "view":
					publicFuncs.append(eachEle["name"])
					processedCG[eachEle["name"]] = {"parent": [], "child":[]}
		

		for eachCall in CallGraph[key]["CALLS"]:
			[calleeContent,callerInfo] = eachCall.strip().split("==>")
			callerInfo = callerInfo.split(",")
			currContractName = callerInfo[0].split(":")[1].strip()
			currFuncName = callerInfo[1].split(":")[1].strip()




			# if currFuncName.strip().endswith("(ct)") or (currContractName != mainContractName):
			if currFuncName.strip().endswith("(ct)"):
				continue

			# print(key)
			# print(mainContractName)
			# print(publicFuncs)
			# print(processedCG)
			# print(currContractName)
			# print(currFuncName)
			# return

			# callee and caller are both in the public function
			for eachFunc in publicFuncs:
				option1 = eachFunc + "("
				option2 = "." + eachFunc + "("
				try:
					if calleeContent.strip().startswith(option1) or (option2 in calleeContent):
						if currFuncName not in publicFuncs: # filter internal function / private function
							continue
						if eachFunc not in processedCG[currFuncName]["child"]:
							processedCG[currFuncName]["child"].append(eachFunc)
						if currFuncName not in processedCG[eachFunc]["parent"]:
							processedCG[eachFunc]["parent"].append(currFuncName)
				except:
					continue
		

		#analyse independency 
		total_public += len(processedCG)
		for keyword in processedCG:
			if len(processedCG[keyword]["parent"]) > 0:
				filtered_public += 1
				
				
		
		print(total_public,filtered_public,end = "\r")

	print(total_public,filtered_public)
analyseIndependency()