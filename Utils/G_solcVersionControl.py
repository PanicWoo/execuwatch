import json
import os
import sys

if __name__ == "__main__":
	required_solc_version = sys.argv[1]
	#load the package json file
	with open("/Users/BAOandPAN/Desktop/HonoursProject/package.json","r") as f_read:
		data = json.load(f_read)
		data["dependencies"]["solc"] = required_solc_version
	
	os.remove("/Users/BAOandPAN/Desktop/HonoursProject/package.json")
	#write the new version in the file
	with open("/Users/BAOandPAN/Desktop/HonoursProject/package.json","w") as f_write:
		json.dump(data,f_write,indent=4)

	# update the local npm package
	os.system("npm install")
	
