from G_randomSelector import fileRandomSelector
import re

##################################
# FIND FUNCTION RANGES
##################################
def findFunctionRanges(content,contractStartPoint):
	functionRanges = {} # name: [ (startLineNo,finishLineNo, funcNo), (.. , .. , ..), .. ] 
	lineNo = 0
	isFunction = False
	functionRangeCount = -1
	contractRangeCount = -1
	scanningStart = False
	for line in content:
		#skip the line before the main contrat
		if lineNo < contractStartPoint:
			lineNo += 1
			continue
		# elif lineNo == contractStartPoint:
		# 	scanningStart = True

		#skip the comment line
		if line.strip().startswith("//") or line.strip().startswith("/*") or line.strip().startswith("*") or line.strip().startswith("*/"):
			lineNo += 1
			continue
		
		# working on current function
		if isFunction:
			if functionRangeCount == -1 and (len(re.findall(r"{",line)) > 0):
					functionRangeCount += 1
			functionRangeCount += len(re.findall(r"{",line))
			functionRangeCount -= len(re.findall(r"}",line)) 
			# check whether we should terminate
			if functionRangeCount == 0:
				isFunction = False
				functionRangeCount = -1
				functionFinishPointer = lineNo
				#insert the function range to the "functionRange"
				if functionName in functionRanges:
					funcNo = len(functionRanges[functionName]) + 1
					functionRanges[functionName].append( (functionStartPoint,functionFinishPointer,funcNo) )
				else:
					functionRanges[functionName] = [(functionStartPoint,functionFinishPointer,1)]
				lineNo += 1
				continue
			else:
				lineNo += 1
				continue
			
		#identify the function start point
		if line.strip().startswith("function") and (not line.strip().endswith(";")):
			isFunction = True
			functionStartPoint = lineNo
			# (&@^*&^#>?@>:>@"!")!
			functionName = line.strip().split("function")[1].strip().split("(")[0]
			if functionName != "":
				functionName = functionName.split("(")[0]
			else:
				functionName = "()"
			if len(re.findall(r"{",line)) > 0:
				functionRangeCount += len(re.findall(r"{",line)) + 1
				functionRangeCount -= len(re.findall(r"}",line)) 
		else:
			# working on only main contract range
			if "{" in line and (not scanningStart):
				contractRangeCount += 1
				scanningStart = True
			contractRangeCount += len(re.findall(r"{",line))
			contractRangeCount -= len(re.findall(r"}",line))

		if scanningStart and (contractRangeCount == 0) :
			return functionRanges
		
		lineNo += 1
	# worse case, if the range can not be recon
	return functionRanges

##################################
# FIND CONTRACT STARTPOINT
##################################
def findContractStartPoint(content,contractName):
	lineNo = 0
	contractStartPoint = None
	findMainContract = False

	for line in content:
		if line.startswith("contract " + contractName):
			findMainContract = True
			contractStartPoint = lineNo
		elif findMainContract and line.strip().startswith("function"):
			break
		lineNo += 1
	
	return contractStartPoint


if __name__ == "__main__":

	total_checking = 28000
	functionDetails = {}
	contractCount = 0
	total_func = 0
	total_func_without_repeat = 0
	# keyDate = fileRandomSelector()
	for _ in range(total_checking):
		keyDate = fileRandomSelector()
		with open("/Users/BAOandPAN/Desktop/SummerResearch/Solidity_Analyse/z_allContracts/" + keyDate + ".txt","r") as file:
			content = file.readlines()

		# find the function source
		contractName = content[1].split(":")[-1].strip()
		contractStartPoint = findContractStartPoint(content,contractName)
		if contractStartPoint == None:
			continue
		contractCount += 1

		functionRanges = findFunctionRanges(content,contractStartPoint)
		for eachFuncName in functionRanges:
			total_func += 1
			funcStart = functionRanges[eachFuncName][-1][0]
			funcFinish = functionRanges[eachFuncName][-1][1]
			funcContent = "".join(content[funcStart:funcFinish + 1])
			# Detect the repeat function
			if eachFuncName not in functionDetails:
				functionDetails[eachFuncName] = [funcContent,1]
				total_func_without_repeat += 1
			elif funcContent != functionDetails[eachFuncName]:
				functionDetails[eachFuncName][1] += 1
				total_func_without_repeat += 1

			print(contractCount, total_func , total_func_without_repeat, end = "\r")

	print(contractCount, total_func , total_func_without_repeat)