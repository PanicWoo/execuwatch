import sys
import re
import json
import itertools
import numpy as np
import os
import statistics

# read the home address
with open("./Home.txt","r") as home_file:
	home = home_file.readlines()[0]


##################################
# ANALYSE/EXTRACT VARIABLES
##################################
def AnalyseVariables(varContent):
	Vars = []
	isStruct = False
	StructCount = -1
	for line in varContent:
		#Check the content of the "struct"
		if isStruct:
			if "{" in line:
				if StructCount == -1:
					StructCount += 2
				else:
					StructCount += 1
			elif "}" in line:
				StructCount -= 1
			if StructCount == 0:
				isStruct = False
				StructCount = -1
			else:
				continue
		if line.strip().lower().startswith("struct "):
			isStruct = True
			if "{" in line:
				StructCount += 2
		# Analyse the line whetherh contains some new-defined global variables
		# eles = re.findall(r"^(string|uint|int|address|bytes|byte|bool)(\d+|)(\[]|)(.+?);",line.strip())
		eles = re.findall(r"^(string|uint|int|address|bool)(\d+|)(\[]|)\s(.+?);",line.strip())
		if len(eles) == 0:
			continue
		elif "private" in eles[0][-1]:
			continue
		if eles[0][2].startswith("["):
			ArrayType = eles[0][2]
			isArray = True
		else:
			ArrayType = None
			isArray = False
		typeName = eles[0][0]
		# (&@^*&^#>?@>:>@"!")!
		if eles[0][-1].split("=")[0].strip() == "":
			continue
		varName = eles[0][-1].split("=")[0].strip().split()[-1]
		Vars.append((typeName,varName,isArray,ArrayType))
	return Vars

##################################
# FIND parents contract 
##################################
def identifyParents(content,startPoint):
	stop = False
	lineNo = startPoint
	allParents = []
	while not stop:
		currentLine = content[lineNo].split("//")[0]
		if "{" in currentLine:
			stopPoint = lineNo
			titleContent = " ".join( content[startPoint:stopPoint + 1] )
			if " is " in titleContent:
				startEditPos = titleContent.find(" is ") + 4
				stopEdidPos = titleContent.find("{")
				allParents = titleContent[startEditPos:stopEdidPos].strip().split(",")
			stop = True
		lineNo += 1

	return allParents


#############################
# Inject the console contract for debugging purpose
#############################
def injectConsoleContractRelationship(content,StartLineNo):
	# inject the inheritence relationship
	is_Position = content[StartLineNo].find(" is ")
	if is_Position != -1: # there is inheritence
		content[StartLineNo] = content[StartLineNo][:is_Position + 4] + " FailureReport, " + content[StartLineNo][is_Position + 4:]
	else:
		editPosition = content[StartLineNo].find("{")
		if editPosition != -1: # e.g. contract A is FailureReport{ ...
			# "{" is on the line
			content[StartLineNo] = content[StartLineNo][:editPosition] + " is FailureReport" + content[StartLineNo][editPosition:]
		else:									# e.g. contract A  is FailureReport...
			# "{" is not on current line, so we just need to append the relationship "is FailureReport"
			content[StartLineNo] += " is FailureReport"



##################################
# FIND CONTRACT RANGES
##################################
def findContractRanges(content,mainContractName):
	lineNo = 0
	contractRanges = []
	contractStartPoint = None
	findContract = False
	currContractName = None

	# we should find all contract class and analysis all
	for line in content:
		#inject the console contract
		if line.startswith("pragma solidity"):
			with open(home + "/consoleContract_LineMap.txt","r") as f:
				consoleContract = f.read()
				content[lineNo] += consoleContract +  "\n"
		# find MAIN CONTRACT
		if line.startswith("contract ") and (not findContract):
			contractStartPoint = lineNo
			#get the contract name 
			currContractName = line.split()[1]
			if currContractName.endswith("{"):
				currContractName = currContractName[:-1]

			findContract = True
		# record CONTRACTS
		elif findContract and ( line.strip().startswith("function ") or line.strip().startswith("modifier ") or line.strip().startswith("constructor ")): # too limited 
			contractRanges.append( (currContractName, (contractStartPoint,lineNo) ) )
			findContract = False
			# BREAK if the main contract has been found 
			if currContractName == mainContractName:
				break
		elif findContract and ( line.strip().startswith("contract ") or (lineNo == len(content)) ):
			# to handle the contract without the function to terminate 
			contractRanges.append( (currContractName , (contractStartPoint,lineNo)) )
			contractStartPoint = lineNo
			currContractName = line.split()[1]
		lineNo += 1

	return contractRanges
##################################
# FIND FUNCTION FIRST LINE
##################################
def findFuncStartPoint(content,startPoint):
	currPoint = startPoint
	while True:
		if content[currPoint].split("//")[0].strip().endswith("{"):
			return currPoint
		elif content[currPoint].split("//")[0].strip().endswith(";"):
			return False
		currPoint += 1

def isFunc(content,startPoint):
	while startPoint < len(content):
		funcLine = content[startPoint].split("//")[0].strip()
		if funcLine.endswith("{"):# or funcLine.endswith("}") :
			return True
		elif funcLine.endswith(";"):
			return False
		startPoint += 1

##################################
# FIND FUNCTION RANGES
##################################
def findFunctionRanges(content,contractStartLineNo):
	functionRanges = {} # name: [ (startLineNo,finishLineNo, funcNo), (.. , .. , ..), .. ] 
	lineNo = contractStartLineNo
	isFunction = False
	functionRangeCount = -1
	contractRangeCount = -1
	scanningStart = False
	for line in content[contractStartLineNo:]:
		#skip the comment line
		if line.strip().startswith("//") or line.strip().startswith("/*") or line.strip().startswith("*") or line.strip().startswith("*/"):
			lineNo += 1
			continue
		
		# working on current function
		if isFunction:
			if functionRangeCount == -1 and (len(re.findall(r"{",line)) > 0):
					functionRangeCount += 1
			functionRangeCount += len(re.findall(r"{",line))
			functionRangeCount -= len(re.findall(r"}",line)) 
			# check whether we should terminate
			if functionRangeCount == 0:
				isFunction = False
				functionRangeCount = -1
				functionFinishPointer = lineNo
				#insert the function range to the "functionRange"
				if functionName in functionRanges:
					funcNo = len(functionRanges[functionName]) + 1
					functionRanges[functionName].append( (functionStartPoint,functionFinishPointer,funcNo) )
				else:
					functionRanges[functionName] = [(functionStartPoint,functionFinishPointer,1)]
				lineNo += 1
				continue
			else:
				lineNo += 1
				continue
			
		#identify the function start point
		if line.strip().startswith("function"):
			if isFunc(content,lineNo):
				isFunction = True
			else:
				lineNo += 1
				continue
			
			functionStartPoint = lineNo
			# (&@^*&^#>?@>:>@"!")!
			line  = line.strip().split("//")[0]
			functionName = line.split("function")[-1].split()[0]
			if functionName != "()":
				functionName = functionName.split("(")[0]
			if len(re.findall(r"{",line)) > 0:
				functionRangeCount += len(re.findall(r"{",line)) + 1
				functionRangeCount -= len(re.findall(r"}",line)) 
		else:
			# working on only main contract range
			if "{" in line and (not scanningStart):
				contractRangeCount += 1
				scanningStart = True
			contractRangeCount += len(re.findall(r"{",line))
			contractRangeCount -= len(re.findall(r"}",line))

		if scanningStart and (contractRangeCount == 0) :
			return functionRanges
		
		lineNo += 1

	# worse case, if the range can not be recongnize
	return functionRanges


##################################
# GET THE PUBLIC FUNCTION NAMES (based on ABI)
##################################
def getFunctionNames(keyDate,contractName):
	abi_path = home + "/_Data/ABI.npz"
	abi = np.load(abi_path)

	if keyDate in abi:
		current_ABI = abi[keyDate]
	else:
		return None
	# else:
	# 	abi_path = home + "/ContractTest/"  + contractName + ".json"
	# 	with open(abi_path,"r") as json_file:
	# 		info = json.load(json_file)
	# 		current_ABI = info["ABI"]
	# 		current_ABI = json.loads(current_ABI)
	

	funcNames = []
	funcInputs = {}
	for eachELE in current_ABI:
		if eachELE["type"] == "function":
			funcNames.append(eachELE["name"])
			funcInputs[eachELE["name"]] = eachELE["inputs"]
	
	# only the public function name will be presented
	return funcNames,funcInputs

##################################
# INSERT NEW EVENT FOR ARRAY TYPE
##################################
# Var: (type,name,isArray,ArrayType)
def createNewEvent(Var):
	varName = Var[1]
	varType = Var[0] + Var[3]
	newEvent = "\n" + "event " + "log" + varName +"(string,"+ varType + ");"
	return newEvent


##################################
# CONSTRUCT LOGGING LINE
##################################
def constructLoggingLine(Vars,label):
	if label == "GV" or label == "LV" or label == "IV":
		LoggingLine = ""
	else:
		return None,None

	newEventLine = ""
	for var in Vars:
		varType = var[0]
		varName = var[1]
		isArray	= var[2]
		varName_str = "'" + label +"_" + varName + "'"
		
		# for array type variables, we create/inject new event in the current contract
		if isArray: 
			# create new event
			newEventLine += createNewEvent(var)
			# update the logging line
			LoggingLine += "emit log" +  varName + "("  +varName_str+ "," + varName + ");"
		
		# for non-array type variables, we use the log func from the inherited contract
		else:
			LoggingLine += "log" + varType + "("  +varName_str+ "," + varName + ");"

	return LoggingLine,newEventLine

##################################
# CONSTRUCT return content
##################################
def editLine(funcName, LogContent,lineNo, returnType):
	returnLine = "if( " + "lineNumbers[bytes4(keccak256(\""+ funcName + "\"))] == " + str(lineNo + 1) + "){" + LogContent + "return  ;}"
	return returnLine

##################################
# edit public Func
##################################
def editPublicFunc(content,funcStart,funcEnd,logLine,funcName):
	def searchNextLine(content,nextLineMark,funcEnd):
		isNextLine = False
		nextLine = ""
		while (not isNextLine):
			nextLineMark += 1
			if nextLineMark > funcEnd:
				nextLine = "Last line is reached;"
				break
			else:
				nextLine = content[nextLineMark]

			# filter the fake next lien: which is end without ";"
			if nextLine.split("//")[0].strip().endswith(";"):
				isNextLine = True
			if nextLine.startswith("//") or nextLine.startswith("/*") or nextLine.startswith("*/")  or nextLine.startswith("*") :
				isNextLine = False
			
		next_line_Content = "logstring( 'Line' , \""  + nextLine.strip().replace("\"" , "'") + "\");" 
		return next_line_Content

	inputDetails = []
	# #back-up the original function
	# org_func = content[funcStart:funcEnd + 1]

	# first line
	funcContentStartPoint = findFuncStartPoint(content,funcStart)
	newFirstLine = "".join(content[funcStart:funcContentStartPoint + 1]).replace("\n","")

	# function return type
	if "returns" in newFirstLine:
		returnType = newFirstLine.split("returns")[-1].split("{")[0]
		if "(" in returnType:
			returnType = returnType.strip()[1:-1].split()[0]
	else:
		returnType = " "

	# input variables
	IV_LogLine = ""
	IV_newEvent = ""
	# print(newFirstLine)
	inputs = ( newFirstLine.split("(")[1].split(")")[0] ).strip().split(",")
	if not (len(inputs) == 1 and inputs[0] == ''):
		pretented_lines = [] 
		for eachInput in inputs:
			if eachInput == '':
				continue
			pretented_lines.append(eachInput + " ;") # the space before ';' is necessary for some input without name defined
			inputType = eachInput.strip().split()[0] # suppose to be a list of value: [type, name] or [type]
			inputDetails.append(inputType)

		# construct Input variable logging lines
		inputVars = AnalyseVariables(pretented_lines)
		IV_LogLine,IV_newEvent = constructLoggingLine(inputVars,"IV")
		# print("IV", IV_newEvent,inputVars)
	

	# CASE1: line 0 
	# print(newFirstLine)
	next_line_Content = searchNextLine(content,funcContentStartPoint,funcEnd)
	# print("Line 0 :" + next_line_Content)
	content[funcContentStartPoint] += editLine(funcName,next_line_Content + logLine + IV_LogLine,0,returnType)

	# CASE2: line 1 - *
	total_LV_LogLine = ""
	total_LV_newEvent = ""
	codeLineCounter = 0
	for lineNo in range(funcContentStartPoint+1,funcEnd):
		line = content[lineNo]
		# TO filter some lines
		if line.startswith("//") or line.startswith("/*")  or line.startswith("*/")  or line.startswith("*"):
			continue
		if line.strip() == "":
			continue
		if not line.split("//")[0].strip().endswith(";"):
			continue

		codeLineCounter += 1

		Vars = AnalyseVariables([line])
		if len(Vars) > 0:
			LV_LogLine,LV_newEvent = constructLoggingLine(Vars,"LV")
			total_LV_LogLine += LV_LogLine
			total_LV_newEvent += LV_newEvent
		
		# find next line
		next_line_Content = searchNextLine(content,lineNo,funcEnd)
		# print("Line "+str(lineNo - funcContentStartPoint)+" :" + next_line_Content)
		content[lineNo] +=  editLine(funcName,next_line_Content + logLine + IV_LogLine + total_LV_LogLine,lineNo - funcContentStartPoint,returnType)

		if next_line_Content == "Last line is reached;":
			break
	
	return total_LV_newEvent + IV_newEvent ,str(codeLineCounter) ,inputDetails

def findModifier(corContent,typeOfConstraints):
	for line in corContent:
		if line.strip().startswith("modifier "):
			print("Modifier",line)
			typeOfConstraints[0] += 1
	return typeOfConstraints

	
##################################
# MAIN
##################################
if __name__ == "__main__":
	# inputContract = sys.argv[1]
	# print(inputContract)
	# singleSRC_path = "./ExecutionWatch/org_contracts/" + inputContract + ".sol"
	DataFolder = "/Users/BAOandPAN/Desktop/SummerResearch/Solidity_Analyse/z_allContracts"
	for keyDate in os.listdir(DataFolder):
		singleSRC_path = DataFolder + "/" +keyDate
		keyDate = keyDate[:-4]
		with open(singleSRC_path,"r") as file:
			content = file.readlines()

		#Basic Details
		contractName = content[1].split(":")[-1].strip()
		print(contractName)
		keyDate = content[2].strip().split()[-1]
		# functionNames,functionInputs = getFunctionNames(keyDate,contractName)
		# print(functionInputs)

		# Find all ranges of contract class
		contractRanges = findContractRanges(content,contractName)

		#Reconstruct the each public function
		totalRequirement = 0
		typeOfConstraints = [0,0,0]
		functionDetails = {}

		#Modifier-defined constraints
		typeOfConstraints = findModifier(content,typeOfConstraints)

		#Require-declared constraints
		if len(contractRanges) != 0:
			for eachContract in contractRanges:
				currContractStart = eachContract[1][0]
				currGlobalFinishPoint = eachContract[1][1]

				# Get the Ranges of all function of current contract
				functionRanges = findFunctionRanges(content,currContractStart)  
				# print(functionRanges)
				
				for funcName in functionRanges:
					# if funcName not in functionNames: # make sure that the function is state mutable functions 
					# 	continue
					#''funcRange'': (functionStartPoint,functionFinishPointer,funcNo)
					funcRange = functionRanges[funcName][0]

					for line in content[funcRange[0]:funcRange[1]]:
						if line.strip().startswith("require"):
							requirementContent = re.findall(r'require[\s]*\((.+?)(,|)\);',line)
							if len(requirementContent) > 0:
								totalRequirement += 1
								typeOfConstraints[1] += 1
								print("Require",line)
						elif line.strip().startswith("assert"):
							assertionContent = re.findall(r'assert[\s]*\((.+?)\);',line)
							if len(assertionContent) > 0:
								totalRequirement += 1
								typeOfConstraints[1] += 1
								print("Require",line)

						# Solidity-Specific Constraints
						if ".transfer(" in line:
							transferContent = re.findall(r'.transfer\((.+)\)',line)
							if len(transferContent) > 0:
								transferContent = transferContent[0]
								if "," not in transferContent:
									typeOfConstraints[2] += 1
									print("Transfer",line)
						elif ".send(" in line:
							sendContent = re.findall(r'.send\((.+)\)',line)
							if len(sendContent) > 0:
								if "," not in sendContent:
									typeOfConstraints[2] += 1
									print("Transfer",line)
						elif ".call" in line and ".value(" in line:
							typeOfConstraints[2] += 1
							print("Transfer",line)
		
		
		print("StaticalResults",typeOfConstraints)
		print("\n")


	# print(totalRequirement)

# with open("./test_constraintsType.txt","r") as f:
# 	content = f.readlines()
# 	total = 0
# 	modifier = 0
# 	require = 0
# 	transfer = 0
# 	for ele in content:
# 		types = ele.strip()[1:-1].split(",")
# 		# print(types)
# 		if int(types[0]) == 1:
# 			modifier += 1
# 		if int(types[1]) == 1:
# 			print(types[1])
# 			require += 1
# 		if int(types[2]) == 1:
# 			transfer += 1

# 	print(modifier,require,transfer)


# with open("./test_record.txt","r") as f:
# 	content = f.readlines()
# 	onlyM = 0
# 	onlyR = 0
# 	onlyT = 0
# 	none = 0
# 	for line in content:
# 		types = line.split("[")[-1].split("]")[0].split(",")
# 		if (int(types[0]) == 0) and (int(types[1]) == 0) and (int(types[2]) == 0):
# 			none += 1
# 		else:
# 			if (int(types[0]) == 0) and (int(types[1]) == 0):
# 				onlyT += 1
# 			elif (int(types[0]) == 0) and (int(types[2]) == 0):
# 				onlyR += 1
# 			elif (int(types[1]) == 0) and (int(types[2]) == 0):
# 				onlyM += 1

# 	print("onlyM",onlyM)
# 	print("onlyR",onlyR)
# 	print("onlyT",onlyT)
# 	print("none",none)


# # The median and mean of the funtion number
# with open("./test_funcNum.txt","r") as f:
# 	content = f.readlines()
# 	numb = []
# 	for line in content:
# 		numb.append(int(line.strip()))
# 	print("Median",statistics.median(numb))
# 	print("Mean",statistics.mean(numb))
	

