import networkx as nx
from networkx.drawing.nx_agraph import write_dot,graphviz_layout

import matplotlib.pyplot as plt
import json
import numpy as np

def analyseCall(funcSetInfo,calleeInfo,CallerKeyName,processedCG):
	CallerContractName = CallerKeyName.split(".")[0].strip()
	funcSetContractName = funcSetInfo[0]
	funcSet = funcSetInfo[1]
	for eachFunc in funcSet:
		option1 = eachFunc + "("
		option2 = "." + eachFunc + "("
		CalleeKeyName = funcSetContractName + "." + eachFunc
		try:
			if (calleeInfo.startswith(option1) and (funcSetContractName==CallerContractName) ):
				processedCG[CallerKeyName]["child"].append(CalleeKeyName)
				processedCG[CalleeKeyName]["parent"].append(CallerKeyName)
				continue
			if (option2 in calleeInfo):
				processedCG[CallerKeyName]["child"].append(CalleeKeyName)
				processedCG[CalleeKeyName]["parent"].append(CallerKeyName)
		except:
			continue

#
def analyseCG(date_key):
	abi = np.load("/Users/BAOandPAN/Desktop/HonoursProject/analyseABI/ABI.npz")
	# src_path = "/Users/BAOandPAN/Desktop/HonoursProject/_Data/CallGraph.json"

	# with open(src_path) as json_file:
	# 	CallGraph = json.load(json_file)

	CallGraph_file = open("/Users/BAOandPAN/Desktop/HonoursProject/CG_example.txt",'r')
	CallGraph = CallGraph_file.readlines()	
	mainContractName = CallGraph[0].strip()
	processedCG = {}
	currentABI = abi[date_key]
	publicFuncs = []

	#collect all functions
	all_functions = {}
	for eachCall in CallGraph[1:]:
		[CalleeInfo,CallerInfo]= eachCall.strip().split("==>")
		CallerInfo = CallerInfo.split(",")
		contractName = CallerInfo[0].split(":")[1].strip()
		funcName = CallerInfo[1].split(":")[1].strip()
		if funcName.endswith("(ct)"):
			continue
		# fill in the processed CG
		processedCG[contractName + "." + funcName] = {"parent": [], "child":[]}

		# fill the all_functions for further analysing
		if contractName not in all_functions:
			all_functions[contractName] = [funcName]
		else:
			all_functions[contractName].append(funcName)
	
	# Collect Public Functions
	if currentABI.all() == None:
		return 
	for eachEle in currentABI:
		if (eachEle["type"] == "function") and ("stateMutability" in eachEle):
			if eachEle["stateMutability"] != "view":
				publicFuncs.append(eachEle["name"])

	# Processing the lines
	for eachCall in CallGraph[1:]:
		[CalleeInfo,CallerInfo]= eachCall.strip().split("==>")
		CallerInfo = CallerInfo.split(",")
		contractName = CallerInfo[0].split(":")[1].strip()
		funcName = CallerInfo[1].split(":")[1].strip()
		CallerKeyName = contractName + "." + funcName
		if funcName.endswith("(ct)"):
			continue
		
		# go through each functions of each contract
		for eachContractClass in all_functions:
			funcSet = all_functions[eachContractClass]
			funcSetInfo = [eachContractClass,funcSet]
			analyseCall(funcSetInfo,CalleeInfo,CallerKeyName,processedCG)

	#
	# print(len(publicFuncs))
	# for eachPubFunc in publicFuncs:
	# 	print(mainContractName + "." + eachPubFunc)
	# 	print(processedCG[mainContractName + "." + eachPubFunc])
	# print(processedCG)
	return processedCG,[mainContractName,publicFuncs]
	



def plotCG():
	processedCG,ABI_info = analyseCG("2018_9_10_73")
	mainContractName = ABI_info[0]
	publicFuncs = ABI_info[1]
	# print(publicFuncs)

	# Initial a Graph
	G = nx.DiGraph()

	# Create nodes
	pubFunc_nodes = []
	privateFunc_nodes = []
	otherFunc_nodes = []
	labels = {}
	for key in processedCG:
		keyInfo = key.split(".")
		currentContractName = keyInfo[0].strip()
		currentFuncName = keyInfo[1].strip()
		if currentContractName == mainContractName:
			if currentFuncName in publicFuncs:
				pubFunc_nodes.append(key)
				labels[key] = key
			else:
				privateFunc_nodes.append(key)
				labels[key] = key
		else:
			if (len(processedCG[key]["parent"]) == 0) and (len(processedCG[key]["child"]) == 0) :
				continue
			if currentFuncName in publicFuncs:
				otherFunc_nodes.append(key)
			else:
				privateFunc_nodes.append(key)
			labels[key] = key

			 
	G.add_nodes_from(processedCG.keys())

	# Create edges
	# nodes = []
	for key in processedCG:
		# nodes.append(key)
		children = processedCG[key]["child"]
		for eachChild in children:
			G.add_edge(key,eachChild)


	#Drawing the graph

	#1. Draw nodes
	nodePos = graphviz_layout(G, prog='dot')
	#public function nodes
	nx.draw_networkx_nodes(G,nodePos,node_size=100,node_shape = "o", node_color = "r", nodelist = pubFunc_nodes)
	#private function nodes in main contract
	nx.draw_networkx_nodes(G,nodePos,node_size=100,node_shape = "o", node_color = "g", nodelist = privateFunc_nodes)
	#others
	nx.draw_networkx_nodes(G,nodePos,node_size=100,node_shape = "o", node_color = "b", nodelist = otherFunc_nodes)
	#2. Draw edges
	nx.draw_networkx_edges(G,nodePos)

	#3.Draw label
	nx.draw_networkx_labels(G,nodePos,labels,font_size = 3)

	# Show the final result
	plt.show()
	# plt.savefig(mainContractName + '.png')
plotCG()