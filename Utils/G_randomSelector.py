# random select k contracts from the original contracts base 
import os
import json
import random

with open("/Users/BAOandPAN/Desktop/HonoursProject/Utils/FileRange.json","r") as json_file:
	fileRange = json.load(json_file)


def constructRangeFile():
	fileRange = {}
	days = [31,30,31,31,30,31,30,13]
	src_path = "/Users/BAOandPAN/Desktop/SummerResearch/Solidity_Analyse/data/source_code/2018_"
	for month in range(5,13):
		fileRange[str(month)] = []
		cor_days = days[month - 5]
		for eachDay in range(1,cor_days+1):
			file_no = len( os.listdir(src_path + str(month) + "_" + str(eachDay) ) )
			fileRange[str(month)].append( (eachDay,file_no) )
			# print(fileRange[str(month)])

	with open("./Utils/FileRange.json","w") as json_file:
		json.dump(fileRange,json_file)

def fileRandomSelector():
	randomMon = random.randint(5,12)
	randomDay = random.randint(1,len(fileRange[str(randomMon)]))
	randomFileNo = random.randint(1,fileRange[str(randomMon)][randomDay - 1][1])
	return "2018_" + str(randomMon) + "_" + str(randomDay) + "_" + str(randomFileNo)

def mvToFolder(From,To,fileAmount = 100):
	# From = "0.4.24"
	# To = "ContractTest24"
	# fileAmount = 100
	data_folder_path = "/Users/BAOandPAN/Desktop/HonoursProject/_Data/ContractsVer/" + From
	fileNames = os.listdir(data_folder_path)
	for _ in range(fileAmount):
		randInt = random.randint(0,len(fileNames))
		fileName = fileNames[randInt]
		with open(data_folder_path + "/" + fileName,"r") as f:
			content = f.readlines()
			contractName = content[1].split(":")[-1].strip()
			writePath = "/Users/BAOandPAN/Desktop/HonoursProject/"+To+"/" + contractName + ".sol"
			with open(writePath,"w") as writeTOfile:
				writeTOfile.writelines(content)

if __name__ == "__main__":
	# Versions = ["0.4.24" ,"^0.4.21","^0.4.23","^0.4.24", "^0.4.25"]
	Versions = ["^0.4.4"]
	for eachVer in Versions:
		From = eachVer
		To = "ContractTest" + eachVer
		Amount = 150
		mvToFolder(From,To,Amount)
		