const fs = require("fs");

// This function is mainly doing comilation and Deployment
function CompileSmartContract(contractName,contractPath){
	//1. Setting up for all moudles we needed
	const fs = require('fs');
	const solc = require('solc');
	const source = fs.readFileSync(contractPath);

	//2. Compile the contract
	var Bytecode; 
	var Abi; 
	try{
		const output = solc.compile(source.toString(), (res)=>{console.log(res)}) //(res)=>{console.log(res)})
		// console.log(output)
		Bytecode = output.contracts[":" + contractName].bytecode;
		Abi = output.contracts[":" + contractName].interface;
		// console.log(abi);
		// console.log(bytecode);
		if ( (typeof Bytecode !== "undefined") && (typeof Abi !== "undefined") ){

			return [Abi,Bytecode]
		} else{
			console.log("Compilation Failed: " +contractName  + "(Undefined abi or bytecode)")
			return false
		}
	}catch(e){
		console.log("Compilation Failed: " +contractName )
		// console.log(e)
		return false
	}
}

var contractName = process.argv[2];
var contractPath = "./ContractTest/" + contractName + ".sol";
let compileResult = CompileSmartContract(contractName,contractPath);

let data = {
	"ABI": compileResult[0]
}
fs.writeFileSync("./ContractTest/" + contractName + ".json",JSON.stringify(data),err => {console.log(err)} )