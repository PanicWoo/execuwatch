let fs = require('fs')
let crypto = require('crypto')
// Bytes
function getRandomBytes(len){
	return crypto
		.randomBytes(Math.ceil(len/2))
		.toString('hex')
		.slice(0,len)
}
// INT, UINT
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min; //The max and min are both inclusive
}

function randomValidValueSelector(type){
	type = type.toLowerCase()
	// INT type
	// We convert decimal int in hex form to handle big number
	if(type === "int"){ // int = int256
		return "0x" +getRandomInt( -(2**128), 2**128 -1 ).toString(16)
	}else if(type.startsWith("int")){
		let size = Math.floor(type.substr(3)/2) 
		return "0x" +getRandomInt( -(2**size), 2**size -1 ).toString(16)
	}

	//UINT type
	// We convert decimal uint in hex form to handle big number
	if(type === "uint"){ // int = int256
		return "'0x" + getRandomInt( 0, 2**256 -1 ).toString(16) + "'"
	}else if(type.startsWith("uint")){
		let size = type.substr(4);
		return "'0x" + getRandomInt( 0, 2**size -1 ).toString(16) + "'"
	}

	//BYTE
	if (type === 'byte'){
		return "0x" + getRandomBytes(2)
	}

	//BYTES
	if (type === 'bytes'){
		let len = getRandomInt(0,10) // dynamic array 
		let BYtes = []
		for (i = 0; i < len; i++){
			BYtes.push('0x' + getRandomBytes(2))
		}
		return "[" + BYtes.toString() + "]"
	//BYTES_I
	}else if (type.startsWith('bytes') ) {
		let size = type.substr(5);
		return '0x' + getRandomBytes(size*2)
	}

}

function distributeCandidateValue(type,k){
	let Seeds = fs.readFileSync("/Users/BAOandPAN/Desktop/HonoursProject/Utils/ArguSeeds.json");
	let AddressMap = fs.readFileSync("/Users/BAOandPAN/Desktop/HonoursProject/Utils/_AddressMap.json")
	// let Seeds = fs.readFileSync("./ArguSeeds.json");
	Seeds = JSON.parse(Seeds)
	AddressMap = JSON.parse(AddressMap)
	let candidateValues = []

	for(i = 0; i<k; i++){
		let Prob = getRandomInt(1,100)
		//INT, UINT, Byte, Bytes
		type = type.toLowerCase()
		// address has its onw seed map
		if ( type.startsWith("address")){ 
			let payableOption = getRandomInt(0,1)
			if (payableOption === 0){ // payable address
				var potentialSeeds = AddressMap["Payable"]
			}else{
				var potentialSeeds = AddressMap["Payable"]// var potentialSeeds = AddressMap["nonPayable"]
			}
			let randomIndex = getRandomInt(0,potentialSeeds.length -1)
			let seed = potentialSeeds[randomIndex]
			candidateValues.push(seed)
		}
		else if (Prob>50 || type.startsWith("string") || type.startsWith("bool") || type.startsWith("int") || type.startsWith("uint") ){ 
			//Select Argument from "Seeds"
			let typeSeeds = []
			if (type.startsWith("int")){
				typeSeeds = Seeds["int"]
			}else if(type.startsWith("uint")){
				typeSeeds = Seeds["uint"]
			}else if(type.startsWith("string")){
				typeSeeds = Seeds["string"]
			}else if(type === "byte"){
				typeSeeds = Seeds["byte"]
			}else if(type === "bytes[]"){
				typeSeeds = Seeds["bytes[]"]
			}else if(type.startsWith("bytes")){
				typeSeeds = Seeds["bytes"]// the extra bytes will  be truncated 
			}else if(type.startsWith("bool")){
				typeSeeds = Seeds["bool"]
			}
			let randomIndex = getRandomInt(0,typeSeeds.length -1)
			let seed = typeSeeds[randomIndex]
			// polish string type to make it valid
			if(type.startsWith("string")){
				seed = "'" + seed + "'"
			}
			candidateValues.push(seed)
		}else{				//Select Argument from Valid Range
			candidateValues.push( randomValidValueSelector(type))
		}
	}

	return candidateValues
	
}

//"k" : amount of values for one single type of argument
function prepareCandidateValue(type,k){

	// check whether the argument is an array
	let dynArrPat = /(.*)\[\].*$/;
	let fixedArrPat = /(.*)\[([\d]+)\]+$/
	var isArray = false;

	var rex1 = dynArrPat.exec(type)
	var rex2 = fixedArrPat.exec(type)
	if (rex1 !== null){
		type = rex1[1]
		var isDynamic = true;
		isArray = true
	}else if (rex2 !== null){
		type = rex2[1]
		var isDynamic = false;
		isArray = true
	}

	// construct candidate values according to data type
	if (isArray){ // array type argument
		if (isDynamic){
			var candidateValues = []
			for ( count = 0; count< k; count ++){
				var arrSize = 2//getRandomInt(1,5) // 1
				var Value = distributeCandidateValue(type,arrSize)
				Value = "[" + Value.toString() +"]"
				candidateValues.push(Value)
			}
		}else{
			var candidateValues = []
			for ( count = 0; count< k; count ++){
				var arrSize = rex2[2]
				var Value = distributeCandidateValue(type,arrSize)
				Value = "[" + Value.toString() +"]"
				candidateValues.push(Value)
			}
		}
		
	}else{ // not array type argument
		var candidateValues = distributeCandidateValue(type,k)
	}

	return candidateValues
}

//
function randomSelectorInputs(k,arguCandidates){
	let arguInputList = []
	let count = 0;
	do{
		let len = arguCandidates.length
		let candidateSize = arguCandidates[0].length
		let theInput = ""
		for(i = 0; i<len; i++){
			let index = getRandomInt(0,candidateSize - 1)
			theInput += arguCandidates[i][index] + ","
		}
		arguInputList.push(theInput.slice(0,-1))
		count += 1
	}while(count<k)

	return arguInputList
}


// "arguTypes" : [type1, type2, ..]
const fuzz = function fuzz(arguTypes,k){
	//Generate the candidate values for each type
	let arguCandidates = [];
	let arguSize = arguTypes.length

	for(j =0; j < arguSize;j++){
		let type = arguTypes[j] 
		candidateValues = prepareCandidateValue(type,20)
		arguCandidates.push(candidateValues)
	}
	// console.log(arguCandidates)

	//arrange the value to form inputs for funcSig
	// i.e.  [int, string, address]  => ["1,'world','0xdhafhdabfa..'" , "3,'hello','0x1234fhdabfa..'" , ......]
	
	//Method1: randomly formate k testing cases
	arguInputList = randomSelectorInputs(k,arguCandidates)

	//Method2: formate all testing cases based on the candidateValues
	//..

	return arguInputList
}

// console.log("start")
// let a  = fuzz(["address","uint256"."bytes"])
// console.log(a)

module.exports = {
	fuzz
};