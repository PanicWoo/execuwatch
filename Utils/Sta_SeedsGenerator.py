from G_randomSelector import fileRandomSelector
import re
import os

##################################
# FIND FUNCTION RANGES
##################################
def findFunctionRanges(content,contractStartPoint):
	functionRanges = {} # name: [ (startLineNo,finishLineNo, funcNo), (.. , .. , ..), .. ]
	lineNo = 0
	isFunction = False
	functionRangeCount = -1
	contractRangeCount = -1
	scanningStart = False
	for line in content:
		#skip the line before the main contrat
		if lineNo < contractStartPoint:
			lineNo += 1
			continue
		# elif lineNo == contractStartPoint:
		# 	scanningStart = True

		#skip the comment line
		if line.strip().startswith("//") or line.strip().startswith("/*") or line.strip().startswith("*") or line.strip().startswith("*/"):
			lineNo += 1
			continue

		# working on current function
		if isFunction:
			if functionRangeCount == -1 and (len(re.findall(r"{",line)) > 0):
					functionRangeCount += 1
			functionRangeCount += len(re.findall(r"{",line))
			functionRangeCount -= len(re.findall(r"}",line))
			# check whether we should terminate
			if functionRangeCount == 0:
				isFunction = False
				functionRangeCount = -1
				functionFinishPointer = lineNo
				#insert the function range to the "functionRange"
				if functionName in functionRanges:
					funcNo = len(functionRanges[functionName]) + 1
					functionRanges[functionName].append( (functionStartPoint,functionFinishPointer,funcNo) )
				else:
					functionRanges[functionName] = [(functionStartPoint,functionFinishPointer,1)]
				lineNo += 1
				continue
			else:
				lineNo += 1
				continue

		#identify the function start point
		if line.strip().startswith("function ") and (not line.strip().endswith(";")):
			isFunction = True
			functionStartPoint = lineNo
			# (&@^*&^#>?@>:>@"!")!
			functionName = line.strip().split("function")[1].strip().split("(")[0]
			if functionName != "":
				functionName = functionName.split("(")[0]
			else:
				functionName = "()"
			if len(re.findall(r"{",line)) > 0:
				functionRangeCount += len(re.findall(r"{",line)) + 1
				functionRangeCount -= len(re.findall(r"}",line))
		else:
			# working on only main contract range
			if "{" in line and (not scanningStart):
				contractRangeCount += 1
				scanningStart = True
			contractRangeCount += len(re.findall(r"{",line))
			contractRangeCount -= len(re.findall(r"}",line))

		if scanningStart and (contractRangeCount == 0) :
			return functionRanges

		lineNo += 1
	# worse case, if the range can not be recon
	return functionRanges

##################################
# FIND CONTRACT STARTPOINT
##################################
def findContractStartPoint(content,contractName):
	lineNo = 0
	contractStartPoint = None
	findMainContract = False

	for line in content:
		if line.startswith("contract " + contractName):
			findMainContract = True
			contractStartPoint = lineNo
		elif findMainContract and line.strip().startswith("function"):
			break
		lineNo += 1

	return contractStartPoint

def detectConstraints(content,functionRanges):
	resultLines = []
	total_constraints = 0
	for eachFuncName in functionRanges:
		if eachFuncName.strip().startswith("("):
			# print("fallback function:")
			# print("".join(content[functionRanges[eachFuncName][-1][0]:functionRanges[eachFuncName][-1][1] + 1]) )
			continue
		funcStart = functionRanges[eachFuncName][-1][0]
		funcFinish = functionRanges[eachFuncName][-1][1]

		funcInputs = re.findall(re.compile('function '+ eachFuncName+'\((.*?)\)'),content[funcStart])

		if (len(funcInputs) == 0) or (len(funcInputs[0]) == 0):
			continue

		funcInputs = funcInputs[0].split(",")
		# print(funcInputs)
		for eachLine in content[funcStart+1:funcFinish+1]:
			try:
				if eachLine.startswith("//") or eachLine.startswith("/*") or eachLine.startswith("*") or eachLine.startswith("*/"):
					continue
				for eachInput in funcInputs:
					theInput = eachInput.split()[-1]
					if theInput in eachLine:
						if eachLine.strip().startswith("require") or eachLine.strip().startswith("assert"):
							if ("==" in eachLine ) or ("!=" in eachLine ) or ("<" in eachLine ) or (">" in eachLine ):
						# if ".length" in eachLine:
						# if ".transfer(" in eachLine:
						# if ".send(" in eachLine:
						# if ".call.value" in eachLine:
								print(eachLine)
								resultLines.append(eachLine.strip() + "\n")
								total_constraints += 1
								break
			except:
				continue

	return total_constraints,resultLines

def testSingleCase():
	keyDate = fileRandomSelector()
	# keyDate = "2018_9_18_13"
	with open("/Users/BAOandPAN/Desktop/SummerResearch/Solidity_Analyse/z_allContracts/" + keyDate + ".txt","r") as file:
		content = file.readlines()

	# find the function source
	contractName = content[1].split(":")[-1].strip()
	contractStartPoint = findContractStartPoint(content,contractName)
	if contractStartPoint == None:
		return False

	print(keyDate,contractName)
	# print(contractStartPoint)
	functionRanges = findFunctionRanges(content,contractStartPoint)
	sub_constraints,resultLines = detectConstraints(content,functionRanges)
	# print(sub_constraints)

def sta_analysis():
	total_resultLines = []
	total_func = 0
	total_constraints = 0
	total_contract = 0
	for namekey in os.listdir("/Users/BAOandPAN/Desktop/SummerResearch/Solidity_Analyse/z_allContracts"):
		total_contract += 1
		# print(namekey[:-4])
		with open("/Users/BAOandPAN/Desktop/SummerResearch/Solidity_Analyse/z_allContracts/" + namekey ,"r") as file:
			content = file.readlines()


		contractName = content[1].split(":")[-1].strip()
		contractStartPoint = findContractStartPoint(content,contractName)
		if contractStartPoint == None:
			continue

		# print(namekey,contractName,contractStartPoint)
		functionRanges = findFunctionRanges(content,contractStartPoint)
		sub_constraints,resultLines = detectConstraints(content,functionRanges)
		total_resultLines += resultLines
		total_func += len(functionRanges)
		total_constraints += sub_constraints

		print(total_contract,total_constraints,total_func, end = "\r")

	print(total_contract,total_constraints,total_func)

	with open("resultLines.txt","w") as f:
		f.writelines(total_resultLines)
def mappingStruc():
	keyDate = fileRandomSelector()
	# keyDate = "2018_9_18_13"
	with open("/Users/BAOandPAN/Desktop/SummerResearch/Solidity_Analyse/z_allContracts/" + keyDate + ".txt","r") as file:
		content = file.readlines()

	# find the function source
	contractName = content[1].split(":")[-1].strip()
	# contractStartPoint = findContractStartPoint(content,contractName)
	# if contractStartPoint == None:
	# 	return False

	print(keyDate,contractName)

	for line in content:
		if line.strip().startswith("mapping"):
			print(line)


# "funcContent" : A list of lines contains the content of the function 
def constrainDefine(funcName,funcContent):
	resultLines = []
	total_constraints = 0
	funcInputs = re.findall(re.compile('function '+ funcName +'\((.*?)\)'),funcContent[0])

	if (len(funcInputs) == 0) or (len(funcInputs[0]) == 0):
		pass

	funcInputs = funcInputs[0].split(",")
	# print(funcInputs)
	for eachLine in funcContent[1:]:
		try:
			if eachLine.startswith("//") or eachLine.startswith("/*") or eachLine.startswith("*") or eachLine.startswith("*/"):
				continue
			for eachInput in funcInputs:
				theInput = eachInput.split()[-1]
				if theInput in eachLine:
					if eachLine.strip().startswith("require") or eachLine.strip().startswith("assert"):
						if ("==" in eachLine ) or ("!=" in eachLine ) or ("<" in eachLine ) or (">" in eachLine ):
					# if ".length" in eachLine:
					# if ".transfer(" in eachLine:
					# if ".send(" in eachLine:
					# if ".call.value" in eachLine:
							print(eachLine)
							resultLines.append(eachLine.strip() + "\n")
							total_constraints += 1
							break
		except:
			continue
	return total_constraints,resultLines


if __name__ == "__main__":
	# funcContent = ['  function totalSupply(uint a) public view returns (uint256) {\n','require( a == nothing happens)', '    return totalSupply_;\n', '  }\n']
	# funcName = "totalSupply"
	# constrainDefine(funcName,funcContent)
	# testSingleCase()
	#sta_analysis()
	mappingStruc()
