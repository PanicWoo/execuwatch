

pkill geth

sleep 1

geth  --datadir ./Ethereum/$1 --rpc -rpcaddr "0.0.0.0"  --rpcport "8545" --rpccorsdomain "*" --port "30303" --nodiscover  --rpcapi "db,eth,net,web3,miner,net,personal,net,txpool,admin"  --networkid 666  --targetgaslimit "9000000000000" --allow-insecure-unlock  --verbosity 1 &

echo "Run Geth..."

sleep 2

geth --jspath "./Ethereum/Utils/" --exec "loadScript('gethMining.js')" attach ipc:./Ethereum/$1/geth.ipc & 

sleep 2

# node ./FailureDetection/PreAllocate.js $2
# node ./FailureDetection/FailureDetecting.js $2
node ./FailureDetection/FailureLocating_lineMap.js $2 

sleep 2

pkill geth

echo "Exit Geth..."