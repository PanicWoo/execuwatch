# echo "Deploy Started..."

#kill the geth process 
pkill geth

sleep 1

#Run the Chain: first argument($1) -> chain name
geth  --datadir ./Ethereum/$1 --rpc -rpcaddr "0.0.0.0"  --rpcport "8545" --rpccorsdomain "*" --port "30303" --nodiscover  --rpcapi "db,eth,net,web3,miner,net,personal,net,txpool,admin"  --networkid 666  --targetgaslimit "9000000000000" --allow-insecure-unlock --verbosity 1 & 

echo "Your Chain: ##$1## is started."

sleep 2

geth --jspath "./Ethereum/Utils/" --exec "loadScript('gethMining.js')" attach ipc:./Ethereum/$1/geth.ipc & 

echo "Mining is started."
sleep 2

#RE-Construction 
python ./ExecutionWatch/ExecutionWatch.py $2

echo "Reconstruction is done."

#Deployment
node ./ExecutionWatch/EW_Deploy.js $1 $2

sleep 2

#Terminate the mining process
pkill geth

echo "Your Chain: ##$1## is terminated."