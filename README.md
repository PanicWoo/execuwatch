
## ExecuWatch

**ExecuWatch** is prototype tool to detect and locate the failure of each (callable)function in smart contract.

---

##Prerequisites
To run **ExecuWatch** requires the following source installed:
	python
	node js
	golang
	C compiler

## SetUp

1. Install (Modified-Version) geth
	
	```
	>> cd ./go-ethereum-modified
	>> make geth	 
	```
	
2. Install required source organized by npm
	
	```
	>> cd /ExecutionWatch
	>> npm install
	```

---

## Run ExecuWatch with provied smart contract (Full Execution)
**Step 0** Save Smart Contract to corresponding Location
	
	# If you want to test your own smart contracts, you just need to save your .sol files into **./ExecutionWatch/org_contracts** before you go through the steps below.

**Step 1** (Deployment) Transform && Deploy Smart Contract

	# Deploy one smart contract(.sol) saved in "./ExecutionWatch/org_contracts"
	>> ./DeployContract.sh ChainName ContractName

	# Deploy all smart contracts(.sol) of the folder
	>> ./DeployAllContract.sh ChainName

**Step 3** (Failure Detecting) Detect Failure via **ExecuWatch** with current **Constraints**
	
	# To detect the failure of each function in one smart contract, we firstly construct(fuzzing) some testing cases. In addtion, we have to options: 1)With constraints, 2)Without constraints.

**Step 4** (Failure Locating) Locate the failure line based on the result (**Passed**/**Partially Failed**/**All Failed**) found in step 3.


**Step 5** (Constraint Construction)Extract Constraints via analysing **Failure Information**

**Step 6** Go back to **Step 3** with extracted constraints

Click [here](https://bitbucket.org/PanicWoo/execuwatch/src/master/ExecutionWatch/) to see more details of **ExecuWatch**

---
