# echo "Deploy Started..."

#kill the geth process 
pkill geth

sleep 1

#Run the Chain: first argument($1) -> chain name
geth  --datadir ./Ethereum/$1 --rpc -rpcaddr "0.0.0.0"  --rpcport "8545" --rpccorsdomain "*" --port "30303" --nodiscover  --rpcapi "db,eth,net,web3,miner,net,personal,net,txpool,admin"  --networkid 666  --targetgaslimit "9000000000000" --allow-insecure-unlock --verbosity 1 & 

echo "Your Chain: ##$1## is started."

sleep 2

geth --jspath "./Ethereum/Utils/" --exec "loadScript('gethMining.js')" attach ipc:./Ethereum/$1/geth.ipc & 

echo "Mining is started."
sleep 2

# node ./ExecutionWatch/EW_FailureDetection_envSet.js $1 "ABCToken"
# Run the Process
count=0
for entry in "./ExecutionWatch/org_contracts"/*.sol
do
	filename="$(echo "$entry" | cut -d"/" -f 4)"
	name="$(echo "$filename" | cut -d"." -f 1)"

	#Counting the smart contract 
	count=`expr $count + 1`

	echo $count $name >> ./Result_Deploy.txt

	#RE-Construction 
	python ./ExecutionWatch/ExecutionWatch.py "$name"
	python ./ExecutionWatch/AllocationFuncInject.py "$name"

	#Deployment
	echo $count $name >> ./Result_Deploy.txt
	node ./ExecutionWatch/EW_Deploy.js "$name" >> ./Result_Deploy.txt
	sleep 1

	#Failure Detection 
	echo $count $name >> ./Result_FD.txt
	node ./ExecutionWatch/EW_FailureDetection_envSet.js "$name" >> ./Result_FD.txt

	#Failure Locating
	echo $count $name >> ./Result_FL.txt
	node ./ExecutionWatch/EW_FailureLocating.js "$name" >> ./Result_FL.txt

done



sleep 2

pkill geth

echo "Exit Geth..."

